# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This is the OAuth callback for any given plugin
  get '/storage/:backend/callback' do
    # Do not allow a non-logged in account to mess with this
    if current_person.nil?
      status 404
    end

    # We want the CSRF Token and the auth token

    # Compare the CSRF Token and Update the corresponding Auth Token
    instances = Occam::System::Storage.view(params[:backend], current_account)
    puts "looking for #{params["state"]}"
    puts params["code"]
    instances.each do |instance|
      if instance.accountInfo[:oauthCSRFToken] == params["state"]
        puts "found #{params["state"]}"
        data = {}

        # Reset the CSRF Token
        data[:oauthCSRFToken] = ""

        # Append the authorization token
        data[:oauthAuthToken] = params["code"]

        # Apply the token
        instance = instance.set(data)
      end
    end

    # Redirect to the storage tab again
    redirect "/people/#{current_person.uuid}/storage"
  end
end
