# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # Update the system configuration
  #post '/system' do
  #end

  #delete '/system/associations' do
  #end

  #post '/system/associations' do
  #end

  get '/system/code' do
    # TODO: fix the uuid
    occam = Occam::Object.new(:id => "dd44fcce-5274-11e5-b1d4-dc85debcef4e")
    redirect occam.url(:path => "files")
  end

  # Get the test coverage for the tests, if it exists
  get '/system/tests/coverage' do
    redirect '/system/tests/coverage/'
  end

  get '/system/tests/coverage/*' do
    begin
      subpath = params[:splat][0]
      basepath = File.join(File.dirname(__FILE__), "..", "coverage")
      basepath = File.realpath(basepath)

      if subpath == "/" or subpath == "" or subpath.nil?
        path = File.join(basepath, "index.html")
      else
        path = File.join(basepath, subpath)
      end

      path = File.realpath(path)

      if path.start_with?(basepath) and File.exist?(path)
        send_file path
      else
        status 404
      end
    rescue
      status 404
    end
  end

  get '/system/tests/client/?:tab?' do
    basepath = File.join(File.dirname(__FILE__), "..", "spec", "tests.json")
    basepath = File.realpath(basepath)

    if defined?(@@test_run)
      if @@testRunTime < File.mtime(basepath)
        @@test_run = nil
      end
    end

    if !defined?(@@test_run) or @@test_run.nil?
      # Only parse the test suite once per server run
      @@testRunTime = Time.now

      @@test_run = Occam::TestRun.new(:file => basepath)
    end

    render :haml, :"tests/index", :locals => {
      :test_run => @@test_run,
      :path     => "/system/tests/client"
    }
  end

  # Get the test results
  get '/system/tests/?:tab?' do

    render :haml, :"tests/index", :locals => {
      :test_run => @@server_test_run,
      :path     => "/system/tests"
    }
  end

  # Get the system configuration
  get '/system/?:tab?' do
    system = Occam::System.new(:account => current_account)

    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    if defined?(@@occamSpecPath) and defined?(@@occamSpecPathTime) and @@occamSpecPath
      if @@occamSpecPathTime < File.mtime(@@occamSpecPath)
        @@occamSpecPath = nil
        @@server_test_run = nil
      end
    end

    if !defined?(@@occamSpecPath) || @@occamSpecPath.nil?
      @@occamSpecPath = nil
      @@occamSpecPathTime = Time.now

      # Get the location of the daemon
      path = `which occam`.strip

      if File.exist?(path)
        path = File.dirname(path)
        path = File.join(path, "..", "tests.json")
        if File.exist?(path)
          @@occamSpecPath = File.realpath(path)
        end
      end
    end

    specPath = @@occamSpecPath

    if !defined?(@@server_test_run) or @@server_test_run.nil?
      @@server_test_run = nil
      # Only parse the test suite once per server run
      if specPath
        @@server_test_run = Occam::TestRun.new(:file => specPath)
      end
    end

    system = Occam::System.new(:account => current_account)

    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    case format
    when 'text/html'
      # TODO: render nodes
      layout = Occam::Layout.new(:view => :desktop,
                                 :tab  => params[:tab])
      render :haml, :"system/index", :locals => {
        :system   => system,
        :nodes    => [],
        :test_run => @@server_test_run,
        :path     => "/system/tests",
        :layout   => layout
      }
    when 'application/json'
      content_type 'application/json'

      system.info.to_json
    end
  end

end
