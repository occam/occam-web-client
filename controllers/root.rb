class Occam
  OBJECT_BASE_ROUTE_REGEX = %r{(/objects)?/(?<id>[0-9a-zA-Z]{46})}
  OBJECT_ROUTE_REGEX = %r{(/objects)?/(?<id>[0-9a-zA-Z]{46})(/(?<revision>[0-9a-zA-Z]{30,46})(?<index>([/]\d+)+)?)?}

  # Presenter helpers
  def handleCORS(origin=nil)
    # Allow Same-Origin CORS Requests for AJAX components
    if origin == "*"
      domain = "*"
    else
      origin = origin || request["Origin"] || request.env["HTTP_ORIGIN"] || "null"
      domain = "#{request.scheme}://#{request.host_with_port}"
    end

    if ["null", domain].include? origin
      headers "Access-Control-Allow-Origin" => origin,
              "Access-Control-Allow-Headers" => "x-requested-with, range, accept-ranges, accept, Accept-Ranges, Content-Type, X-Occam-Token",
              "Access-Control-Expose-Headers" => "X-Occam-Token, X-Content-Length, Accept-Ranges, Content-Encoding, Content-Length, Content-Range, Content-Type, Allow, Cache-Control",
              "Access-Control-Allow-Methods" => "GET,OPTIONS",
              "Origin" => origin

    end

    if origin == "null"
      headers "Access-Control-Allow-Credentials" => "true"
    end

    headers "X-Frame-Options" => "ALLOWALL"
  end

  # Resolves the object from the route
  def resolveObject(as = Occam::Object)
    @object = nil if !defined?(@object)

    # When we upgrade an Object to a Workflow or other specific type,
    # we can retain the info that was retrieved so we don't get it again
    info = nil
    if @object && as != Occam::Object
      info = @object.info
    end

    require 'cgi'

    as.new(:id       => params[:id],
           :revision => params[:revision],
           :path     => params[:path],
           :index    => ((params[:index] || "").split("/")[1..-1] || []).map(&:to_i),
           :link     => params["link"],
           :account  => current_account,
           :tokens   => (CGI.unescape(cookies[:tokens] || "")).split(',') + [params["token"]],
           :info     => info)
  end

  # Before each route that is looking at an object, pull the object in question
  # Cause a 404 if it does not.
  before %r{#{OBJECT_BASE_ROUTE_REGEX}(/.+)?} do
    @object = resolveObject()

    if current_account
      cookies[:token] = current_account.token
    else
      cookies[:token] = nil
    end

    if params[:accept]
      request.env["HTTP_ACCEPT"] = params[:accept]
    end

    if not @object.exists?
      handleCORS('null')
      status 404
      halt
    end

    if @object.resource?
      @object = @object.as(Occam::Resource)
    end
  end

  before %r{#{OBJECT_ROUTE_REGEX}(/.+)?} do
    @object = resolveObject()

    if not @object.exists?
      handleCORS('null')
      status 404
      halt
    end

    if @object.resource?
      @object = @object.as(Occam::Resource)
    end
  end

  require_relative "svg"
  require_relative "static"
  require_relative "accounts"
  require_relative "git"
  require_relative "people"
  require_relative "search"
  require_relative "session"
  require_relative "system"
  require_relative "task"
  require_relative "identities"
  require_relative "viewers"
  require_relative "objects"
  require_relative "storage"
  require_relative "redirects"
end
