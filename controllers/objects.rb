# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
require 'rss'

class Occam
  VALID_TAB_ROUTES = ["eula", "license", "licenses", "metadata", "files", "raw", "history", "admin", "access", "run", "view", "workflow", "social", "configure", "authorships", "trust", "inputs", "output", "contents"]

  # Deletes the given comment if permissions are correct and the comment exists.
  delete %r{#{OBJECT_ROUTE_REGEX}/social/comments/(?<comment_id>.*?)} do
    # Fetch the original comment in order to check that we are allowed to edit
    # it.
    match = @object.viewComments(:simple => true, :only_matching => true, :id => params[:comment_id])
    if match.nil? or match.empty? or current_account.nil? or match[0][:person][:id] != current_person.id
      status 404
      return
    end

    record = @object.deleteComment(:comment_id => params[:comment_id])

    record[:numReplies] ||= 0
    record[:replies]    ||= []

    begin
      record[:person].delete(:revision)
      record[:person][:ownerInfo] = record[:person]
      record[:person][:info] = record[:person]
      record[:person][:account] = current_account
      record[:person] = Occam::Person.new(record[:person])
    rescue
      record.delete :person
    end

    if request.xhr?
      content_type "text/html"
      render :haml, :"objects/social/_comment", :layout => false, :locals => {
        :comment => record,
        :object  => @object
      }
    else
      redirect @object.url(:path => "social")
    end
  end

  # Posts the comment to the object.
  post %r{#{OBJECT_ROUTE_REGEX}/social/comments/?} do
    if current_account.nil?
      status 404
      return
    end

    record = @object.addComment(:comment => params[:comment], :anon => params[:anon], :inReplyTo => params["inReplyTo"])

    record[:numReplies] ||= 0
    record[:replies] ||= []

    begin
      record[:person].delete(:revision)
      record[:person][:ownerInfo] = record[:person]
      record[:person][:info] = record[:person]
      record[:person][:account] = current_account
      record[:person] = Occam::Person.new(record[:person])
    rescue
      record.delete :person
    end

    if request.xhr?
      content_type "text/html"
      render :haml, :"objects/social/_comment", :layout => false, :locals => {
        :comment => record,
        :object  => @object
      }
    else
      redirect @object.url(:path => "social", :query => {:comment => record[:id], :inReplyTo => params["inReplyTo"]})
    end
  end

  # Edit the given comment with the provided changes.
  post %r{#{OBJECT_ROUTE_REGEX}/social/comments/(?<comment_id>.*)} do
    # Fetch the original comment in order to check that we are allowed to edit
    # it.
    match = @object.viewComments(:simple => true, :only_matching => true, :id => params[:comment_id])
    if not match or current_account.nil? or match[0][:person][:id] != current_person.id
      status 404
      return
    end

    record = @object.editComment(:comment => params[:comment], :edit => params[:comment_id])

    record[:numReplies] ||= 0
    record[:replies] ||= []

    begin
      record[:person].delete(:revision)
      record[:person][:ownerInfo] = record[:person]
      record[:person][:info] = record[:person]
      record[:person][:account] = current_account
      record[:person] = Occam::Person.new(record[:person])
    rescue
      record.delete :person
    end

    if request.xhr?
      content_type "text/html"
      render :haml, :"objects/social/_comment", :layout => false, :locals => {
        :comment => record,
        :object  => @object
      }
    else
      redirect @object.url(:path => "social", :query => {:comment => record[:id], :inReplyTo => params["inReplyTo"]})
    end
  end

  # Helper function that fills up the maker feed with comments.
  def appendCommentsToRSS(maker, comments)
    max_time = nil
    comments.each do |comment|
      comment_time = Time.parse(comment[:createTime])
      maker.items.new_item do |item|
        item.guid.content = @object.url(:path => "social/comments/?comment=#{comment[:id]}")
        item.link = @object.url(:path => "social/comments/?comment=#{comment[:id]}")
        item.title = "New comment on \"#{@object.info[:name]}\""
        item.content.content = comment[:content]
        item.updated = comment_time.to_s
      end
      child_max_time = appendCommentsToRSS(maker, comment[:replies])
      # Child must have been created after the parent.
      if child_max_time
        if (max_time and child_max_time > max_time) or not max_time
          max_time = child_max_time
        end
      elsif (max_time and comment_time > max_time) or not max_time
        max_time = comment_time
      end
    end
    return max_time
  end

  # RSS feed for the object.
  get %r{#{OBJECT_ROUTE_REGEX}/feed?} do
    # TODO: Provide other updates about the object.
    since = request.env['If-Modified-Since']
    if since
      since = Time.parse(since).strftime("%F %T")
    end

    comments = @object.viewComments(:inReplyTo => params["inReplyTo"], :simple => true, :all => true, :since => since)
    content_type "application/atom+xml"
    last_modified = nil

    # Create an ATOM feed.
    rss = RSS::Maker.make("atom") do |maker|
      maker.channel.author = "OCCAM"
      maker.channel.title = @object.info[:name]
      maker.channel.id = @object.url()

      # If nothing was added recently, just report the current time.
      last_modified = appendCommentsToRSS(maker, comments)
      last_modified ||= Time.now

      maker.channel.updated = last_modified.to_s
    end

    headers['Last-Modified'] = last_modified.strftime("%a, %d %b %Y %T GMT")
    rss.to_s
  end

  # Gathers the comments of the object, in a generic sense.
  get %r{#{OBJECT_ROUTE_REGEX}/social/comments/?} do

    # TODO: should be able to pull HTML or JSON for this
    #       the HTML should render the replies as a list

    @object.viewComments(:inReplyTo => params["inReplyTo"], :simple => true).to_json
  end

  # Retrieve form to edit the object's metadata
  get %r{#{OBJECT_ROUTE_REGEX}/metadata-editor/?} do
    render :haml, :"objects/metadata-editor", :layout => !request.xhr?, :locals => {
      :object => @object,
      :errors => []
    }
  end

  # Edit an object's metadata to hold new values
  post %r{#{OBJECT_ROUTE_REGEX}/metadata-editor/?} do
    # Update Name, Description, type
    # Add conditional for these, where if the value passed in is blank, then delete the tag.
    @object = @object.set([[:name, params["name"]], [:type, params["type"]], [:description, params["description"]]])

    #redirect @object.url(:path => "markdown_preview", description: params["description"])
    redirect @object.url
  end

  # Retrieve form to edit the object's metadata
  get %r{#{OBJECT_ROUTE_REGEX}/markdown_preview/?} do
    render :haml, :"objects/markdown_preview", :layout => !request.xhr?, :locals => {
      :object => @object,
      :errors => []
    }
  end

  # Edit an object's metadata to hold new values
  post %r{#{OBJECT_ROUTE_REGEX}/markdown_preview/?} do
    # Update Name, Description, type
    # Add conditional for these, where if the value passed in is blank, then delete the tag.
    @object = @object.set([[:name, params["name"]], [:type, params["type"]], [:description, params["description"]]])

    redirect @object.url
  end

  post %r{#{OBJECT_ROUTE_REGEX}/metadata/?} do
    keys_arr = [:name, :collaborators, :website, :source, :backed,
                :generator, :configures, :views, :configuration,
                :environment, :architecture, :"run.file"]

    keys_arr.each do |x|
        if params[x] && params[x] != ''
          @object = @object.set([[x, params[x]]])
        elsif params[x] == ''
          @object = @object.set([[x]])
        end
    end

    redirect request.referrer
  end

  # Retrives replies for a comment
  get %r{#{OBJECT_ROUTE_REGEX}/social/comments/(?<comment_id>.*?)/replies/?} do
    # TODO: ActivityStreams

    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])
    case format
    when 'application/json'
      content_type "application/json"

      comments = @object.viewComments(:inReplyTo => params[:comment_id], :simple => true)
      comments.to_json
    else
      comments = @object.viewComments(:inReplyTo => params[:comment_id])
      render :haml, :"objects/social/_replies", :layout => !request.xhr?, :locals => {:comments => comments, :object => @object}
    end
  end

  # Retrives a single comment.
  get %r{#{OBJECT_ROUTE_REGEX}/social/comments/(?<comment_id>.*?)/?} do
    if current_account.nil?
      status 404
      return
    end

    @object.viewComments(:id => params[:comment_id], :simple => true).to_json
  end

  # Retrieve the object status
  get %r{#{OBJECT_ROUTE_REGEX}/status/?} do
    content_type "application/json"
    @object.status.to_json
  end

  # This routes pushes a run job
  post %r{#{OBJECT_ROUTE_REGEX}/run/?} do
  end

  # This routes pushes a build job
  post %r{#{OBJECT_ROUTE_REGEX}/build/?} do
    # Pull out the object
    @object.build
  end

  # This route creates or links an object into the given object "contains"
  post %r{#{OBJECT_ROUTE_REGEX}/objects/?} do
    new_object = @object.create(params)

    redirect new_object.url
  end

  # This route creates or links an object into the given workflow
  post %r{#{OBJECT_ROUTE_REGEX}/workflow/?} do
    params[:type]="configuration"
    params[:name]="temporary_name"
    #new_object = object.create(params)

    #redirect new_object.url
  end

  # Renders the input for an object
  get %r{#{OBJECT_ROUTE_REGEX}/inputs/?((?<input_index>\d+)/?)?} do
    # Allow AJAX requests for this route
    handleCORS()

    # Determine the input index to render
    index = params[:input_index].to_i

    # Get the node index, if there is one
    nodeIndex = (params["node_index"] || 0).to_i

    inputs = @object.info[:inputs] || []

    if 0 > index || inputs.length <= index
      status 404
      return
    end

    input = inputs[index]

    if input[:type] == "configuration" and !input[:schema].nil?
      # This is a configuration
      # We can render a form for this

      # First, retrieve the schema (if it is a file)
      schema = input[:schema]
      if !schema.is_a?(Hash)
        schema = @object.retrieveJSON(input[:schema])
      end

      # Use this to render the form
      return render :haml, :"objects/_configuration", :layout => !request.xhr?, :locals => {
        :reviewing           => @params[:params] && @params[:params].has_key?("review"),
        :object              => @object,
        :revision            => @object.revision,
        :schema              => schema,
        :configuration_index => index,
        :index               => nodeIndex,
        :defaults            => {}
      }
    end

    # This is a normal input
    # Render something else then
    ""
  end

  # Retrieves the configurations of a node in a workflow
  get %r{#{OBJECT_ROUTE_REGEX}/configure/(?<node_index>\d+)/?} do
    # Get the workflow
    workflow = resolveObject(Occam::Workflow)

    params[:node_index] = params[:node_index].to_i

    render :haml, :"workflows/_configuration_pane", :layout => !request.xhr?, :locals => {
      :workflow => workflow,
      :index    => params[:node_index],
      :connection => workflow.connectionAt(params[:node_index])
    }
  end

  # Updates a configuration of a workflow object
  post %r{#{OBJECT_ROUTE_REGEX}/configure/(?<node_index>\d+)/(?<wire_index>\d+)/?} do
    # Get the workflow
    workflow = resolveObject(Occam::Workflow)

    # Get the wire index so we know which configuration this is
    # If there is an existing configuration (And this workflow owns it)
    # We will update it
    nodeIndex = params[:node_index].to_i
    wireIndex = params[:wire_index].to_i

    inputs = workflow.inputsAt(nodeIndex, wireIndex)

    # If there is no object connected, update the workflow to include an empty configuration
    if inputs.empty?
      # Add a blank configuration
      input = workflow.createConfigurationFor(nodeIndex, wireIndex)
      input.info(true)
      workflow = input.parent.as(Occam::Workflow)
      workflow.info(true)
    else
      input = workflow.resolveNode(inputs[0])
    end

    input = input.as(Occam::Configuration)
    input.info(true)

    # Decode the configuration form data sent from the browser
    # data = configuration.decodeData()
    data = Occam::Workflow.decode_base64(params[:data])

    # Submit the new configuration data to the configuration object
    input = input.configure(data)
    input.info(true)

    if input.parent
      workflow = input.parent.as(Occam::Workflow)
    end

    inputs = workflow.inputsAt(nodeIndex, wireIndex)
    if inputs.empty?
      # Update workflow data to add this to the structure (as a hidden node)
      inputIndex = workflow.append(input)
      workflow.connect(inputIndex, -1, nodeIndex, wireIndex, :visibility => :hidden)
      inputs = [workflow.connectionAt(inputIndex)]
    end

    # Update configuration node in workflow
    inputs[0][:revision] = input.revision

    # Return to the new version of the object
    workflow = workflow.save
    workflow.info(true)
    workflow.data.to_json

    if request.xhr?
    else
      redirect workflow.parent.url
    end
  end

  # Removes an author
  delete %r{#{OBJECT_ROUTE_REGEX}/authorships/(?<person_id>[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})/?} do
    role     = params["role"]
    objectId = params[:person_id]
    username = params["username"]

    record = Occam::Authorship.new(:role    => role,
                                   :object  => @object,
                                   :name    => username,
                                   :person  => Occam::Person.new(:id => objectId),
                                   :account => current_account)

    record.destroy!

    if request.referrer
      redirect request.referrer
    else
      redirect @object.url
    end
  end

  # Adds an author
  post %r{#{OBJECT_ROUTE_REGEX}/authorships/?} do
    role     = params["role"]
    objectId = params["object-id"]
    username = params["username"]

    Occam::Authorship.create(:role    => role,
                             :object  => @object,
                             :name    => username,
                             :person  => Occam::Person.new(:id => objectId),
                             :account => current_account)

    if request.referrer
      redirect request.referrer
    else
      redirect @object.url
    end
  end

  # Creates a trust association
  post %r{#{OBJECT_ROUTE_REGEX}/trust/?} do
    @object.trust!

    if request.xhr?
    else
      if request.referrer
        redirect request.referrer
      else
        redirect @object.url
      end
    end
  end

  # Removes a trust association
  delete %r{#{OBJECT_ROUTE_REGEX}/trust/?} do
    @object.untrust!

    if request.referrer
      redirect request.referrer
    else
      redirect @object.url
    end
  end

  # Creates a review-link
  post %r{#{OBJECT_ROUTE_REGEX}/review-links/?} do
    Occam::ReviewCapability.create(@object)

    if request.xhr?
    else
      if request.referrer
        redirect request.referrer
      else
        redirect @object.url
      end
    end
  end

  # Removes a review-link
  delete %r{#{OBJECT_ROUTE_REGEX}/review-links/?} do
    reviewLink = Occam::ReviewCapability.new(:object  => @object,
                                             :account => current_account)
    reviewLink.destroy!

    if request.referrer
      redirect request.referrer
    else
      redirect @object.url
    end
  end

  # Updates object permissions
  post %r{#{OBJECT_ROUTE_REGEX}/permissions/?} do
    key     = params["update"]
    value   = params["value"]
    person  = params["person"]
    person  = params["object-id"] || person
    section = params["section"]

    if person
      # Pull out the object
      person = Occam::Person.new(:id => person)
    end

    if value == ""
      value = nil
    else
      value = value == "on"
    end

    # Update permission
    row = @object.setPermission(key, value, section == "children", person)

    if request.xhr?
      render :haml, :"objects/permissions/_row",
                    :layout => false,
                    :locals => {
        :access  => row,
        :section => section,
        :object  => @object,
      }
    else
      if request.referrer
        redirect request.referrer
      else
        redirect @object.url(:path => "access")
      end
    end
  end

  # Deletes the permissions for a particular person.
  delete %r{#{OBJECT_ROUTE_REGEX}/permissions/?} do
    person  = params["person"]
    section = params["section"]

    if person
      # Pull out the object
      person = Occam::Person.new(:id => person)
    end

    # Delete permission row
    @object.resetPermission(section == "children", person)

    if request.xhr?
    else
      if request.referrer
        redirect request.referrer
      else
        redirect @object.url(:path => "access")
      end
    end
  end

  # Fork an object to the given object
  post %r{#{OBJECT_ROUTE_REGEX}/fork/?} do
    # Determine index
    params["to"]["index"] = params["to"]["index"].split("/").map(&:to_i)

    # Symbolize the keys so we can pass them to Occam::Object.new
    options = symbolize_keys(params["to"])

    # Get the object we are cloning 'to'
    to = nil
    if options[:id] && options[:id] != ""
      to = Occam::Object.new(options)
    end

    # Clone
    newObject = @object.clone(to, :name => params["name"])
    url = newObject.url

    link_id = nil
    if to.nil?
      # Link to the object
      link_id = current_person.createLink(:relationship => "active",
                                          :object       => newObject,
                                          :account      => current_account,
                                          :tracked      => true)

      url = newObject.url(:query => {:link => link_id})
    end


    # Return the new object information if that is requested, otherwise redirect to the new object
    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])
    case format
    when 'application/json'
      content_type "application/json"

      # When json is requested, this is considered an API call and we will return
      # the command's output
      {
        :object => {
          :id => newObject.withinUuid,
          :revision => newObject.withinRevision,
          :index => newObject.index,
          :link => newObject.link
        },
        :url => url
      }.to_json
    else
      # When html is requested (common case)
      # We will redirect to the html content (the object itself)
      redirect url
    end
  end

  def render_qrcode(format = nil)
    if format.nil?
      format = request.preferred_type(['image/png', 'image/svg+xml', 'application/svg'])
    end

    case format
    when 'application/svg', 'image/svg+xml'
      type = :svg
    else
      type = :png
    end

    if request.xhr?
      content_type "text/html"
      render :haml, :"objects/qrcode", :layout => false, :locals => {
        :object => @object
      }
    elsif type == :svg
      content_type "image/svg+xml"
      @object.qrcode(request, :type => :svg)
    else
      content_type "image/png"
      @object.qrcode(request, :type => :png)
    end
  end

  # Retrieve form to fork the object
  get %r{#{OBJECT_ROUTE_REGEX}/fork/?} do
    render :haml, :"objects/fork", :layout => !request.xhr?, :locals => {
      :object => @object,
      :errors => []
    }
  end

  # Retrieve the qrcode
  get %r{#{OBJECT_ROUTE_REGEX}/qrcode} do
    render_qrcode
  end

  # Retrieve the qrcode (as a png)
  get %r{#{OBJECT_ROUTE_REGEX}/qrcode.png} do
    render_qrcode("image/png")
  end

  # Retrieve the qrcode (as an svg)
  get %r{#{OBJECT_ROUTE_REGEX}/qrcode.svg} do
    render_qrcode("application/svg")
  end

  # Retrieve queued workflow runs
  get %r{#{OBJECT_ROUTE_REGEX}/runs/?} do
    # Pull out the object (as a workflow)
    workflow = resolveObject(Occam::Workflow)

    content_type "application/json"
    workflow.runs.to_json
  end

  # Queue a build
  post %r{#{OBJECT_ROUTE_REGEX}/builds/?} do
    # Pull out the object (as a workflow, if needed)
    run = @object.build

    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    case format
    when 'application/json'
      content_type "application/json"
      run.to_json
    else
      render :haml, :"objects/_run-list-item", :layout => false, :locals => {
        :run => run,
        :object => @object
      }
    end
  end

  # Render a previous build
  get %r{#{OBJECT_ROUTE_REGEX}/builds/(?<build_id>[^/]+)/?} do
    buildID = params[:build_id]
    build = @object.builds.select do |item|
      item.id == buildID
    end.first

    if not build
      status 404
      return
    end

    render :haml, :"objects/_build", :layout => false, :locals => {
      :object => @object,
      :build  => build,
      :task   => Occam::Object.new(:id => buildID, :account => current_account)
    }
  end

  # Queue a run
  post %r{#{OBJECT_ROUTE_REGEX}/runs/?} do
    input = nil
    if params[:input_object_id]
      input = Occam::Object.new(:id       => params[:input_object_id],
                                :revision => params[:input_object_revision],
                                :account  => @object.account)
    end

    # Pull out the object (as a workflow, if needed)
    if @object.info[:type] == "workflow" || @object.info[:type] == "experiment"
      workflow = resolveObject(Occam::Workflow)
      runInfo = workflow.queue
      run = Occam::Run.new(:id => runInfo[:id], :info => runInfo)
    else
      interactive = params[:interactive] == true || params[:interactive] == "true"
      run = @object.run(:input => input, :interactive => interactive)
    end

    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    case format
    when 'application/json'
      content_type "application/json"
      run.to_json
    else
      render :haml, :"objects/_run-list-item", :layout => false, :locals => {
        :run => run,
        :object => @object
      }
    end
  end

  # Cancels the run
  post %r{#{OBJECT_ROUTE_REGEX}/runs/(?<run_index>\d+)/(?<action>\w+)/?} do
  if (params[:action] == "cancel")
      # Pull out the object (as a workflow)
      workflow = resolveObject(Occam::Workflow)

      runInfo = workflow.runInfo(:runID => params[:run_index])
      run = Occam::Run.new( :id => runInfo[:run][:id],
                            :info => runInfo,
                            :account => @object.account)


      run.cancel
    end
    if request.xhr?
      content_type "application/json"
      run.info.to_json
    elsif request.referrer
      redirect request.referrer
    end
  end

  # Cancel the job
  post %r{#{OBJECT_ROUTE_REGEX}/jobs/(?<job_index>\d+)/(?<action>\w+)/?} do
    if (params[:action] == "cancel")

      job = Occam::Job.new(:id      => params[:job_index],
                           :account => @object.account)

      job.cancel
    end

    if request.xhr?
      content_type "application/json"
      job.info.to_json
    elsif request.referrer
      redirect request.referrer
    end
  end

  # Retrieve the job task instantiation
  get %r{#{OBJECT_ROUTE_REGEX}/jobs/?((?<job_index>\d+)/task/?)?} do
    job = Occam::Job.new(:id      => params[:job_index],
                         :account => @object.account)

    info = job.taskInfo
    if not info
      status 404
      return
    end

    content_type "application/json"
    info.to_json
  end

  # Retrieve the job network instantiation
  get %r{#{OBJECT_ROUTE_REGEX}/jobs/?((?<job_index>\d+)/network/?)?} do
    job = Occam::Job.new(:id      => params[:job_index],
                         :account => @object.account)

    info = job.networkInfo
    if not info
      status 404
      return
    end

    content_type "application/json"
    info.to_json
  end

  # Retrieve the job status
  get %r{#{OBJECT_ROUTE_REGEX}/jobs/?((?<job_index>\d+)/?)?} do
    job = Occam::Job.new(:id      => params[:job_index],
                         :account => @object.account)

    content_type "application/json"
    {"job" => job.info}.to_json
  end

  # Retrieve the workflow run status
  get %r{#{OBJECT_ROUTE_REGEX}/runs/?((?<run_index>\d+)/?)?} do
    # Pull out the object (as a workflow)
    workflow = resolveObject(Occam::Workflow)

    format = request.preferred_type(['application/json', 'text/html'])

    run = workflow.runInfo(:runID => params[:run_index])
    case format
    when 'text/html'
      jobs = run[:nodes].values.flatten.map{|job| Occam::Job.new(job) }
      render :haml, :"objects/_job-list", :layout => !request.xhr?, :locals => {
        :jobs   => jobs,
        :object => @object,
      }
    else
      content_type "application/json"

      run.to_json
    end
  end

  # Retrieve the workflow run job list
  get %r{#{OBJECT_ROUTE_REGEX}/runs/(?<run_index>\d+)/((?<node_index>\d+)/?)} do
    # Pull out the object (as a workflow)
    workflow = resolveObject(Occam::Workflow)

    format = request.preferred_type(['application/json', 'text/html'])

    run = workflow.runInfo(:runID => params[:run_index])

    # Determine if the node exists (and is known) to the run
    node_index = params["node_index"].to_s.intern

    # If not, return a 404
    if not run[:nodes].has_key?(node_index)
      status 404
      return
    end

    # Render the job listing
    case format
    when 'text/html'
      jobs = run[:nodes][node_index][:jobs].map{|job| Occam::Job.new(job) }
      render :haml, :"objects/_job-list", :layout => !request.xhr?, :locals => {
        :jobs   => jobs,
        :object => @object,
      }
    else
      content_type "application/json"

      run[:nodes][node_index].to_json
    end
  end

  # Updates a file within an object
  post %r{#{OBJECT_ROUTE_REGEX}/files/(?<path>.+)?} do
    if params[:fileToUpload]
      # Parse JSON for drag n drop file upload
      params[:path] ||= ""
      @object = @object.set(params[:fileToUpload][:tempfile].read, params[:path] + "/" + params[:fileToUpload][:filename])

      return {
        :url => @object.url(:path => "files/" + params[:path] + params[:fileToUpload][:filename])
      }.to_json
    elsif request.xhr? && !params[:data]
      # Uploading a file by simply posting data to the correct path
      @object = @object.set(request.body.read, params[:path])

      return {
        :url => @object.url(:path => "files/" + params[:path])
      }.to_json
    else
      # Normal updating of JSON data
      items=[]
      data = params[:data]
      if !data.is_a?(Hash)
        data = JSON.parse(data)
      end
      data.each do |key, value|
        items << [key, value.to_json]
      end

      # Update the object
      @object = @object.set(items, params[:path], :type => "json")
    end

    format = request.preferred_type(['text/html', 'application/json'])
    case format
    when 'application/json'
      content_type "application/json"

      # When json is requested, respond with the API return data
      {
        :url => @object.url(:path => "files/" + params[:path])
      }.to_json
    else
      # In the common case, redirect to the updated file
      if params[:referrer]
        redirect @object.url(:path => params[:referrer])
      else
        redirect @object.url(:path => "files/" + params[:path])
      end
    end
  end

  # Gets the file content of the object
  get %r{#{OBJECT_ROUTE_REGEX}/file/?} do
    format = request.preferred_type(['text/html', 'text/html+configuration', 'application/json'])
    case format
    when "text/html+configuration"
      # Render the object data as a configuration form

      # First, retrieve the schema
      schema = @object.schema

      # Use this to render the form
      return render :haml, :"objects/_configuration", :layout => !request.xhr?, :locals => {
        :reviewing           => @params[:params] && @params[:params].has_key?("review"),
        :object              => @object,
        :revision            => @object.revision,
        :schema              => schema,
        :configuration_index => 0,
        :index               => 0,
        :defaults            => {}
      }
    else
      filePath = @object.file
      redirect @object.url(:path => "files/#{filePath}")
    end
  end

  # Gets the contents of the object
  get %r{#{OBJECT_ROUTE_REGEX}/contains/?} do
    format = request.preferred_type(['text/html', 'application/json'])
    case format
    when 'application/json'
      content_type "application/json"

      @object.contents.map(&:info).to_json
    else
      render :haml, :"objects/_object_tree", :layout => !request.xhr?, :locals => {
        :root    => false,
        :type    => params["type"],
        :objects => @object.contents
      }
    end
  end

  # Retrieves the EULA form (or 404 if it doesn't have one)
  get %r{#{OBJECT_ROUTE_REGEX}/eula/?} do
    if @object.eula.nil?
      status 404
      return
    end

    format = request.preferred_type(['text/html', 'application/json'])
    case format
    when 'application/json'
      content_type "application/json"

      @object.eula.info.to_json
    else
      # HTML
      render :haml, :"objects/eula", :layout => !request.xhr?, :locals => {
        :object => @object,
        :license => @object.eula
      }
    end
  end

  # Retrieves the first license text
  get %r{#{OBJECT_ROUTE_REGEX}/license/?} do
    redirect @object.url(:path => "/licenses/0")
  end

  # Retrieves the given license text
  get %r{#{OBJECT_ROUTE_REGEX}/licenses/?((?<license_index>\d+)/?)?} do
    format = request.preferred_type(['text/html', 'application/json'])
    case format
    when 'application/json'
      content_type "application/json"

      @object.licenses[params[:license_index].to_i].info.to_json
    else
      # HTML
      render :haml, :"objects/license", :layout => !request.xhr?, :locals => {
        :object => @object,
        :license => @object.licenses[params[:license_index].to_i]
      }
    end
  end

  options %r{#{OBJECT_ROUTE_REGEX}/files/(?<path>.+)?} do
    # Resolve the object a second time to set the path
    @object = resolveObject()

    handleCORS('null')

    status 200
  end

  options %r{#{OBJECT_ROUTE_REGEX}/raw/(?<path>.+)?} do
    # Resolve the object a second time to set the path
    @object = resolveObject()

    handleCORS('null')

    status 200
  end

  get %r{#{OBJECT_ROUTE_REGEX}/citations/(?<format>.+)} do
    "<div class='card'>" +
      @object.citation(:format => params[:format]) +
      "</div>"
  end

  # Return a list of possible backends that can run this object
  get %r{#{OBJECT_ROUTE_REGEX}/backends/?} do
  end

  post %r{#{OBJECT_ROUTE_REGEX}/publish/(?<backend>[^/]+)(/(?<id>[^/+]))?} do
    if current_person.nil?
      status 404
    end

    globalStore = Occam::System::Storage.list[params[:backend].intern]

    if not globalStore
      return 404
    end

    if globalStore.accountInfo != {}
      store = Occam::System::Storage.view(params[:backend], current_account, params[:id]).first
      store.push(@object)
    else
      globalStore.push(@object)
    end
  end

  # Renders an object view
  #
  # Examples:
  # GET /bb79a35e-528e-11e6-bb9d-f23c910a26c8/fbbe5cadd1c5afc73348fb2fa6b51c624953082e/run
  #   Look at this object's run tab
  # GET /bb79a35e-528e-11e6-bb9d-f23c910a26c8/fbbe5cadd1c5afc73348fb2fa6b51c624953082e/raw/object.json
  #   Download this object's object.json metadata
  # GET /bb79a35e-528e-11e6-bb9d-f23c910a26c8/fbbe5cadd1c5afc73348fb2fa6b51c624953082e/files/foo.png
  #   View the file from within this object
  # GET /objects/bb79a35e-528e-11e6-bb9d-f23c910a26c8/fbbe5cadd1c5afc73348fb2fa6b51c624953082e/files/foo.png
  #   Optionally, place /objects in the front of the route.
  get %r{#{OBJECT_ROUTE_REGEX}(/?((?<tab>[^/]+)(?<path>.+)?)?)?} do
    if params[:_occam_tokens]
      url = request.url.gsub(/_occam_tokens=#{Regexp.quote(CGI.escape(params[:_occam_tokens]))}[&]?/, "")
      url = url.gsub(/[&?]$/, "")
      cookies[:tokens] = params[:_occam_tokens]
      redirect url
      return
    end

    if params[:tab] && !Occam::VALID_TAB_ROUTES.include?(params[:tab])
      status 404
      return
    end

    # Resolve the object a second time to set the path
    if params[:tab] == "raw" or params[:tab] == "files"
      @object = resolveObject()
      @object = @object.as(Occam::Resource) if @object.resource?
    end

    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml', 'application/zip'])

    if params[:tab] == "raw"
      handleCORS('null')

      # Pull out file data
      meta = @object.retrieveFileStat(@object.path)

      if meta.nil?
        status 404
        return
      end

      cache_control :max_age => 31536000

      if meta[:type] == "tree"
        content_type "application/json"
        return @object.retrieveDirectory(@object.path).to_json
      end

      content_type((meta[:mime] || [])[0] || "application/octet-stream")

      if meta && meta.has_key?(:size)
        headers['X-Content-Length'] = meta[:size].to_s
      end

      # Ensure a reasonable content policy
      #
      # This means if the content is loaded by the browser, no matter the
      # actual origin... the scripts inside cannot interact with the current
      # user session or cookies (They get a NULL origin)
      #
      # Browser Support: https://caniuse.com/#feat=contentsecuritypolicy

      if not @object.trusted?
        sandbox = "sandbox allow-pointer-lock allow-scripts;"

        headers["X-WebKit-CSP"]              = sandbox # Fallback (Android)
        headers["X-Content-Security-Policy"] = sandbox # Fallback (IE)
        headers["Content-Security-Policy"]   = sandbox
      end

      start = nil
      length = nil
      range = Rack::Utils.get_byte_ranges(request.env["HTTP_RANGE"], meta[:size])
      if range && range.any?
        start = range[0].first
        length = range[0].last - range[0].first
      end
      data = @object.retrieveFile(@object.path, :start => start, :length => length, :stream => true)

      return stream :keep_open do |out|
        while true
          bytes = data.read(1024*10)
          if bytes.nil? || bytes.length == 0
            break
          else
            out << bytes
          end
        end
      end
    end

    if format == "application/zip" && params[:tab] == "files"
      # Returns the compressed version of the requested path/file
      handleCORS('null')

      # Pull out file data
      meta = @object.retrieveFileStat(@object.path)

      if meta.nil?
        status 404
        return
      end

      # We cache the zipped content
      cache_control :max_age => 31536000

      content_type "application/zip"

      data = @object.retrieveFile(@object.path, :compress => "zip", :stream => true)

      return stream :keep_open do |out|
        while true
          bytes = data.read(1024*10)
          if bytes.nil? || bytes.length == 0
            break
          else
            out << bytes
          end
        end
      end
    end

    handleCORS()

    case format
    when 'application/json'
      content_type "application/json"

      if params[:tab] == "raw"
        # List file stat or directory stat
        stat = @object.retrieveFileStat(@object.path)
        if stat[:type] == "tree"
          @object.retrieveDirectory(@object.path).to_json
        else
          stat.to_json
        end
      elsif params[:tab] == "history"
        @object.retrieveHistory.to_json
      elsif not params[:tab]
        @object.info.to_json
      else
        stat = @object.retrieveFileStat(@object.path)
        stat.to_json
      end
    else
      if request.xhr? && params[:tab] == "files" && @object.path
        if @object.isGroup?(@object.path)
          render :haml, :"objects/_directory", :layout => false, :locals => {
            :object => @object,
            :workset => nil
          }
        else
          render :haml, :"objects/_filedata", :layout => false, :locals => {
            :object => @object,
            :data_mime_type => ""
          }
        end
      elsif request.xhr? && ["social", "files", "output", "access", "history", "publish"].include?(params[:tab])
        render :haml, :"objects/_#{params[:tab]}", :layout => !request.xhr?, :locals => {
          :object => @object,
        }
      else
        render :haml, :"objects/show", :layout => !request.xhr?, :locals => {
          :object => @object,
        }
      end
    end
  end

end
