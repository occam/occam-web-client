# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  get '/' do
    #System.setHostAndPort(request)

    haml :"index"
  end

  get '/styleguide' do
    haml :"static/styleguide"
  end

  get '/about' do
    markdown :"static/about", :layout => :static
  end

  get '/acknowledgements' do
    markdown :"static/credits", :layout => :static
  end

  # 404 route
  not_found do
    markdown :"static/404", :layout => :static
  end

  # 500 route
  error do
    markdown :"static/500", :layout => :static
  end

  # Successfully get the 404 page
  get '/404' do
    markdown :"static/404", :layout => :static
  end

  # Successfully get the 500 page
  get '/500' do
    markdown :"static/500", :layout => :static
  end

  # Redirects to the index of the code API
  get '/daemon-docs' do
    redirect "/daemon-docs/index.html"
  end

  # Get the code API, if it exists
  get '/daemon-docs/?*' do
    if !defined?(@@occamDocPath) || @@occamDocPath.nil?
      # Get the location of the daemon
      path = `which occam`.strip

      if File.exist?(path)
        path = File.dirname(path)
        path = File.join(path, "..", "docs", "html")
        if File.exist?(path)
          @@occamDocPath = File.realpath(path)
        end
      end
    end

    if defined?(@@occamDocPath)
      dirs = params[:splat][0].split("/")
      path = @@occamDocPath
      dirs.each do |dir|
        if dir == ".."
          status 404
          return
        end
        path = File.join(path, dir)
      end

      path = File.realpath(path)

      if path.start_with? @@occamDocPath
        send_file path
      end
    end
  end

  # HTTPS Public Key
  get '/public_key/?' do
    content_type "text/plain"
    send_file "key/server.crt"
  end
end
