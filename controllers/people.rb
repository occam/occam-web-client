# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # Get a list of all people
  get '/people' do
    people = Occam::Person.all
    render :haml, :"people/index", :locals => {
      :people => people
    }
  end

  # Form to create a new person
  get '/people/new' do
    render :haml, :"people/new", :layout => !request.xhr?, :locals => {:errors => nil}
  end

  # Creates an account
  post '/people' do
    if params["newGroup"]
      if !logged_in?
        status 406
        return
      end

      person = current_person.newPerson(params["username"], [params["subtype"]])
      redirect person.url
    else
      username = params["username"]
      password = params["password"]

      account = Occam::Account.create(username, password)
      login(username, password)

      person_id = account.person.id

      redirect "/people/#{person_id}"
    end
  end

  # Deletes bookmarks
  delete '/people/:id/bookmarks/:target' do
    person = Occam::Person.new(:id       => params[:id],
                               :account  => current_account)
    if !person.exists? || person.id != current_person.id
      # TODO: confirm this is not a 406
      status 404
      return
    end

    # Craft the link
    link = Occam::Link.new(:target       => Occam::Object.new(:id => params[:target]),
                           :relationship => "bookmark",
                           :account      => current_account,
                           :source       => current_person)

    # Destroy the link
    link.destroy!

    if request.xhr?
    else
      if request.referrer
        redirect request.referrer
      else
        redirect object.url
      end
    end
  end

  # Creates bookmarks
  post '/people/:id/bookmarks' do
    person = Occam::Person.new(:id       => params[:id],
                               :account  => current_account)
    if !person.exists? || person.id != current_person.id
      # TODO: confirm this is not a 406
      status 404
      return
    end

    # Get the object
    object_id = params["object_id"]
    object = Occam::Object.new(:id => object_id)

    if !object.exists?
      # TODO: this is likely an argument error
      status 404
      return
    end

    # Create bookmark
    current_person.createBookmark(object)

    if request.xhr?
    else
      if request.referrer
        redirect request.referrer
      else
        redirect object.url
      end
    end
  end

  # Adds an object to the list of recently used objects
  post '/people/:id/recentlyUsed' do
    person = Occam::Person.new(:id       => params[:id],
                               :account  => current_account)
    if !person.exists? || person.id != current_person.id
      # TODO: confirm this is not a 406
      status 404
      return
    end

    # Get the object
    object_id = params["object_id"]
    object = Occam::Object.new(:id => object_id)

    if !object.exists?
      # TODO: this is likely an argument error
      status 404
      return
    end

    # Create bookmark
    current_person.createRecentlyUsed(object)

    if request.xhr?
    else
      if request.referrer
        redirect request.referrer
      else
        redirect object.url
      end
    end
  end

  # Updates people memberships
  post '/people/:id/members' do
    person = Occam::Person.new(:id       => params[:id],
                               :account  => current_account)

    if !person.exists?
      # TODO: confirm this is not a 406
      status 404
      return
    end

    member  = params["person"]
    member  = params["object-id"] || person

    if member
      # Pull out the object
      member = Occam::Person.new(:id => member)
    end

    # Update permission
    person.addMember(member)

    if request.xhr?
      render :haml, :"people/memberships/_row",
                    :layout => false,
                    :locals => {
        :person => person,
        :member => member
      }
    else
      if request.referrer
        redirect request.referrer
      else
        redirect person.url(:path => "memberships")
      end
    end
  end

  # Deletes the membership of the given member in the given person
  delete '/people/:id/members/:member_id' do
    person = Occam::Person.new(:id       => params[:id],
                               :account  => current_account)

    if !person.exists?
      status 404
      return
    end

    member = Occam::Person.new(:id       => params[:member_id],
                               :account  => current_account)

    if !member.exists?
      # TODO: confirm this is not a 406
      status 404
      return
    end

    # Delete permission row
    person.removeMember(member)

    if request.xhr?
    else
      if request.referrer
        redirect request.referrer
      else
        redirect person.url(:path => "memberships")
      end
    end
  end

  # Update person information
  post '/people/:id' do
    person = Occam::Person.new(:id       => params[:id],
                               :account  => current_account)

    if not person.exists?
      status 404
      return
    end

    if not logged_in?
      status 404
      return
    end

    # Do not authorize this edit if they aren't logged in
    # TODO: allow person editing for administrators
    if person.id != current_person.id
      status 406
      return
    end

    data = []

    ["organization", "name", "description", "email"].each do |key|
      data << [key, params[key]]
    end

    person = person.set(data)
    if person.nil?
      status 422
      return
    end

    # Update avatar image if it is given
    if params["avatar"]
      if params["avatar"][:tempfile]
        person.set(params["avatar"][:tempfile].read, "avatar.jpg")
      end
    end

    redirect person.url
  end

  # Form to edit an person's profile
  get '/people/:id/edit' do
    person = Occam::Person.new(:id       => params[:id],
                               :account  => current_account)

    if not person.exists?
      status 404
    else
      # TODO: allow person editing for administrators
      if current_person.id != person.id
        status 406
      else
        render :haml, :"people/edit", :locals => {
          :errors  => nil,
          :person => current_person
        }
      end
    end
  end

  # Retrieves the avatar image for the particular person
  get '/people/:id/avatar' do
    person = Occam::Person.new(:id       => params[:id],
                               :account  => current_account)

    if not person.exists?
      status 404
    else
      url = person.avatar_url((params["size"] || 64).to_i)

      if url.start_with?("data")
        content_type "image/png"
        person.avatar
      else
        redirect url
      end
    end
  end

  # Retrieve a specific person page
  get '/people/:id/?:tab?' do
    person = Occam::Person.new(:id       => params[:id],
                               :account  => current_account)
    if not person.exists?
      status 404
    else
      render :haml, :"people/show", :locals => {
        :errors         => nil,
        :tab            => params[:tab],
        :help           => params['help'],
        :person         => person,
        :worksets       => person.worksets,
        :collaborations => person.collaborations
      }
    end
  end

  # Retrieves the avatar image for the particular person
  get '/people/:id/:revision/avatar' do
    person = Occam::Person.new(:id       => params[:id],
                               :revision => params[:revision],
                               :account  => current_account)

    if not person.exists?
      status 404
    else
      url = person.avatar_url((params["size"] || 64).to_i)

      if url.start_with?("data")
        content_type "image/png"
        person.avatar
      else
        redirect url
      end
    end
  end

  # Adds a new object to the collection.
  # It may create a new object if there is no "id" passed.
  post '/people/:uri/collection' do
    if current_person.nil? || params[:uri] != current_person.id
      status 404
    end

    id   = params["id"]
    type = params["type"]
    name = params["name"]

    if id.nil?
      # Create a new object
      object = Occam::Object.create(:name    => name,
                                    :type    => type,
                                    :account => current_account)

      # Link to the object
      link_id = current_person.createLink(:relationship => "active",
                                          :object       => object,
                                          :account      => current_account,
                                          :tracked      => true)

      redirect object.url(:query => {:link => link_id})
    end
  end

  post '/people/:id/storage/:backend' do
    # Do not allow a non-logged in account to mess with this
    if current_person.nil? || params[:id] != current_person.id
      status 404
    end

    record = Occam::System::Storage.info(:backend => params[:backend], :account => current_account)
    if record
      data = {}
      record.accountInfo.each do |k, v|
        if params.has_key? k.to_s
          data[k] = params[k.to_s]
        end
      end
      record = record.set(data)
    end

    # Determine if this should push an oauth token?
    if record.accountInfo[:oauthURL] && record.accountInfo[:oauthAccessToken] == ""
      redirect record.accountInfo[:oauthURL]
    end

    # Render the updated record
    record
  end

  # Update a storage backend instantiation
  post '/people/:id/storage/:backend/:info_id' do
    # Do not allow a non-logged in account to mess with this
    if current_person.nil? || params[:id] != current_person.id
      status 404
    end

    records = Occam::System::Storage.view(params[:backend], current_account, params[:info_id])

    status 404 if records.empty?

    record = records.first
    data = {}
    record.accountInfo.each do |k, v|
      if params.has_key? k.to_s
        data[k] = params[k.to_s]
      end
    end
    record = record.set(data)

    redirect request.referrer
  end

  # Remove a storage backend instantiation
  delete '/people/:id/storage/:backend/:info_id' do
    # Do not allow a non-logged in account to mess with this
    if current_person.nil? || params[:id] != current_person.id
      status 404
    end

    records = Occam::System::Storage.view(params[:backend], current_account, params[:info_id])

    status 404 if records.empty?

    record = records.first
    record.destroy!

    redirect request.referrer
  end

  get '/people/:id/runs/running' do
    "{}"
  end
end
