# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This module represents the routes for Virtual Machine creation.

  # This route will yield a task object for the given backend set and the
  # given object metadata. It only needs the object's build/run and environment
  # and architecture information to craft the possible VM object.
  get '/task' do
    # A target environment and architecture
    toEnvironment  = params["toEnvironment"]
    toArchitecture = params["toArchitecture"]
    toBackend      = params["toBackend"]

    # The object metadata the node desires to run
    fromEnvironment    = params["fromEnvironment"]
    fromArchitecture   = params["fromArchitecture"]
    fromObjectId       = params["fromObject"]
    fromObjectRevision = params["fromRevision"]

    # Inputs to the object (<id>,<id>,...)
    inputIds         = (params["inputs"] || "").split(',')

    options = {}

    # Account
    options[:account] = current_account

    # Access token
    if params["token"]
      options[:token] = params["token"]
    end

    if toBackend
      options[:backend] = toBackend
    elsif !(toEnvironment.nil? || toArchitecture.nil?)
      options[:targetEnvironment]  = toEnvironment
      options[:targetArchitecture] = toArchitecture
    else
      status 404
      return
    end

    if fromObjectId
      options[:object] = Occam::Object.new(:id => fromObjectId, :revision => fromObjectRevision)
    elsif !(fromEnvironment.blank? || fromArchitecture.blank?)
      options[:environment]  = fromEnvironment
      options[:architecture] = fromArchitecture
    else
      status 404
      return
    end

    # TODO: distinguish
    options[:inputs]  = []
    options[:running] = []

    inputIds.each do |inputId|
      if params["path"]
        inputId += "#{params["path"].start_with?("/") ? "" : "/"}#{params["path"]}"
      end
      options[:inputs]  << inputId
      options[:running] << inputId
    end

    content_type "application/json"

    task = Task.new(options)

    # We will return the task manifest
    task.info.to_json
  end

  # This will list the viewers that can possibly view the given object
  # or object description.
  get '/task/viewers' do
  end

  # This will list the runners that can possibly execute the given object
  # or object description.
  get '/task/runners' do
  end

  # This route will return the backends that can be used to run the given object
  # with the given objects as input.
  get '/task/backends' do
    content_type "application/json"

    return ['docker'].to_json
  end
end
