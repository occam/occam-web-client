# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  get '/search' do
    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    query = params["search"] || params["query"]

    # Pass along the off types as excluded types
    excludeTypes = ["task"]
    params["search-facet-type-name"] = (params["search-facet-type-name"] || {}).values
    params["search-facet-type"] = (params["search-facet-type"] || {}).values
    (params["search-facet-type-name"] || []).zip(params["search-facet-type"] || []).each do |type, value|
      if value != "on"
        if !excludeTypes.include?(type)
          excludeTypes << type
        end
      elsif excludeTypes.include?(type)
        excludeTypes.delete(type)
      end
    end

    data    = {}
    objects = []
    types   = []

    if params["types"] == "on"
      types = Object.types(:query => query).map do |type|
        { :type => type }
      end
    end

    if params["objects"] == "on" || params["types"].nil?
      data = Object.fullSearch(:name         => query,
                               :type         => params["type"],
                               :environment  => params["environment"],
                               :architecture => params["architecture"],
                               :viewsType    => params["viewsType"],
                               :viewsSubtype => params["viewsSubtype"],
                               :excludeTypes => excludeTypes)
      objects = data[:objects]

      # Keep track of which types are turned off because they obviously won't be
      # in the types list. We have to render them.
      data[:excludeTypes] = excludeTypes
    end

    case format
    when 'application/json'
      {
        "types" => types.map do |type|
          type.update(:icon => Occam::Object.iconURLFor(type[:type]))
        end,
        "objects" => objects.map do |object|
          object.info.update(:revision  => object.revision,
                             :icon      => object.iconURL,
                             :smallIcon => object.iconURL(:small => true))
        end
      }.to_json
    when 'text/html'
      render :haml, :"search/results", :layout => !request.xhr?, :locals => {
        :types   => types,
        :data    => data,
        :objects => objects,
        :query   => query
      }
    end
  end
end
