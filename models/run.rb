class Occam
  class Run
    attr_reader :id

    INFO_CACHE_TIME = 2.0 # Time in seconds to keep status information

    def initialize(options = {})
      @id   = options[:id]
      @info = options[:info]
      @account = options[:account]
    end

    def info
      if false && (@last.nil? || (Time.now - @last) > INFO_CACHE_TIME)
        # Cache for a certain amount of time
        arguments  = [self.id]
        cmdOptions = {
          "-j" => true
        }

        if @account
          cmdOptions["-T"] = @account.token
        end

        result = Occam::Worker.perform("jobs", "status", arguments, cmdOptions)
        begin
          @info = JSON.parse(result[:data], :symbolize_names => true)
        rescue
          @info
        end

        @last = Time.now
      end

      @info || {}
    end

    # Cancels the run
    def cancel
      arguments  = [self.id]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("workflows", "cancel", arguments, cmdOptions)
      result[:data]
    end

    # Retrieves job information
    def jobs
      self.info[:jobs]
    end
  end
end
