# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  require_relative 'object'

  # This wraps functionality for a Workflow, which is a particular type of
  # Object.
  class Configuration < Occam::Object
    # Updates the given configuration with the given data
    #
    # If data is an array, it is set via keys. The contents of the array are
    # a set of key, value tuples.
    #
    # If you wish to replace a file
    # completely with the contents of a hash, convert it directly to a json
    # string first and supply the string as the data parameter.
    def configure(data, options={})
      if data.is_a? Hash
        formedData = []

        def gatherData(key, value, formedData)
          case value
          when Hash
            value.each do |k, v|
              newKey = key.dup
              if !newKey.empty?
                newKey << "."
              end
              newKey << k.gsub("\\", "\\\\").gsub(".", "\\.").gsub("[", "\\[").gsub("]", "\\]")

              gatherData(newKey, v, formedData)
            end
          when Array
            value.each_with_index do |item, index|
              newKey = key.dup
              newKey << "[#{index}]"

              gatherData(newKey, item, formedData)
            end
          else
            formedData << [key, value]
          end
        end

        gatherData("", data, formedData)

        return self.configure(formedData, options)
      end

      cmdOptions = {}
      if @account
        cmdOptions["-T"] = @account.token
      end
      cmdOptions["-j"] = true

      stdinData = nil

      if data.is_a? Array
        data.each do |k, v|
          cmdOptions["-i"] ||= []
          cmdOptions["-i"] << [k, v]
        end
      else
        cmdOptions["-"] = true
        stdinData = data
      end

      if options[:message]
        cmdOptions["-m"] = options[:message]
      end

      result = Occam::Worker.perform("configurations", "set", [self.fullID], cmdOptions, stdinData)
      data = JSON.parse(result[:data], :symbolize_names => true)

      root = nil
      ret = nil
      if data[:updated]
        root = data[:updated][0]
        index = data[:updated][1..-1].map do |item|
          item[:position]
        end

        ret = self.class.new(:id => root[:id], :revision => root[:revision], :index => index, :account => @account, :link => @link, :roots => data[:updated][0..-2], :root => root, :info => data[:updated][-1])
      end

      ret
    rescue Exception => e
      puts e
      nil
    end
  end
end
