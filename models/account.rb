# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Account
    # Represents an Account in the OCCAM system.

    attr_reader :token
    attr_reader :person
    attr_reader :username
    attr_reader :roles

    def initialize(options)
      @token  = options[:token]
      @person = options[:person]
      if @person.nil? && options[:person_id]
        @person = Occam::Person.new(:id => options[:person_id], :account => self)
      end

      if @person && @person.status && @person.status[:account]
        @username = @person.status[:account][:username] || "unknown"
        @roles  = @person.status[:account][:roles] || []
      else
        @username = ""
        @roles = []
      end
    end

    def hasRole?(role)
      (@roles || []).include?(role)
    end

    def exists?
      !@person.nil? && @person.exists? && !@person.status.nil?
    end

    def trust?(object)
    end

    def bookmarked?(object)
    end

    def self.generateToken(username, password)
      # Authenticates using the given username and password.
      #
      # Returns a token, roles, and associated Person information
      #
      # Occam::Account.generateToken("foo", "foobar")
      # => {
      #      :token => "899a10cb-bcd8-41a0-b835-b53a3a16b0f2:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwZXJzb25faWQiOjg4MywicGVyc29uIjoiODk5YTEwY2ItYmNkOC00MWEwLWI4MzUtYjUzYTNhMTZiMGYyIn0.78oHmB5hkGGv65K8i6wwDB6pXKAN2S8I1jjHLSwiPix",
      #      :roles => ["administrator", "experimentalist"],
      #      :person => {
      #        :id => "899a10cb-bcd8-41a0-b835-b53a3a16b0f2",
      #        :name => "foo"
      #      }
      #    }

      arguments  = [username]
      cmdOptions = {
        "-p" => password,
        "-t" => true
      }
      result = Occam::Worker.perform("accounts", "login", arguments, cmdOptions)

      if result[:code] != 0
        return nil
      end

      JSON.parse(result[:data], :symbolize_names => true)
    end

    def self.authenticate(token)
      # Returns True when the given person_id and token authenticate.
      #
      # Occam::Account.authenticate("899a10cb-bcd8-41a0-b835-b53a3a16b0f2:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwZXJzb25faWQiOjg4MywicGVyc29uIjoiODk5YTEwY2ItYmNkOC00MWEwLWI4MzUtYjUzYTNhMTZiMGYyIn0.78oHmB5hkGGv65K8i6wwDB6pXKAN2S8I1jjHLSwiPix")
      # => true
      #
      # Occam::Account.authenticate("bogus token")
      # => false
      arguments  = [token]
      cmdOptions = {
        "-a" => true
      }
      result = Occam::Worker.perform("accounts", "login", arguments, cmdOptions)

      # This authenticates as long as the exit code for the command is 0
      result[:code] == 0
    end

    def self.create(username, password)
      # Creates a new account with the given username and password.
      #
      # Returns the authentication token for the new account.

      arguments  = [username]
      cmdOptions = {
        "-p" => password
      }
      result = Occam::Worker.perform("accounts", "new", arguments, cmdOptions)

      if result[:code] != 0
        return nil
      end

      authorization = Occam::Account.generateToken(username, password)
      Occam::Account.new(:token     => authorization[:token],
                         :person_id => result[:data].strip.split("\n")[-1])
    end
  end
end
