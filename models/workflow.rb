# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'run'
class Occam
  require_relative 'object'

  # This wraps functionality for a Workflow, which is a particular type of
  # Object.
  class Workflow < Occam::Object
    RUN_INFO_CACHE_TIME = 2.0 # Time in seconds to keep run status information

    def initialize(options)
      @last = nil
      @runInfo = nil

      super(options)
    end

    def self.createIn(object, options = {})
      cmdOptions = {}

      if options[:account]
        cmdOptions["-T"] = options[:account].token
      end

      cmdOptions["--to"] = object.fullID
      cmdOptions["--internal"] = true
      cmdOptions["-j"] = true

      result = Occam::Worker.perform("workflows", "new", [options[:name]], cmdOptions)
      data = JSON.parse(result[:data], :symbolize_names => true)

      root = nil
      ret = nil

      if data[:updated]
        root = data[:updated][0]
        index = data[:updated][1..-1].map do |item|
          item[:position]
        end

        ret = Occam::Object.new(:id => root[:id], :revision => root[:revision], :index => index, :account => @account, :link => @link)
      end

      ret
    end

    def self.create(options = {})
      super.create(:type => "workflow", :name => "Main")
    end

    # Returns the connections array
    def connections
      self.data()[:connections]
    end

    # Returns tail connections, which are nodes without inputs
    def tail
      self.connections.select do |connection|
        not (connection[:inputs] || []).any?
      end
    end

    # Returns the node information for the given node index
    def connectionAt(nodeIndex)
      puts self.data
      self.data[:connections][nodeIndex]
    end

    # Gathers all node data attached at the given node and wire
    def inputsAt(nodeIndex, wireIndex)
      (self.connectionAt(nodeIndex)[:inputs][wireIndex][:connections] || []).map do |wire|
        self.connectionAt(wire[:to][0])
      end
    rescue
      []
    end

    # Gathers all node data attached at the given node and wire
    def outputsAt(nodeIndex, wireIndex)
      (self.connectionAt(nodeIndex)[:outputs][wireIndex][:connections] || []).map do |wire|
        self.connectionAt(wire[:to][0])
      end
    rescue
      []
    end

    # Adds the given object to the workflow as a node and returns the node index
    def append(object)
      # TODO: synch this
      node = {
        :id       => object.id,
        :revision => object.revision,
        :name     => object.info[:name],
        :type     => object.info[:type],
        :inputs   => (object.info[:inputs]  || []).map{|_| {:connections => []}},
        :outputs  => (object.info[:outputs] || []).map{|_| {:connections => []}},
      }

      if object.parent.id == self.id
        node[:reference] = {:index => object.index[-1]}
      end

      self.data()[:connections] << node

      self.data()[:connections].length - 1
    end

    def connect(nodeIndexA, wireIndexA, nodeIndexB, wireIndexB, options = {})
      # TODO: synch
      nodeA = self.connectionAt(nodeIndexA)
      nodeB = self.connectionAt(nodeIndexB)

      nodeA[:outputs] ||= []
      nodeB[:inputs]  ||= []

      if wireIndexA == -1
        nodeA[:self] ||= {:connections => [],
                          :name        => "self",
                          :type        => nodeA[:type]}
        wireA = nodeA[:self]
      else
        wireA = nodeA[:outputs][wireIndexA]
      end

      wireA[:connections] ||= []
      wireB = nodeB[:inputs][wireIndexB]
      wireB[:connections] ||= []

      wireA[:connections] << {:to => [nodeIndexB, wireIndexB, 0]}
      wireB[:connections] << {:to => [nodeIndexA, wireIndexA, 0]}

      wireA = wireA.update(options)
      wireB = wireB.update(options)

      self.data
    end

    # Commits the new data to the workflow and returns an instance to that updated workflow.
    def save
      self.set(self.data.to_json, self.info[:file], :message => "Updating workflow.")
    end

    # Takes node information and returns the Occam::Object represented by it.
    def resolveNode(node)
      if node[:reference]
        self.contents[node[:reference][:index]]
      else
        Occam::Object.new(:id       => node[:id],
                          :revision => node[:revision],
                          :account  => @account)
      end
    end

    # Creates a configuration for the given node and input
    def createConfigurationFor(nodeIndex, wireIndex)
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end
      cmdOptions["--to"] = self.fullID
      cmdOptions["-j"] = true

      obj = self.resolveNode(self.connectionAt(nodeIndex))
      arguments  = [obj.fullID, wireIndex.to_s]

      puts "CREATING CONFIGURATION"

      result = Occam::Worker.perform("configurations", "new", arguments, cmdOptions)
      data = JSON.parse(result[:data], :symbolize_names => true)

      puts "CONFIGURATION DATA"
      puts(data)

      root = nil
      ret = nil
      if data[:updated]
        root = data[:updated][0]
        index = data[:updated][1..-1].map do |item|
          item[:position]
        end

        ret = Occam::Object.new(:id => root[:id], :revision => root[:revision], :index => index, :account => @account, :link => @link, :roots => data[:updated][0..-2])
      end

      ret
    end

    # For forms, we encode the keys as urlsafe_base64 so that they can serve
    # as ids. Our validation library optimistically requires them. We will need
    # to decode them before we can store them.
    def self.decode_base64(hash)
      hash ||= {}

      if hash.is_a? Hash
        hash.keys.each do |k|
          original_key = k
          v = hash[k]

          # Decode base 64

          # Add padding back
          if (k.length % 4) > 0
            k = k + ("=" * (4 - (k.length % 4)))
          end

          k = Base64.urlsafe_decode64(k)
          hash[k] = v
          hash.delete original_key

          if v.is_a? Hash
            self.decode_base64(v)
          end
        end
      end

      hash
    end

    # Returns a list of runs
    def runs
      arguments  = [self.fullID]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("workflows", "list", arguments, cmdOptions)
      ret=[]
      JSON.parse(result[:data], :symbolize_names => true)[:runs].each do |run|
        ret << Occam::Run.new(:id=>result[:id], :info=>run)
      end
      ret
    end

    # Returns the workflow status for the given queued run.
    def runInfo(options = {})
      if @last.nil? || (Time.now - @last) > RUN_INFO_CACHE_TIME
        arguments  = [self.fullID, options[:runID]]
        cmdOptions = {}

        if @account
          cmdOptions["-T"] = @account.token
        end

        result = Occam::Worker.perform("workflows", "status", arguments, cmdOptions)
        @runInfo = JSON.parse(result[:data], :symbolize_names => true)

        @last = Time.now
      end

      @runInfo || {}
    end

    def queue
      arguments  = [self.fullID]
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = @account.token
      end

      # Ok, the result should be any other tasks that must be performed...
      result = Occam::Worker.perform("workflows", "queue", arguments, cmdOptions)
      data = JSON.parse(result[:data], :symbolize_names => true)
      data[:run]
    end

    # Returns each Occam::Job for the given node.
    def nodeStatus(options = {})
      status = self.runInfo(options[:runID])

      if status[:nodes].has_key? options[:nodeIndex]
        status[:nodes][options[:nodeIndex]][:jobs].map do |jobInfo|
          Occam::Job.new(:id => jobInfo[:id], :account => @account)
        end
      end
    end
  end
end
