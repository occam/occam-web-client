# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This class represents license metadata.
  #
  # This is something that is represented within the Occam::Object metadata.
  class License
    # The name of the license
    attr_reader :name

    # The license contents
    attr_reader :text

    # The filename for this license
    attr_reader :file

    # The full metadata block
    attr_reader :info

    # The index of the license within the object
    attr_reader :index

    def initialize(options)
      @info = options

      @name  = @info[:name]
      @text  = @info[:text]
      @file  = @info[:file]
      @index = @info[:index]

      @info.delete(:index)
    end
  end
end
