class Occam
  class System
    class Storage
      # The id the refers to this storage entry
      attr_reader :id

      # The name of the storage backend
      attr_reader :backend

      # The account information currently stored in this entry
      attr_reader :accountInfo

      def self.list
        # Grab list of storage plugin names
        if !defined?(@@list)
          @@list = {}
          arguments = []
          cmdOptions = {
            "-j" => true
          }
          result = Occam::Worker.perform("storage", "list", arguments, cmdOptions)
          begin
            ret = JSON.parse(result[:data], :symbolize_names => true)
          rescue
            ret = {}
          end

          ret.each do |k,v|
            @@list[k] = Occam::System::Storage.new(v)
          end
        end

        @@list
      end

      def self.iconURLFor(name, options = {})
        # Detect if the icon exists for this type
        publicPath = File.join("public", "images", "icons", "storage", name)

        basePath = "/images/dynamic"
        if options[:color]
          basePath = basePath + "/color/#{options[:color]}"
        elsif options[:hue]
          basePath = basePath + "/hue/#{options[:hue]}/sat/#{options[:sat]}/light/#{options[:light]}"
        elsif options[:hex]
          basePath = basePath + "/hex/#{options[:hex]}"
        else
          basePath = "/images"
        end

        if File.exist?(publicPath + ".svg")
          "#{basePath}/icons/storage/#{name}.svg"
        elsif File.exist?(publicPath + ".png")
          "#{basePath}/icons/storage/#{name}.png"
        else
          nil
        end
      end

      def self.imageURLFor(name, options = {})
        # Detect if the icon exists for this type
        publicPath = File.join("public", "images", "storage", name)

        basePath = "/images/dynamic"
        if options[:color]
          basePath = basePath + "/color/#{options[:color]}"
        elsif options[:hue]
          basePath = basePath + "/hue/#{options[:hue]}/sat/#{options[:sat]}/light/#{options[:light]}"
        elsif options[:hex]
          basePath = basePath + "/hex/#{options[:hex]}"
        else
          basePath = "/images"
        end

        if File.exist?(publicPath + ".svg")
          "#{basePath}/storage/#{name}.svg"
        elsif File.exist?(publicPath + ".png")
          "#{basePath}/storage/#{name}.png"
        else
          nil
        end
      end

      def initialize(options = {})
        @backend     = options[:backend] || "unknown"
        @accountInfo = options[:accountInfo] || {}
        @id          = options[:id]
        @account     = options[:account]
      end

      def self.info(options = {})
        b = self.list[options[:backend].intern]
        if b
          o = options.dup
          o.update({:accountInfo => b.accountInfo})
          Occam::System::Storage.new(o)
        else
          nil
        end
      end

      # Yields a list of Storage instances for the given backend and account.
      def self.view(backend, account, id=nil)
        arguments = [backend, id].compact
        cmdOptions = {
          "-j" => true,
        }

        if account
          cmdOptions["-T"] = account.token
        end

        result = Occam::Worker.perform("storage", "view", arguments, cmdOptions)
        begin
          ret = JSON.parse(result[:data], :symbolize_names => true)
        rescue
          ret = []
        end

        ret.map do |info|
          info[:account] = account
          Occam::System::Storage.new(info)
        end
      end

      # Creates a new entry for the given Account
      def self.create(backend, account)
        arguments = [backend]
        cmdOptions = {
          "-j" => true,
          "-T" => account.token
        }

        result = Occam::Worker.perform("storage", "set", arguments, cmdOptions)
        begin
          ret = JSON.parse(result[:data], :symbolize_names => true)
        rescue
          # TODO: error?
          ret = {}
        end

        Occam::System::Storage.new(ret)
      end

      # Publishes the given object to this storage backend
      def push(obj)
        arguments = [obj.fullID, self.backend, self.id].compact
        cmdOptions = {
        }

        if @account
          cmdOptions["-T"] = @account.token
        end

        result = Occam::Worker.perform("storage", "push", arguments, cmdOptions)
        begin
          ret = JSON.parse(result[:data], :symbolize_names => true)
        rescue
          ret = {}
        end
      end

      # Updates the token for the given storage backend entry
      def set(info)
        arguments = [self.backend, self.id]
        cmdOptions = {
          "-j" => true
        }

        if @account
          cmdOptions["-T"] = @account.token
        end

        cmdOptions["-i"] = []
        info.each do |k,v|
          cmdOptions["-i"] << [k, v]
        end

        result = Occam::Worker.perform("storage", "set", arguments, cmdOptions)
        begin
          ret = JSON.parse(result[:data], :symbolize_names => true)
        rescue
          ret = {}
        end

        Occam::System::Storage.new(ret)
      end

      # Uninstantiates this particular storage backend access
      def destroy!
        arguments = [self.backend, self.id]
        cmdOptions = {
          "-j" => true
        }

        if @account
          cmdOptions["-T"] = @account.token
        end

        Occam::Worker.perform("storage", "set", arguments, cmdOptions)

        true
      end

      # Retrieves the URL for the icon representation of this storage backend.
      def iconURL
        Occam::System::Storage.iconURLFor(self.backend)
      end

      # Retrieves the full image or logo representing this storage backend.
      def imageURL
        Occam::System::Storage.imageURLFor(self.backend)
      end
    end
  end
end
