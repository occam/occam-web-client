class Occam
  class System
    class Theme
      include Occam::HashHelpers

      attr_reader :config
      attr_reader :name

      # Retrieves a list of themes
      def self.list
        # Grab list of theme names
        if !defined?(@@list)
          @@list = {}
          Dir[File.join("config", "themes", "**", "*.yml")].each do |path|
            path = File.basename(path, ".yml")
            if not @@list.has_key?(path)
              @@list[path] = Occam::System::Theme.new(path)
            end
          end
        end

        @@list
      end

      # Loads the theme for the given name
      def initialize(name)
        file_path = File.join("config", "themes", name + ".yml")
        @config = deep_symbolize_keys(YAML.load_file(file_path) || {})
        @name = @config[:name] || "Unnamed"
      end

      # Returns the generated scss for this theme
      def scss
        "/* Contains color scheme */\n" +
        self.config.map do |k1, v1|
          if v1.is_a? Hash
            v1.map do |k2, v2|
              if v2.is_a? Hash
                v2.map do |k3, v3|
                  "$#{k1}-#{k2}-#{k3}: #{v3};"
                end.join("\n")
              else
                ""
              end
            end.join("\n")
          else
            ""
          end
        end.join("\n")
      end

      # Gives the structure of the theme editor and the possible theme options.
      def selectors
        key_path = File.join("config", "theme.yml")
        ret = (YAML.load_file(key_path) || {})
        deep_deep_symbolize_keys(ret)
      end

      # Yields a list of rows for the given section of the theme.
      #
      # This is used to render the theme configuration table.
      def selectorList(key, subkey)
        self.selectors[key][subkey].map do |tag, value|
          if value.is_a? Hash
            [{tag => 1}, value.map do |a,b|
              if b.is_a? Hash
                [{a => 2}, b.map{|x,y| {x => y}}].flatten
              else
                {a => b}
              end
            end]
          else
            {tag => value}
          end
        end.flatten
      end
    end
  end
end
