# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Task

    attr_reader :info

    def initialize(options)
      cmdOptions = {
      }

      if options[:account]
        cmdOptions["-T"] = options[:account].token
      end

      if options[:token]
        cmdOptions["-T"] = options[:token]
      end

      if options[:backend]
        cmdOptions["--target-backend"] = options[:backend]
      end

      if options[:environment]
        cmdOptions["--environment"] = options[:environment]
      end

      if options[:architecture]
        cmdOptions["--architecture"] = options[:architecture]
      end

      if options[:targetEnvironment]
        cmdOptions["--target-environment"] = options[:targetEnvironment]
      end

      if options[:targetArchitecture]
        cmdOptions["--target-architecture"] = options[:targetArchitecture]
      end

      if options[:inputs]
        options[:inputs].each do |input|
          cmdOptions["--input"] ||= []
          cmdOptions["--input"] << [input]
        end
      end

      if options[:running]
        options[:running].each do |input|
          cmdOptions["--running"] ||= []
          cmdOptions["--running"] << [input]
        end
      end

      result = Occam::Worker.perform("manifests", "run", [options[:object].fullID], cmdOptions)

      @info = JSON.parse(result[:data], :symbolize_names => true)
    end
  end
end
