# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  require_relative 'object'

  # This represents a Resource Object.
  #
  # These types of objects do not have a linear, versioned history. Normal
  # Objects in the system are repository-backed (git) and capture a tree-based
  # representation of their history.
  #
  # In a Resource Object, this is generally large file information or data
  # that is externally sourced. In these cases, the system brings in this
  # information for storage and can only hope to tag its lineage by hashing
  # the content, which takes place of the normal 'revision' of the Object.
  class Resource < Occam::Object
    def ownerInfo
      self.info
    end

    def info
      self.status
    end

    def links(*args)
      []
    end

    def linksTo(*args)
      []
    end

    def retrieveHistory
      # TODO: this might be a list of known hashes/changes
      # Conserve the result (it won't change)
      if !defined?(@commits)
        arguments  = [self.fullID(path)]
        cmdOptions = {
          "-j" => true,
        }

        if @account
          cmdOptions["-T"] = @account.token
        end

        result = Occam::Worker.perform("resources", "history", arguments, cmdOptions)
        @commits = JSON.parse(result[:data], :symbolize_names => true).map do |commit|
          begin
            commit[:date] = DateTime.iso8601(commit[:date])
          rescue
          end
          commit
        end
      end

      @commits
    end

    # Retrieves a file from the resource store from the given path.
    def retrieveFileStat(path)
      arguments  = [self.fullID(path)]
      cmdOptions = {
        "-j" => true,
      }

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("resources", "status", arguments, cmdOptions)

      JSON.parse(result[:data], :symbolize_names => true)
    rescue
      nil
    end

    # Retrieves a file from the resource store from the given path.
    def retrieveFile(path, options={})
      arguments  = [self.fullID(path)]
      cmdOptions = {
      }

      if @account
        cmdOptions["-T"] = @account.token
      end

      if options[:compress]
        cmdOptions["-c"] = options[:compress]
      end

      result = Occam::Worker.perform("resources", "view", arguments, cmdOptions, nil, options[:stream])

      if options[:stream]
        result
      else
        result[:data]
      end
    end

    # Lists the given directory from the resource store.
    def retrieveDirectory(path)
      arguments  = [self.fullID(path)]
      cmdOptions = {
        "-l" => true,
        "-j" => true,
      }

      if @account
        cmdOptions["-T"] = @account.token
      end

      result = Occam::Worker.perform("resources", "list", arguments, cmdOptions)
      JSON.parse(result[:data], :symbolize_names => true)
    rescue
      []
    end
  end
end
