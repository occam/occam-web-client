# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Identity
    attr_reader :uri

    def initialize(uri, options = {})
      @uri = uri
    end

    # Returns the public key for this Identity.
    #
    # This key is used to verify verification keys.
    def publicKey(options = {})
      arguments = [self.uri]
      cmdOptions = {
        "-f" => options[:format] || "PEM",
      }

      if options[:verifyKey]
        cmdOptions["-v"] = true
      end

      result = Occam::Worker.perform("keys", "view", arguments, cmdOptions)
      result[:data]
    end

    # Returns the public verification key for this Person.
    #
    # This key is used to verify signatures.
    def verifyKey(options = {})
      self.publicKey(options.update(:verifyKey => true))
    end

    def info
      arguments = [self.uri]

      cmdOptions = {
        "-a" => true
      }

      result = Occam::Worker.perform("keys", "view", arguments, cmdOptions)
      JSON.parse(result[:data], :symbolize_names => true)
    end
  end
end
