class Occam
  class TestGroup
    attr_reader :tests

    # The different name fields based on the group type
    attr_reader :name
    attr_reader :group
    attr_reader :class
    attr_reader :method
    attr_reader :function
    attr_reader :feature

    attr_reader :describes
    attr_reader :features

    def initialize(options)
      @name     = options[:name]
      @group    = options[:group]
      @method   = options[:method]
      @function = options[:function]
      @feature  = options[:feature]
      @class    = options[:class]
      @type     = options[:type]

      # Describe/It style group
      @describes = {}
      if options[:describes]
        options[:describes].each do |key, group|
          @describes[key] = Occam::TestGroup.new(group)
        end
      end

      # Feature/Scenario style group
      @features = {}
      if options[:features]
        options[:features].each do |key, group|
          @features[key] = Occam::TestGroup.new(group)
        end
      end

      @tests = (options[:tests] || []).map do |test|
        Occam::Test.new(test)
      end
    end

    def type
      if @type
        @type.intern
      elsif self.method
        :method
      elsif self.function
        :function
      elsif self.class
        :class
      elsif self.feature
        :feature
      else
        :group
      end
    end

    def header
      self.method || self.function || self.class || self.feature || self.group || self.name
    end

    def items
      self.tests || self.describes || self.features
    end

    def total
      if !defined?(@total)
        total = (self.tests || []).length

        (self.describes || {}).each do |key, group|
          total += group.total
        end
        (self.features || {}).each do |key, group|
          total += group.total
        end

        @total = total
      end

      @total
    end

    def statistics
      if !defined?(@statistics)
        @statistics = {
          :passes   => 0,
          :errors   => 0,
          :failures => 0,
          :skips    => 0,
          :total    => self.total
        }

        (self.tests || []).each do |test|
          if test.type.intern == :passed
            @statistics[:passes]   += 1
          elsif test.type.intern == :skipped
            @statistics[:skips]    += 1
          elsif test.type.intern == :failed
            @statistics[:failures] += 1
          elsif test.type.intern == :error
            @statistics[:errors]   += 1
          end
        end

        (self.describes || {}).each do |key, group|
          @statistics[:passes]   += group.statistics[:passes]
          @statistics[:failures] += group.statistics[:failures]
          @statistics[:errors]   += group.statistics[:errors]
          @statistics[:skips]    += group.statistics[:skips]
        end

        (self.features || {}).each do |key, group|
          @statistics[:passes]   += group.statistics[:passes]
          @statistics[:failures] += group.statistics[:failures]
          @statistics[:errors]   += group.statistics[:errors]
          @statistics[:skips]    += group.statistics[:skips]
        end
      end

      @statistics
    end
  end
end
