class Occam
  class Build
    # The task object's ID
    attr_reader :id

    # The task object's revision
    attr_reader :revision

    # The known metadata for the build
    attr_reader :info

    # The Occam::Object that this is a build of
    attr_reader :object

    # The Occam::Object representing the build task
    attr_reader :task

    def initialize(options = {})
      @id = options[:id]
      @revision = options[:revision]
      @object = options[:object]
      @task = Occam::Object.new(:id => @id, :revision => @revision, @account => @object.account, :info => options[:info])
      @info = options[:info]
    end

    # Retrieve the build log (the console output for that build)
    def log(options = {})
      arguments  = [self.object.fullID, self.task.fullID]
      cmdOptions = {}

      if self.object.account
        cmdOptions["-T"] = self.object.account.token
      end

      if self.object.tokens && self.object.tokens.any?
        cmdOptions["-T"] = self.object.tokens[0]
      end

      result = Occam::Worker.perform("builds", "view", arguments, cmdOptions)
      result[:data]
    end
  end
end
