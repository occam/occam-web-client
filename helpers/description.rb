# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  module DescriptionHelpers
    # These helper methods are for rendering the description sections.

    def render_description(markdown, object, options = {})
      engine = ::Redcarpet::Markdown.new(Redcarpet::Render::HTML, options)
      result = engine.render(markdown)

      # Get the proper subpath
      subpath = "files"
      if options[:path]
        subpath = File.join(subpath, options[:path])
      end

      # Update links in result
      result.gsub!(/<a\s+href\s*=\s*"([^:"]+)"/, "<a href=\"#{object.url(:path => subpath)}/\\1\"")

      subpath = "raw"
      if options[:path]
        subpath = File.join(subpath, options[:path])
      end
      result.gsub!(/<img\s+src\s*=\s*"([^:"]+)"/, "<img src=\"#{object.url(:path => subpath)}/\\1\"")
      result
    end
  end

  helpers DescriptionHelpers
end
