# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This module implements encrypted_token, cookie_encrypter, set_token, and get_token
  module DateHelpers
    def renderDuration(datetime, endTime = nil)
      if endTime.nil?
        endTime = Time.now.utc.to_datetime
      end

      if endTime.is_a? String
        endTime = DateTime.iso8601(endTime)
      end

      if datetime.is_a? String
        datetime = DateTime.iso8601(datetime)
      end

      # Get the number of days difference
      diff = endTime - datetime

      # Convert to seconds
      diff = diff * 24 * 60 * 60

      ago = true
      if diff < 0
        diff = -diff
        ago = false
      end

      diff = diff.to_i

      if diff == 0
        "just now"
      else
        duration = ChronicDuration.output(diff, :weeks => true, :units => 1)
        duration = duration + (ago ? " ago" : " in the future")
      end
    end

    def renderDateTime(datetime, options={})
      if datetime.is_a? String
        begin
          datetime = DateTime.iso8601(datetime)
        rescue
          return datetime
        end
      end

      I18n.l(datetime, options)
    end

    def renderDate(datetime, options={})
      if datetime.is_a? String
        begin
          datetime = DateTime.iso8601(datetime)
        rescue
          return datetime
        end
      end

      I18n.l(datetime.to_date, options)
    end
  end

  helpers DateHelpers
end
