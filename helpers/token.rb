# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This module implements encrypted_token, cookie_encrypter, set_token, and get_token
  module TokenHelpers
    def cookie_encrypter
      Rack::Session::EncryptedCookie::Encryptor.new(Occam::Config.configuration['secret'])
    end

    def encrypt_token
      if current_account
        cookie_encrypter.encrypt({:token => current_account.token, :person_id => current_person.id}.to_json)
      else
        cookie_encrypter.encrypt("{}")
      end
    end

    def decrypt_token(token)
      JSON.parse(cookie_encrypter.decrypt(token), :symbolize_names => true)
    rescue
      {}
    end
  end

  helpers TokenHelpers
end
