# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  module SVGHelpers
    # Returns safe HTML to embed an SVG into a page
    #
    # Taken from https://coderwall.com/p/d1vplg/embedding-and-styling-inline-svg-documents-with-css-in-rails
    # Thanks James Martin! I've added width/height setting.
    def embed_svg(path, options={})
      require 'nokogiri'
      url = path
      if path.start_with?("/")
        path = path[1..-1]
      end
      path = File.join("public", path)
      if path.end_with?("png")
        # Abort
        return "<img src='#{url}'>"
      end
      doc = Nokogiri::HTML::DocumentFragment.parse File.read(path)
      svg = doc.at_css 'svg'
      if options[:class]
        svg['class'] = options[:class]
      end
      if options[:width]
        svg['width'] = options[:width]
      end
      if options[:height]
        svg['height'] = options[:height]
      end
      doc.to_html
    end
  end

  helpers SVGHelpers
end
