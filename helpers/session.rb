# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This module implements current_account and current_person
  # These can be used anywhere to talk about the logged in Person or Account
  module SessionHelpers
    # Retrieves the current Occam::Account
    def current_account
      if !defined?(@@current_account)
        @@current_account = nil
      end

      # Do not accept any session data from third-party or sandboxed content
      origin = (request["Origin"] || request.env["HTTP_ORIGIN"] || request.base_url)
      if origin != request.base_url
        request.session_options[:skip] = true
        return nil
      end

      if request.env["HTTP_X_OCCAM_TOKEN"]
        token = decrypt_token(request.env["HTTP_X_OCCAM_TOKEN"])
        session[:token] = token[:token]
        session[:person_id] = token[:person_id]
      end

      if session[:token]
        if !(@@current_account && @@current_account.token == session[:token])
          @@current_account = Occam::Account.new(session)
          if not @@current_account.exists?
            @@current_account = nil
            logout
          end
        end
      else
        @@current_account = nil
      end

      @@current_account
    end

    # Retrieves the logged in Occam::Person
    def current_person
      if current_account
        current_account.person
      else
        nil
      end
    end

    # Returns true when an account is logged in
    def logged_in?
      !!current_account
    end

    # Logs the account out
    def logout
      session[:token]  = nil
      session[:roles]  = nil
      session[:person_id] = nil
    end

    # Logs in with the given credentials
    def login(username, password)
      authorization = Occam::Account.generateToken(username, password)

      if authorization.nil?
        return nil
      end

      session[:token]       = authorization[:token]
      session[:roles]       = authorization[:roles]
      session[:person_id] = authorization[:person][:id]

      current_account
    end
  end

  helpers SessionHelpers
end
