# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  module LocalRequestHelpers
    def local_request?
      require 'ipaddr'
      request.host == "localhost" || Socket.ip_address_list.map(&:ip_address).select{|info| IPAddr.new(info) == IPAddr.new(request.ip || "0.0.0.0")}.any?
    rescue
      false
    end
  end

  helpers LocalRequestHelpers
end
