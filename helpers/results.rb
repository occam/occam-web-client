# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  module ResultsHelpers
    def render_data(hash, schema, id = "data")
      require 'cgi'
      require 'base64'

      if hash.is_a? Hash
        keys = hash.keys
        keys_array = keys.select{|k| hash[k].is_a?(Array) }
        keys_else  = keys.select{|k| !hash[k].is_a?(Array) }

        "<ul class='hash'>" + keys_else.sort.map{|k|
          if k
            base64_key = Base64.urlsafe_encode64(k.to_s)
          end

          v = hash[k]
          inner_schema = schema
          inner_schema = schema[k] if schema.has_key?(k)
          if inner_schema.is_a?(Hash) && inner_schema.has_key?(:units)
            units = inner_schema[:units]
          end

          "<li data-id='#{id}-#{base64_key}' data-key='#{base64_key}'>" +
          "<span class='key' data-key='#{base64_key}'>#{CGI.escapeHTML(k.to_s)}</span>" +
          "<span class='value'#{" data-units='#{units}'" if units}>#{render_data(v, inner_schema, "#{id}-#{base64_key}")}</span> <span class='units'>#{units}</span></li>"
        }.join("") + keys_array.sort.map{ |k|
          if k
            base64_key = Base64.urlsafe_encode64(k.to_s)
          end

          v = hash[k]
          inner_schema = schema
          inner_schema = schema[k] if schema.has_key?(k)

          "<li data-id='#{id}-#{base64_key}' data-key='#{base64_key}'>" +
          "<span class='expand shown'>&#9662;</span>" +
          "<span class='key' data-key='#{base64_key}'>#{CGI.escapeHTML(k.to_s)}</span>" +
          "<span class='value'>#{render_data(v, inner_schema, "#{id}-#{base64_key}")}</span></li>"
        }.join("") + "</ul>"
      elsif hash.is_a? Array
        if schema.is_a? Array
          schema = schema.first
        end
        "<ul class='array'>" + hash.each_with_index.map { |e, i|
          "<li class='element' data-id='#{id}-#{i}' data-key='#{i}'>" +
          "<span class='expand shown'>&#9662;</span>" +
          "#{render_data(e, schema, "#{id}-#{i}")}</li>"
        }.join("") + "</ul>"
      else
        return hash.to_s
      end
    end
  end

  helpers ResultsHelpers
end
