# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'silence_warnings'

silence_warnings do
  require 'rbconfig'
  if (RbConfig::CONFIG['host_os'] =~ /mswin|mingw|cygwin/)
    require 'em/pure_ruby'
  end

  require 'bundler'
  Bundler::require

  require_relative '../config/environment'
  require_relative './websocket'

  require 'sinatra/reloader'
  require 'sinatra/cookies'
end

# Application root
class Occam < Sinatra::Base
  # Establish the environment
  set :environment, (ENV["RACK_ENV"] || "development").intern

  # Use root directory as root
  set :app_file => '.'
  set :server, :puma

  # Use HTML5
  set :haml, :format => :html5

  # Asset Management
  set :assets, Sprockets::Environment.new
  configure :production do
    assets.js_compressor = Uglifier.new(:harmony => true)
  end
  assets.append_path "public/js"

  get "/assets/js/*" do
    env["PATH_INFO"].sub!("/assets/js", "")
    settings.assets.call(env)
  end

  # Markdown
  Tilt.register Tilt::RedcarpetTemplate, 'md'
  Tilt.prefer   Tilt::RedcarpetTemplate
  set :markdown, :layout_engine => :haml,
                 :layout        => :markdown

  # SSL
  set :ssl_certificate, File.join(File.dirname(__FILE__), "..", "key", "server.crt")
  set :ssl_key        , File.join(File.dirname(__FILE__), "..", "key", "server.key")

  # Use sessions
  use Rack::Session::EncryptedCookie, :key    => 'rack.session',
                                      :expire_after => 2592000,
                                      :time_to_live => Occam::Config.configuration['time-to-live'],
                                      :secret => Occam::Config.configuration['secret']

  # I18n
  I18n.load_path += Dir[File.join(File.dirname(__FILE__), "..", 'config', 'locales', '*.yml')]
  I18n.load_path += Dir[File.join(Gem::Specification.find_by_name('rails-i18n').gem_dir, 'rails', 'locale', '*.yml')]

  # SASS
  require 'sass/plugin/rack'
  Sass::Plugin.options[:template_location] = "./views/stylesheets"
  Sass::Plugin.options[:css_location]      = "./public/stylesheets"
  use Sass::Plugin::Rack

  use Rack::MethodOverride
  use Occam::WebSocketBackend

  class RequestLogger
    def initialize(app)
      @app = app
    end

    def call(env)
      request = Rack::Request.new(env)
      if not request.path.end_with?("runs/running")
        puts "-------------------------"
        puts request.path
      end
      status, headers, response = @app.call(env)
      if not request.path.end_with?("runs/running")
        puts "  -> #{status}"
      end
      [status, headers, response]
    end
  end

  # Helpers
  helpers Sinatra::ContentFor
  helpers Sinatra::Cookies

  # Reloader
  configure :development do
    puts "***** In Development Mode *****"
    puts "Using Reloader"

    register Sinatra::Reloader

    %w(helpers models).each do |dir|
      Dir[File.join(File.dirname(__FILE__), "..", dir, '**', '*.rb')].each do |file|
        also_reload file
      end
    end

    puts "*******************************"
  end

  def self.load_tasks
    Dir[File.join(File.dirname(__FILE__), "tasks", '**', '*.rb')].each do |file|
      require file
    end
  end

  helpers do
    def partial(page, options={})
      if page.to_s.include? "/"
        page = page.to_s.sub(/[\/]([^\/]+)$/, "/_\\1")
      else
        page = "_#{page}"
      end
      haml page.to_sym, options.merge!(:layout => false)
    end
  end
end

require_relative "../controllers/root"

# Load application source
%w(helpers models).each do |dir|
  Dir[File.join(File.dirname(__FILE__), "..", dir, '**', '*.rb')].each do |file|
    require_relative file
  end
end

# Runs occam commands
require_relative '../lib/worker'

# The occam daemon backend
require_relative '../lib/daemon'

# The OCCAM server needs an SSL key for replication
require_relative '../lib/https'
Occam::HTTPS.ensureKey

# The OCCAM server should know itself
puts "Looking for path of occam toolchain: "
if system("command -v occam")
  puts "Committing occam-web object"
  `occam commit`
end

# Enable chunked streaming (this is required for some git clients)
require_relative './stream'
