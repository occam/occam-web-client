# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam < Sinatra::Base
  class WebSocketBackend
    require 'json'
    require 'open3'
    require 'tmpdir'
    require 'fileutils'

    KEEPALIVE_TIME = 15 # in seconds

    def initialize(app)
      @app        = app
      @clients    = []
      @terminals  = {}
      @websockets = {}
      @proxys     = {}
    end

    # For 'thin' servers, we need an adapter
    Faye::WebSocket.load_adapter('puma')

    def call(env)
      if Faye::WebSocket.websocket?(env)
        req = Rack::Request.new(env)

        # WebSockets logic goes here
        ws = Faye::WebSocket.new(env, ['binary'], {
          ping: KEEPALIVE_TIME
        })

        ws.on :error do |event|
          puts "Websocket Error"
        end

        ws.on :open do |event|
          @terminals[ws] = {}
          @clients << ws
        end

        if false #Occam::Backend.connected
          # Proxy the websocket to a backend

          # Open Websocket
          backendHost = Occam::Backend.config['host']
          backendPort = Occam::Backend.config['port']

          url = "ws://#{backendHost}:#{backendPort}"
          puts "Opening proxy websocket at #{url}"

          websocket = Faye::WebSocket::Client.new(url, ['binary'], :headers => { "origin" => "localhost" })

          websocket.on :open do |event|
            # When client connects
            puts "Proxy Websocket: Connected"
            @proxys[req] = websocket
          end

          websocket.on :message do |event|
            # Forward received data
            ws.send(event.data)
          end

          websocket.on :error do |event|
            puts "Proxy Websocket: Error"
          end

          websocket.on :close do |event|
            # Server closed connection
            if @proxys[req]
              @proxys[req].close
            end
            @proxys.delete req
            if ws
              ws.close
            end
          end

          ws.on :message do |event|
            # Writes to the backend websocket

            # Talk to open websocket
            websocket = @proxys[req]

            if websocket
              # Respond with data from vnc port
              websocket.send(event.data)
            end
          end
        elsif req.path.start_with?("/vnc/")
          vncPort = req.path[5..-1].to_i

          # Create our own WebSocket to that port locally
          # (or via a backend)

          # Open VNC Websocket
          websocket = Faye::WebSocket::Client.new("ws://localhost:#{vncPort}", ['binary'], :headers => { "origin" => "localhost" })

          websocket.on :open do |event|
            # When client connects
            @websockets[vncPort] = websocket
          end

          websocket.on :message do |event|
            # Forward received data
            begin
              ws.send(event.data)
            rescue
            end
          end

          websocket.on :error do |event|
            puts "SECONDARY WEBSOCKET: video-process-#{vncPort}: Error"
          end

          websocket.on :close do |event|
            # Server closed connection
            puts "Closing VNC connection"
            if @websockets[vncPort]
              @websockets[vncPort].close
            end
            @websockets.delete vncPort
            if ws
              ws.close
            end
          end

          ws.on :message do |event|
            # Writes to the VNC webserver at the requested port

            # Talk to open websocket
            websocket = @websockets[vncPort]

            if websocket
              # Respond with data from vnc port
              websocket.send(event.data)
            end
          end
        else
          ws.on :message do |event|
            begin
              data = JSON.parse(event.data)
            rescue
              data = {}
            end

            tag = data['tag']
            data = data['data']

            request = data["request"]

            case request
            when "vncOpen"
            when "send"
              # Input writes to the open session
              begin
                if @terminals[ws][data["terminal"]][:socket]
                  @terminals[ws][data["terminal"]][:socket].write(data["input"])
                end
              rescue
              end
            when "open"
              if data.has_key? "spawning"
                case data["spawning"]
                when "build-log"
                  rows = data["rows"]
                  cols = data["cols"]
                  object_id       = data["data"]["object_id"]
                  object_revision = data["data"]["object_revision"]
                  build_id       = data["data"]["build_id"]
                  build_revision = data["data"]["build_revision"]
                  account_token   = data["data"]["token"]

                  obj = Occam::Object.new(:id => object_id, :revision => object_revision, :account => Occam::Account.new(:token => account_token))
                  build = Occam::Build.new(:id => build_id, :revision => build_revision, :object => obj)


                  # Create a response thread I guess
                  @terminals[ws][data["terminal"]] = {}
                  @terminals[ws][data["terminal"]][:build]    = build
                  @terminals[ws][data["terminal"]][:thread] = Thread.new do
                    # TODO: stream the build log
                    new = build.log(:from => 0)

                    # Base 64 encode the data
                    new = Base64.encode64(new)

                    # Send it
                    ws.send({
                      :tag => tag,
                      :data => {
                        :response => "data",
                        :base64 => new,
                        :terminal => data["terminal"],
                      }
                    }.to_json)
                  end
                when "build"
                  rows = data["rows"]
                  cols = data["cols"]
                  object_id       = data["data"]["object_id"]
                  object_revision = data["data"]["object_revision"]
                  account_token   = data["data"]["token"]

                  obj = Occam::Object.new(:id => object_id, :revision => object_revision, :account => Occam::Account.new(:token => account_token))

                  job = obj.build

                  # Create a response thread I guess
                  @terminals[ws][data["terminal"]] = {}
                  @terminals[ws][data["terminal"]][:job]    = job
                  @terminals[ws][data["terminal"]][:thread] = Thread.new do
                    position = 0
                    while(job.status != "finished") do
                      new = job.log(:from => position)
                      if new.length > 0
                        ws.send({
                          :tag => tag,
                          :data => {
                            :response => "data",
                            :output   => new,
                            :terminal => data["terminal"],
                          }
                        }.to_json)
                      end
                      position = position + new.length

                      if position > 0 && !job.running?
                        break
                      end
                    end
                  end
                when "events"
                  data["data"] = data["data"] || {}
                  job_id = data["data"]["job_id"]
                  job = Occam::Job.new(:id => job_id)

                  # Create a response thread I guess
                  @terminals[ws][data["terminal"]] = {}
                  @terminals[ws][data["terminal"]][:job]    = job
                  @terminals[ws][data["terminal"]][:thread] = Thread.new do
                    position = 0
                    while(job.status != "finished" && job.status != "failed") do
                      new = job.log(:from => position, :events => true)
                      if new.length > 0
                        ws.send({
                          :tag => tag,
                          :data => {
                            :response => "data",
                            :output   => new,
                            :terminal => data["terminal"],
                          }
                        }.to_json)
                      end
                      position = position + new.length

                      if position > 0 && !job.running?
                        break
                      end
                    end
                  end
                when "connect"
                  data["data"] = data["data"] || {}
                  job_id = data["data"]["job_id"]

                  job = Occam::Job.new(:id => job_id)

                  # Create a response thread I guess
                  @terminals[ws][data["terminal"]] = {}
                  @terminals[ws][data["terminal"]][:job]    = job
                  @terminals[ws][data["terminal"]][:thread] = Thread.new do
                    # Get the socket and read and write to it directly
                    socket = job.connect
                    socket = socket.socket # Get the internal socket (so we can select())

                    @terminals[ws][data["terminal"]][:socket] = socket

                    while true
                      rs, _, _ = IO.select([socket])

                      if rs.include? socket
                        if socket.eof?
                          break
                        end

                        new = socket.read_nonblock(4096)
                        if new.length > 0
                          ws.send({
                            :tag => tag,
                            :data => {
                              :response => "data",
                              :output   => new,
                              :terminal => data["terminal"],
                            }
                          }.to_json)
                        end
                      end
                    end
                  end
                when "logs"
                  data["data"] = data["data"] || {}
                  job_id = data["data"]["job_id"]

                  job = Occam::Job.new(:id => job_id)

                  # Create a response thread I guess
                  @terminals[ws][data["terminal"]] = {}
                  @terminals[ws][data["terminal"]][:job]    = job
                  @terminals[ws][data["terminal"]][:thread] = Thread.new do
                    position = 0

                    loop do
                      new = job.log(:from => position)
                      if new.length > 0
                        ws.send({
                          :tag => tag,
                          :data => {
                            :response => "data",
                            :output   => new,
                            :terminal => data["terminal"],
                          }
                        }.to_json)
                      end
                      position = position + new.length

                      if position > 0 && !job.running?
                        break
                      end
                    end
                  end
                when "run"
                  rows = data["rows"]
                  cols = data["cols"]
                  object_id       = data["data"]["object_id"]
                  object_revision = data["data"]["object_revision"]
                  account_token   = data["data"]["token"]

                  obj = Occam::Object.new(:id => object_id, :revision => object_revision, :account => Occam::Account.new(:token => account_token))

                  job = obj.run

                  # Create a response thread I guess
                  @terminals[ws][data["terminal"]] = {}
                  @terminals[ws][data["terminal"]][:job]    = job
                  @terminals[ws][data["terminal"]][:thread] = Thread.new do
                    position = 0
                    while(job.status != "finished") do
                      new = job.log(:from => position)
                      ws.send({
                        :tag => tag,
                        :data => {
                          :response => "data",
                          :output   => new,
                          :terminal => data["terminal"],
                        }
                      }.to_json)
                      position = position + new.length

                      if position > 0 && !job.running?
                        break
                      end
                    end
                  end
                else
                  command = ""
                end
              end
            end
          end
        end

        ws.on :close do |event|
          @terminals[ws].each do |k, v|
            if v.has_key? :thread
              puts "Killing Thread"
              Thread.kill(v[:thread])
            end
          end
          @clients.delete(ws)
          ws = nil
        end

        # Return async Rack response
        ws.rack_response
      else
        @app.call(env)
      end
    end
  end
end
