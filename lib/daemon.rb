# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Daemon
    require 'socket'
    require 'json'

    attr_reader :host
    attr_reader :port

    attr_reader :socket

    class PromotedSocket
      attr_reader :socket

      def initialize(socket, daemon)
        @socket = socket
        @daemon = daemon
      end

      def send(*args, &block)
        @socket.send(*args, &block)
      end

      def read(*args, &block)
        @socket.read(*args, &block)
      end

      def write(*args, &block)
        @socket.write(*args, &block)
      end
    end

    class Error < StandardError
      attr_reader :message
      attr_reader :response

      def initialize(message, response)
        @response = response
        @message  = message

        super(message)
      end
    end

    def initialize(host=nil, port=nil)
      # Creates an instance by connecting to the running daemon.

      # Get configuration
      @config = Occam::Config.configuration()

      # Gather the host and port for the daemon
      @host = host || @config['host'] || "localhost"
      @port = port || @config['port'] || 32000

      @promoted = false

      self.connect
    end

    def connect
      # Connect to the daemon
      @socket = TCPSocket.new(@host, @port)

      # Finalize to close the socket cleanly
      ObjectSpace.define_finalizer(self, self.class.finalize(@socket))
    end

    # Returns a Hash representing the response for the command.
    def execute(component, command, arguments = [], options = {}, stdin=nil, promote = false)
      if promote
        if @promoted
          raise "Cannot issue a command to a promoted daemon"
        end
        @promoted = true
      end

      # TODO: clean up
      begin
        options.each do |k,v|
          if v != true
            if v.is_a? Array
              options[k] = v.map do |item|
                if item.is_a? Array
                  item.dup.map(&:to_s)
                else
                  item.to_s
                end
              end
            else
              options[k] = v.to_s
            end
          end
        end

        payload = {}
        payload["component"] = component
        payload["command"] = command
        payload["arguments"] = arguments.map(&:to_s)
        payload["options"] = options

        if promote
          payload["promote"] = true
        end

        if stdin
          if not stdin.is_a? String
            raise "STDIN should be a string"
          end
          payload["stdin"] = stdin.length
        end
        payload = payload.to_json
        @socket.puts payload

        if stdin
          @socket.write stdin
        end

        # Wait for response
        headerSize = @socket.gets
        headerSize = headerSize.to_i

        # Read header
        headerData = @socket.read(headerSize)
        headerData = JSON.parse(headerData, :symbolize_names => true)

        # Read Data size
        dataSize = @socket.gets
        dataSize = dataSize.to_i

        # Read Data
        data = @socket.read(dataSize)

        ret = {
          :code   => headerData[:code] || (headerData[:status] == "ok" ? 0 : -1),
          :header => headerData,
          :data   => data
        }
      rescue Exception => e
        ret = {
          :code => -1,
          :header => {
            :status => "error",
            :message => e.message
          },
          :data => ""
        }
      end

      if ret[:header][:status] == "error"
        raise Occam::Daemon::Error.new(ret[:header][:message], ret)
      end

      if promote
        # Keep an instance of the daemon so the socket isn't closed
        # until the socket is destroyed or drops out of scope
        PromotedSocket.new(@socket, self)
      else
        ret
      end
    end

    def disconnect
      @socket.close()
    end

    def self.finalize(socket)
      proc {
        puts "Closing Socket"
        socket.close()
      }
    end
  end
end
