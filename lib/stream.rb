# This file is in the public domain.
#
# This will ensure all streams are chunked

# This code is based off of: https://gist.github.com/uu59/1671771
module Sinatra
  module Helpers
    class Stream
      remove_method :each
      def each(&front)
        @front = front
        callback do
          @front.call("0\r\n\r\n")
        end

        @scheduler.defer do
          begin
            @back.call(self)
          rescue Exception => e
            @scheduler.schedule { raise e }
          end
          close unless @keep_open
        end
      end

      remove_method :<<
      def <<(data)
        @scheduler.schedule do
          size = data.to_s.bytesize
          @front.call([size.to_s(16), "\r\n", data.to_s, "\r\n"].join)
        end
        self
      end
    end
  end
end

class Occam
  # This class wraps and io to output http chunked encoding chunks
  # Only useful when we need to use rack.hijack (which we don't atm)
  class ChunkWriter
    def initialize(io)
      @io = io
    end

    def <<(data)
      size = data.to_s.bytesize
      @io.write([size.to_s(16), "\r\n", data.to_s, "\r\n"].join)
    end
  end

  def stream(*args)
    # This would be for any non-conforming servers
    # Those can be checked via 'settings.server == :puma' etc
    if false
      out = env['rack.hijack'].call
      out = env['rack.hijack_io']
      out.write "HTTP/1.1 200 OK\r\n"
      out.write "Status: 200\r\n"
      out.write "Connection: keep-alive\r\n"
      response.headers.each do |k,v|
        out.write "#{k}: #{v}\r\n"
      end
      out.write "Transfer-Encoding: chunked\r\n"
      out.write "X-Content-Type-Options: nosniff\r\n"
      out.write "\r\n"

      yield(ChunkWriter.new(out))

      out.write("0\r\n\r\n")
      out.close

      return [-1, {}, []]
    end

    headers "Transfer-Encoding" => "chunked"

    super(*args)
  end

  def promote(*args)
    stream do |out|
    end
  end
end
