require_relative "helper"

describe Occam::StringHelpers, :type => :helper do
  class HelperTest < HelperContext
    include Occam::StringHelpers
  end

  before do
    @app = HelperTest.new
  end

  describe "direction" do
    it "should return :rtl when the string is Hebrew text" do
      @app.direction("قلب").must_equal :rtl
    end

    it "should return :rtl when the string is Arabic text" do
      @app.direction("לֵב").must_equal :rtl
    end

    it "should return :ltr on English text" do
      @app.direction("heart").must_equal :ltr
    end

    it "should return :ltr when the string is nil" do
      @app.direction(nil).must_equal :ltr
    end
  end
end
