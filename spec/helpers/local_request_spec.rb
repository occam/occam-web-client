require_relative "helper"

describe Occam::LocalRequestHelpers, :type => :helper do
  class HelperTest < HelperContext
    include Occam::LocalRequestHelpers
  end

  describe "local_request?" do
    before do
      @ipv4 = mock('Addrinfo')
      @ipv6 = mock('Addrinfo')
      @ipv4.stubs(:ip_address).returns("192.10.10.255")
      @ipv6.stubs(:ip_address).returns("2a00:1450:4007:815::200e")
      @equivalent_ipv6 = "2a00:1450:4007:0815:0:0000:0:200e"

      Socket.stubs(:ip_address_list).returns([@ipv4, @ipv6])
    end

    it "should return false when the ip address is invalid" do
      app = HelperTest.new(:url => "http://www.example.com/foo", :env => {"REMOTE_ADDR" => "@#%@^#"})
      app.local_request?.must_equal false
    end

    it "should return false when the queried ip address is invalid" do
      app = HelperTest.new(:url => "http://www.example.com/foo", :env => {"REMOTE_ADDR" => "192.10.10.255"})
      Socket.stubs(:ip_address_list).returns(["#%$&$%^"])
      app.local_request?.must_equal false
    end

    it "should return true when the host is localhost" do
      app = HelperTest.new(:url => "http://localhost/foo", :env => {"REMOTE_ADDR" => "192.10.10.255"})
      app.local_request?.must_equal true
    end

    it "should return false when the host is a global domain" do
      app = HelperTest.new(:url => "http://www.example.com/foo")
      app.local_request?.must_equal false
    end

    it "should return false when the client ipv4 is not the local ip" do
      app = HelperTest.new(:url => "http://www.example.com/foo", :env => {"REMOTE_ADDR" => "111.13.3.100"})
      app.local_request?.must_equal false
    end

    it "should return true when the client ipv4 is the local ip" do
      app = HelperTest.new(:url => "http://www.example.com/foo", :env => {"REMOTE_ADDR" => "#{@ipv4.ip_address}"})
      app.local_request?.must_equal true
    end

    it "should return false when the client ipv6 is not the local ip" do
      app = HelperTest.new(:url => "http://www.example.com/foo", :env => {"REMOTE_ADDR" => "2001:41d0:1:81a1::1"})
      app.local_request?.must_equal false
    end

    it "should return true when the client ipv6 is the local ip" do
      app = HelperTest.new(:url => "http://www.example.com/foo", :env => {"REMOTE_ADDR" => "#{@ipv6.ip_address}"})
      app.local_request?.must_equal true
    end

    it "should return true when the non-truncated client ipv6 is the local ip" do
      app = HelperTest.new(:url => "http://www.example.com/foo", :env => {"REMOTE_ADDR" => "#{@equivalent_ipv6}"})
      app.local_request?.must_equal true
    end
  end
end
