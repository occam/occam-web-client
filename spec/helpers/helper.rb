require_relative "../models/helper"

class HelperContext
  attr_reader :session
  attr_reader :params
  attr_reader :response

  def initialize(options={})
    @session = options[:session] || {}
    @params  = options[:params]  || {}
    @env     = options[:env]     || {}
    @url     = options[:url]     || '/'
    @headers = {}
    @response = Sinatra::Response.new
  end

  def request
    Rack::Request.new(Rack::MockRequest.env_for(@url, :params => @params).update(@env))
  end

  def headers(hash = nil)
    response.headers.merge! hash if hash
    response.headers
  end
end
