require_relative "helper"

describe Occam::SVGHelpers, :type => :helper do
  before do
    class HelperTest < HelperContext
      include Occam::SVGHelpers
    end

    @app = HelperTest.new

    require 'nokogiri'

    @document = mock('Nokogiri::HTML::Document')
    @svgTag = {}
    @document.stubs(:at_css).returns(@svgTag)
    @document.stubs(:to_html).returns("<HTML>")

    Nokogiri::HTML::DocumentFragment.stubs(:parse).returns(@document)

    File.stubs(:read).returns("FILE")
  end

  describe "embed_svg" do
    it "should return HTML" do
      @app.embed_svg("images/add_item.svg").must_equal "<HTML>"
    end

    it "should read SVG file from public" do
      File.expects(:read).with("public/images/add_item.svg")
      @app.embed_svg("images/add_item.svg")
    end

    it "should read SVG file from public, ignoring starting slashes" do
      File.expects(:read).with("public/images/add_item.svg")
      @app.embed_svg("/images/add_item.svg")
    end

    it "should pass file data into Nokogiri" do
      Nokogiri::HTML::DocumentFragment.expects(:parse).with("FILE").returns(@document)
      @app.embed_svg("images/add_item.svg")
    end

    it "should modify the svg tag to include the given class" do
      @app.embed_svg("images/add_item.svg", :class => "FOO")
      @svgTag['class'] = "FOO"
    end

    it "should modify the svg tag to include the given width" do
      @app.embed_svg("images/add_item.svg", :width => 42)
      @svgTag['width'] = 42
    end

    it "should modify the svg tag to include the given height" do
      @app.embed_svg("images/add_item.svg", :height => 42)
      @svgTag['height'] = 42
    end
  end
end
