require_relative "helper"

describe Occam::DateHelpers, :type => :helper do
  before do
    class HelperTest < HelperContext
      include Occam::DateHelpers
    end

    @app = HelperTest.new
  end

  describe "renderDuration" do
    it "should return a string" do
      @app.renderDuration(DateTime.now).must_be_instance_of String
    end

    it "should take an iso8601 timestamp as a starting time" do
      time = DateTime.now
      @app.renderDuration(time.iso8601, time).must_be_instance_of String
    end

    it "should take an iso8601 timestamp as an ending time" do
      time = DateTime.now
      @app.renderDuration(time, time.iso8601).must_be_instance_of String
    end

    it "should return 'just now' when the given times are the same" do
      time = DateTime.now
      @app.renderDuration(time, time).must_equal "just now"
    end

    it "should return 'just now' when the given times are the same" do
      time = DateTime.now
      @app.renderDuration(time, time).must_equal "just now"
    end

    it "should return 'in the future' when the starting date is ahead of the end time" do
      time = DateTime.now
      @app.renderDuration(time, time-1).must_match(/in the future$/)
    end

    it "should return 'ago' when the starting date is before the end time" do
      time = DateTime.now
      @app.renderDuration(time-1, time).must_match(/ago$/)
    end

    it "should return a string that reflects how many seconds ago the event happened" do
      number = rand(23) + 1
      final = DateTime.now
      start = final - Rational(number, 60*60*24)
      @app.renderDuration(start, final).must_match(/^#{number} sec.* ago$/)
    end

    it "should return a string that reflects how many minutes ago the event happened" do
      number = rand(23) + 1
      final = DateTime.now
      start = final - Rational(number, 60*24)
      @app.renderDuration(start, final).must_match(/^#{number} min.* ago$/)
    end

    it "should return a string that reflects how many hours ago the event happened" do
      number = rand(23) + 1
      final = DateTime.now
      start = final - Rational(number, 24)
      @app.renderDuration(start, final).must_match(/^#{number} h.*r.* ago$/)
    end

    it "should return a string that reflects how many seconds in the future the event happened" do
      number = rand(23) + 1
      final = DateTime.now
      start = final + Rational(number, 60*60*24)
      @app.renderDuration(start, final).must_match(/^#{number} sec.* in the future$/)
    end

    it "should return a string that reflects how many minutes in the future the event happened" do
      number = rand(23) + 1
      final = DateTime.now
      start = final + Rational(number, 60*24)
      @app.renderDuration(start, final).must_match(/^#{number} min.* in the future$/)
    end

    it "should return a string that reflects how many hours in the future the event happened" do
      number = rand(23) + 1
      final = DateTime.now
      start = final + Rational(number, 24)
      @app.renderDuration(start, final).must_match(/^#{number} h.*r.* in the future$/)
    end
  end

  describe "renderDateTime" do
    it "should return a string" do
      @app.renderDateTime(DateTime.now).must_be_instance_of String
    end

    it "should pass the given DateTime through to the I18n library" do
      time = DateTime.now
      I18n.expects(:l).with(time, anything).returns("")
      @app.renderDateTime(time)
    end

    it "should pass the format option through to the I18n library" do
      I18n.expects(:l).with(anything, has_entry(:format, :short)).returns("")
      @app.renderDateTime(DateTime.now, :format => :short)
    end

    it "should allow an iso8601 string" do
      time = DateTime.now.iso8601
      I18n.expects(:l)
          .with(all_of(instance_of(DateTime),
                       resolves(lambda {|arg| arg.iso8601 == time})),
                anything)
          .returns("")
      @app.renderDateTime(time)
    end
  end

  describe "renderDate" do
    it "should return a string" do
      @app.renderDate(DateTime.now).must_be_instance_of String
    end

    it "should pass the given Date through to the I18n library" do
      time = DateTime.now.to_date
      I18n.expects(:l).with(time, anything).returns("")
      @app.renderDate(time)
    end

    it "should pass the format option through to the I18n library" do
      I18n.expects(:l).with(anything, has_entry(:format, :short)).returns("")
      @app.renderDateTime(DateTime.now, :format => :short)
    end

    it "should allow an iso8601 string" do
      time = DateTime.now.to_date.to_datetime.iso8601
      I18n.expects(:l)
          .with(all_of(instance_of(Date),
                       resolves(lambda {|arg| arg.to_datetime.iso8601 == time})),
                anything)
          .returns("")
      @app.renderDate(time)
    end
  end
end
