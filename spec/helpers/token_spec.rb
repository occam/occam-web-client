require_relative "helper"

describe Occam::TokenHelpers, :type => :helper do
  class HelperTest < HelperContext
    include Occam::TokenHelpers
  end

  before do
    @app = HelperTest.new
    @cookie_encrypter = mock('Rack::Session::EncryptedCookie::Encryptor')

    @encrypted = "encrypted-token-#{uuid()}"
    @decrypted = "{\"token\": \"token-#{uuid()}\"}"
    Rack::Session::EncryptedCookie::Encryptor.any_instance.stubs(:encrypt).with(@decrypted).returns(@encrypted)
    Rack::Session::EncryptedCookie::Encryptor.any_instance.stubs(:decrypt).with(anything).returns("bogus")
    Rack::Session::EncryptedCookie::Encryptor.any_instance.stubs(:decrypt).with(@encrypted).returns(@decrypted)
  end

  describe "cookie_encrypter" do
    it "should return an instance of an Encryptor" do
      @app.cookie_encrypter.must_be_instance_of Rack::Session::EncryptedCookie::Encryptor
    end
  end

  describe "encrypt_token" do
    it "should encrypt an empty dictionary when nobody is logged in" do
      Rack::Session::EncryptedCookie::Encryptor.any_instance.expects(:encrypt).with("{}").returns("foo")
      @app.encrypt_token
    end

    it "should return the encrypted token when a valid is account is logged in" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      Rack::Session::EncryptedCookie::Encryptor.any_instance.stubs(:encrypt).with(anything).returns(@encrypted)
      @app.session[:token] = account.token
      @app.session[:roles] = account.roles
      @app.session[:person_id] = account.person.id
      @app.encrypt_token.must_equal @encrypted
    end

    it "should form the encrypted token with the valid logged in account" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      @app.session[:token] = account.token
      @app.session[:roles] = account.roles
      @app.session[:person_id] = account.person.id
      Rack::Session::EncryptedCookie::Encryptor.any_instance
                                               .expects(:encrypt)
                                               .with(resolves(lambda { |arg|
                                                  arg = JSON.parse(arg)
                                                  arg.has_key?('token') and arg['token'] == account.token and
                                                  arg.has_key?('person_id') and arg['person_id'] == account.person.id
                                               }))
                                               .returns(@encrypted)
      @app.encrypt_token
    end
  end

  describe "decrypt_token" do
    it "should return a Hash" do
      @app.decrypt_token(@encrypted).must_be_instance_of Hash
    end

    it "should return an empty dictionary when the JSON is invalid" do
      @app.decrypt_token("@#f34ASD%@#").must_equal({})
    end

    it "should return the decrypted and parsed JSON document" do
      @app.decrypt_token(@encrypted).must_equal(JSON.parse(@decrypted, :symbolize_names => true))
    end
  end
end
