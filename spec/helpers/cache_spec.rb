require_relative "helper"

describe Occam::CacheHelpers, :type => :helper do
  class HelperTest < HelperContext
    include Occam::CacheHelpers
  end

  before do
    @app = HelperTest.new
  end

  describe "no_cache" do
    it "should set the Expires header to the past" do
      @app.no_cache

      expires = Time.httpdate(@app.headers["Expires"])
      (expires < Time.now).must_equal true
    end

    it "should set the Pragma header to 'no-cache'" do
      @app.no_cache

      @app.headers["Pragma"].must_equal "no-cache"
    end

    it "should set the Cache-Control header to no-cache" do
      @app.no_cache

      @app.headers["Cache-Control"].must_match(/no-cache/)
    end

    it "should set the Cache-Control header to use a max-age of 0" do
      @app.no_cache

      @app.headers["Cache-Control"].must_match(/max-age=0($|,)/)
    end

    it "should set the Cache-Control header to have must-revalidate" do
      @app.no_cache

      @app.headers["Cache-Control"].must_match(/must-revalidate($|,)/)
    end
  end

  describe "forever_cache" do
    it "should set the Expires header to quite some time in the future" do
      @app.forever_cache

      expires = Time.httpdate(@app.headers["Expires"])
      (expires > (Time.now + 1000000)).must_equal true
    end

    it "should set the Date header to around the current time" do
      now = Time.now
      Time.stubs(:now).returns(now)

      @app.forever_cache

      @app.headers["Date"].must_equal now.to_s
    end

    it "should set the Cache-Control header to use the public cache" do
      @app.forever_cache

      @app.headers["Cache-Control"].must_match(/public/)
    end

    it "should set the Cache-Control header to use a large max-age" do
      @app.forever_cache

      @app.headers["Cache-Control"].must_match(/max-age=\d{8}\d*/)
    end
  end
end
