require_relative "helper"

feature "Creating Workset and Experiment" do
  background do
  end

  scenario "Creating a fresh workset and a fresh experiment" do
    username = "wilkie-#{uuid()}"
    password = "foobar-#{uuid()}"

    account = Occam::Account.create(username, password)
    person  = account.person

    visit '/login'

    fill_in 'username', :with => username
    fill_in 'password', :with => password

    click_button "login"

    # We should be on the login page
    current_path.must_equal "/people/#{person.id}"

    # Create the workset
    fill_in 'name', :with => "new workset"
    click_button "add"

    # We should be on the workset page
    page.find('#object-name a').text.must_equal "new workset"
    page.find('#object-type').text.must_equal "workset"

    # Now create an experiment
    fill_in 'name', :with => "new experiment"
    find('[name=add_object]').click

    # We should be on the experiment page
    page.find('#object-name a').text.must_equal "new experiment"
    page.find('#object-type').text.must_equal "experiment"
  end
end
