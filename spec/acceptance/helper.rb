# Create a test occam root
specPath = File.join(File.dirname(__FILE__), "..")
occamPath = File.join(specPath, ".occam")
if File.exist?(occamPath)
  puts
  puts "Deleting existing test occam root"
  FileUtils.remove_dir(occamPath)
end

puts
puts "Initializing an occam instance"
`OCCAM_ROOT=#{occamPath} occam system initialize > /dev/null 2> /dev/null`
puts "Done"

# Copy a web config in
FileUtils.cp(File.join(specPath, "web-config.yml"), File.join(occamPath, "web-config.yml"))
require_relative "../helper"

# Ensure the new configuration takes hold
Occam::Config.configuration(true)

# Run the daemon at the requested port
port = Occam::Config.configuration['port']

puts
puts "Starting a test daemon on port #{port}"
`OCCAM_ROOT=#{occamPath} occam daemon start --port #{port}`

puts "Waiting for daemon"
sleep 2

puts "Let's do this"

Minitest.after_run do
  # Wait for all the other threads, if they exist
  #puts
  #puts "Waiting on pending threads..."
  #Thread.list.each do |t|
  #  t.join if t != Thread.current
  #end

  puts
  puts "Disconnecting from test daemon on port #{port}"
  daemon = Thread.current[:occam_daemon]
  if daemon
    daemon.disconnect
  end

  puts
  puts "Stopping test daemon on port #{port}"
  `OCCAM_ROOT=#{occamPath} occam daemon stop --port #{port}`
end

puts

silence_warnings do
  require 'capybara'
  require 'capybara/dsl'
  require 'capybara/minitest'
  require 'capybara/minitest/spec'
  require "selenium/webdriver"
end

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

Capybara.register_driver :headless_chrome do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    chromeOptions: { args: %w(headless disable-gpu no-sandbox disable-dev-shm-usage),
                     binary: "/usr/bin/chromium"}
  )

  options = ::Selenium::WebDriver::Chrome::Options.new
  options.add_argument("--headless")
  options.add_argument("--no-sandbox")
  options.add_argument("--disable-dev-shm-usage")
  options.add_argument("--disable-gpu")

  Capybara::Selenium::Driver.new(app,
                                 :browser => :chrome,
                                 :options => options,
                                 :desired_capabilities => capabilities)
end

class AcceptanceTest < MiniTest::Spec
  include Capybara::DSL
  include Capybara::Minitest::Assertions

  instance_eval do
    # Allow scenario/given to be the DSL of choice
    alias :background :before
    alias :scenario   :it
    alias :given      :let
  end

  def setup
    setup_base

    Capybara.reset_sessions!
    # Capybara.use_default_driver
    Capybara.current_driver = :headless_chrome

  end

  def teardown
    teardown_base

    Capybara.reset_sessions!
    # Capybara.use_default_driver
    Capybara.current_driver = :headless_chrome

  end

  Capybara.app = Occam
  Capybara.register_driver :rack_test do |app|
    Capybara::RackTest::Driver.new(app, :headers => {
      'User-Agent' => 'Capybara'
    })
  end

  extend MiniTest::Spec::DSL

  register_spec_type(self) do |desc, *add|
    add.length > 0 and add[0][:type] == :feature
  end
end

def admin
  ret = nil
  silence_warnings do
    if !defined?(@@admin)
      @@admin = Occam::Account.create("admin", "admin")
    end
    ret = @@admin
  end
  ret
end
admin()

# Login as an administrator
def login_as_admin
  login_as(admin(), "admin", "admin")
end

# Login as a basic user
def login_as_user
  ret = nil
  silence_warnings do
    if !defined?(@@user)
      @@username = "user-#{uuid()}"
      @@password = "foobar-#{uuid()}"
      @@user = Occam::Account.create(@@username, @@password)
    end

    ret = login_as(@@user, @@username, @@password)
  end

  ret
end

# Create a new account with the given username and password
# Log in as that account
def login_as_new_account(username, password)
  account = Occam::Account.create(username, password)

  login_as(account, username, password)
end

# Use the given account and attempt to log in with the given username/password
def login_as(account, username, password)
  @account = account
  @person  = @account.person

  @username = username
  @password = password

  visit '/login'

  fill_in 'username', :with => username
  fill_in 'password', :with => password

  click_button "login"

  @person
end

# Allow "feature" to be the DSL of choice

def feature(*args, &block)
  opts = {}
  if args[-1].is_a? Hash
    opts = args.pop
  end
  opts[:type] = :feature
  describe(*args, opts, &block)
end
