require_relative "helper"

feature "Signing in", :js => true do
  background do
  end

  scenario "Signing in with a valid username/password" do
    username = "wilkie-#{uuid()}"
    password = "foobar-#{uuid()}"

    account = Occam::Account.create(username, password)
    person  = account.person

    visit '/login'

    fill_in 'username', :with => username
    fill_in 'password', :with => password

    click_button "login"
    current_path.must_equal "/people/#{person.id}"
  end

  scenario "Signing in with an invalid password" do
    username = "wilkie-#{uuid()}"
    password = "foobar-#{uuid()}"

    Occam::Account.create(username, password)

    visit '/login'

    fill_in 'username', :with => username
    fill_in 'password', :with => "bogus"

    click_button "login"
    current_path.must_equal "/login"
  end

  scenario "Signing in with an invalid username" do
    username = "wilkie-#{uuid()}"
    password = "foobar-#{uuid()}"

    Occam::Account.create(username, password)

    visit '/login'

    fill_in 'username', :with => "bogus"
    fill_in 'password', :with => password

    click_button "login"
    current_path.must_equal "/login"
  end

  scenario "Signing in with an invalid username/password" do
    username = "wilkie-#{uuid()}"
    password = "foobar-#{uuid()}"

    Occam::Account.create(username, password)

    visit '/login'

    fill_in 'username', :with => "bogus"
    fill_in 'password', :with => "bogus"

    click_button "login"
    current_path.must_equal "/login"
  end
end
