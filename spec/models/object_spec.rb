require_relative "helper"

# This will duplicate tests where one uses an object with a token and one doesn't
def authenticated_tests(&block)
  [:authenticated, :unauthenticated].each do |type|
    describe type do
      before(:each) do
        @object = (type == :unauthenticated ? object : authenticated_object)
        @type   = type
      end

      instance_eval do
        def authentication_test(*args, &block)
          instance_eval do
            @type = nil if !defined?(@type)
            it "should #{@type == :unauthenticated ? "not " : ""}send an authentication token when querying the daemon" do
              query = has_entry("-T", (@type == :unauthenticated ? nil : @object.account.token))
              query = Not(query) if @type == :unauthenticated

              if args.length == 2
                args << anything
              end

              Occam::Daemon.any_instance.expects(:execute).with(*args, query, anything, anything).returns(daemonSuccess)

              instance_eval(&block)
            end
          end
        end
      end

      instance_eval(&block)
    end
  end
end

describe Occam::Object do
  # An unauthenticated object
  let(:object) do
    new_object()
  end

  # An object with an @account
  let(:authenticated_object) do
    new_object(:account => new_account())
  end

  describe "create" do
  end

  describe "associationFor" do
  end

  describe "types" do
  end

  describe "search" do
  end

  describe "#initialize" do
  end

  describe "#as" do
  end

  describe "#resolve" do
  end

  describe "#contents" do
  end

  describe "#authorships" do
  end

  describe "#collaboratorships" do
  end

  describe "#resetPermission" do
  end

  describe "#setPermission" do
  end

  describe "#permissions" do
  end

  describe "#revision" do
  end

  describe "#ownerUUID" do
  end

  describe "#parentRevision" do
  end

  describe "#childRevisions" do
  end

  describe "#configurators" do
  end

  describe "#exists?" do
  end

  describe "#currentRevision" do
  end

  describe "#rootRevision" do
  end

  describe "#status" do
    authenticated_tests do
      authentication_test("objects", "status") do
        @object.status
      end

      it "should cache the result after the first invocation" do
        Occam::Daemon.any_instance.expects(:execute).with("objects", "status", any_of(includes(@object.id), includes(@object.fullID)), anything, anything, anything).returns(daemonSuccess).once
        @object.status
        @object.status
      end

      it "should return the parsed JSON from the daemon's response" do
        Occam::Daemon.any_instance.stubs(:execute).with("objects", "status", any_of(includes(@object.id), includes(@object.fullID)), anything, anything, anything).returns({
          :code => 0,
          :data => {:foo => "bar"}.to_json
        })
        @object.status.must_equal({:foo => "bar"})
      end
    end
  end

  describe "#shortID" do
  end

  describe "#fullID" do
  end

  describe "#ownerInfo" do
    authenticated_tests do
      authentication_test("objects", "view") do
        @object.ownerInfo
      end

      it "should cache the result after the first invocation" do
        Occam::Daemon.any_instance.expects(:execute).with("objects", "view", any_of(includes(@object.id), includes(@object.fullID)), anything, anything, anything).returns(daemonSuccess).once
        @object.ownerInfo
        @object.ownerInfo
      end

      it "should return the parsed JSON from the daemon's response" do
        Occam::Daemon.any_instance.stubs(:execute).with("objects", "view", any_of(includes(@object.id), includes(@object.fullID)), anything, anything, anything).returns({
          :code => 0,
          :data => {:foo => "bar"}.to_json
        })
        @object.ownerInfo.must_equal({:foo => "bar"})
      end
    end
  end

  describe "#set" do
  end

  describe "#info" do
    authenticated_tests do
      authentication_test("objects", "view") do
        @object.info
      end

      it "should cache the result after the first invocation" do
        Occam::Daemon.any_instance.expects(:execute).with("objects", "view", any_of(includes(@object.id), includes(@object.fullID)), anything, anything, anything).returns(daemonSuccess).once
        @object.info
        @object.info
      end

      it "should return the parsed JSON from the daemon's response" do
        Occam::Daemon.any_instance.stubs(:execute).with("objects", "view", any_of(includes(@object.id), includes(@object.fullID)), anything, anything, anything).returns({
          :code => 0,
          :data => {:foo => "bar"}.to_json
        })
        @object.info.must_equal({:foo => "bar"})
      end
    end
  end

  describe "#retrieveHistory" do
    authenticated_tests do
      authentication_test("objects", "history") do
        @object.retrieveHistory
      end

      it "should cache the result after the first invocation" do
        Occam::Daemon.any_instance.expects(:execute).with("objects", "history", id_matches(@object), anything, anything, anything).returns(daemonSuccess).once

        @object.retrieveHistory
        @object.retrieveHistory
      end

      it "should return the parsed JSON from the daemon's response" do
        Occam::Daemon.any_instance.stubs(:execute).with("objects", "history", any_of(includes(@object.id), includes(@object.fullID)), anything, anything, anything).returns({
          :code => 0,
          :data => ["foo", "bar"].to_json
        })
        @object.retrieveHistory.must_equal(["foo", "bar"])
      end
    end
  end

  describe "#retrieveFileStat" do
    authenticated_tests do
      authentication_test("objects", "status") do
        @object.retrieveFileStat("foo")
      end

      it "should not cache the result after the first invocation" do
        #Occam::Daemon.any_instance.expects(:execute).with("objects", "view", id_matches(@object), anything, anything, anything).returns(daemonSuccess).once
        Occam::Daemon.any_instance.expects(:execute).with("objects", "status", id_matches(@object), anything, anything, anything).returns(daemonSuccess).twice
        @object.retrieveFileStat("foo")
        @object.retrieveFileStat("foo")
      end

      it "should return the parsed JSON from the daemon's response" do
        Occam::Daemon.any_instance.stubs(:execute).with("objects", "status", id_matches(@object), anything, anything, anything).returns({
          :code => 0,
          :data => {:foo => "bar"}.to_json
        })
        @object.retrieveFileStat("foo").must_equal({:foo => "bar"})
      end
    end
  end

  describe "#retrieveFile" do
    authenticated_tests do
      authentication_test("objects", "view") do
        @object.retrieveFile("foo")
      end

      it "should not cache the result after the first invocation" do
        #Occam::Daemon.any_instance.expects(:execute).with("objects", "view", id_matches(@object), anything, anything, anything).returns(daemonSuccess).once
        Occam::Daemon.any_instance.expects(:execute).with("objects", "view", id_matches(@object), anything, anything, anything).returns(daemonSuccess).twice
        @object.retrieveFile("foo")
        @object.retrieveFile("foo")
      end

      it "should return the data directly from the daemon's response" do
        Occam::Daemon.any_instance.stubs(:execute).with("objects", "view", id_matches(@object), anything, anything, anything).returns({
          :code => 0,
          :data => "bar"
        })
        @object.retrieveFile("foo").must_equal("bar")
      end
    end
  end

  describe "#retrieveDirectory" do
    authenticated_tests do
      authentication_test("objects", "list") do
        @object.retrieveDirectory("foo")
      end

      it "should not cache the result after the first invocation" do
        Occam::Daemon.any_instance.expects(:execute).with("objects", "list", id_matches(@object), anything, anything, anything).returns(daemonSuccess).twice
        @object.retrieveDirectory("foo")
        @object.retrieveDirectory("foo")
      end

      it "should return the parsed JSON from the daemon's response" do
        Occam::Daemon.any_instance.stubs(:execute).with("objects", "list", id_matches(@object), anything, anything, anything).returns({
          :code => 0,
          :data => {:foo => "bar"}.to_json
        })
        @object.retrieveDirectory("foo").must_equal({:foo => "bar"})
      end
    end
  end

  describe "#retrieveJSON" do
    authenticated_tests do
      authentication_test("objects", "view") do
        @object.retrieveJSON("foo")
      end

      it "should not cache the result after the first invocation" do
        Occam::Daemon.any_instance.expects(:execute).with("objects", "view", id_matches(@object), anything, anything, anything).returns(daemonSuccess).twice
        @object.retrieveJSON("foo")
        @object.retrieveJSON("foo")
      end

      it "should return the parsed JSON from the daemon's response" do
        Occam::Daemon.any_instance.stubs(:execute).with("objects", "view", id_matches(@object), anything, anything, anything).returns({
          :code => 0,
          :data => {:foo => "bar"}.to_json
        })
        @object.retrieveJSON("foo").must_equal({:foo => "bar"})
      end
    end
  end

  describe "#runnable?" do
  end

  describe "#viewer?" do
  end

  describe "#trusted?" do
  end

  describe "#parents" do
  end

  describe "#parent" do
  end

  describe "#belongsTo" do
  end

  describe "#groups" do
  end

  describe "#experiments" do
  end

  describe "#clone" do
    authenticated_tests do
      let(:cloned_obj) do
        new_object()
      end

      let(:new_root_obj) do
        new_object()
      end

      authentication_test("objects", "clone") do
        @object.clone
      end

      it "should return an Occam::Object based on the daemon's response" do
        Occam::Daemon.any_instance.stubs(:execute).with("objects", "clone", id_matches(@object), anything, anything, anything).returns({
          :code => 0,
          :data => {:updated => [{:id => @object.id, :revision => revision()}, {:id => cloned_obj.id, :revision => cloned_obj.revision}]}.to_json
        })
        @object.clone.must_be_instance_of Occam::Object
      end

      it "should pass along the desired name to the daemon" do
        Occam::Daemon.any_instance.expects(:execute).with("objects", "clone", id_matches(@object), has_entry("--name", "foo"), anything, anything).returns({
          :code => 0,
          :data => {:updated => [{:id => @object.id, :revision => revision()}, {:id => cloned_obj.id, :revision => cloned_obj.revision}]}.to_json
        })

        @object.clone(nil, :name => "foo")
      end

      it "should pass along the object to clone into" do
        Occam::Daemon.any_instance.expects(:execute).with("objects", "clone", id_matches(@object), has_entry("--to", new_root_obj.fullID), anything, anything).returns({
          :code => 0,
          :data => {:updated => [{:id => @object.id, :revision => revision()}, {:id => cloned_obj.id, :revision => cloned_obj.revision}]}.to_json
        })

        @object.clone(new_root_obj)
      end
    end
  end

  describe "#root" do
  end

  describe "#canEdit?" do
  end

  describe "#canView?" do
  end

  describe "#canClone?" do
  end

  describe "#canExecute?" do
  end

  describe "#viewers" do
  end

  describe "#runners" do
  end

  describe "#eachFile" do
  end

  describe "#fileType" do
  end

  describe "#mimeType" do
  end

  describe "#isText?" do
  end

  describe "#workflow" do
  end

  describe "#tail_connections" do
  end

  describe "#isImage?" do
  end

  describe "#isBinary?" do
  end

  describe "#isGroup?" do
  end

  describe "#revisionsFor" do
  end

  describe "#notes" do
  end

  describe "#backends" do
  end

  describe "#supports?" do
  end

  describe "#versions" do
  end

  describe "#includes" do
  end

  describe "#dependencies" do
  end

  describe "#createLink" do
  end

  describe "#destroyLink!" do
  end

  describe "#linksTo" do
  end

  describe "#links" do
  end

  describe "#url" do
  end

  describe "#configurations" do
  end

  describe "#datapoints?" do
  end

  describe "#review_capabilities" do
  end

  describe "#create" do
  end

  describe "#run" do
  end

  describe "#build" do
  end

  describe "#generated" do
  end

  describe "#schema" do
  end

  describe "#data" do
  end

  describe "#addComment" do
  end

  describe "#viewComments" do
  end
end
