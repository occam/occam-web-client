require_relative "helper"

describe Occam::Account do
  describe "authenticate" do
    before do
      @account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
    end

    it "should return true when the username and password are accepted by the daemon" do
      Occam::Account.authenticate(@account.token).must_equal true
    end

    it "should return false when the username and password are rejected by the daemon" do
      Occam::Account.authenticate("bogus").must_equal false
    end
  end

  describe "generateToken" do
    before do
      @username = "wilkie-#{uuid()}"
      @password = "foobar-#{uuid()}"
      @account = new_account(:username => @username, :password => @password, :roles => ["foo"])
    end

    it "should return true when the username and password are accepted by the daemon" do
      ret = Occam::Account.generateToken(@username, @password)

      ret.wont_equal nil
      ret[:token].must_equal @account.token
    end

    it "should return false when the username and password are rejected by the daemon" do
      Occam::Account.generateToken(@username, "bogus").must_be_nil
    end
  end

  describe "create" do
    before do
      @username = "wilkie-#{uuid()}"
      @password = "foobar-#{uuid()}"

      @account = new_account(:username => @username,
                             :password => @password)

      # Stub for daemon execution
      Occam::Daemon.any_instance.stubs(:execute).with("accounts", "new", includes(@username), has_entry("-p", @password), anything, anything).returns({
        :code => 0,
        :data => @account.person.id
      })
    end

    it "should return an instance of Occam::Account" do
      Occam::Account.create(@username, @password)
                    .must_be_instance_of Occam::Account
    end

    it "should return an Account with a valid token" do
      Occam::Account.create(@username, @password)
                    .token.must_equal Occam::Account.generateToken(@username, @password)[:token]
    end

    it "should return an Account with a Person" do
      Occam::Account.create(@username, @password)
                    .person.id.must_equal @account.person.id
    end
  end

  describe "#hasRole?" do
    it "should return true for roles that exist within the Account" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      account.hasRole?("foo").must_equal true
    end

    it "should return false for roles that do not exist" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      account.hasRole?("bar").must_equal false
    end
  end

  describe "#exists?" do
    it "should return true when the account exists" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      account.exists?.must_equal true
    end

    it "should return false when the account does not exist" do
      # Creating a bogus Account (without using new_person/new_account)
      account = Occam::Account.new(:token => "abc", :person_id => "foo")
      account.exists?.must_equal false
    end
  end
end
