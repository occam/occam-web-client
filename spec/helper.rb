require_relative '../lib/silence_warnings'

# Ensure we always point to our nonsense Occam root just in case
specPath  = File.dirname(__FILE__)
occamPath = File.join(specPath, ".occam")
ENV['OCCAM_ROOT'] = occamPath

# Code Coverage
require 'simplecov'

SimpleCov.profiles.define 'occam' do
  add_filter 'spec/'
  add_filter 'config/'
  add_filter 'key/'

  add_group 'Controllers', 'controllers'
  add_group 'Models',      'models'
  add_group 'Helpers',     'helpers'
  add_group 'Libraries',   'lib'
  add_group 'Views',       'views'

  command_name "Minitest"
end

SimpleCov.start 'occam'

silence_warnings do
  require "rack/test"

  # Load the testing framework
  require 'minitest/spec'
  require 'minitest/reporters'

  if ENV["OUTPUT"] == "SPEC"
    Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new]
  elsif ENV["OUTPUT"] == "JSON"
    require 'minitest/reporters/json_reporter'

    class MiniTest::Reporter
      def io
        if !defined?(@_file)
          @_file = StringIO.new("")
        end

        @_file
      end
    end

    puts "Using the JSON reporter. Look for a file in 'spec/tests.json'"

    reporter = Minitest::Reporters::JsonReporter.new(:verbose => true)

    Minitest::Reporters.use! [ reporter ]

    Minitest.after_run do
      # Format our own test report
      #
      specPath  = File.dirname(__FILE__)
      f = reporter.io
      f.seek(0)
      test_data = JSON.parse(f.read, :symbolize_names => true)

      # Gather groups
      tests = (test_data[:passes] || []) + (test_data[:fails] || []) + (test_data[:skips] || []) + (test_data[:errors] || [])

      output = {}
      output[:metadata]   = test_data[:metadata]
      output[:statistics] = test_data[:statistics]
      output[:timings]    = test_data[:timings]
      output[:groups]     = {}

      output[:groups][:models] = {
        :name => "Models",
        :describes => {}
      }

      output[:groups][:helpers] = {
        :name => "Helpers",
        :describes => {}
      }

      output[:groups][:libraries] = {
        :name => "Libraries",
        :describes => {}
      }

      output[:groups][:acceptance] = {
        :name => "Acceptance",
        :features => {}
      }

      tests.each do |test|
        test[:number] = test[:name].match(/^test_(\d+)_/)[1].to_i
        if test[:class].match(/{:type\s*=>\s*:feature}/)
          feature = test[:class].gsub(/::{:type.*$/, "")
          test[:scenario] = test[:name].match(/^test_\d+_(.*)$/)[1]

          output[:groups][:acceptance][:features][feature] ||= {
            :feature => feature,
          }
          output[:groups][:acceptance][:features][feature][:tests] ||= []
          output[:groups][:acceptance][:features][feature][:tests] << test
        else
          section = test[:class].match(/::{:type=>:([^}]+)}/)
          if section
            section = section[1]
          end
          test[:class] = test[:class].gsub(/::{:type[^}]+}/, "")
          sections = test[:class].split("::")
          sections = [sections[0..1].join("::")] + sections[2..-1]

          if section
            section = section.intern
          else
            section = :models
          end

          type = :class
          if section == :helper
            section = :helpers
            type    = :module
          elsif section == :library
            section = :libraries
            type    = :class
          end

          currentSection = output[:groups][section]
          sections.each_with_index do |describe, i|
            currentSection[:describes] ||= {}
            currentSection = currentSection[:describes]
            currentSection[describe] ||= {}

            currentSection[describe][:name] = describe
            if i == 0
              currentSection[describe][:type] = type
            elsif i == 1
              if describe.start_with?("#")
                currentSection[describe][:type] = :method
              else
                currentSection[describe][:type] = :function
              end
            else
              currentSection[describe][:type] = :group
            end

            currentSection = currentSection[describe]
          end

          test[:it] = test[:name].match(/^test_\d+_(.*)$/)[1]
          currentSection[:tests] ||= []
          currentSection[:tests] << test
        end
        test.delete :class
        test.delete :name
      end

      of = File.open(File.join(specPath, "tests.json"), "w+")
      of.write(JSON.pretty_generate(output))
      of.close
    end
  end

  require 'minitest/autorun'
end

require_relative "../lib/application"

# Creates a fake random uuid
def uuid()
  # Random UUID4
  SecureRandom.uuid
end

# Creates a normal SHA256 multihash
def multihash()
  require 'multihashes'
  require 'digest'
  require 'base58'

  digest = Digest::SHA256.digest(uuid())
  multihash_binary_string = Multihashes.encode digest, 'sha2-256'
  Base58.binary_to_base58(multihash_binary_string, :bitcoin)
end

# Creates a fake random revision
def revision()
  # Random SHA1
  SecureRandom.hex(40)
end

class MiniTest::Test
  remove_method :setup if respond_to? :setup
  def setup
  end

  alias_method :setup_base, :setup

  remove_method :teardown if respond_to? :teardown
  def teardown
  end

  alias_method :teardown_base, :teardown
end
