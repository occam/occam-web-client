**Occam** (If you like acronyms, you can say it means the Open Curation of
Computation And Metadata) is a project that serve as the catalyst for the tools,
education, and community-building needed to bring openness, accountability,
comparability, and repeatability to software distribution and scientific
digital exploration.
