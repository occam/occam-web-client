<h1 class='credits'>
  <div class="large-icon error"></div>
  <div class="name">
    Acknowledgements
  </div>
</h1>
<div class="bar"></div>

## System Designer and Lead Developer

* [wilkie](http://wilkie.io)

## Developers

* Luís Oliveira
* Chelsea Mafrica
* Junhui Chen
* Jay McAleer
* Gennady Martynenko
* Brian Dicks
* Long Pham
* Ben Moncuso
* Jim Devine
* Nicholas Alberts
* Phillip Faust
* Cullen Strouse
* Christopher Iwaszko

## Artists

OCCAM is supported by the open art from various artists. Without their support
of the commons, we would not be nearly as pretty.

<div style="margin: 15px">
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/gear.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/recipe.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/fork.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/person.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/objects/simulator.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/pillar.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/objects/benchmark.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/objects/game.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/workflowItemConfiguration.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/workflowItemView.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/bookmark.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/role_administrator.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/objects/paper.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/ui/login.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/objects/installer.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/objects/album.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/objects/audio.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/metadata/license.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/save.svg"> </div>
</div>

* Gear designed by [Reed Enger](https://www.thenounproject.com/reed) from the [Noun Project](https://thenounproject.com).
* Flask designed by [Renan Ferreira Santos](https://www.thenounproject.com/renanfsdesign) from the [Noun Project](https://thenounproject.com).
* Fork designed by [Dmitry Baranovskiy](https://www.thenounproject.com/DmitryBaranovskiy) from the [Noun Project](https://thenounproject.com).
* User designed by [Andreas Bjurenborg](https://www.thenounproject.com/andreas.bjurenborg) from the [Noun Project](https://thenounproject.com).
* Cpu based on design by [Michael Anthony](https://www.thenounproject.com/m.j.anthony) from the [Noun Project](https://thenounproject.com).
* Column based on design by [Benedikt Martens](https://www.thenounproject.com/Benedikt) from the [Noun Project](https://thenounproject.com).
* Gear/Magnifying Glass based on design by [irene hoffman](https://www.thenounproject.com/i) from the [Noun Project](https://thenounproject.com).
* Game Controller designed by [Jack](https://www.thenounproject.com/SkeletonJack666) from the [Noun Project](https://thenounproject.com).
* Wrench and Screwdriver by [Cheesefork](https://thenounproject.com/Cheesefork) from the [Noun Project](https://thenounproject.com).
* Magnifying Glass based on design by [irene hoffman](https://www.thenounproject.com/i) from the [Noun Project](https://thenounproject.com).
* Bookmark based on design by [icon 54](https://www.thenounproject.com/icon54app) from the [Noun Project](https://thenounproject.com).
* Gear by [Shawn Erdely](https://thenounproject.com/shawn4) from the [Noun Project](https://thenounproject.com).
* Diagram by [Aha-Soft](https://thenounproject.com/ahasoft) from the [Noun Project](https://thenounproject.com).
* Lock by [Claudio Gomboli](https://thenounproject.com/clagom) from the [Noun Project](https://thenounproject.com).
* Custom Software by [www.yugudesign.com](https://thenounproject.com/YuguDesign) from the [Noun Project](https://thenounproject.com).
* Album by [Hello Many](https://thenounproject.com/HelloMany) from the [Noun Project](https://thenounproject.com).
* Sound Wave by [Kid A](https://thenounproject.com/indygo) from the [Noun Project](https://thenounproject.com).
* Scale by [Nikhil Dev](https://thenounproject.com/yendev) from the [Noun Project](https://thenounproject.com).
* Save by [cathy moser](https://thenounproject.com/cathymoser) from the [Noun Project](http://thenounproject.com).

## Open Source Software

The OCCAM software project is proud to use and support the following software
projects. This is nowhere near a complete list of open source software that we
make use of. That list is almost impractical to generate!

### Server

* [Ruby](https://www.ruby-lang.org/en/) - an expressive scripting language.
* [haml](http://haml.info/) - markup preprocessor for html.
* [Sass](http://sass-lang.com/) - markup preprocessor for css.
* [minitest](https://github.com/seattlerb/minitest) - minimal testing framework.
* [mocha](http://gofreerange.com/mocha/docs/) - mocking/stubbing framework for testing.
* [postgres](http://www.postgresql.org/) - robust SQL database implementation for deployed environments.
* [Sinatra](http://www.sinatrarb.com/) - a minimalistic web framework for Ruby.
* [puma](http://puma.io/) - a minimal webserver we use for development.
* [chronic_duration](https://github.com/hpoydar/chronic_duration) - a library to pretty print time durations.
* [i18n](http://rails-i18n.org/wiki) - internationalization library.
* [redcarpet](https://github.com/vmg/redcarpet) - markdown parsing library.
* [capybara](https://jnicklas.github.io/capybara/) - acceptance testing selenium driver for the front-end.

### Occam Toolchain

* [Python 3](https://python.org) - A robust scripting language.
* [pycrypto](https://pypi.python.org/pypi/pycrypto) - Crypto helper library.
* [pyyaml](http://pyyaml.org/) - YAML parser and library.
* [bcrypt](https://pypi.python.org/pypi/bcrypt) - Python bcrypt implementation.
* [pytest](http://pytest.org/) - Python testing framework.

### Javascript and Visualization Tools

* [Spectrum](https://bgrins.github.io/spectrum/) - Javascript color picker library.
* [node-uuid.js](https://github.com/broofa/node-uuid) - Javascript UUID generation library.
* [noVNC](http://kanaka.github.io/noVNC/) - Javascript vnc client library.
* [jQuery](http://jquery.com/) - Javascript document manipulation library.
* [d3](http://d3js.org/) - Data-oriented Visualization and SVG library.
