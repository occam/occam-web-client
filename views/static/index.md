![fullwidth|!OCCAM provides architecture simulations on demand](/images/logo_large.svg)

OCCAM (Open Curation for Computer Architecture Modeling) is a project that will
serve as the catalyst for the tools, education, and community-building needed to
bring openness, personability, comparability, and repeatability to computer
architecture experimentation.

[Developers](/simulators/new) will benefit by being able to easily distribute
their benchmarks.

[Experimentalists](/experiments/new) will be able to make use of developers'
work and rigorously evaluate their novel architecture capabilities.

## System
* [Configuration](/system)

## People

* [All](/people)
* [Sign up](/people/new)

## Simulators

* [All](/simulators)
* [Create](/simulators/new)

## Worksets

* [All](/worksets)
* [Create](/worksets/new)

## Experiments

* [All](/experiments)
* [Create](/experiments/new)
