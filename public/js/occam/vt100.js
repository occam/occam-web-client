/*
 * This module implements a terminal emulator for VT100-esque terminal commands.
 * This is used by the Terminal class when in TTY mode to display an accurate
 * terminal window much like expected on UNIX style systems.
 */

var initOccamVT100 = function(Occam) {
  var VT100 = Occam.VT100 = {};

  VT100.BUFFER_MAX = 500;

  VT100.initializeContext = function() {
    return {
      "lineCount": 0,
      "buffer": {"mode": "normal", "saved": []},
      "cursor": {"visible": true, "blinking": false},
      "scroll": {"start": 0, "end": 0},
      "saved": {"x": 0, "y": 0, "fg": "#ddd", "bg": "#333"},
      "lineWrap": true,
      "charHeight": 0,
      "charWidth":  0,
      "height":     40,
      "width":      80,
      "x":  0,
      "y":  0,
      "fg": "#ddd",
      "bg": "#333",
      "caret": $("<div></div>").addClass("caret")
    };
  };

  VT100.hideCaret = function(element, context) {
    context.cursor.visible = false;
    context.caret.css({
      "visibility": "hidden"
    });
  };

  VT100.showCaret = function(element, context) {
    context.cursor.visible = true;
    context.caret.css({
      "visibility": "visible"
    });
  };

  VT100.fitCoords = function(x, y, element, context, fromCaret, scrollDown) {
    if (fromCaret == undefined) {
      fromCaret = false;
    }

    if (scrollDown == undefined) {
      scrollDown = false;
    }

    if (y > context.scroll.end) {
      if (fromCaret && scrollDown) {
        VT100.addLines(y - context.scroll.end, element, context, context.scroll.end + 1);
        y = context.scroll.end;
      }
    }
    if (y < 0) {
      y = 0;
    }
    if (x >= context.width) {
      x = context.width - 1;
    }
    if (x < 0) {
      x = 0;
    }

    return {
      "x": x,
      "y": y
    };
  };

  VT100.move = function(x, y, element, context, fromCaret, scrollDown) {
    if (fromCaret == undefined) {
      fromCaret = false;
    }

    if (scrollDown == undefined) {
      scrollDown = false;
    }

    var pos = VT100.fitCoords(x, y, element, context, fromCaret, scrollDown);
    context.x = pos.x;
    context.y = pos.y;

    var lines       = element.children('.line');
    var currentLine = lines.eq(lines.length-(context.height - context.y));

    // Update the caret text
    context.caret.text(VT100.charAt(x, y, element, context));

    // Move the caret
    context.caret.css({
      "left": currentLine[0].offsetLeft + context.x * context.charWidth,
      "top":  currentLine.position().top + element[0].scrollTop,
      "background-color": context.fg,
      "color": context.bg
    });
  };

  VT100.deleteLines = function(count, element, context, position) {
    if (position == undefined) {
      // Remove last line, count times
      position = context.height-1;
    }

    var lines = element.children('.line');

    for (var i = 0; i < count; i++) {
      lines.eq(lines.length - context.height + position).remove();
    }
  };

  VT100.addLines = function(count, element, context, position) {
    if (position == undefined) {
      // Add to end
      position = context.height;
    }

    var lines = element.children('.line');
    var beforeLine = lines.eq(lines.length - context.height + position);

    var blankLine = new Array(context.width + 1).join("&nbsp;");
    for (var i = 0; i < count; i++) {
      var line = $("<div class='line'></div>")
      if (position == context.height) {
        element.append(line);
      }
      else {
        beforeLine.before(line);
      }

      context.lineCount++;

      // Remove first line if the buffer is full
      while (context.lineCount > VT100.BUFFER_MAX) {
        element.children('.line:first').remove();
        context.lineCount--;
      }

      var appendSpan = $("<span style='color:" + context.fg + ";background-color:" + context.bg + ";'>" + blankLine + "</span>");
      line.append(appendSpan);
    }
  };

  VT100.lineAt = function(y, element, context) {
    var lines = element.children('.line');
    var currentLine = lines.eq(lines.length-(context.height - y));

    var ret = "";

    currentLine.children('span').each(function() {
      var span = $(this);
      ret += span.text();
    });

    return ret;
  };

  VT100.charAt = function(x, y, element, context) {
    return VT100.lineAt(y, element, context)[x];
  };

  VT100.print = function(x, y, data, element, context, fromCaret, scrollDown) {
    if (fromCaret == undefined) {
      fromCaret = false;
    }

    if (scrollDown == undefined) {
      scrollDown = false;
    }

    var pos = VT100.fitCoords(x, y, element, context, fromCaret, scrollDown);
    x = pos.x;
    y = pos.y;

    if (data.length == 0) {
      return {
        "x": x,
        "y": y
      };
    }

    // Look at coordinates and append the text with the current context
    // Combine contexts when appropriate
    // Newlines/backspaces/tabs are resolved here
    data = data.replace("\t", "    ");

    var lines      = element.children('.line');
    var lineCount  = lines.length;

    var currentLine = lines.eq(lines.length-(context.height - y));
    var currentSpan = null;
    var currentX = 0;
    currentLine.children().each(function() {
      currentSpan = $(this);
      if (currentSpan.text().length > 0 && (currentX + currentSpan.text().length >= x)) {
        // Split this span
        var splitPosition = x - currentX;
        var tmp = currentSpan.clone();
        var left = currentSpan.text().substring(0, splitPosition);
        var right = currentSpan.text().substring(splitPosition);
        currentSpan.text(left);
        tmp.text(right);

        if (right.length > 0) {
          currentSpan.after(tmp);
        }

        if (splitPosition == 0) {
          currentSpan.remove();
          currentSpan = tmp;
        }

        if (right.length > 0) {
          currentSpan = tmp;
        }
        else {
          currentSpan = currentSpan.next();
        }

        currentX = 0;
        return false;
      }
      currentX += currentSpan.text().length;
    });

    var appendSpan = $("<span style='color:" + context.fg + ";background-color:" + context.bg + ";'>" + "</span>");
    currentSpan.before(appendSpan);

    for (var i = 0, len = data.length; i < len; i++) {
      // Characters that precede us on the line stay there
      // Characters are overwritten as we write to the terminal
      var chr = data[i];
      if (chr == "\x07") {
        console.log("ding");
        // Bell
        continue;
      }
      else if (chr == " ") {
        // All spaces should be non-breaking
        chr = "\u00a0";
      }
      else if (chr == "\r") {
        // Go to beginning of line
        if (x != 0) {
          if (appendSpan.text().length == 0) {
            appendSpan.remove();
          }
          return VT100.print(0, y, data.substring(i+1), element, context, fromCaret);
        }
        continue;
      }
      else if (chr == "\b") {
        // Backspace, just jump back one character
        if (x > 0) {
          if (appendSpan.text().length == 0) {
            appendSpan.remove();
          }
          return VT100.print(x - 1, y, data.substring(i+1), element, context, fromCaret);
        }
        continue;
      }
      else if (chr == "\n") {
        // Go to next line
        if (appendSpan.text().length == 0) {
          appendSpan.remove();
        }
        return VT100.print(0, y + 1, data.substring(i+1), element, context, fromCaret, true);
      }

      if (chr != "") {
        // Add character to our span
        appendSpan.text(appendSpan.text() + chr);

        // Overwrite the current character
        // Maintain the context throughout
        if (currentSpan == null) {
        }
        else if (currentSpan.text().length == 1) {
          // Destroy this span
          var tmp = currentSpan;
          currentSpan = currentSpan.next();
          tmp.remove();
        }
        else {
          // Remove first character of current span
          currentSpan.text(currentSpan.text().substring(1));
        }

        x++;
        if (x >= context.width) {
          if (appendSpan.text().length == 0) {
            appendSpan.remove();
          }
          return VT100.print(0, y + 1, data.substring(i+1), element, context, fromCaret, context.lineWrap);
        }
      }
    }

    if (appendSpan.text().length == 0) {
      appendSpan.remove();
    }

    return {
      "x": x,
      "y": y
    };
  }

  VT100.interpretHome = function(values, element, context) {
    // Cursor move
    if (values[0] == "") {
      values[0] = 1;
    }
    if (values.length < 2) {
      values[1] = "";
    }
    if (values[1] == "") {
      values[1] = 1;
    }
    if (values[0] < 1) {
      values[0] = 1;
    }
    if (values[1] < 1) {
      values[0] = 1;
    }
    VT100.move(values[1] - 1, values[0] - 1, element, context);
  }

  VT100.interpretScrollSet = function(values, element, context) {
    // Scroll set
    if (values.length < 2) {
      values[0] = 1;
      values[1] = context.height;
    }

    // values[0] - start row
    // values[1] - end row
    context.scroll.start = values[0] - 1;
    context.scroll.end   = values[1] - 1;
  }

  VT100.interpretCursorSave = function(values, element, context) {
    // Save cursor position
    context.saved.x = context.x;
    context.saved.y = context.y;
  }

  VT100.interpretCursorRestore = function(values, element, context) {
    VT100.move(context.saved.x, context.saved.y, element, context);
  }

  VT100.interpretQueryCursorPosition = function(values, element, context) {
    context.response += "\e[" + context.y + 1 + ";" + context.x + 1 + "R";
  }

  VT100.interpretSettingsReset = function(values, element, context) {
    // Terminal settings reset
    context.lineWrap = true;
    context.scroll.start = 0;
    context.scroll.end = context.height - 1;
  }

  VT100.interpretClearLine = function(values, element, context) {
    if (values.length == 0 || values[0] == 0 || values[0] == "") {
      // Clear until end of the line
      var blankLine = new Array(context.width - context.x + 1).join(" ");
      VT100.print(context.x, context.y, blankLine, element, context);
    }
    else if (values[0] == 1) {
      // Clear from cursor to beginning of line
      var blankLine = new Array(context.x + 1).join(" ");
      VT100.print(0, context.y, blankLine, element, context);
    }
    else if (values[0] == 2) {
      // Clear entire line
      var blankLine = new Array(context.width + 1).join(" ");
      VT100.print(0, context.y, blankLine, element, context);
    }
  }

  VT100.interpretSendDevice = function(values, element, context) {
    if (values.length == 0) {
      values[0] = 0;
    }

    if (values[0] == 0) {
      // Respond with terminal id code
      // Tell them we are a VT100, firmware patch 95, rom 0 (a don't care value)
      context.response += "\x1b>0;95;0c";
    }
  }

  VT100.interpretModeClear = function(values, element, context) {
    // xterm DEC Private Mode Set
    if (values.length == 0) {
    }
    else {
      values.forEach(function(value) {
        if (value == 7) {
          // Line wrap disable
          context.lineWrap = false;
        }
        else if (value == 12) {
          // Stop blinking cursor
          context.cursor.blinking = false;
        }
        else if (value == 25) {
          // Show Cursor
          VT100.hideCaret(element, context);
        }
        else if (value == 1049) {
          // Go back to normal buffer from alternative buffer
          context.buffer.mode = "normal";

          // Restore buffer
          context.buffer.saved.each(function() {
            element.append($(this).clone());
          });

          // Restore cursor
          VT100.move(context.saved.x, context.saved.y, element, context);
        }
      });
    }
  }

  VT100.interpretModeSet = function(values, element, context) {
    // xterm DEC Private Mode Set
    if (values.length == 0) {
    }
    else {
      values.forEach(function(value) {
        if (value == 7) {
          // Line wrap enable
          context.lineWrap = true;
        }
        else if (value == 12) {
          // Start blinking cursor
          context.cursor.blinking = true;
        }
        else if (value == 25) {
          // Show Cursor
          VT100.showCaret(element, context);
        }
        else if (value == 1049) {
          // Save buffer
          var lines = element.children('.line');
          context.buffer.saved = lines.slice(lines.length - context.height);

          // Clear buffer (interpret a 2J code)
          VT100.interpretClear(false, [2], element, context);

          // Go to alternative buffer, clearing it
          context.buffer.mode = "alternate";

          // Save cursor
          context.saved.x = context.x;
          context.saved.y = context.y;
        }
      });
    }
  }

  VT100.interpretClear = function(values, element, context) {
    if (values.length == 0 || values[0] == 0) {
      // Clear from cursor to end of screen

      // Clear rest of this line
      var blankLine = new Array(context.width - context.x + 1).join(" ");
      VT100.print(context.x, context.y, blankLine, element, context);

      // Clear rest of screen
      blankLine = new Array(context.width + 1).join(" ");
      for (var i = context.y+1; i < context.height; i++) {
        VT100.print(0, i, blankLine, element, context);
      }
    }
    else if (values[0] == 1) {
      // Clear from cursor to beginning of screen

      // Clear from cursor to beginning of line
      var blankLine = new Array(context.x + 1).join(" ");
      VT100.print(0, context.y, blankLine, element, context);

      // Clear rest of screen
      blankLine = new Array(context.width + 1).join(" ");
      for (var i = 0; i < context.y; i++) {
        VT100.print(0, i, blankLine, element, context);
      }
    }
    else if (values[0] == 2) {
      // Clear screen.
      VT100.addLines(context.height, element, context);
    }
  }

  VT100.interpretCursorUp = function(values, element, context) {
    if (values.length == 0 || values[0] == "") {
      values[0] = 1;
    }
    VT100.move(context.x, context.y - values[0], element, context);
  }

  VT100.interpretCursorDown = function(values, element, context) {
    // Cursor down
    if (values.length == 0 || values[0] == "") {
      values[0] = 1;
    }
    VT100.move(context.x, context.y + values[0], element, context);
  }

  VT100.interpretCursorForward = function(values, element, context) {
    if (values.length == 0 || values[0] == "") {
      values[0] = 1;
    }
    VT100.move(context.x + values[0], context.y, element, context);
  }

  VT100.interpretCursorBack = function(values, element, context) {
    if (values.length == 0 || values[0] == "") {
      values[0] = 1;
    }
    VT100.move(context.x - values[0], context.y, element, context);
  }

  VT100.interpretNextLine = function(values, element, context) {
    if (values.length == 0) {
      values[0] = 1;
    }
    VT100.move(0, context.y + values[0], element, context);
  }

  VT100.interpretPreviousLine = function(values, element, context) {
    if (values.length == 0) {
      values[0] = 1;
    }
    VT100.move(0, context.y - values[0], element, context);
  }

  VT100.interpretUpdateAttributes = function(values, element, context) {
    // Change color
    var new_fg_color = context.fg;
    var new_bg_color = context.bg;

    var bright = false;

    var colors = [
      "#333",              // Black
      "rgb(194, 54, 33)",  // Red
      "rgb(37, 188, 36)",  // Green
      "rgb(173, 173, 39)", // Yellow
      "rgb(73, 46, 225)",  // Blue
      "rgb(211, 56, 211)", // Magenta
      "rgb(51, 187, 200)", // Cyan
      "#ddd"               // White
    ];

    var brights = [
      "rgb(85, 85, 85)",   // Black
      "rgb(252, 57, 31)",  // Red
      "rgb(49, 231, 34)",  // Green
      "rgb(234, 236, 35)", // Yellow
      "rgb(88, 51, 255)",  // Blue
      "rgb(249, 53, 248)", // Magenta
      "rgb(20, 240, 240)", // Cyan
      "#fff"               // White
    ];

    if (values.length == 0) {
      values = [0];
    }

    // Check for bright flag
    values.forEach(function(value) {
      if (value == 1) {
        bright = true;
      }
    });

    // Go through color values and other parameters
    values.forEach(function(value) {
      if (value == 0) {
        // Return to default
        new_fg_color = colors[7];
        new_bg_color = colors[0];
      }
      else if (value >= 30 && value <= 37) {
        if (bright) {
          new_fg_color = brights[value - 30];
        }
        else {
          new_fg_color = colors[value - 30];
        }
      }
      else if (value >= 40 && value <= 47) {
        if (bright) {
          new_bg_color = brights[value - 40];
        }
        else {
          new_bg_color = colors[value - 40];
        }
      }
    });

    if (new_fg_color != context.fg || new_bg_color != context.bg) {
      context.fg = new_fg_color;
      context.bg = new_bg_color;
    }
  }

  VT100.interpretInsertLines = function(values, element, context) {
    if (values.length == 0 || values[0] == "") {
      values[0] = 1;
    }

    // Add lines to the top
    VT100.addLines(values[0], element, context, context.y);

    // Remove lines from the bottom
    VT100.deleteLines(values[0], element, context, context.scroll.end);
  }

  VT100.interpretCode = function(code, values, element, context) {
    if (code == "H" || code == "f") {
      VT100.interpretHome(values, element, context);
    }
    else if (code == "r") {
      VT100.interpretScrollSet(values, element, context);
    }
    else if (code == "s") {
      VT100.interpretCursorSave(values, element, context);
    }
    else if (code == "u") {
      VT100.interpretCursorRestore(values, element, context);
    }
    else if (code == "n") {
      VT100.interpretQueryCursorPosition(values, element, context);
    }
    else if (code == "c") {
      VT100.interpretSettingsReset(values, element, context);
    }
    else if (code == "K") {
      VT100.interpretClearLine(values, element, context);
    }
    else if (code == ">c") {
      VT100.interpretSendDevice(values, element, context);
    }
    else if (code == "l" || code == "?l") {
      VT100.interpretModeClear(values, element, context);
    }
    else if (code == "h" || code == "?h") {
      VT100.interpretModeSet(values, element, context);
    }
    else if (code == "J") { // Clear
      VT100.interpretClear(values, element, context);
    }
    else if (code == "A") {
      VT100.interpretCursorUp(values, element, context);
    }
    else if (code == "B") {
      VT100.interpretCursorDown(values, element, context);
    }
    else if (code == "C") {
      VT100.interpretCursorForward(values, element, context);
    }
    else if (code == "D") {
      VT100.interpretCursorBack(values, element, context);
    }
    else if (code == "E") {
      VT100.interpretNextLine(values, element, context);
    }
    else if (code == "F") {
      VT100.interpretPreviousLine(values, element, context);
    }
    else if (code == "m") {
      VT100.interpretUpdateAttributes(values, element, context);
    }
    else if (code == "L") {
      VT100.interpretInsertLines(values, element, context);
    }
    else {
      console.log("unimplemented code: " + code + " values: " + values);
    }
  };

  VT100.append = function(data, element, context) {
    var pos = VT100.print(context.x, context.y, data, element, context, true);
    VT100.move(pos.x, pos.y, element, context, true);
  };

  VT100.parse = function(data, element, context) {
    context.response = "";

    // Replace escaped colors and such
    // Find each escape sequence and interpret it
    var esc_sequence_re = new RegExp("\u001b\\[([?>!])?(?:(?:(\\d*);)+)?(\\d*)([a-zA-Z])", 'g');
    var re = new RegExp("\u001b", 'g');

    var lastIndex = 0;
    var match = null;
    while ((match = re.exec(data)) != null) {
      // Is this a single code or an escape sequence?
      var chr = data[match.index+1];
      if (chr == "=") {
        // Alternate Keypad Mode
      }
      else if (chr != '[') {
        console.log("Escape sequence: " + chr);
      }
      else if (chr == '[') {
        esc_sequence_re.lastIndex = match.index
        if ((match = esc_sequence_re.exec(data)) != null) {
          // Interpret code
          var code = match[match.length-1];

          VT100.append(data.substring(lastIndex, match.index), element, context);
          lastIndex = esc_sequence_re.lastIndex;

          values = match.slice(2,match.length-1);
          if (values[0] == undefined) {
            values = values.slice(1);
          }

          if (match[1] == undefined) {
            match[1] = "";
          }
          code = match[1] + code;

          values = values.map(function(value) {
            if (value == "") {
              return "";
            }
            else {
              return parseInt(value);
            }
          });

          VT100.interpretCode(code, values, element, context);
        }
      }
    }

    VT100.append(data.substring(lastIndex), element, context);

    return context.response;
  };
};
