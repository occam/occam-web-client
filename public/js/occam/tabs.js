/*
 * This module handles Tab strips on the site. It allows you to wrap a ul.tabs
 * element and provides functions to add a tab, set the current tab, etc.
 */

var initOccamTabs = function(Occam) {
  'use strict';

  /*
   * This constructor creates an object that represents a tabstrip. You pass in
   * the jQuery element for the tabstrip to attach the object to the given
   * element on the page. If more than one element are passed, the first is
   * used.
   */
  var Tabs = Occam.Tabs = function(element) {
    if (element) {
      this.element = element;
      if (element.hasAttribute("data-panels")) {
        this.panels = document.querySelector(".tab-panels#" + element.getAttribute("data-panels"));
      }
      if (!(this.panels)) {
        this.panels = element.parentNode.querySelector('.tabs + .tab-panels');
      }
      if (!(this.panels)) {
        this.panels = element.parentNode.parentNode.querySelector('* > .tab-panels');
      }
      if (this.element.getAttribute('data-tabs-index') == null) {
        Tabs._count++;

        this.element.setAttribute('data-tabs-index', Tabs._count);
        Tabs._loaded[this.element.getAttribute('data-tabs-index')] = this;
      }

      // Whether or not there is a sidebar reveal button
      this.containsSidebarButton = !!element.querySelector(".tab.sidebar:not(:last-of-type)");
      this.containsRightSidebarButton = !!element.querySelector(".tab.sidebar:last-of-type");

      // Event handlers
      this.events = {};

      // Bind event handlers
      this.bindEvents();
    }
    else {
      throw("Requires an element to be passed");
    }
  };

  Tabs._count = 0;
  Tabs._loaded = {};

  Tabs.loadAll = function(element) {
    element.querySelectorAll('ul.tabs').forEach(function(tabElement) {
      Tabs.load(tabElement);
    });
  };

  Tabs.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element;
    if (typeof element != "number") {
      index = element.getAttribute('data-tabs-index');
    }

    if (index) {
      return Occam.Tabs._loaded[index];
    }

    return new Occam.Tabs(element);
  };

  Tabs.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Tabs.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  /*
   * Ensures that the sidebar button, if it exists, is shown.
   */
  Tabs.prototype.showSidebarButton = function(event) {
    var sidebarButton = this.element.querySelector(".tab.sidebar");
    if (sidebarButton) {
      sidebarButton.classList.remove("reveal");
    }
  };

  /*
   * This method is the event callback that will reveal the bound tab.
   */
  Tabs.revealTabEvent = function(event) {
    var self = Occam.Tabs.load(this.parentNode);

    // Get the index of the tab that invoked the event
    var index = getChildIndex(this, "li");
    if (self.containsSidebarButton) {
      index = index - 1;

      if (index == -1) {
        // Sidebar button pressed
        // Reveal!
        this.classList.add("reveal");

        // Trigger sidebar event
        self.trigger('sidebar');

        return;
      }
    }

    if (self.containsRightSidebarButton) {
      if (this.classList.contains("sidebar")) {
        // Reveal!
        this.classList.add("reveal");

        // Trigger sidebar event
        self.trigger('sidebar');

        return;
      }
    }

    // Select that tab
    self.select(index);
  };

  Tabs.prototype.updateState = function(state) {
    this.select(state.index, true);
  };

  /*
   * This method returns the unique index for this tab strip.
   */
  Tabs.prototype.index = function() {
    return parseInt(this.element.getAttribute('data-tabs-index'));
  };

  /*
   * This function selects the given tab by its index or the element.
   *
   * The first tab is at index 0.
   */
  Tabs.prototype.select = function(index, doNotPushState) {
    if (typeof index == "object") {
      index = getChildIndex(index, "li");
    }
    else {
      index = parseInt(index);
    }

    var oldIndex = this.selected();

    if (oldIndex == index) {
      return;
    }

    var tabIndex = index;
    if (this.containsSidebarButton) {
      tabIndex = index + 1;
    }

    var tab = this.element.querySelector('li.tab:nth-of-type(' + (tabIndex + 1) + ')');
    if (!tab) {
      // Select last
      tab = this.element.querySelector('li.tab:last-of-type');
    }
    if (tab && tab.classList.contains("sidebar")) {
      tab = tab.previousElementSibling;
      if (!tab.matches("li")) {
        tab = null;
      }
    }
    if (!tab) {
      return;
    }

    tab.parentNode.querySelectorAll(':scope > .active').forEach(function(item) {
      var link = item.querySelector("a");
      item.classList.remove('active');
      link.setAttribute("tabindex", "-1");
      link.setAttribute("aria-selected", "false");
    });

    var link = tab.querySelector('a');
    link.setAttribute("aria-selected", "true");
    link.setAttribute("tabindex", "0");
    tab.classList.add('active');

    // Hide tab
    var currentTab = this.panels.querySelector(':scope > .tab-panel.active');
    if (currentTab) {
      currentTab.classList.remove('active');
      currentTab.setAttribute('aria-hidden', 'true');
    }

    // Reveal tab
    var tabPanel = this.panels.querySelector(':scope > .tab-panel:nth-of-type(' + (index + 1) + ')');
    if (!tabPanel) {
      tabPanel = this.panels.querySelector(':scope > .tab-panel:last-of-type');
    }
    tabPanel.classList.add('active');
    tabPanel.setAttribute('aria-hidden', 'false');

    // Dynamic load, if necessary
    if (link.getAttribute('data-pjax') === "true") {
      link.setAttribute('data-pjax', 'complete');

      Occam.Util.get(link.getAttribute('href'), function(html) {
        tabPanel.classList.remove('unloaded');
        tabPanel.innerHTML = html;

        Occam.loadAll(tabPanel);
      }, "text/html");
    }

    // Update the browser url
    if (this.element.hasAttribute("data-push-navigation")) {
      if (!doNotPushState) {
        var state = {"tab": oldIndex, "html": "", "pageTitle": ""};
        Occam.NavigationState.pushState("Tabs", this.index(), {'index': oldIndex}, {'index': index}, tab.querySelector('a').getAttribute('href'), "");
      }
    }
    if (this.element.hasAttribute("data-replace-navigation")) {
      Occam.NavigationState.updateLocation(tab.querySelector('a').getAttribute('href'));
    }

    this.trigger("change", index);
    
    return this;
  };

  /*
   * This method will return the index of the currently selected tab.
   */
  Tabs.prototype.selected = function() {
    var self = this;

    var currentTab = self.element.querySelector(':scope > li.tab.active');

    if (!currentTab) {
      return -1;
    }

    var index = getChildIndex(currentTab, "li");

    if (this.containsSidebarButton) {
      index = index + 1;
    }

    return index;
  };

  /*
   * This method will ensure that tab events are bound to this tabstrip. You
   * can give a jQuery tab element which will have its events bound individually
   * or, when called without arguments, the events will be bound to all tabs on
   * the strip.
   */
  Tabs.prototype.bindEvents = function(tab) {
    var self = this;
    if (tab) {
      if (tab.classList.contains("tab-bound")) {
        return;
      }
      tab.classList.add("tab-bound");

      tab.addEventListener('mousedown', function(event) {
        // Get the index of the tab that invoked the event
        var index = getChildIndex(this, "li");

        // Do not outline it for a mouse click
        var tab = self.element.querySelector('li.tab:nth-of-type(' + (index + 1) + ') a');
        if (tab) {
          tab.style.outline = "none";
        }
      });
      tab.addEventListener('click', Tabs.revealTabEvent);
      var link = tab.querySelector(':scope > a');
      link.addEventListener('click', function(event) {
        event.preventDefault();
      });

      // Do not allow it to be tabbed to
      link.setAttribute("tabindex", "-1");

      // Allow the current tab to be tabbed to
      var selected = self.element.querySelector(':scope > li.active a');
      if (selected) {
        selected.setAttribute("tabindex", "0");
      }

      // Allow the navigation of the tabstrip via keyboard
      link.addEventListener("keydown", function(event) {
        var toggle = null;
        if (event.keyCode == 37) { // left arrow
          toggle = tab.previousElementSibling;
        }
        else if (event.keyCode == 39) { // right arrow
          toggle = tab.nextElementSibling;
        }
        else if (event.keyCode == 40) { // down arrow
          // Unfocus the tabstrip and go to content
          event.preventDefault();
          event.stopPropagation();

          var panel = self.tabPanelAt(self.selected());
          panel.setAttribute("tabindex", "0");
          panel.focus();
          panel.setAttribute("tabindex", "-1");

          return;
        }

        if (toggle) {
          var index = window.getChildIndex(toggle, "li");
          if (self.containsSidebarButton) {
            index = index - 1;
          }

          self.select(index);
          toggle.querySelector("a").focus();
        }
      });
    }
    else {
      var self = this;
      self.element.querySelectorAll(':scope > li.tab').forEach(function(tab) {
        self.bindEvents(tab);
      });
    }

    return this;
  };

  /*
   * This method adds a tab at the given index with the given name. If no index
   * is given, the tab is appended to the end of the tabstrip (the right-hand
   * side assuming a left-to-right rendering)
   */
  Tabs.prototype.addTab = function(name_or_element, b, c) {
    var callback = b;
    var atIndex = b;

    if (arguments.length > 2) {
      callback = c;
    }
    else {
      callback = null;
    }

    var index = atIndex || this.element.children.length;
    if (atIndex == 0) {
      index = 0;
    }

    var tab = name_or_element;
    if (typeof tab == "string") {
      tab = document.createElement("li");
      tab.classList.add("tab");
      var link = document.createElement("a");
      link.textContent = name_or_element;
      tab.appendChild(link);
    }

    var tabPanel = null;
    if (Array.isArray(name_or_element)) {
      tab = name_or_element[0];
      tabPanel = name_or_element[1];
    }
    else {
      tabPanel = document.createElement("li");
      tabPanel.classList.add("tab-panel");
    }

    tab.classList.remove("active");
    tabPanel.classList.remove("active");

    var referenceTab = this.element.querySelector(':scope > li.tab:nth-of-type(' + (index + 1) + ')');
    var referencePanel = this.panels.querySelector(':scope > li.tab-panel:nth-of-type(' + (index + 1) + ')');

    if (!referenceTab) {
      referenceTab = this.element.querySelector(':scope > li.tab.sidebar:last-of-type');
      referencePanel = null;
    }

    if (!referenceTab) {
      this.element.appendChild(tab);
      this.panels.appendChild(tabPanel);
    }
    else {
      this.element.insertBefore(tab, referenceTab);

      if (referencePanel) {
        this.panels.insertBefore(tabPanel, referencePanel);
      }
      else {
        this.panels.appendChild(tabPanel);
      }
    }

    this.bindEvents(tab);

    if (callback) {
      callback(tabPanel);
    }

    return this;
  };

  /* This method replaces the tab at the given index with the one passed in.
   */
  Tabs.prototype.replaceTab = function(atIndexOrElement, newTab) {
    // What we will do is remove this tab and then add the given elements at this index.
    var atIndex = atIndexOrElement;
    if (typeof atIndex == "object") {
      atIndex = getChildIndex(atIndex, "li");
    }

    var selected = this.selected();

    this.removeTab(atIndexOrElement);
    this.addTab(newTab, atIndex);

    if (selected == atIndex) {
      this.select(atIndex);
    }

    return this;
  };

  /*
   * This method removes the tab at the given index or via the passed in element.
   */
  Tabs.prototype.removeTab = function(atIndexOrElement) {
    var atIndex = atIndexOrElement;
    if (typeof atIndex == "object") {
      atIndex = getChildIndex(atIndex, "li");
    }

    var selected = this.selected();
    this.element.querySelector(':scope > li.tab:nth-of-type(' + (atIndex + 1) + ')').remove();
    this.panels.querySelector(':scope > li.tab-panel:nth-of-type(' + (atIndex + 1) + ')').remove();

    // If this tab was the selected tab, select the next index.
    if (selected == atIndex) {
      this.select(atIndex);
    }

    return this;
  };

  /*
   * This method returns the jQuery element of the tab at the given index.
   */
  Tabs.prototype.tabAt = function(atIndex) {
    return this.element.querySelector(':scope > li.tab:nth-of-type(' + (atIndex + 1) + ')');
  };

  /*
   * This method returns a reference to the tab panels element.
   */
  Tabs.prototype.tabPanels = function() {
    return this.panels;
  };

  /*
   * This method returns the jQuery element of the tab-panel at the given index.
   */
  Tabs.prototype.tabPanelAt = function(atIndex) {
    return this.panels.querySelector(':scope > li.tab-panel:nth-of-type(' + (atIndex + 1) + ')');
  };

  /* This method gives you the number of tabs
   */
  Tabs.prototype.tabCount = function() {
    return this.element.querySelectorAll(':scope > .tab').length;
  };
};

