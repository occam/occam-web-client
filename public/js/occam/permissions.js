/* This file handles the permission and memberships list.
 */

// Initialize all selectors on the page
var initOccamPermissions = function(Occam) {
  var Permissions = Occam.Permissions = function(element) {
    var self = this;

    if (element === undefined) {
      return;
    }

    this.element = element;

    Permissions.count++;
    this.element.setAttribute('data-permissions-index', Permissions.count);

    Permissions._loaded[this.element.getAttribute('data-permissions-index')] = this;

    this.bindEvents();
    this.events = {};
  };

  Permissions.count = 0;
  Permissions._loaded = {};

  Permissions.loadAll = function(element) {
    var elements = element.querySelectorAll('ul.permissions');

    elements.forEach(function(element) {
      Permissions.load(element);
    });
  };

  Permissions.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-permissions-index');

    if (index) {
      return Occam.Permissions._loaded[index];
    }

    return new Occam.Permissions(element);
  };

  Permissions.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Permissions.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  Permissions.prototype.bindRowEvents = function(row) {
    // Override each permission field to create a dropdown instead.
    var items = row.querySelectorAll("span.key");
    items.forEach(function(itemElement) {
      // Value can be "on", "off", or null
      var key   = itemElement.getAttribute("data-key");
      var value = itemElement.getAttribute("data-value");

      // Nullify the form
      var itemForm = itemElement.querySelector("form");
      itemForm.addEventListener("submit", function(event) {
        event.preventDefault();
      });

      // Attach the dropdown events
      var dropdown = Occam.Dropdown.load(itemElement.querySelector(".dropdown-menu"));
      dropdown.on("selected", function(data) {
        // Update value of input
        var inputElement = itemForm.querySelector("input[name=value]");
        var key = itemForm.querySelector("input[name=update]").getAttribute("value");
        if (data.index == 0) {
          itemElement.setAttribute("data-value", "on")
          inputElement.setAttribute("value", "on");
        }
        else if (data.index == 1) {
          itemElement.setAttribute("data-value", "off")
          inputElement.setAttribute("value", "off");
        }
        else {
          itemElement.removeAttribute("data-value")
          inputElement.setAttribute("value", "");
        }

        // Send request
        var oReq = new XMLHttpRequest();
        oReq.addEventListener("load", function() {
        });
        oReq.open("POST", itemForm.getAttribute("action"));
        oReq.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        var data = new FormData(itemForm);
        data.set("update", key);
        oReq.send(data);
      });
    });

    // Override the remove action to asynchronously post the delete request.
    var deleteLink = row.querySelector("input.link.delete");
    if (deleteLink) {
      var deleteForm = deleteLink.parentNode;

      deleteForm.addEventListener("submit", function(event) {
        event.preventDefault();

        var oReq = new XMLHttpRequest();
        oReq.addEventListener("load", function() {
          // Remove the row from the document
          row.remove();
        });
        oReq.open("POST", deleteForm.getAttribute("action"));
        oReq.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        oReq.send(new FormData(deleteForm));
      });
    }
  };

  Permissions.prototype.bindEvents = function() {
    var self = this;

    var permissionRows = self.element.querySelectorAll(":scope > li");

    permissionRows.forEach(function(row) {
      self.bindRowEvents(row);
    });

    var autoComplete = Occam.AutoComplete.load(self.element.parentNode.querySelector(".add input.auto-complete"));
    autoComplete.on("change", function(event) {
      // Re-enable add button
      self.element.parentNode.querySelector('.add input[type="submit"]').removeAttribute("disabled");
    });

    var permissionAddRowButton = self.element.parentNode.querySelector(".add a.add-person");

    var permissionList = self.element;

    var form = permissionAddRowButton.previousElementSibling;
    permissionAddRowButton.addEventListener("click", function(event) {
      event.preventDefault();
      event.stopPropagation();

      // Find form and reveal it. Unreveal the button.
      // The form precedes the button

      permissionAddRowButton.style.display = "none";
      form.removeAttribute("hidden");
    });

    form.addEventListener("submit", function(event) {
      event.preventDefault();

      // Submit the form asynchronously and update the DOM with the result
      var oReq = new XMLHttpRequest();
      var loadingElement = form.previousElementSibling;
      var warningElement = loadingElement.previousElementSibling;
      loadingElement.style.width  = permissionList.querySelector("li").clientWidth  + "px";
      loadingElement.style.height = permissionList.querySelector("li").clientHeight + "px";
      loadingElement.removeAttribute("hidden");
      form.setAttribute("hidden", "");
      permissionAddRowButton.style.display = "";

      oReq.addEventListener("load", function() {
        loadingElement.setAttribute("hidden", "");
        var newElement = document.createElement("div");
        newElement.innerHTML = this.responseText;
        newElement = newElement.querySelector("li");
        newElement.classList.add("reveal");

        // Get the person id, if it exists here
        var personInput = newElement.querySelector("input[name=person]");
        if (personInput) {
          var personId = personInput.getAttribute("value");
          var existingRow = permissionList.querySelector("input[value='" + personId + "']");

          if (existingRow) {
            // Do not add
            // Instead, show a dismissable warning
            var deleteElement = warningElement.querySelector(".delete");
            var removeEvent = function(event) {
              warningElement.setAttribute("hidden", "");
              warningElement.querySelector(".person-already-exists").setAttribute("hidden", "");
            };
            deleteElement.removeEventListener("click", removeEvent);
            deleteElement.addEventListener("click", removeEvent);

            warningElement.querySelector(".person-already-exists").removeAttribute("hidden");

            warningElement.style.width      = permissionList.querySelector("li").clientWidth  + "px";
            warningElement.style.height     = permissionList.querySelector("li").clientHeight + "px";
            warningElement.style.lineHeight = permissionList.querySelector("li").clientHeight + "px";
            warningElement.removeAttribute("hidden");
            return;
          }
        }

        // Add the row to the document
        permissionList.appendChild(newElement);

        // Add events
        self.bindRowEvents(newElement);
      });
      oReq.open("POST", form.getAttribute("action"));
      oReq.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      oReq.send(new FormData(form));
    });
  };
};
