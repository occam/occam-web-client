/* This file handles the help bubble reveal and sizing.
 */

var initOccamHelp = function(Occam) {
  var Help = Occam.Help = function(element) {
    var self = this;

    if (element === undefined) {
      return;
    }

    this.element = element;

    if (Help.fakeCard === undefined) {
      // Fake card
      Help.fakeCard = document.createElement("div");
      Help.fakeCard.setAttribute("role", "presentation");
      Help.fakeCard.setAttribute("aria-hidden", "true");
      Help.fakeCard.classList.add("card");
      Help.fakeCard.style.opacity = 0;
      Help.fakeCard.style.position = "absolute";
      // Ensure that the help bubble isn't the first element so
      // the correct styling is applied (except for when the help bubble
      // *IS* the first element)
      Help.fakeCard.appendChild(document.createElement("div"));
      document.querySelector(".content").appendChild(Help.fakeCard);
    }

    Help.count++;
    this.element.setAttribute('data-help-index', Help.count);

    Help._loaded[this.element.getAttribute('data-help-index')] = this;

    this.bindEvents();
    this.events = {};
  };

  Help.count = 0;
  Help._loaded = {};

  Help.loadAll = function(element) {
    var elements = element.querySelectorAll('.help-bubble');

    elements.forEach(function(element) {
      Help.load(element);
    });
  };

  Help.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-help-index');

    if (index) {
      return Occam.Help._loaded[index];
    }

    return new Occam.Help(element);
  };

  Help.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Help.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  Help.prototype.bindEvents = function() {
    var self = this;

    // Help bubble expansion
    var helpText = this.element.nextElementSibling;
    if (helpText === null) {
      helpText = this.element.parentNode.nextElementSibling;
    }

    // Measure help text
    var dup = helpText.cloneNode(true);
    Help.fakeCard.appendChild(dup);

    dup.removeAttribute("hidden");
    dup.style.height = "";
    dup.style.minHeight = "";

    // Get the height
    var height = dup.clientHeight;

    dup.remove();

    helpText.style.minHeight = "0px";
    var initiallyHidden = helpText.getAttribute("hidden") !== null;
    if (initiallyHidden) {
      helpText.style.height = "0px";
      helpText.style.opacity = 0;
      helpText.classList.add("closed");
    }
    else{
      helpText.style.height = height + "px";
      helpText.style.opacity = 1;
      helpText.classList.add("open");
    }
    helpText.removeAttribute("hidden");

    // Add 'delete' button to help bubbles
    var deleteNode = document.createElement("div");
    deleteNode.classList.add("delete");
    helpText.appendChild(deleteNode);

    var openCloseHandler = function(event) {
      event.stopPropagation();
      event.preventDefault();

      if (!helpText.classList.contains("bound")) {
        // Make sure the help text has a static height to begin with

        helpText.style.minHeight = "0px";
        helpText.style.position = "";
        if (initiallyHidden) {
          helpText.style.height = "0px";
          helpText.classList.add("closed");
        }
        else{
          helpText.style.height = height + "px";
          helpText.style.opacity = 1;
          helpText.classList.add("open");
        }

        helpText.classList.add("bound");
      }

      if (helpText.classList.contains("closed")) {
        // Show the help text
        helpText.style.height = height + "px";
        helpText.style.opacity = 1;
        helpText.style.borderWidth = "1px";

        helpText.classList.remove("closed");
        helpText.classList.add("open");
        helpText.setAttribute("aria-hidden", "false");
        self.element.setAttribute("aria-expanded", "true");
      }
      else {
        // Hide the help text
        helpText.style.height = "0px";
        helpText.style.opacity = 0;
        helpText.style.borderWidth = 0;

        helpText.classList.remove("open");
        helpText.classList.add("closed");
        helpText.setAttribute("aria-hidden", "true");
        self.element.setAttribute("aria-expanded", "false");
      }
    };

    this.element.addEventListener("click", openCloseHandler);
    deleteNode.addEventListener("click", openCloseHandler);
  };
};
