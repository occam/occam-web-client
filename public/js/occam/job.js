/* This file handles job status and polling.
 */

var initOccamJob = function(Occam) {
  var Job = Occam.Job = function(element) {
    var self = this;

    // Initialize events
    self.events = {};

    // Initially hasn't started
    self.started = false;

    self.element = element;
    self.tag = Math.floor(Math.random() * 10000);

    // Look for a run id
    self.jobID = self.element.getAttribute("data-job-id");

    if (self.jobID) {
      var job_object_id = self.element.getAttribute("data-job-object-id");
      if (job_object_id) {
        self.object = new Occam.Object(self.element.getAttribute("data-job-object-id"),
                                       self.element.getAttribute("data-job-object-revision"));
      }
      else {
        self.object = Occam.object;
      }
      self.jobData(self.object, self.jobID, function(data) {
        // Create a polling timer for updating the job (if it is not finished/failed)
        if (!data.job.failureTime && !data.job.finishTime) {
          self.pollJobTimer = window.setInterval(function() {
            self.poll();
          }, Job.POLL_TIME);
        }
      });
    }

    Job.count++;
    self.element.setAttribute('data-loaded-job-index', 'job-' + Job.count);
    Job._loaded[self.element.getAttribute('data-loaded-job-index')] = self;
  };

  Job.count = 0;
  Job._loaded = {};

  Job.load = function(element) {
    if (!element) {
      return;
    }

    var index = element.getAttribute('data-loaded-job-index');

    if (index) {
      return Occam.Job._loaded[index];
    }

    return new Occam.Job(element);
  };

  /* The time in milliseconds between polling for updates in a run.
   */

  Job.POLL_TIME = 2000;

  /* Fires a callback for the given event.
   */
  Job.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      var eventInfo = this.events[name];
      eventInfo.callback.call(eventInfo.target, data);
    };
    return this;
  };

  /* Triggers an event with the given event data.
   */
  Job.prototype.postEvent = function(event) {
    this.trigger("event", event);
  };

  /* Will call the callback with the job's network manifest.
   */
  Job.prototype.networkInfo = function(callback) {
    if (this._networkInfo) {
      callback(this._networkInfo);
      return;
    }

    var self = this;
    Occam.Util.get(self.object.url({"path": "jobs/" + self.jobID + "/network"}), function(data) {
      if (data) {
        self._networkInfo = data;
        callback(data);
      }
    }, "json");
  };

  /* Will call the callback with the job's task manifest.
   */
  Job.prototype.taskInfo = function(callback) {
    if (this._taskInfo) {
      callback(this._taskInfo);
      return;
    }

    var self = this;
    Occam.Util.get(self.object.url({"path": "jobs/" + self.jobID + "/task"}), function(data) {
      if (data) {
        self._taskInfo = data;
        callback(data);
      }
    }, "json");
  };

  /* Opens a log socket and calls the given callback for when the log is updated.
   */
  Job.prototype.log = function() {
    var self = this;
    this.logWS = Occam.WebSocket.route("job-log-" + this.tag, function(data) {
      // Buffer until a newline and parse the JSON if possible
      self.trigger("log-update", data);
    }, this)

    this.logWS.send({
      "request": "open",
      "spawning": "logs",
      "terminal": "log",
      "data": {"job_id": this.jobID}
    });
  };

  /* Opens a two-way connection with the running job.
   */
  Job.prototype.connect = function(callback) {
    var self = this;
    this.connectWS = Occam.WebSocket.route("job-connect-" + this.tag, function(data) {
      // TODO: Buffer until a newline and parse the JSON if possible
      callback(data);
    }, this)

    this.connectWS.send({
      "request": "open",
      "spawning": "connect",
      "terminal": "connect",
      "data": {"job_id": this.jobID}
    });

    return {
      "send": function(data) {
        self.connectWS.send({
          "request": "send",
          "terminal": "connect",
          "input": data,
          "data": {"job_id": self.jobID}
        });
      }
    }
  };

  /* Opens a log socket and calls the given callback for each new event.
   */
  Job.prototype.eventsLog = function(callback) {
    var self = this;

    var buffer = "";
    this.eventWS = Occam.WebSocket.route("job-event-log-" + this.tag, function(data) {
      // Buffer until a newline and parse the JSON if possible
      if (data.output) {
        buffer = buffer + data.output;
      }
      do {
        // JavaScript's split function is terrible wow
        var newline = buffer.indexOf("\n");
        if (newline > -1) {
          var line = buffer.substring(0, newline);
          buffer = buffer.substring(newline + 1);
          self.postEvent(JSON.parse(line));
        }
      } while (newline > -1)
    }, this)

    this.eventWS.send({
      "request": "open",
      "spawning": "events",
      "terminal": "events",
      "data": {"job_id": this.jobID}
    });
  };

  /* Establishes an event callback.
   */
  Job.prototype.on = function(name, callback, target) {
    if (callback === undefined) {
      return this.events[name];
    }

    if (target === undefined) {
      target = this;
    }

    this.events[name] = {
      "callback": callback,
      "target": target
    };
    return this;
  };

  Job.prototype.poll = function() {
    var self = this;

    self.jobData(self.object, self.jobID, function(data) {
      if (data && data.job) {
        if (data.job.startTime) {
          if (!self.started) {
            self.started = true;
            self.trigger('start', data);
          }
        }

        if (data.job.finishTime || data.job.failureTime) {
          window.clearInterval(self.pollJobTimer);
          self.trigger('done', data);
        }
      }
    });
  };

  Job.prototype.jobData = function(object, jobID, callback) {
    Occam.Util.get(this.object.url({"path": "jobs/" + jobID}), function(data) {
      callback(data);
    }, "json");
  };
};
