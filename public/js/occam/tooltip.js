/* This file handles tooltips throughout the page.
 */

var initOccamTooltip = function(Occam) {
  var Tooltip = Occam.Tooltip = function(element) {
    var self = this;

    if (element === undefined) {
      return;
    }

    if (Tooltip.element == undefined) {
      var tooltipElement = document.createElement("div");
      tooltipElement.classList.add("tooltip");

      var tooltipText = document.createElement("span");
      tooltipElement.appendChild(tooltipText);

      var tooltipBeforeElement = document.createElement("div");
      tooltipBeforeElement.classList.add("before");
      tooltipElement.appendChild(tooltipBeforeElement);

      var tooltipAfterElement = document.createElement("div");
      tooltipAfterElement.classList.add("after");
      tooltipElement.appendChild(tooltipAfterElement);

      document.body.appendChild(tooltipElement);

      tooltipElement.addEventListener("mouseleave", function(event) {
        this.style.display = "none";
      });

      Tooltip.element = tooltipElement;
    }

    this.element = element;

    Tooltip.count++;
    this.element.setAttribute('data-tooltip-index', Tooltip.count);

    Tooltip._loaded[this.element.getAttribute('data-tooltip-index')] = this;

    this.bindEvents();
    this.events = {};
  };

  Tooltip.count = 0;
  Tooltip._loaded = {};

  Tooltip.loadAll = function(element) {
    var elements = element.querySelectorAll('[title]');

    elements.forEach(function(element) {
      Tooltip.load(element);
    });
  };

  Tooltip.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-tooltip-index');

    if (index) {
      return Occam.Tooltip._loaded[index];
    }

    return new Occam.Tooltip(element);
  };

  Tooltip.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Tooltip.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  Tooltip.prototype.bindEvents = function() {
    var self = this;

    var tooltipText = this.element.getAttribute("title");
    this.element.removeAttribute("title");

    this.element.addEventListener("mouseenter", function(event) {
      var timer = window.setTimeout(function() {
        if (self.element.classList.contains("disable-tooltip")) {
          return;
        }

        var position = self.element.getBoundingClientRect();

        // Set the text and then reveal the tooltip so that it can be measured
        Tooltip.element.querySelector("span").textContent = tooltipText;
        Tooltip.element.style.display = "block";

        var tooltipBounds = Tooltip.element.getBoundingClientRect();

        // Place it above the element with the tooltip
        Tooltip.element.style.top  = ((position.top + parseInt(self.element.style.paddingTop || "0") - (tooltipBounds.bottom - tooltipBounds.top)) - 10) + "px";

        // Place the tooltip in the center
        var width = tooltipBounds.right - tooltipBounds.left;
        var left = position.left + ((position.right - position.left) / 2) - (width / 2);

        var arrowLeft = (width / 2) - 7;

        // Unless it is hitting the boundary of the page, then position it at the page boundary.
        if ((left + width) > (document.body.clientWidth - 5)) {
          var delta = left - ((document.body.clientWidth - 5) - width);
          left -= delta;
          arrowLeft += delta;
        }
        else if (left < 5) {
          var delta = (left - 5);
          left -= delta;
          arrowLeft += delta;
        }

        Tooltip.element.querySelector(".after").style.left = arrowLeft + "px";
        Tooltip.element.querySelector(".before").style.left = (arrowLeft + 4) + "px";
        Tooltip.element.style.left = left + "px";

        // Show the tooltip
        Tooltip.element.classList.add('reveal');
      }, 500);

      self.element.addEventListener("mouseleave", function(event) {
        window.clearTimeout(timer);
        Tooltip.element.classList.remove('reveal');
        Tooltip.element.style.display = "none";
      });
    });
  };
};
