/*
 * This file contains the main websocket that will connect for a page view.
 * The web socket handles the parsing of log data, vnc connections, and run
 * progress for updating several of the widgets on the screen.
 *
 * Obviously the websocket functionality requires websocket support and
 * javascript on the client-side.
 */

var initOccamWebSocket = function(Occam) {
  'use strict';

  Occam.WebSocket = {};

  // The amount of time between a disconnect and attempting to reconnect
  Occam.WebSocket.RECONNECT_PERIOD = 1000;

  // The handle for the actual websocket.
  Occam.WebSocket.ws = null;

  // A map between unique tags and messages.
  Occam.WebSocket.router = {};

  /*
   * This method will open the main websocket on the current domain.
   */
  Occam.WebSocket.initialize = function() {
    var scheme = "ws://";
    if (window.document.location.protocol == "https:") {
      scheme = "wss://";
    }
    var uri    = scheme + window.document.location.host + "/";
    console.log(uri);

    try {
      var ws = new WebSocket(uri);
    }
    catch (e) {
      return;
    }

    $(window).on('beforeunload', function(){
      ws.close();
    });

    ws.onmessage = Occam.WebSocket.onmessage;
    ws.onopen    = Occam.WebSocket.onopen;
    ws.onclose   = Occam.WebSocket.onclose;
    ws.onerror   = Occam.WebSocket.onerror;

    Occam.WebSocket.ws = ws;
  };

  /*
   * This method handles websocket connections. The parameter 'event'
   * contains the websocket event.
   */
  Occam.WebSocket.onopen = function(event) {
    if (Occam.WebSocket._reconnect_timer) {
      window.clearInterval(Occam.WebSocket._reconnect_timer);
      Occam.WebSocket._reconnect_timer = null;
    }

    if (Occam.DEBUG) {
      console.log("websocket opened");
    }
  };

  /*
   * This method handles websocket disconnection. The parameter 'event'
   * contains the websocket event.
   */
  Occam.WebSocket.onclose = function(event) {
    // Attempt to reconnect after a delay
    Occam.WebSocket._reconnect_timer = window.setTimeout(function() {
      Occam.WebSocket.initialize();
    }, Occam.WebSocket.RECONNECT_PERIOD);
  };

  /*
   * This method handles websocket messages incoming. The parameter 'message'
   * contains the message from the server.
   */
  Occam.WebSocket.onmessage = function(message) {
    // By default, we simply route the message to a callback
    // that was registered previously.
    var data = JSON.parse(message.data);

    if (data.tag) {
      var tuple = Occam.WebSocket.router[data.tag];
      var callback = tuple[0];
      var self     = tuple[1];

      callback.call(self, data.data);
    }
  };

  /*
   * This method captures any errors of the websocket.
   */
  Occam.WebSocket.onerror = function(event) {
  };

  /*
   * This method adds the given item to the routing table such that it will
   * invoke the callback whenever it sees a websocket message with 'item' as a
   * tag.
   */
  Occam.WebSocket.route = function(item, callback, self) {
    Occam.WebSocket.router[item] = [callback, self];
    return {
      "send": function(data) {
        var taggedData = {
          "tag": item,
          "data": data
        };

        Occam.WebSocket.ws.send(JSON.stringify(taggedData));
      }
    };
  };

  Occam.WebSocket.initialize();
};
