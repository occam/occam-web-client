/* This file handles the javascript for the theme manager.
 */

var initOccamThemeManager = function(Occam) {
  var ThemeManager = Occam.ThemeManager = function(element) {
    this.element = element;

    this.bindEvents();
  };

  ThemeManager.loadAll = function(element) {
    var themePanel = element.querySelectorAll('.theme-selection');

    themePanel.forEach(function(subElement) {
      var panel = new Occam.ThemeManager(subElement);
    });
  };

  ThemeManager.prototype.bindEvents = function() {
    const buttons = this.element.querySelectorAll(".theme-selection button.color");

    buttons.forEach(function(button) {
      const selectors = JSON.parse(button.getAttribute("data-selectors"));
      const input = this.element.querySelector('input[name="' + button.getAttribute('id') + '"]');
      const style = this.element.querySelector('input[name="' + button.getAttribute('id') + '-style"]');
      const re  = /^hsl\(\s*([^,]+)\s*,\s*([^,]+)%\s*,\s*([^,]+)%\s*\)/;
      const matches = re.exec(input.value);
      const originalValue = input.value;

      const update = function() {
        // Update selectors
        selectors.forEach(function(data) {
          const elements = document.querySelectorAll(data.selector);

          elements.forEach(function(element) {
            const apply = function() {
              element._oldValue = element.style[data.key];

              var value = "#" + input.value;
              if (style) {
                value = style.value + " " + value;
                if (style.value.trim() == "") {
                  value = "none";
                }
              }
              element.style[data.key] = value;
              if (data.hover) {
                element.style.animation = "none";
              }
            };

            const unapply = function() {
              element.style[data.key] = element._oldValue;
            }

            if (data.hover) {
              element.removeEventListener("mouseover", apply);
              element.addEventListener("mouseover", apply);

              element.removeEventListener("mouseout", unapply);
              element.addEventListener("mouseout", unapply);
            }
            else {
              apply();
            }
          });
        });
      };

      if (style) {
        style.addEventListener("change", update);
        style.addEventListener("keyup", update);
      }

      const picker = new jscolor(button, {
        valueElement: input,
        closable: true,
        onFineChange: update,
      });
      if (matches) {
        var rgb = null;
        try {
          rgb = hslToRgb(parseFloat(matches[1])/360, parseFloat(matches[2])/100, parseFloat(matches[3])/100);
        }
        catch (e) {
          rgb = [0, 0, 0];
        }

        picker.fromRGB(rgb[0], rgb[1], rgb[2]);
      }
      else {
        picker.fromString(originalValue);
      }
    }, this);
  };
};
