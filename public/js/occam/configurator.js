/*
 * This module handles the Configurator widgets which displays graphical forms
 * that give people an interactive way to configure objects.
 */

// TODO: Tie in better with Configuration object

var initOccamConfigurator = function(Occam) {
  'use strict';

  /* This object represents a configurator widget object on the page.
   * The element that is attached is the button that will attach the
   * configurator to the object workflow.
   */
  var Configurator = Occam.Configurator = function(element) {
    var self = this;

    this.element = element;

    this.bindEvents();

    this.configurations = [];

    // Collection Configurations
    this.element.parent().children('.tab-panel.configuration').each(function() {
      self.configurations.push(Occam.Configuration.load($(this)));
    });

    return this;
  };

  Configurator.loadAll = function(element) {
    var configurators = element.querySelectorAll('.content .tab-panel.configurator');

    configurators.forEach(function(element) {
      var configurator = new Occam.Configurator($(element));
    });
  };

  /* Returns urlsafe base64 encoding
   */
  Configurator.base64_urlencode = function(key) {
    var code = $.base64.encode(key);
    // Replace + / with - _
    code = code.replace('+', '-').replace('/', '_');
    // Remove padding
    while (code.charAt(code.length-1) == '=') {
      code = code.slice(0, code.length-1);
    }

    return code;
  };

  Configurator.configurationStringToKey = function(connection, configuration, key) {
    var base = "data[" + connection + "][" + configuration + "]";
    var ret = base;

    var parts = key.split('.');

    var current = "";
    var index = 0;
    parts.forEach(function(part) {
      current = current + part;

      if (current.length > 0) {
        if (current[current.length-1] != '\\' && index < parts.length) {
          // We have a complete key
          ret = ret + "[" + base64_urlencode(current) + "]";
          current = "";
        }
      }

      index += 1;
    });

    return ret;
  };

  Configurator.prototype.getConfigurationElement = function(connection, configuration, key) {
    /* We have to do this precision to eliminate hijacking by
     * widgets trying to fool us. */
    var tag_base = ".tab-panel.configuration > .configuration-search-options ";

    var configuration_key = Configurator.configurationStringToKey(connection, configuration, key);

    // Escape brackets
    configuration_key = configuration_key.replace(/([[\]])/g, "\\$1");

    var tag_search = tag_base + "*:not([type=hidden])[name=" + configuration_key + "]";

    return this.element.parent().find(tag_search);
  };

  Configurator.prototype.setConfigurationOption = function(connection, configuration, key, value) {
    var element = this.getConfigurationElement(connection, configuration, key);

    element.val(value);
  };

  Configurator.prototype.testConfigurationOption = function(connection, configuration, key, value) {
    var element = this.getConfigurationElement(connection, configuration, key);

    // TODO
  };

  Configurator.prototype.getConfigurationOption = function(connection, configuration, key) {
    var element = this.getConfigurationElement(connection, configuration, key);

    var info = {};

    var root = element.parent();

    // Is this an enumerated value? If so, get the possible values.
    if (root.hasClass('select')) {
      root = root.parent();
      info.options = element.children('option').map(function () { return $(this).text(); }).toArray();
      info.value = element.children('option[selected=selected]').text();
    }
    else {
      info.options = [];
      if (element.attr('type') == 'checkbox') {
        info.value = element.val() == "on";
      }
      else {
        info.value = element.val();
      }
    }

    info.description = root.children('.description').text();
    info.label = root.children('label').text();
    info.type = root.children('label').attr('class');

    return info;
  };

  Configurator.prototype.loadWidget = function(widget) {
    var self = this;
    var configurator_index = widget.data('index');

    // Retrieve information about what configurator widget to load
    var connection_index = this.element.data('connection-index');
    var configurator_url = this.element.find('input[name="attach_configurator_file\[' + configurator_index + '\]"]').val();
    var configurator_uid = this.element.find('input[name="attach_configurator_id\[' + configurator_index + '\]"]').val();
    var configurator_rev = this.element.find('input[name="attach_configurator_revision\[' + configurator_index + '\]"]').val();

    // Remove the list
    this.element.children().remove();

    // Determine the widget url
    var widgetUrl = "/" + configurator_uid + "/" + configurator_rev
                                + "/raw/" + configurator_url;

    // Add the widget
    var iFrame = $('<iframe></iframe>');
    iFrame.attr('src', widgetUrl);

    window.addEventListener('message', function(event) {
      // React if the source of the message is the iframe
      if (event.source === iFrame[0].contentWindow) {
        var name = event.data.name;
        if (name === "getMetadata") {
          var index = event.data.index;
          var key   = event.data.key;
          event.data.info = self.getConfigurationOption(connection_index, index, key);
          console.log("Getting metadata for " + event.data.index + " x " + event.data.key);
          console.log(event)

          event.source.postMessage(event.data, '*');
        }
        else if (name === "getValue") {
          var index = event.data.index;
          var key   = event.data.key;
          info = self.getConfigurationOption(connection_index, index, key);
          event.data.value = info.value;
          console.log("Getting value for " + event.data.index + " x " + event.data.key);
          console.log(event)

          event.source.postMessage(event.data, '*');
        }
        else if (name === "setValue") {
          var index = event.data.index;
          var key   = event.data.key;
          var value = event.data.value;
          self.setConfigurationOption(connection_index, index, key, value);
        }
        else {
          console.log("unknown message from iFrame");
        }
      }
    });

    this.element.append(iFrame);
  };

  Configurator.prototype.bindEvents = function() {
    var self = this;

    self.element.find('input.attach-configurator.button')
                .on('click', function(event) {
      self.loadWidget($(this));
    });

    return self;
  };
};
