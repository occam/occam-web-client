/* This file maintains the Occam.Object class which handles functionality
 * related to Occam Objects. This class can pull down information and metadata
 * and post updates to Occam, if that is allowed by the object.
 */

/* Objects have queues of pending actions which need to be ACK'd before
 * continuing. That way actions are invoked in the correct order. An object's
 * revision is updated whenever an action is acknowledged. Some actions
 * require an object to be up-to-date in the backend worker, and thus must wait
 * until the queue is empty. For instance, running the object in the browser.
 * Basically, these actions are just in the queue as well... but we should
 * somehow indicate that the actions are delayed.
 */

var initOccamObject = function(Occam) {
  var Object = Occam.Object = function(id, revision, type, name, file, index, link, token) {
    var self = this;

    // If no id/revision are given, pull out the object represented on the
    // page (if any)
    this.main = false;

    if (id === undefined) {
      this.main = true;
      this.element = document.querySelector('body > .content > h1');

      this.link = getParameterByName("link");
      this.token = getParameterByName("token");

      if (!this.element) {
        return;
      }

      if (this.element.querySelector('#object-id')) {
        this.id = this.element.querySelector('#object-id').textContent.trim();
        this.type = this.element.querySelector('#object-type').textContent.trim();
        this.revision = this.element.querySelector('#object-revision').textContent.trim();
      }

      // Discover the root object
      var rootIdElement = this.element.querySelector('#object-root-id')
      if (rootIdElement) {
        this.rootId = rootIdElement.textContent.trim();
      }
      var rootRevisionElement = this.element.querySelector('#object-root-revision')
      if (rootRevisionElement) {
        this.rootRevision = rootRevisionElement.textContent.trim();
      }

      // Discover the index
      this.index = [];
      var indexElement = this.element.querySelector('ol#object-index');
      if (indexElement) {
        indexElement.querySelectorAll('li').forEach(function(itemElement) {
          self.index.push(itemElement.textContent.trim());
        });
      }

      // Discover the path/file
      var pathElement = this.element.querySelector('#object-path')
      this.path = pathElement && pathElement.textContent.trim();
      var fileElement = this.element.querySelector('#object-file')
      this.file = fileElement && fileElement.textContent.trim();

      // Initialize any object viewers
      var objectViewers = document.querySelectorAll('.card.object-viewer');

      if (objectViewers.length > 0) {
        this.applyConfigurationEvents(objectViewers);
      }

      // Initialize the Runner

      // Build tab

      var element = document.querySelector('.card.terminal .terminal#build-terminal');

      if (element) {
        var buildTerminal = Occam.Terminal.load($(element));
      }

      // Console tab

      element = document.querySelector('.card.terminal .terminal#console-terminal');

      if (element) {
        var consoleTerminal = Occam.Terminal.load($(element));
      }

      // Run tab

      element = document.querySelector('.run-terminal .terminal');

      if (element) {
        var runTerminal = Occam.Terminal.load($(element));
      }
    }
    else {
      // This is just an object abstraction
      this.id = id;
      this.type = type;
      this.name = name;
      this.revision = revision;
      this.index = index || [];
      this.link = link;
      this.token = token;

      // It is not represented in the DOM
      this.element = null;
    }

    this.queue = [];
    this.pending = 0;
    this.queueLock = false;

    this.file = file;

    return this;
  };

  /* This constant sets the number of preview panes that can be loading at a
   * time. This will help limit the load on the server and client when loading
   * a whole page of widgets.
   */
  Object.MAX_CONCURRENT_PREVIEW_LOADS = 8;

  /* This is the amount of time in milliseconds to wait for a widget to give
   * a "loaded" event. We will remove the progress indicator and allow
   * interaction only when receiving that message. Otherwise, after the
   * timeout, we will display an error notification.
   */
  Object.PREVIEW_TIMEOUT = 10000;

  /* This method yields the object viewer for the given object if it exists.
   */
  Object.prototype.viewer = function() {
  };

  /*
   * This method ensures that the browser's current URL is correct for the
   * revision of this object/workset.
   */
  Object.prototype.updateLocation = function() {
    var newURL = "/" + this.id +
                 "/" + this.revision;

    if (this.workset) {
      newURL = "/worksets/" + this.workset.id +
               "/"          + this.workset.revision +
               newURL;
    }

    newURL = newURL + Occam.NavigationState.currentPath();

    Occam.NavigationState.updateLocation(newURL)
  };

  /* This method returns the url to this object.
   */
  Object.prototype.url = function(options) {
    var ret = "";

    if (this.rootId) {
      ret = "/" + this.rootId +
            "/" + this.rootRevision;
    }
    else {
      ret = "/" + this.id +
            "/" + this.revision;
    }

    if (this.index) {
      this.index.forEach(function(index) {
        ret = ret + "/" + index;
      });
    }

    var query = {};

    if (this.link) {
      query.link = this.link;
    }

    if (this.token) {
      query.token = this.token;
    }

    if (options.path) {
      if (!options.path.startsWith("/")) {
        ret = ret + "/";
      }
      ret = ret + options.path
    }

    // Amend query parameters
    var queryKeys = window.Object.keys(query);
    if (queryKeys.length > 0) {
      var queryKey = queryKeys.pop();
      ret = ret + "?" + queryKey + "=" + query[queryKey];
    }

    queryKeys.forEach(function(queryKey) {
      ret = ret + "&" + queryKey + "=" + query[queryKey];
    });

    return ret;
  };

  /* This method adds an append event to the queue. When an empty value is
   * given, this will remove the key from the object metadata.
   */
  Object.prototype.queuePushSet = function(key, value, callback) {
    var args = [];

    args.push({ "values": [key] });

    if (value !== undefined) {
      args.push({ "values": [JSON.stringify(value)] });
    }

    args.push({ "key": "--input-type",
             "values": ['json'] });

    this.queuePush({
      "command": "set",
      "arguments": args
    }, callback);

    return this;
  }

  /* This method adds an append event to the queue. The optional 'at' field
   * will determine the index it will push the new item.
   */
  Object.prototype.queuePushAppend = function(key, value, at, callback) {
    var args = [];

    args.push({ "values": [key] });
    args.push({ "values": [JSON.stringify(value)] });

    if (at !== undefined && at !== null) {
      args.push({ "key": "--at",
               "values": [""+at] });
    }

    args.push({ "key": "--input-type",
             "values": ['json'] });

    this.queuePush({
      "command": "append",
      "arguments": args
    }, callback);

    return this;
  }

  /* This method adds an attach event to the queue.
   */
  Object.prototype.queuePushAttach = function(connection_index, object_id, object_revision, callback) {
    var args = [];

    if (connection_index >= 0) {
      args.push({
        "values": [connection_index.toString()]
      });
    }

    args.push({ "key": "--id",
             "values": [object_id] });
    args.push({ "key": "--object-revision",
             "values": [object_revision] });

    this.queuePush({
      "command": "attach",
      "arguments": args
    }, callback);

    return this;
  };

  Object.prototype.queuePushDetach = function(connection_index, callback) {
    this.queuePush({
      "command": "detach",
      "arguments": [
        { "values": [connection_index.toString()] }
      ]
    }, callback);

    return this;
  };

  /* This method adds an event to the queue.
   */
  Object.prototype.queuePush = function(command, callback) {
    this.queue.push([command, callback]);

    // ok. so when we have a command, issue it, and then have the ack of the
    // command issue the next one in sequence while firing any callback.
    // when there is no command, then just stop

    if (this.queueLock == false) {
      this.queueIssue();
    }

    return this;
  };

  /* This method yields the queue size.
   */
  Object.prototype.queueCount = function() {
    return this.queue.length;
  };

  /* This method, which is generally called internally and not meant to be
   * used externally, will invoke the next queued command.
   */
  Object.prototype.queueIssue = function() {
    var self = this;

    if (this.queue.length == 0) {
      return this;
    }

    // Lock queue
    self.queueLock = true;

    // Pull next action
    var queueItem = this.queue[0];
    var action    = queueItem[0];
    var callback  = queueItem[1];

    // Truncate queue
    self.queue = self.queue.slice(1);
    self.pending += 1;

    // Form action url
    var url = "/worksets/" + this.workset.id + "/" + this.workset.revision +
              "/"  + this.id    + "/" + this.revision + "/history";

    // Reveal 'saving...' box
    $('.content h1 .saving').attr('aria-hidden', 'false');

    // Perform action and issue another command when it is successful
    $.ajax({
      type: "POST",
      url: url,
      data: JSON.stringify(action),
      dataType: 'json',
      contentType: 'application/json',
      success: function(revisions) {
        var revision = revisions['objectRevision'];
        var worksetRevision = revisions['worksetRevision'];
        var worksetId = self.workset.id;
        self.workset = new Occam.Object(worksetId, worksetRevision, "workset");

        self.revision = revision;

        // Update location
        self.updateLocation();

        // Call the callback once completed successfully
        if (callback !== undefined) {
          callback.call(self, true);
        }
      }
    }).fail(function(error) {
      console.log("error??");
      document.querySelector('.content h1 .saving.flash').getAttribute('aria-hidden', 'true');
      document.querySelector('.content h1 .error.flash').getAttribute('aria-hidden', 'false');

      // Call the callback and indicate a failure
      if (callback !== undefined) {
        callback.call(self, false);
      }

      // TODO: we should flush the queue and try to react to failures
      //       everywhere we issue them. All flushed queue items should
      //       have their callbacks issued in failure.
      self.pending = 0;
    }).always(function() {
      // Issue another command
      self.pending -= 1;
      if (self.pending == 0 && self.queue.length == 0) {
        document.querySelector('.content h1 .saving').setAttribute('aria-hidden', 'true');
      }

      // Unlock queue
      if (self.pending == 0) {
        self.queueLock = false;
      }

      self.queueIssue();
    });

    return this;
  };

  /*
   * This method returns whether or not the given backend is supported as a
   * possible means of running this object. Will return a true or false to the
   * given callback.
   */
  Object.prototype.runsOn = function(backend, callback) {
    var self = this;

    if (this._runsOn          !== undefined &&
        this._runsOn[backend] !== undefined) {
      // Pull result from cache
      callback(this._runsOn[backend]);
    }
    else {
      var objectURL = "/" + this.id + "/" + this.revision + "/runsOn" +
                      "?backend=" + backend;
      if (this.id === null) {
        return this;
      }
      $.getJSON(objectURL, function(data) {
        // Cache result
        self._runsOn = self._runsOn || {};
        self._runsOn[backend] = data;

        callback(data);
      })
    }

    return this;
  };

  /*
   * This method returns whether or not the given environment/architecture pair
   * are supported as a possible means of running this object. Will return a
   * true or false to the given callback.
   */
  Object.prototype.supports = function(environment, architecture, callback) {
    var self = this;

    if (this._supports               !== undefined &&
        this._supports[environment]  !== undefined &&
        this._supports[architecture] !== undefined) { 
      // Pull result from cache
      callback(this._supports[environment][architecture]);
    }
    else {
      var objectURL = "/" + this.id + "/" + this.revision + "/supports" +
                      "?environment="  + environment +
                      "&architecture=" + architecture;
      if (this.id === null) {
        return this;
      }
      $.getJSON(objectURL, function(data) {
        // Cache result
        self._supports = self._supports || {};
        self._supports[environment] = self._supports[environment] || {};
        self._supports[environment][architecture] = data;

        callback(data);
      })
    }

    return this;
  };

  /*
   * This method retrieves a list of backends that can be used to run this
   * object. Will pass that array of strings to the given callback.
   */
  Object.prototype.backends = function(environmentList, callback) {
    var self = this;

    if (callback == undefined) {
      callback = environmentList;
      environmentList = undefined;
    }

    if (this._backends !== undefined) {
      // Pull result from cache
      callback(this._backends);
    }
    else {
      var objectURL = "/" + this.id + "/" + this.revision + "/backends";

      if (environmentList) {
        objectURL = objectURL + "?";
        var i = 1;

        environmentList.forEach(function(pair) {
          if (i > 1) {
            objectURL = objectURL + "&";
          }
          objectURL = objectURL + "environment_"  + i + "=" + pair[0] + "&" +
                                  "architecture_" + i + "=" + pair[1];

          i++;
        });
      }

      if (this.id === null) {
        return this;
      }

      console.log(objectURL);
      Occam.Util.get(objectURL, function(data) {
        // Cache result
        self._backends = data;
        callback(data);
      })
    }

    return this;
  };

  /* This method retrieves json content from within the object.
   */
  Object.prototype.retrieveJSON = function(path, callback) {
    var self = this;
    if (this.id === null) {
      return this;
    }

    var objectURL = "/" + this.id;
    if (this.revision) {
      objectURL += "/" + this.revision;
    }
    objectURL += "/raw/" + path;

    $.getJSON(objectURL, function(data) {
      callback(data);
    });

    return this;
  };

  /* This method retrieves the object info for this object.
   */
  Object.prototype.objectInfo = function(callback) {
    var self = this;

    if (this._objectInfo !== undefined) {
      // Pull result from cache
      callback(this._objectInfo);
    }
    else {
      var objectURL = "/" + this.id;
      if (this.revision) {
        objectURL += "/" + this.revision;
      }
      if (this.id === null) {
        return this;
      }
      $.getJSON(objectURL, function(data) {
        // Cache result
        self._objectInfo = data;

        if (self.file) {
          self._objectInfo.file = self.file;
        }

        callback(self._objectInfo);
      });
    }

    return this;
  };

  /*
   * This method is for objects with View tabs with configurations available.
   * These types of objects (for instance, our graph widgets) can be viewed
   * and played with interactively.
   */
  Object.prototype.applyConfigurationEvents = function(viewerElements) {
    var self = this;

    viewerElements.forEach(function(viewerElement) {
      var preview = viewerElement.classList.contains('preview');

      var load = function() {
        viewerElement.setAttribute('data-status', 'loading');

        // Is this a preview?
        var configurationData = {};

        var iframe = viewerElement.querySelector('iframe');

        // Previews don't load the viewer widget until they load their input
        if (preview) {
          timer = window.setTimeout(function() {
            error();
          }, Object.PREVIEW_TIMEOUT);

          iframe.setAttribute('data-fail-timer-id', timer);
          iframe.src = iframe.getAttribute('data-src');
          var loader = iframe.parentNode.querySelector('.loading');
        }

        var inputObject = self;
        if (iframe && iframe.getAttribute('data-input-object-id') !== undefined) {
          inputObject = new Object(iframe.getAttribute('data-input-object-id'),
                                   iframe.getAttribute('data-input-object-revision'),
                                   iframe.getAttribute('data-input-object-type'),
                                   iframe.getAttribute('data-input-object-name'),
                                   iframe.getAttribute('data-input-file'));
        }

        if (!preview) {
          var configurationCard = viewerElement.nextElementSibling;
          if (configurationCard) {
            configurationCard = configurationCard.nextElementSibling;
          }
          if (configurationCard) {
            configurationCard = configurationCard.nextElementSibling;
          }

          if (configurationCard) {
            var configurationTabElement = configurationCard.querySelector(':scope > ul.tabs')
            if (configurationTabElement) {
              var configurationTabs = new Occam.Tabs(configurationTabElement);

              var viewerObjectLabel = configurationCard.querySelector('.widget-object > a');
              var associationForm = configurationCard.querySelector('form.inline.association');

              var optionsOpenLink = configurationCard.querySelector('a.open-options');
              optionsOpenLink.addEventListener('click', function(event) {
                event.preventDefault();
                event.stopPropagation();

                this.setAttribute('aria-hidden', 'true');
                this.nextElementSibling.setAttribute('aria-hidden', 'false');

                configurationCard.querySelector(':scope > ul.tab-panels').setAttribute('aria-hidden', 'false');
                configurationCard.querySelector(':scope > ul.tabs').setAttribute('aria-hidden', 'false');
              });

              var optionsCloseLink = configurationCard.querySelector('a.close-options');
              optionsCloseLink.addEventListener('click', function(event) {
                event.preventDefault();
                event.stopPropagation();

                this.setAttribute('aria-hidden', 'true');
                this.previousElementSibling.setAttribute('aria-hidden', 'false');

                configurationCard.querySelector(':scope > ul.tab-panels').setAttribute('aria-hidden', 'true');
                configurationCard.querySelector(':scope > ul.tabs').setAttribute('aria-hidden', 'true');
              });

              // Attach object viewer/runner
              configurationCard.querySelector('form.widget-selection input.button')
                               .addEventListener('click', function(event) {
                event.stopPropagation();
                event.preventDefault();

                // Remove old configuration tabs
                while(configurationTabs.tabCount() > 2) {
                  configurationTabs.removeTab(3);
                }

                var object_id = this.parentNode.querySelector(':scope > input.hidden[name=object-id]').value;

                var objectURL = "/" + object_id;

                $.getJSON(objectURL, function(data) {
                  var object_revision = data['revision'];

                  var viewPortal = viewerElement;

                  viewerObjectLabel.textContent = data['name'];
                  viewerObjectLabel.parentNode.setAttribute('class', 'large-icon widget-object');
                  viewerObjectLabel.parentNode.classList.add(data['type']);

                  associationForm.querySelector('input[name=object_id]').value = object_id;

                  if (data['architecture'] == 'html') {
                    viewerElements.forEach(function(subViewerElement) {
                      if (subViewerElement.classList.contains('terminal')) {
                        subViewerElement.setAttribute('aria-hidden', 'true');
                      }
                      else {
                        subViewerElement.setAttribute('aria-hidden', 'false');
                        viewerElement = subViewerElement;
                        iframe = viewerElement.querySelector('iframe');
                        viewPortal = subViewerElement;
                      }
                    });

                    iframe.setAttribute('data-object-id', object_id);
                    iframe.setAttribute('data-object-revision', object_revision);
                    iframe.setAttribute('data-object-name', data['name']);
                    iframe.setAttribute('data-object-type', data['type']);

                    //self.createWidgetConfigurationTabs(iframe, data, configurationTabs, object_id, object_revision);

                    var widgetURL = "/" + object_id + "/" + object_revision + "/raw/" + data["file"];
                    iframe.setAttribute('src', widgetURL);
                  }
                  else {
                    viewerElements.forEach(function(subViewerElement) {
                      if (viewerElement.classList.contains('terminal')) {
                        viewerElement.setAttribute('aria-hidden', 'false');
                        viewerElement = subViewerElement;
                        viewPortal = subViewerElement;
                      }
                      else {
                        subViewerElement.setAttribute('aria-hidden', 'true');
                      }
                    });

                    // Update the terminal settings
                    var terminal = viewPortal.querySelector('.run-terminal .terminal');
                    terminal.setAttribute('data-object-id', object_id);
                    terminal.setAttribute('data-object-revision', object_revision);

                    var input_id         = terminal.setAttribute('data-input-id');
                    var input_revision   = terminal.setAttribute('data-input-revision');

                    // Retrieve the terminal object
                    var runTerminal = Occam.Terminal.retrieve(terminal);

                    // Update the data message
                    runTerminal.data = {
                      "object_id":        object_id,
                      "object_revision":  object_revision,
                      "input_id":         input_id,
                      "input_revision":   input_revision,
                    };
                  }

                  // Post to the recently used list
                  var currentPerson = document.querySelector('#username a');
                  var recentlyUsedURL = currentPerson.getAttribute('href') + "/recentlyUsed";
                  $.post(recentlyUsedURL, JSON.stringify({
                    "object_id": object_id,
                    "object_revision": object_revision
                  }), function(data) {
                  }, 'json');
                });
              });

              var recentlyUsedList = configurationCard.querySelector('ul.object-list.recently-used');
              if (recentlyUsedList) {
                recentlyUsedList.querySelectorAll('li').forEach(function(item) {
                  item.addEventListerner('click', function(event) {
                    event.preventDefault();
                    event.stopPropagation();

                    var objectId     = this.querySelector('input[name=object-id]').value.trim();
                    var attachButton = configurationCard.querySelector('form.widget-selection input.button');
                    var selectorName = attachButton.parentNode.querySelector('input[name=name]');
                    selectorName.value = this.querySelector(':scope > p').textContent.trim();
                    var selectorId   = attachButton.parentNode.querySelector('input.hidden[name=object-id]');
                    selectorId.value = objectId;

                    attachButton.trigger('click');
                  });
                });
              }

              configurationCard.querySelectorAll('li.tab-panel.configuration').forEach(function(configurationPanel) {
                Occam.Configuration.load($(configurationPanel), function(configuration) {
                  configurationData[configuration.label] = configuration.data();

                  configuration.on('change', function(event) {
                    configurationData[configuration.label] = event;

                    iframe.contentWindow.postMessage({
                      name: 'updateConfiguration',
                      data: configurationData
                    }, '*');
                  });
                });
              });
            }
          }
        }
      };

      var pull = function() {
        var previewList = viewerElement.parents('ul.object-preview-list');
        var loadingCount = previewList.find('.object-viewer[data-status=loading]').length;
        if (loadingCount < Object.MAX_CONCURRENT_PREVIEW_LOADS) {
          viewerElement = previewList.find('.object-viewer[data-status=queued]').slice(0,1);
          if (viewerElement.length > 0) {
            load();
          }
        }
      };

      var queue = function() {
        viewerElement.attr('data-status', 'queued');
        pull();
      };

      var error = function() {
        var iframe = viewerElement.querySelector('iframe');
        var loader = iframe.parentNode.querySelector('.loading');
        loader.classList.add('failed');
        iframe.src = "about:blank";
        finish();
      };

      var finish = function() {
        viewerElement.setAttribute('data-status', 'done');
        if (preview) {
          pull();
        }
      };

      if (preview) {
        var iframe = viewerElement.querySelector('iframe');
        var loader = document.createElement("div");
        loader.classList.add("loading");

        var iframeDimensions = {
          'width':  viewerElement.clientWidth,
          'height': viewerElement.clientHeight
        };

        loader.css({
          'width':  iframeDimensions.width,
          'height': iframeDimensions.height,
          'position': 'absolute',
          'top': 0,
          'left': 0,
        });

        iframe.parentNode.appendChild(loader);
        viewerElement.appear();
        viewerElement.one('appear', queue);
      }
      else {
        load();
      }
    });
  };
}
