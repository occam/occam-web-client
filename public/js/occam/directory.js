/* This file handles the javascript for the object/workset directory. This
 * handles the expansions and dynamic loading of group/experiment listings.
 */

var initOccamDirectory = function(Occam) {
  var Directory = Occam.Directory = function(element) {
    this.element = element;

    this.initialize();
    this.bindEvents();
  };

  Directory.loadAll = function(element) {
    var directories = element.querySelectorAll('.directory.information');

    directories.forEach(function(subElement) {
      var directory = new Occam.Directory(subElement);
    });
  };

  Directory.prototype.initialize = function() {
    var self = this;

    // Add triangles to all relevant rows that may expand
    this.element.querySelectorAll('tr.header td').forEach(function(subElement) {
      subElement.style['padding-top'] = "18px";
    });

    this.element.querySelectorAll('tr:not(.add):not(.header) td.expand').forEach(function(subElement) {
      subElement.style["cursor"] = "pointer";
      subElement.textContent = '\u25b8';
    });

    this.element.querySelectorAll('tr:not(.add):not(.header) td').forEach(function(subElement) {
      subElement.style["cursor"] = "pointer";
    });
    this.element.querySelectorAll('tr:not(.add):not(.header) td a').forEach(function(subElement) {
      subElement.addEventListener("click", function(e) {
        e.stopPropagation();
      });
    });
    this.element.querySelectorAll('tr:not(.add):not(.header) td input').forEach(function(subElement) {
      subElement.addEventListener("click", function(e) {
        e.stopPropagation();
      });
    });
    this.element.querySelectorAll('tr:not(.add) td.checkbox').forEach(function(subElement) {
      subElement.style["padding-right"] = "0px";
    });
    this.element.querySelectorAll('tr:not(.add) input.checkbox').forEach(function(subElement) {
      subElement.style["display"] = "none";
    });
    var bindRowDrag = function(row) {
      var drag = function(event) {
        event.stopPropagation();
        event.preventDefault();

        var td = this;
        var tr = td.parentNode;

        var draggable = document.createElement('table');
        draggable.classList.add('draggable');
        draggable.classList.add('directory');
        draggable.classList.add('information');
        draggable.appendChild(tr.cloneNode(true));
        tr.querySelectorAll('td').forEach(function(subElement) {
          var nodes = draggable.querySelectorAll('td');
          Array.prototype.slice.call(nodes, getChildIndex(subElement), getChildIndex(subElement)+1).forEach(function(subSubElement) {
            subSubElement.style["width"] = subElement.clientWidth;
          });
        });

        var draggableOffset = tr.getBoundingClientRect();
        var startLeft = draggableOffset.left;
        var startTop  = draggableOffset.top;
        var curLeft   = startLeft;
        var curTop    = startTop;
        var startX    = event.pageX || event.originalEvent.touches[0].pageX;
        var startY    = event.pageY || event.originalEvent.touches[0].pageY;

        var draggingSurface = document.createElement("div");
        draggingSurface.classList.add("dragging-area");
        document.body.appendChild(draggingSurface);

        var lastPageX = startX;
        var lastPageY = startY;

        var dragEvent = function(event) {
          // Drag the row!
          event.stopPropagation();
          event.preventDefault();

          var pageX = event.pageX || event.originalEvent.touches[0].pageX;
          var pageY = event.pageY || event.originalEvent.touches[0].pageY;

          curLeft = startLeft + (pageX - startX);
          curTop  = startTop  + (pageY - startY);

          draggable.style['left'] = curLeft + "px";
          draggable.style['top']  = curTop  + "px";

          lastPageX = pageX;
          lastPageY = pageY;
        };

        var dragEndEvent = function(event) {
          event.stopPropagation();
          event.preventDefault();

          document.body.removeEventListener('touchmove', dragEvent);
          document.body.removeEventListener('mouseup', dragEndEvent);
          document.body.removeEventListener('touchend', dragEndEvent);
          document.body.removeEventListener('touchcancel', dragEndEvent);

          window.removeEventListener('blur', dragEndEvent);
          window.removeEventListener('mouseup', dragEndEvent);
          window.removeEventListener('touchend', dragEndEvent);
          window.removeEventListener('touchcancel', dragEndEvent);

          // Remove the draggable and add to section it ends up in
          draggable.remove();
          draggingSurface.remove();

          if (draggable.querySelectorAll('tr').length == 0) {
            return;
          }

          // Determine the section it is above
          var root = getParents(tr, '.filelists');
          if (root.length <= 0) {
            return;
          }
          var inputSets = root[0].querySelectorAll('table.directory.information.input-sets tr');

          var pageX = lastPageX;
          var pageY = lastPageY;

          inputSets.forEach(function(trInput) {
            // For every row in the input-sets (right-hand side) look to
            // see if we are on top
            var offset = trInput.getBoundingClientRect();

            if (pageX > offset.left &&
                pageX < (offset.left + trInput.clientWidth) &&
                pageY > offset.top &&
                pageY < (offset.top + trInput.clientHeight)) {
              // Get the input set
              var trHeader = trInput;
              while (!trHeader.classList.contains('header')) {
                trHeader = trHeader.previousElementSibling;
              }
              // Add this file to the input set
              draggable.querySelectorAll('td.size').forEach(function(subElement) {
                subElement.remove();
              });

              var trNew = draggable.querySelector('tr');

              var input = trNew.querySelector('td.checkbox input.checkbox');
              var inputName = input.getAttribute('name');
              var parts = inputName.split('[');
              var connectionIndex = parts[1].substring(0, parts[1].length - 1);
              var fileId          = parts[2].substring(0, parts[2].length - 1);

              // If this is already in the file list (we are moving files around)
              // Then we should reuse the name attribute for it is already an "in_files"
              // attribute.
              if (getParents(tr, 'table.information.input-sets').length > 0) {
                fileId = parts[3].substring(0, parts[3].length - 1);
              }

              parts = trHeader.querySelector('input.checkbox').getAttribute('name').split('[');
              var inputSetId = parts[2].substring(0, parts[2].length - 1);

              // Add a new "Input Set" section 
              if (trHeader.is('tr:last-child')) {
                // This is being added to the new input set
                var newLastInputSet = trHeader.cloneNode(true);
                // Update the attribute name for this header

                // Pull out the input set text and replace the %s with the
                // input set index. The text is placed in the first tr.header
                // which is hidden. This is rendered with the localized
                // text.
                var newInputSetIndex = inputSets.filter('.header').length-2;
                trHeader.querySelector('span.name').textContent = Array.prototype.slice.call(inputSets, 0, 1)[0].textContent.replace("%s", newInputSetIndex);
                var newInputSetId = btoa(newInputSetIndex+1);
                newLastInputSet.find('input').attr('name', "to_files[0][" + newInputSetId + "]");
                trHeader.parentNode.insertBefore(newLastInputSet, trHeader.nextElementSibling);
              }
              trInput.parentNode.insertBefore(trNew, trInput.nextElementSibling);

              var newInputName = 'in_files[' + connectionIndex + '][' + inputSetId + '][' + fileId + ']';
              input.setAttribute('name', newInputName);
              input.setAttribute('checked', true);
              input = trNew.querySelector('input[type="hidden"]');
              input.setAttribute('name', newInputName);

              trNew.stype["opacity"] = 1.0;
              // TODO: animate?

              bindRowDrag(trNew.querySelector('td'));
            }
          });

          if (getParents(tr, 'table.information.input-sets').length > 0) {
            tr.remove();
          }
        };

        draggable.style['left']   = startLeft + "px";
        draggable.style['top']    = startTop  + "px";
        draggable.style['width']  = tr.clientWidth + "px";
        draggable.style['height'] = tr.clientHeight + "px";

        draggable.addEventListener("mouseup", dragEndEvent);
        draggable.addEventListener("touchend", dragEndEvent);
        draggable.addEventListener("touchcancel", dragEndEvent);
        draggable.addEventListener("mousemove", dragEvent);
        draggable.addEventListener("touchmove", dragEvent);
        draggingSurface.addEventListener("mousemove", dragEvent);
        draggingSurface.addEventListener("touchmove", dragEvent);
        document.body.appendChild(draggable);

        document.body.addEventListener('touchmove', dragEvent);
        document.body.addEventListener('mouseup', dragEndEvent);
        document.body.addEventListener('touchend', dragEndEvent);
        document.body.addEventListener('touchcancel', dragEndEvent);

        window.addEventListener('blur', dragEndEvent);
        window.addEventListener('mouseup', dragEndEvent);
        window.addEventListener('touchend', dragEndEvent);
        window.addEventListener('touchcancel', dragEndEvent);
      };
      row.addEventListener('touchstart', drag);
      row.addEventListener('mousedown', drag);
    };
    // TODO: file lists
    /*
    if (getParents(this.element, '.filelists').length > 0) {
      this.element.querySelectorAll('tr.row.file td').forEach(function(subElement) {
        bindRowDrag(subElement);
      });
    }
    */
  };

  Directory.prototype.bindEvents = function() {
    // Bind event to expand hidden sections of the Directory
    this.element.querySelectorAll('tr:not(.add):not(.header) td').forEach(function(subElement) {
      subElement.addEventListener('click', Directory.expandGroup);
    });

    // Bind and initialize workset rows
    this.bindWorksetEvents();

    // Bind and initialize group rows
    this.bindGroupEvents();

    // Bind and initialize experiment rows
    this.bindExperimentEvents();
  };

  Directory.prototype.bindWorksetEvents = function() {
    var worksets = this.element.querySelectorAll('.workset');

    // Add a new table row under each workset that will initially be hidden
    worksets.forEach(function(workset) {
      var new_table = document.createElement('table');
      new_table.classList.add('directory');
      new_table.classList.add('information');

      var table_row = document.createElement("tr");
      table_row.classList.add('row');
      table_row.classList.add('tree');
      table_row.setAttribute('hidden', '');

      var group = document.createElement('td');
      group.setAttribute('colspan', '999');

      group.appendChild(new_table);
      table_row.appendChild(group);

      workset.parentNode.insertBefore(table_row, workset.nextElementSibling);
    });
  };

  /* This function will bind events and initialize rows in the directory
   * that represent Group objects.
   */
  Directory.prototype.bindGroupEvents = function() {
    var groups = this.element.querySelectorAll('.group');

    // Add a new table row under each group that will initially be hidden
    groups.forEach(function(group) {
      var new_table = document.createElement('table');
      new_table.classList.add('directory');
      new_table.classList.add('information');

      var table_row = document.createElement("tr");
      table_row.classList.add('row');
      table_row.classList.add('tree');
      table_row.setAttribute('hidden', '');

      var set = document.createElement('td');
      set.setAttribute('colspan', '999');

      set.appendChild(new_table);
      table_row.appendChild(set);

      group.parentNode.insertBefore(table_row, group.nextElementSibling);
    });
  };

  /* This function will bind events and initialize rows in the directory
   * that represent Experiment objects.
   */
  Directory.prototype.bindExperimentEvents = function() {
    var experiments = this.element.querySelectorAll('td[data-object-type]');

    // Add a new table row under each group that will initially be hidden
    experiments.forEach(function(experiment) {
      experiment = experiment.parentNode;

      var new_table = document.createElement('table');
      new_table.classList.add('directory');
      new_table.classList.add('information');

      var table_row = document.createElement("tr");
      table_row.classList.add('row');
      table_row.classList.add('tree');
      table_row.setAttribute('hidden', '');

      var set = document.createElement('td');
      set.setAttribute('colspan', '999');

      set.appendChild(new_table);
      table_row.appendChild(set);

      experiment.parentNode.insertBefore(table_row, experiment.nextElementSibling);
    });
  };

  Directory.expandGroup = function(event) {
    var span = this.parentNode.querySelector(':scope > td.expand');
    if (!span) {
      return;
    }

    span.classList.toggle('shown');

    // Get associated list
    var group = span.parentNode;
    var group_container = span.parentNode.nextElementSibling;
    var group_list = span.parentNode.nextElementSibling.querySelector(':scope > td > table');
    var directory = getParents(span, 'table.directory.base');

    if (group_list && span.classList.contains('shown')) {
      // Pull in group list, if there are no children
      if (!group_list.querySelector(':scope > tbody > .row')) {
        var group_id         = group.getAttribute('data-group-id');
        var workset_id       = group.getAttribute('data-workset-id');
        var group_revision   = group.getAttribute('data-group-revision');
        var workset_revision = group.getAttribute('data-workset-revision');

        if (group.classList.contains('workset')) {
          $.getJSON('/worksets/' + workset_id + '/' + workset_revision, function(data) {
            data.contains.forEach(function(dependency) {
              if (dependency.type == "group") {
                var item = group.clone();
                item.removeClass('workset').addClass('group');
                item.find('td.expand').removeClass('shown').text("\u25b8");
                item.find('td').on('click', Directory.expandGroup);
                item.find('a').on('click', function(e) { e.stopPropagation(); });
                item.data('group-id', dependency.id);
                item.data('group-revision', dependency.revision);

                var url = "/worksets/" + workset_id + '/' + workset_revision;

                item.find('td.name span').text(dependency.name);
                item.find('td.name a').text(dependency.name).attr('href', url + '/groups/' + dependency.id + '/' + dependency.revision);

                // Workaround a weird jquery bug where it preserves the height (of 0) of a hidden element
                // when showing/hiding.
                item.children('td.name').children('table').css({'height': 'auto'});

                group_list.append(item);

                /* Add sub list */
                item = group.next().clone();
                item.children('td').children('table').children().remove();
                item.hide();
                group_list.append(item);
              }
            });
            data.contains.forEach(function(dependency) {
              if (dependency.type == "experiment") {
                var item = group.clone();
                item.removeClass('workset').addClass('experiment');
                item.find('td.expand').removeClass('shown').text("\u25b8").on('click', Directory.expandGroup);
                item.find('a').on('click', function(e) { e.stopPropagation(); });
                item.data('group-id', dependency.id);
                item.data('group-revision', dependency.revision);

                var url = "/worksets/" + workset_id + '/' + workset_revision;

                item.find('td.name span').text(dependency.name);
                item.find('td.name a').text(dependency.name).attr('href', url + '/experiments/' + dependency.id + '/' + dependency.revision);

                // Workaround a weird jquery bug where it preserves the height (of 0) of a hidden element
                // when showing/hiding.
                item.children('td.name').children('table').css({'height': 'auto'});

                group_list.append(item);

                /* Add sub list */
                item = group.next().clone();
                item.children('td').children('table').children().remove();
                item.hide();
                group_list.append(item);
              }
            });
          });
        }
        else if (group.classList.contains('group')) {
          $.getJSON('/worksets/' + workset_id + '/' + workset_revision + '/groups/' + group_id + '/' + group_revision, function(data) {
            data.contains.forEach(function(dependency) {
              if (dependency.type == "group") {
                var item = group.clone();
                item.find('td.expand').removeClass('shown').text("\u25b8");
                item.find('td').on('click', Directory.expandGroup);
                item.find('a').on('click', function(e) { e.stopPropagation(); });
                item.data('group-id', dependency.id);
                item.data('group-revision', dependency.revision);

                var url = "/worksets/" + workset_id + '/' + workset_revision;

                item.find('td.name a').text(dependency.name).attr('href', url + '/groups/' + dependency.id + '/' + dependency.revision);

                // Workaround a weird jquery bug where it preserves the height (of 0) of a hidden element
                // when showing/hiding.
                item.children('td.name').children('table').css({'height': 'auto'});

                group_list.append(item);

                /* Add sub list */
                item = group.next().clone();
                item.children('td').children('table').children().remove();
                item.hide();
                group_list.append(item);
              }
            });
            data.contains.forEach(function(dependency) {
              if (dependency.type == "experiment") {
                var item = group.clone();
                item.removeClass('group').addClass('experiment');
                item.find('td.expand').removeClass('shown').text("\u25b8");
                item.find('td').on('click', Directory.expandGroup);
                item.find('a').on('click', function(e) { e.stopPropagation(); });
                item.data('group-id', dependency.id);
                item.data('group-revision', dependency.revision);

                var url = "/worksets/" + workset_id + '/' + workset_revision;

                item.find('td.name span').text(dependency.name);
                item.find('td.name a').text(dependency.name).attr('href', url + '/experiments/' + dependency.id + '/' + dependency.revision);

                // Workaround a weird jquery bug where it preserves the height (of 0) of a hidden element
                // when showing/hiding.
                item.children('td.name').children('table').css({'height': 'auto'});

                group_list.append(item);

                /* Add sub list */
                item = group.next().clone();
                item.children('td').children('table').children().remove();
                item.hide();
                group_list.append(item);
              }
            });
          });
        }
        else if (group.classList.contains('experiment')) {
          $.getJSON('/' + group_id + '/invocations/output', function(data) {
            if (group_revision in data) {
              data[group_revision].forEach(function(output) {
                var dependency = output.object;
                if (dependency && dependency.type == "application/json") {
                  var item = group.clone();
                  item.removeClass('experiment').addClass('output').addClass('newTab');

                  item.find('td.expand').remove();
                  item.find('td').css({'cursor': 'pointer'}).on('click', Directory.addOutput);
                  item.find('a').on('click', function(e) { e.stopPropagation(); });

                  item.data('group-id', dependency.id);
                  item.data('group-revision', dependency.revision);

                  var url = "/" +  dependency.id + '/' + dependency.revision;

                  item.find('td.name span').text(dependency.name);
                  item.find('td.name a').text(dependency.name).attr('href', url);

                  // Workaround a weird jquery bug where it preserves the height (of 0) of a hidden element
                  // when showing/hiding.
                  item.children('td.name').children('table').css({'height': 'auto'});

                  group_list.append(item);
                }
              });
            }
          });
        }
      }
      //group_container.slideDown(150);
      //group_container.attr('aria-hidden', 'false');
      span.textContent = "\u25be";
    }
    else {
      //group_container.slideUp(150);
      //group_container.attr('aria-hidden', 'true');
      span.textContent = "\u25b8";
    }
    event.stopPropagation();
    event.preventDefault();
  };

  Directory.addOutput = function(event) {
    event.stopPropagation();
    event.preventDefault();

    var span = $(this);

    var object = span.parent();
    var objectName = object.children('.name').children('span').text();

    var directory = span.parents('table.directory.base');
    var outputTabs = directory.parents('ul.tab-panels.outputs').prev();
    var configTabs = outputTabs.parents('ul.tab-panels').first().children('li.tab-panel.configuration');

    // Add new tab with output data
    var tabStrip = new Occam.Tabs(outputTabs);
    tabStrip.addTab(objectName, function(panel) {
      panel.addClass('results-data');
      panel.data('object-id', object.data('group-id'));
      panel.data('object-revision', object.data('group-revision'));

      var output = new Occam.DataViewer(panel);

      // For each configuration tab... add datapoints
      configTabs.each(function() {
        var configuration = Occam.Configuration.load($(this));
        configuration.dataPoints().each(function() {
          var dataPoint = $(this);
          var labels = configuration.dataPointLabelFor(dataPoint);
          var dataLabel = labels[0];
          var dataKey   = labels[1];

          output.addTarget(dataLabel, dataKey, configuration, dataPoint);
        });
      });

      output.load(function() {
        // Output pane has rendered the data
      });
    });
  };
};
