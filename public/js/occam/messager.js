/*
 * This module handles window messages to and from detached windows and
 * interactive widgets.
 */

var initOccamMessager = function(Occam) {
  'use strict';

  var Messager = Occam.Messager = function() {
    return this;
  };
};
