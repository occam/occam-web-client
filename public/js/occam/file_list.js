'use strict';

/* This file handles the javascript for the object file listings.
 * It also handles file input configuration management for things
 * such as selecting which files to use as input.
 */

var initOccamFileList = function(Occam) {
  var FileList = Occam.FileList = function(element) {
    this.element = element;

    // Initialize events
    this.events = {};

    this.fileListing    = this.element.querySelector(".card.file-list-panel:not(.pending).reveal");
    this.pendingListing = this.element.querySelector(".card.pending.file-list-panel.reveal");

    // Find the file tabs
    this.fileTabs = Occam.Tabs.load(this.element.querySelector("ul.file-tabs.tabs"));

    this.bindEvents();
  };

  /* Fires a callback for the given event.
   */
  FileList.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      var eventInfo = this.events[name];
      eventInfo.callback.call(eventInfo.target, data);
    };
    return this;
  };

  /* Establishes an event callback.
   */
  FileList.prototype.on = function(name, callback, target) {
    if (callback === undefined) {
      return this.events[name];
    }

    if (target === undefined) {
      target = this;
    }

    this.events[name] = {
      "callback": callback,
      "target": target
    };
    return this;
  };

  FileList.loadAll = function(element) {
    var fileLists = element.querySelectorAll('.file-viewer');

    fileLists.forEach(function(subElement) {
      var fileList = new Occam.FileList(subElement);
    });
  };

  /*
   * Loads the given directory by URL.
   *
   * This will load and replace the file listing panel.
   *
   */
  FileList.prototype.loadGroup = function(url, name) {
    var self = this;

    // Remove current file list viewer
    var viewer = this.element.querySelector(".card:not(.viewer):not(.pending)");
    if (viewer) {
      viewer.remove();
    }

    // Show loading graphic
    var pending = this.element.querySelector(".card.pending:not(.viewer)");
    if (pending) {
      pending.removeAttribute("hidden");
    }

    Occam.Util.get(url, function(html) {
      var fakeNode = document.createElement("div");
      fakeNode.innerHTML = html;
      var newCard = fakeNode.querySelector(".card");
      self.element.insertBefore(newCard, self.element.children[0]);
      pending.setAttribute("hidden", true);

      // Rebind the events
      self.bindFileListEvents(newCard);

      Occam.loadAll(newCard);
    });
  };

  /*
   * Loads the given file by URL.
   *
   * The type and name fields will be used to generate the appropriately labeled
   * tab in the file browser.
   *
   */
  FileList.prototype.loadFile = function(url, type, name) {
    var self = this;

    // Show loading graphic
    var pending = this.element.querySelector(".card.pending.viewer");
    if (pending) {
      pending.removeAttribute("hidden");
    }

    // Add a pending tab
    var fileTabTemplate = self.element.querySelector("ul.file-tabs.tabs > template");

    var pendingTab = null;
    if ('content' in fileTabTemplate) {
      pendingTab = document.importNode(fileTabTemplate.content, true);
      pendingTab = pendingTab.querySelector("li");
    }
    else {
      pendingTab = fileTabTemplate.querySelector("li").cloneNode(true);
    }

    var filePanelTemplate = self.element.querySelector("ul.file-tabs.tab-panels > template");

    var pendingPanel = null;
    if ('content' in filePanelTemplate) {
      pendingPanel = document.importNode(filePanelTemplate.content, true);
      pendingPanel = pendingPanel.querySelector("li");
    }
    else {
      pendingPanel = filePanelTemplate.querySelector("li").cloneNode(true);
    }

    // Set the label
    pendingTab.querySelector(".label span.name").textContent = name;
    pendingTab.querySelector(".label span.type").textContent = type;

    // Set the link
    pendingTab.querySelector("a").setAttribute('href', url);

    this.fileTabs.addTab([pendingTab, pendingPanel]);
    this.fileTabs.select(pendingTab);

    Occam.Util.get(url, function(html) {
      var fakeNode = document.createElement("div");
      fakeNode.innerHTML = html;

      var newTab = fakeNode.querySelector(".card.viewer li.tab:first-of-type");

      // Pull the file panel and add that panel
      var newPanel = fakeNode.querySelector(".card.viewer li.tab-panel:first-of-type");

      self.fileTabs.replaceTab(pendingTab, [newTab, newPanel]);

      pending.setAttribute("hidden", true);

      // Pull the file tab and add the tab
      self.bindTabEvents(newTab);

      self.bindDataPanelEvents(newPanel);
      self.setupEditor(url);

      // Apply events on the new card
      Occam.loadAll(newTab);
      Occam.loadAll(newPanel);
    });
  };

  FileList.prototype.bindEvents = function() {
    var self = this;

    this.bindFileListEvents(this.element);
    this.bindDataPanelEvents(this.element);

    this.element.querySelectorAll("ul.file-tabs li.tab:not(.sidebar)").forEach(function(tabElement) {
      self.bindTabEvents(tabElement);
    });

    this.fileTabs.on("change", function() {
      // Make sure the sidebar button reflects the right panel status
      var panel = self.fileTabs.tabPanelAt(self.fileTabs.selected());

      // If there is a panel
      if (panel) {
        var collapse = panel.querySelector(".collapse.right");

        // And that panel has a sidebar (for the run list)
        if (collapse) {
          // Also handle events on that bar
          if (self.collapseEvent) {
            self.collapseEvent[0].removeEventListener("click", self.collapseEvent[1]);
          }
          self.collapseEvent = [collapse, function(event) {
            if (this.classList.contains("reveal")) {
              self.element.querySelector("li.tab.sidebar").classList.add("reveal");
            }
            else if (!this.classList.contains("reveal")) {
              self.element.querySelector("li.tab.sidebar").classList.remove("reveal");
            }
          }];
          collapse.addEventListener("click", self.collapseEvent[1]);
          self.collapseEvent[1].call(collapse);
        }
      }
    });

    this.fileTabs.on("sidebar", function() {
      // Expand the sidebar within the current panel
      var panel = self.fileTabs.tabPanelAt(self.fileTabs.selected());
      if (panel) {
        var collapse = panel.querySelector(".collapse.right");
        collapse.classList.toggle("reveal");

        if (collapse && !collapse.classList.contains("reveal")) {
          self.element.querySelector("li.tab.sidebar").classList.remove("reveal");
        }
      }
    });
  };

  FileList.prototype.bindTabEvents = function(tabElement) {
    var self = this;

    // Handle the close event
    tabElement.querySelector("button.delete").addEventListener('click', function(event) {
      event.stopPropagation();
      event.preventDefault();
      self.fileTabs.removeTab(tabElement);
    });
  };

  FileList.prototype.bindDataPanelEvents = function(element) {
    var self = this;

    this.bindDragDropEvent();

    // Apply collapse event
    const collapseButtons = element.querySelectorAll("h2 > .collapse, ul.file-tabs > .collapse");
    collapseButtons.forEach(function(collapseButton) {
      if (!collapseButton.classList.contains("bound")) {
        collapseButton.addEventListener("mousedown", function(event) {
          self.fileListing.classList.toggle("reveal");
          self.pendingListing.classList.toggle("reveal");
        });
        collapseButton.classList.add("bound");
      }
    });
  };

  FileList.prototype.bindDragDropEvent = function() {
    var self = this;
    let dropArea = document.querySelector(".file-viewer");

    // Associate events
    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
      dropArea.addEventListener(eventName, preventDefaults, false);
      document.body.addEventListener(eventName, preventDefaults, false);
    });

    // Handle dropped files
    dropArea.addEventListener('drop', handleDrop, false);

    //
    function handleDrop(e) {
      var dt = e.dataTransfer;
      var files = dt.files;

      handleFiles(files);
    }

    function handleFiles(files) {
      files = [...files]
      files.forEach(uploadFile)
    }

    function toFriendlyFilesize(bytes) {
      var ret = null;

      Object.entries({
        'B'   : 1024,
        'KiB' : 1024 * 1024,
        'MiB' : 1024 * 1024 * 1024,
        'GiB' : 1024 * 1024 * 1024 * 1024,
        'TiB' : 1024 * 1024 * 1024 * 1024 * 1024
      }).map(function(item) {
        var e = item[0];
        var s = item[1];
        if (!ret && bytes < s) {
          ret = {
            value: (s == 1024 ? bytes : Math.round(bytes / (s / 1024), 2)),
            units: e
          };
        }
      });

      if (!ret) {
        ret = {
          value: bytes,
          units: "B"
        };
      }

      return ret;
    }

    function uploadFile(file, i) {
      var table = self.fileListing.querySelector("table.file-listing tbody");
      var uploadEntry = table.querySelector("tr.upload");
      var form = uploadEntry.querySelector("form");
      var url = form.getAttribute("action");

      // Create a list entry for this uploaded file
      var fileEntryTemplate = self.fileListing.querySelector("table.file-listing template.file");

      var fileUploadEntry;
      if ('content' in fileEntryTemplate) {
        fileUploadEntry = document.importNode(fileEntryTemplate.content, true);
        fileUploadEntry = fileUploadEntry.querySelector("tr");
      }
      else {
        fileUploadEntry = fileEntryTemplate.querySelector("tr").cloneNode(true);
      }

      var sizeTuple = toFriendlyFilesize(file.size);

      fileUploadEntry.classList.add("upload");
      fileUploadEntry.querySelector("td.name").setAttribute("colspan", "2");
      fileUploadEntry.querySelector("td.name span").textContent = file.name;
      fileUploadEntry.querySelector("td.size").textContent = sizeTuple.value;
      fileUploadEntry.querySelector("td.units").textContent = sizeTuple.units;
      fileUploadEntry.querySelector("td.revision").remove();
      table.insertBefore(fileUploadEntry, uploadEntry.nextElementSibling);

      fileUploadEntry.style.backgroundSize = "0% 100%";

      Occam.Util.post(url, {
        fileToUpload: file
      }, {
        onload: function(metadata) {
          window.location = metadata.url;
        },
        onprogress: function(event) {
          if (event.lengthComputable) {
            fileUploadEntry.style.backgroundSize = Math.round((event.loaded / event.total) * 100, 2) + "% 100%";
          }
        }
      }, "json");
    }

    // Prevent defaults
    function preventDefaults(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }

  FileList.prototype.bindFileListEvents = function(element) {
    var self = this;

    this.fileListing = this.element.querySelector(".card.file-list-panel:not(.pending).reveal");

    // Bind click events on each file row
    var rows = element.querySelectorAll("table.file-listing tr td.name a");

    rows.forEach(function(row) {
      row.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        var url = this.getAttribute("href");

        var isGroup = false;

        if (this.parentNode.parentNode.classList.contains("group")) {
          self.loadGroup(url, this.textContent);
          isGroup = true;
        }
        else {
          self.loadFile(url, this.parentNode.parentNode.querySelector("td.type").textContent, this.textContent);
        }
      });
    });

    // When edit is enabled, replace viewer by default editor
    //   Then show development selector radio buttons
    //   Finally hide edit button and show submit & cancel
    //let edit_button=document.getElementById("editbutton");
    //let submit_form = document.getElementById("submit-file-form");
    //let default_viewer=document.getElementById("default-viewer");
    //let default_editor = document.getElementById("default-editor");

    //default_viewer.classList.add("hidden");
    //default_editor.classList.remove("hidden");

    //edit_button.classList.add("hidden");
    //submit_form.classList.remove("hidden");
  };

  FileList.prototype.exitEditMode = function() {
    // When canceling, show edit button again, reset radio
    //  hide form, show viewer, hide editor
    let edit_button=document.getElementById("editbutton");
    let submit_form = document.getElementById("submit-file-form");
    let default_viewer=document.getElementById("default-viewer");
    let default_editor = document.getElementById("default-editor");

    edit_button.classList.remove("hidden");
    submit_form.classList.add("hidden");
    default_viewer.classList.remove("hidden");
    default_editor.classList.add("hidden");
  };

  FileList.prototype.setupEditor = function(url) {
    return;
    var self=this;

    let default_editor = document.getElementById("default-editor");

    let text_area = default_editor.firstElementChild;

    self.current_editor = new OccamEditor(text_area, url);

    var edit_button=document.getElementById("editbutton");
    edit_button.addEventListener("click", function(event) {
      self.enterEditMode();
    });

    let submit_form = document.getElementById("submit-file-form");
    submit_form.querySelector(".cancel").addEventListener("click", function (event){
      event.preventDefault();
      event.stopPropagation();
      self.exitEditMode();
    });

    submit_form.querySelector(".submit").addEventListener("click", function (event){
      event.preventDefault();
      event.stopPropagation();
      alert("Not implemented");
    });

    var radio_selector = document.getElementById("editorselect");
    radio_selector.childNodes.forEach(function(option){
      let editor_class = null;
      switch (option.value) {
        case "ace":
          editor_class = AceEditor;
          break;
        case "codemirror":
          editor_class = CodeMirrorEditor;
          break;
        default:
          editor_class = OccamEditor;
      }
      option.addEventListener('change', function(){
        self.current_editor = self.current_editor.replace(editor_class);
      });
    });
  };
};
