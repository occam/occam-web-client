/* 
 * This module handles the provenance views that visualize tasks.
 */

var initOccamTask = function(Occam) {
  'use strict';

  var Task = Occam.Task = function(element) {
    this.element = element;

    this.buildID = this.element.getAttribute("data-build-id");
    this.taskID = this.element.getAttribute("data-task-id") || this.buildID;

    if (this.taskID) {
      this.task = new Occam.Object(this.taskID);

      this.loadTask();
    }
  };

  /* Renders the task.
   */
  Task.prototype.loadTask = function() {
    const self = this;
    var url = "";
    if (this.buildID) {
      url = "/builds/" + this.buildID;
    }

    Occam.Util.get(Occam.object.url({
      path: url
    }), function(html) {
      self.element.classList.remove("loading");
      self.element.innerHTML = html;

      // Initialize the content
      self.initialize();
    });
  };

  /* Initializes content within the task view
   */
  Task.prototype.initialize = function() {
    // Bind events and initialize other components
    Occam.loadAll(this.element);

    // Load the terminal
    var logTerminalElement = this.element.querySelector(".terminal");
    if (logTerminalElement) {
      Occam.Terminal.load($(logTerminalElement));
    }
  };
}
