'use strict';

/* This file handles the code editor.
 */

// Initialize all selectors on the page
window.addEventListener("load", function() {
  Occam.Tooltip.loadAll(document.body);
});

var initOccamTooltip = function(Occam) {
  var Tooltip = Occam.Tooltip = function(element) {
    var self = this;

    if (element === undefined) {
      return;
    }

    if (Tooltip.element == undefined) {
      var tooltipElement = document.createElement("div");
      tooltipElement.classList.add("tooltip");
      document.body.appendChild(tooltipElement);

      tooltipElement.addEventListener("mouseleave", function(event) {
        this.style.display = "none";
      });

      Tooltip.element = tooltipElement;
    }

    this.element = element;

    Tooltip.count++;
    this.element.setAttribute('data-tooltip-index', Tooltip.count);

    Tooltip._loaded[this.element.getAttribute('data-tooltip-index')] = this;

    this.bindEvents();
    this.events = {};
  };

  Tooltip.count = 0;
  Tooltip._loaded = {};

  Tooltip.loadAll = function(element) {
    var elements = element.querySelectorAll('[title]');

    elements.forEach(function(element) {
      Tooltip.load(element);
    });
  };

  Tooltip.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-tooltip-index');

    if (index) {
      return Occam.Tooltip._loaded[index];
    }

    return new Occam.Tooltip(element);
  };

  Tooltip.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Tooltip.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  Tooltip.prototype.bindEvents = function() {
    var self = this;

    var tooltipText = this.element.getAttribute("title");
    this.element.removeAttribute("title");

    this.element.addEventListener("mouseenter", function(event) {
      var timer = window.setTimeout(function() {
        if (self.element.classList.contains("disable-tooltip")) {
          return;
        }

        var position = self.element.getBoundingClientRect();

        // Set the text and then reveal the tooltip so that it can be measured
        Tooltip.element.innerHTML  = tooltipText;
        Tooltip.element.style.display = "block";

        var tooltipBounds = Tooltip.element.getBoundingClientRect();

        // Place it above the element with the tooltip
        Tooltip.element.style.top  = ((position.top + parseInt(self.element.style.paddingTop || "0") - (tooltipBounds.bottom - tooltipBounds.top)) - 10) + "px";

        // Place the tooltip in the center
        Tooltip.element.style.left = position.left + ((position.right - position.left) / 2) - ((tooltipBounds.right - tooltipBounds.left) / 2) + "px";

        // Show the tooltip
        Tooltip.element.classList.add('reveal');
      }, 500);

      self.element.addEventListener("mouseleave", function(event) {
        window.clearTimeout(timer);
        Tooltip.element.classList.remove('reveal');
        Tooltip.element.style.display = "none";
      });
    });
  };
};
