/* This file handles the search results pages.
 */

var initOccamSearch = function(Occam) {
  var Search = Occam.Search = function(element) {
    // element is the .container.results
    var self = this;

    if (element === undefined) {
      return;
    }

    this.element  = element;
    this.sidebar  = element.querySelector(".search.sidebar");
    this.collapse = element.querySelector(".search.sidebar ~ .collapse");

    // Keep track of the tabs
    this.tabs = Occam.Tabs.load(element.querySelector("ul.tabs"));
    this.tabPanels = this.tabs.tabPanels();

    // Keep track of the sidebar form fields
    this.queryField = this.sidebar.querySelector("input[name=query]");
    this.form = this.sidebar.querySelector("form");

    Search.count++;
    this.element.setAttribute('data-search-index', Search.count);

    Search._loaded[this.element.getAttribute('data-search-index')] = this;

    this.bindEvents();
    this.events = {};
  };

  /* The time in milliseconds before allowing a search.
   * Any searches queued during the timeout will delay the search.
   */

  Search.POLL_TIME = 250;

  Search.count = 0;
  Search._loaded = {};

  Search.loadAll = function(element) {
    var elements = element.querySelectorAll('.container.results');

    elements.forEach(function(element) {
      Search.load(element);
    });
  };

  Search.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-search-index');

    if (index) {
      return Occam.Search._loaded[index];
    }

    return new Occam.Search(element);
  };

  Search.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Search.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  Search.prototype.showFacets = function() {
    this.sidebar.classList.add("reveal");
  };

  Search.prototype.hideFacets = function() {
    this.sidebar.classList.remove("reveal");
    this.tabs.showSidebarButton();
  };

  Search.prototype.submit = function() {
    var self = this;

    // Cancel pending search
    if (self.pendingReq) {
      self.pendingReq.abort();
    }

    // Replace search panels with loading panes

    self.pendingReq = Occam.Util.submitForm(self.form, function(html) {
      // Update the navigation bar
      window.history.replaceState({}, window.document.title, self.pendingReq.responseURL);
 
      // Place html in a container so we can pull from it
      var node = document.createElement("div");
      node.innerHTML = html;
      var panels = node.querySelector(".results ul.tab-panels");

      var newPanels = node.querySelectorAll(".results ul.tab-panels li.tab-panel");
      self.tabPanels.querySelectorAll("li.tab-panel").forEach(function(e, i) {
        e.innerHTML = newPanels[i].innerHTML;
      });

      // Facets
      var newFacets = node.querySelectorAll("ul.facets");
      self.sidebar.querySelectorAll("ul.facets").forEach(function(e, i) {
        e.innerHTML = newFacets[i].innerHTML;
      });

      self.bindFacetEvents();
    });
  };

  Search.prototype.bindFacetEvents = function() {
    var self = this;

    var updateField = function(event) {
      if (self.pollTimer) {
        window.clearTimeout(self.pollTimer);
      }

      self.pollTimer = window.setTimeout(function() {
        self.pollTimer = null;
        self.submit();
      }, Search.POLL_TIME);
    };

    self.sidebar.querySelectorAll("input[type=checkbox]").forEach(function(e) {
      e.addEventListener("change", updateField);
    });
  };

  Search.prototype.bindEvents = function() {
    var self = this;

    self.tabs.on("sidebar", function() {
      self.showFacets();
    });

    // Allow for the sidebar collapse/expand
    self.collapse.addEventListener("click", function(event) {
      event.preventDefault();
      event.stopPropagation();

      if (self.sidebar.classList.contains("reveal")) {
        self.hideFacets();
      }
      else {
        self.showFacets();
      }
    });

    // Progressive search
    var updateField = function(event) {
      event.stopPropagation();
      event.preventDefault();

      if (self.pollTimer) {
        window.clearTimeout(self.pollTimer);
      }

      self.pollTimer = window.setTimeout(function() {
        self.pollTimer = null;
        self.submit();
      }, Search.POLL_TIME);
    };

    self.form.addEventListener("submit", updateField);
    self.queryField.addEventListener("change", updateField);
    self.queryField.addEventListener("keyup", updateField);

    self.bindFacetEvents();
  };
};
