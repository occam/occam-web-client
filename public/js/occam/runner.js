/*
 * This module handles the run panel which lets you select the object to use to
 * run an object and which backend and to queue or run that object. (See:
 * views/objects/_runner.haml)
 */

window.addEventListener("load", function(event) {
});

var initOccamRunner = function(Occam) {
  'use strict';

  var Runner = Occam.Runner = function(element) {
    var self = this;
    this.element = element;

    // Initialize events
    this.events = {};

    this.configurationPanel = element.querySelector(".right-panel + .configuration");

    this.configurations = Array.prototype.map.call(this.configurationPanel.querySelectorAll("form.configuration"), function(e) {
      return Occam.Configuration.load($(e));
    });
    this.configurations.forEach(function(configuration) {
      configuration.on('change', function(data) {
        var loadedPanel = self.currentPanel();
        var iframe = loadedPanel.querySelector("iframe");
        var message = {
          "name": "updateConfiguration",
          "data": {
            "index": 0,
            "name": "",
            "values": data
          }
        };
        if (iframe) {
          iframe.contentWindow.postMessage(message, '*');
        }
      });
    });

    var runListElement = element.querySelector(".right-panel ul.runs");
    if (runListElement) {
      this.runList = Occam.RunList.load(runListElement);

      this.runList.on('change', Occam.Runner.prototype.loadPanel, this);

      this.runList.on('cancel', Occam.Runner.prototype.cancel, this);

      this.runList.on('focus',  Occam.Runner.prototype.focus, this);

      this.runList.on('hidden', function() {
        self.trigger("sidebar-hidden");
      });

      this.runList.on('shown', function() {
        self.trigger("sidebar-shown");
      });
    }

    this.loadedPanel = this.element.querySelector(":scope > .active");

    this.inputInfo = null;
    this.taskInfo  = null;

    this.fullScreen = document.fullScreen         ||
                      document.mozFullScreen      ||
                      document.webkitIsFullScreen ||
                      (window.innerHeight == screen.height);

    var fullScreenEvent = function(event) {
      self.fullScreen = document.fullscreenElement    ||
                        document.mozFullscreenElement ||
                        document.webkitFullscreenElement;
    };

    ['mozfullscreenchange', 'webkitfullscreenchange', 'MSFullscreenChange', 'fullscreenchange'].forEach(function(eventName) {
      document.addEventListener(eventName, fullScreenEvent);
    });

    // Gather the input components
    this.usingAutoComplete = Occam.AutoComplete.load(
        this.element.querySelector('input.auto-complete[name=name]'));
    this.phaseSelector     = Occam.Selector.load(
        this.element.querySelector('button.phase-selector.selector'));
    this.backendSelector   = Occam.Selector.load(
        this.element.querySelector('button.backend-selector.selector'));
    this.dispatchSelector  = Occam.Selector.load(
        this.element.querySelector('button.dispatch-selector.selector'));
    if (this.usingAutoComplete) {
      this.runObject         = this.usingAutoComplete.object();
    }
    this.runButton         = this.element.querySelector('.start-run.button');
    this.queueButton       = this.element.querySelector('.queue-run.button');

    // When 'true', the runner, when the widget requests a resize, can resize
    // the height of the iframe/canvas.
    this.canResize = true;

    // Get the iframe for javascript runs
    this.viewerPanel = this.element.querySelector("div.viewer");
    this.viewerCard = this.element.querySelector('.object-viewer');
    if (this.viewerCard) {
      var iframe = this.viewerCard.querySelector('iframe');
    }

    // Get the terminal for the run
    this.runnerCard = getParents(this.element, '.object-runner.terminal')[0];
    var terminalElement = this.runnerCard.querySelector('.run-terminal .terminal');
    this.terminal = Occam.Terminal.load($(terminalElement));

    this.tabs = Occam.Tabs.load(getParents(this.element, '.tab-panels')[0].parentNode);

    var currentPanel = getParents(this.element, ".tab-panel")[0];

    this.bindEvents();

    if (iframe) {
      this.bindIFrameEvents(iframe);
    }

    if (window.getParameterByName('autorun') === "true") {
      this.run();
    }
  };

  /* Fires a callback for the given event.
   */
  Runner.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      var eventInfo = this.events[name];
      eventInfo.callback.call(eventInfo.target, data);
    };
    return this;
  };

  /* Establishes an event callback.
   */
  Runner.prototype.on = function(name, callback, target) {
    if (callback === undefined) {
      return this.events[name];
    }

    if (target === undefined) {
      target = this;
    }

    this.events[name] = {
      "callback": callback,
      "target": target
    };
    return this;
  };

  Runner.loadAll = function(element) {
    var runners = element.querySelectorAll('.run-viewer');
    runners.forEach(function(element) {
      var runner = new Occam.Runner(element);
    });
  };

  /* Focus on the content of the given run list item.
   */
  Runner.prototype.focus = function(runListItem) {
    var panel = this.loadPanel(runListItem);
    var oldTabIndex = panel.getAttribute("tabindex");
    panel.setAttribute("tabindex", "0");
    panel.focus();
    panel.setAttribute("tabindex", oldTabIndex || "-1");
  };

  /* Asynchronously cancels a run or job
   */
  Runner.prototype.cancel = function(runListItem) {
    var self = this;
    var listItemInfo = this.runList.infoFor(runListItem);

    var form = runListItem.querySelector("form");
    
    Occam.Util.submitForm(form, function(data) {
      //runListItem.setAttribute("data-status", "done");
    });
  };

  Runner.prototype.currentPanel = function() {
    return this.element.querySelector(":scope > .active");
  };

  /* Loads the left-hand panel for the given run list item.
   */
  Runner.prototype.loadPanel = function(runListItem) {
    var self = this;

    var listItemInfo = this.runList.infoFor(runListItem);

    // By default, show run/queue form
    var loadedPanel = this.element.querySelector(".run-form");
    var initialize  = false;
    var panelType   = "queue";

    // Depending on whether or not the execution being tracked is a js viewer, job or a run
    if (listItemInfo.viewerID !== undefined) {
      // Look for the loaded panel
      loadedPanel = this.element.querySelector(':scope > [data-viewer-id="' + listItemInfo.viewerID + '"]');
      panelType = "viewer";
    }
    else if (listItemInfo.runID !== undefined) {
      // Look for the loaded panel
      loadedPanel = this.element.querySelector(':scope > [data-run-id="' + listItemInfo.runID + '"]');
      panelType = "run";

      if (!loadedPanel) {
        // Look at the given run
        var workflowTemplate = this.element.querySelector("template.workflow");
        if ('content' in workflowTemplate) {
          loadedPanel = document.importNode(workflowTemplate.content, true);
          loadedPanel = loadedPanel.querySelector("ul.workflows");
        }
        else {
          loadedPanel = this.workflowTemplate.querySelector("ul.workflows").cloneNode(true);
        }

        loadedPanel.setAttribute("data-run-id", listItemInfo.runID);

        var workflowElement = loadedPanel.querySelector("occam-workflow");
        workflowElement.setAttribute("data-run-id", listItemInfo.runID);
        workflowElement.setAttribute("data-run-object-id", Occam.object.id);
        workflowElement.setAttribute("data-run-object-revision", Occam.object.revision);

        this.element.appendChild(loadedPanel);

        initialize = true;
      }
    }
    else if (listItemInfo.taskID !== undefined) {
      // Look at the given job by opening a terminal
      loadedPanel = this.element.querySelector(':scope > [data-task-id="' + listItemInfo.taskID + '"]');
      panelType = "task";

      if (!loadedPanel) {
        // Look at the given task
        var taskTemplate = this.element.querySelector("template.task");
        if ('content' in taskTemplate) {
          loadedPanel = document.importNode(taskTemplate.content, true);
          loadedPanel = loadedPanel.querySelector(".task-panel");
        }
        else {
          loadedPanel = taskTemplate.querySelector(".task-panel").cloneNode(true);
        }

        loadedPanel.setAttribute("data-build-id", listItemInfo.taskID);

        this.element.appendChild(loadedPanel);

        initialize = true;
      }
    }
    else if (listItemInfo.jobID !== undefined) {
      // Look at the given job by opening a terminal
      loadedPanel = this.element.querySelector(':scope > [data-job-id="' + listItemInfo.jobID + '"]');
      panelType = "job";

      if (!loadedPanel) {
        // Look at the given run
        var terminalTemplate = this.element.querySelector("template.terminal");
        if ('content' in terminalTemplate) {
          loadedPanel = document.importNode(terminalTemplate.content, true);
          loadedPanel = loadedPanel.querySelector(".job-panel");
        }
        else {
          loadedPanel = terminalTemplate.querySelector(".job-panel").cloneNode(true);
        }

        loadedPanel.setAttribute("data-job-id", listItemInfo.jobID);

        this.element.appendChild(loadedPanel);

        initialize = true;
      }
    }
    else if (runListItem.getAttribute("data-status") == "pending") {
      panelType = "pending";
      loadedPanel = this.element.querySelector(".pending");
    }

    self.runList.list.parentNode.setAttribute("data-panel-type", panelType);

    this.loadedPanel.classList.remove("active");
    this.loadedPanel.setAttribute("aria-hidden", "true");
    loadedPanel.setAttribute("aria-hidden", "false");
    loadedPanel.classList.add("active");
    this.loadedPanel = loadedPanel;

    if (initialize) {
      if (listItemInfo.runID !== undefined) {
        // Load the workflow
        var workflow = new Occam.Workflow($(loadedPanel.querySelector("occam-workflow")));
        workflow.on("change", function(runInfo) {
          self.runList.update(runListItem, runInfo.run);
        });

        new Occam.Workflow($(loadedPanel.querySelector("occam-workflow.mock.run")));
      }
      else if (listItemInfo.taskID !== undefined) {
        var task = new Occam.Task(loadedPanel);
      }
      else if (listItemInfo.jobID !== undefined) {
        // Load the terminal
        var terminalElement = loadedPanel.querySelector(".terminal");
        terminalElement.setAttribute("data-terminal-type", "tty");
        terminalElement.setAttribute("data-job-id", listItemInfo.jobID);

        var terminal = Occam.Terminal.load($(terminalElement));
        var iframe   = loadedPanel.querySelector("iframe");
        var events   = loadedPanel.querySelector("ul.events");

        terminal.reset();

        var job = Occam.Job.load(runListItem);

        // Poll for job status
        job.on("start", function(event) {
          self.runList.showDelete(runListItem);
          var conn = job.connect(function(data) {
            terminal.write(data);
          });
          terminal.on("write", function(data) {
            conn.send(data);
          });
        });

        job.on("event", function(event) {
          if (event.type == "port") {
            job.networkInfo(function(networkInfo) {
              var port = event.data.port;
              (networkInfo.ports || []).forEach(function(portInfo) {
                if (portInfo.bind == event.data.port) {
                  port = portInfo.port;
                }
              });

              var eventTemplate = self.element.querySelector('template.event[data-type="port"]');
              var eventElement = null;
              if ('content' in eventTemplate) {
                eventElement = document.importNode(eventTemplate.content, true);
                eventElement = eventElement.querySelector("li.event");
              }
              else {
                eventElement = eventTemplate.querySelector("li.event").cloneNode(true);
              }
              // Update event log entry
              var scheme = event.data.protocol || eventElement.querySelector("span.scheme").textContent.trim();
              var host   = eventElement.querySelector("span.host").textContent.trim();
              var url = scheme + "://" + host + ":" + port + event.data.url;
              eventElement.querySelector("a").setAttribute("href", url);
              eventElement.querySelector("a").textContent = event.data.name || url;
              events.appendChild(eventElement);

              // Ok, but if the url scheme is http, then we cannot open it unless we have a proxy setup
              var proxyURL = url;
              if (scheme == "http" && window.document.location.protocol == "https:") {
                proxyURL = "https://" + window.document.location.host + ":" + (port + 10000) + event.data.url;
              }

              if (event.data.open == "inline") {
                // Hide right panel
                self.runList.showHideList(false);

                terminalElement.setAttribute("hidden", "");
                events.setAttribute("hidden", "");

                iframe.removeAttribute("hidden");
                iframe.src = proxyURL;
              }
            });
          }
        });
        job.eventsLog();
      }
    }

    return loadedPanel;
  };

  Runner.prototype.runData = function(object, runID, callback) {
  };

  Runner.prototype.jobData = function(object, jobID, callback) {
  };

  Runner.prototype.handleUpdateConfiguration = function(message) {
  };

  Runner.prototype.handleUpdateTask = function(iframe, message) {
    var self = this;

    // Resend input data
    if (self.taskInfo) {
      message['data'] = self.taskInfo;
      iframe.contentWindow.postMessage(message, '*');
    }
    else {
      Occam.object.objectInfo(function(info) {
        self.taskInfo = info;
        self.handleUpdateTask(iframe, message);
      });
    }
  };

  Runner.prototype.handleUpdateInput = function(iframe, message) {
    var self = this;

    // Resend input data
    if (self.inputInfo) {
      message['data'] = self.inputInfo;
      if (iframe && iframe.contentWindow) {
        iframe.contentWindow.postMessage(message, '*');
      }
    }
    else {
      var path = iframe.getAttribute('data-input-file');
      Occam.object.objectInfo(function(info) {
        // Force update the path with the requested file
        if (path) {
          info.file = path;
        }

        // Add the object id
        info.id = Occam.object.id;

        // Add the revision to the input file info
        info.revision = Occam.object.revision;

        // Add the access token to the input file info
        if (Occam.object.token) {
          info.token = Occam.object.token;
        }

        self.inputInfo = info;
        self.handleUpdateInput(iframe, message);
      });
    }
  };

  /* File saving from the widget. */
  Runner.prototype.handleUpdateFile = function(iframe, message) {
    if (!message.file.startsWith("/")) {
      message.file += "/";
    }

    var url = Occam.object.url({path: "files" + message.file});

    // POST the new file
    Occam.Util.post(url, message.data, {
      onload: function(metadata) {
        window.location = metadata.url;
      },
      onprogress: function(event) {
        /*if (event.lengthComputable) {
          fileUploadEntry.style.backgroundSize = Math.round((event.loaded / event.total) * 100, 2) + "% 100%";
        }*/
      }
    }, "json");
  };

  Runner.prototype.handleUpdateData = function(iframe, message) {
    var self = this;

    var objectId       = self.id;
    var objectRevision = self.revision;

    if (message.name === 'updateData') {
      var url = "/" + objectId +
        "/"         + objectRevision +
        "/configurations/data";
      $.post(url, JSON.stringify({
        "object_id":       objectId,
        "object_revision": objectRevision,
      }), function(data) {
      });
    }
  };

  Runner.prototype.width = function(value) {
    if (value === undefined) {
      if (this.fullScreen) {
        return window.clientWidth;
      }

      var viewerPanel = this.currentPanel();
      if (viewerPanel) {
        return viewerPanel.clientWidth;
      }

      return this.element.clientWidth;
    }

    return this;
  };

  Runner.prototype.height = function(value) {
    if (value === undefined) {
      if (this.fullScreen) {
        return window.clientHeight;
      }
      return this.currentPanel().clientHeight;
    }

    // Set the height
    if (value == "100%") {
      this.element.style["flex-grow"]   = "1";
      this.element.style["flex-shrink"] = "1";
      this.currentPanel().style["flex-grow"]   = "1";
      this.currentPanel().style["flex-shrink"] = "1";
    }
    else {
      this.element.style["flex-grow"]   = "";
      this.element.style["flex-shrink"] = "";

      //this.element.style.height = value + "px";
    }
    return this;
  };

  Runner.prototype.maxWidth = function() {
    return window.clientWidth;
  };

  Runner.prototype.maxHeight = function() {
    return window.clientHeight;
  };

  Runner.prototype.preferredWidth = function() {
    return this.maxWidth()
  };

  Runner.prototype.preferredHeight = function() {
    if (this.fullScreen) {
      return this.maxHeight();
    }

    return "100%";
  };

  Runner.prototype.resize = function(height) {
    var width = this.width();
    if (this.aspectRatio) {
      height = width / this.aspectRatio;

      if (height > this.preferredHeight()) {
        height = this.preferredHeight();
        width = height * this.aspectRatio;
      }
    }

    if (height) {
      this.height(height);
    }
  };

  /*
   * This event is triggered when the widget requests a resize.
   */
  Runner.prototype.handleUpdateSize = function(iframe, message) {
    if (this.canResize) {
      var height;

      if (message.data && message.data.aspectRatio) {
        // It gave us an aspect ratio, so we can resize width and height.
        this.aspectRatio = message.data.aspectRatio;
        this.resize(this.height());
      }
      else if (message.data && message.data.height) {
        // It gave us an explicit height, so we should just use that (within reason).
        this.aspectRatio = null;
        height = message.data.height;
        this.resize(height);
      }
    }
  };

  Runner.prototype.handleUpdateStatus = function(iframe, message) {
    var self = this;

    if (message.data === 'loading') {
      // The widget has indicated it is loading and we should
      // provide a loading graphic
      self.sendReadyStatus(iframe);
    }
    else if (message.data === 'loaded') {
      // The widget is telling us it has finished loading
    }
  };

  Runner.prototype.sendReadyStatus = function(iframe) {
    var self = this;

    // Tell the widget we are ready
    if (iframe.contentWindow) {
      iframe.contentWindow.postMessage({
        "name":  "updateStatus",
        "data":  "ready",
        "token": iframe.getAttribute("data-token")
      }, '*');
    }
  };

  // Queues a workflow run
  Runner.prototype.queueWorkflow = function() {
    var self = this;

    var input = {};

    var runningObject = Occam.object;
    if (self.usingAutoComplete) {
      runningObject = self.usingAutoComplete.object();

      var input_id       = Occam.object.id;
      var input_revision = Occam.object.revision;

      input = {
        "input_object_id":       input_id,
        "input_object_revision": input_revision
      };
    }

    // Ensure that the backend understands we want an interactive process
    // This is because we will allow people to send input and see the output
    // of the process
    input["interactive"] = true;

    // Create an entry in the run list
    var entryObject = runningObject;
    var entry = this.runList.append({
      status: "pending",
      action: "running",
      name: entryObject.name,
      type: entryObject.type,
      id: entryObject.id,
      revision: entryObject.revision
    });
    this.runList.select(entry);

    // Request the workflow
    var runType = "runs";
    if (this.phaseSelector && this.phaseSelector.selected().getAttribute("data-class") == "build") {
      runType = "builds";
    }

    var urlRuns = runningObject.url({
      "path": runType,
    });

    Occam.Util.post(urlRuns, input, function(data) {
      var fakeNode = document.createElement("div");
      fakeNode.innerHTML = data;
      var newEntry = fakeNode.querySelector("li");
      newEntry = self.runList.replace(entry, newEntry);

      var info = self.runList.infoFor(newEntry);
      if (info.jobID !== undefined) {
        var job = Occam.Job.load(newEntry);

        job.on("done", function(info) {
          if (info.job.status == "failed") {
            newEntry.setAttribute("data-status", "failed");
          }
          else if (info.job.finishTime) {
            newEntry.setAttribute("data-status", "done");
          }
        });
      }
    }, "")
  };

  // Invokes the server to run the current configured run.
  Runner.prototype.run = function(eulaAccepted) {
    var self = this;

    // First, check if there is an EULA that must be agreed upon beforehand
    if (!eulaAccepted && this.runButton.hasAttribute("data-eula-index")) {
      // Get the index of the EULA license
      var eulaIndex = this.runButton.getAttribute("data-eula-index");

      // Bring up a modal for the EULA
      Occam.Modal.open(Occam.object.url({
        path: "/eula"
      }), {
        onSubmit: function(event) {
          event.stopPropagation();
          event.preventDefault();

          self.run(true);

          Occam.Modal.close();
        },
        onClose: function() {
        }
      });
      return;
    }

    // Switch to the log tab if this is a server-driven run
    if (this.isServerSide()) {
      self.queueWorkflow();
      return;

      this.tabs.select(2);

      var object_id = Occam.object.id;
      var object_revision = Occam.object.revision;
      if (self.usingAutoComplete) {
        object_id = self.usingAutoComplete.object().id;
        object_revision = self.usingAutoComplete.object().revision;
      }
      else {
      }
      var input_id = Occam.object.id;
      var input_revision = Occam.object.revision;

      // Update the data message
      this.terminal.data = {
        "object_id":        object_id,
        "object_revision":  object_revision,
        //"input_id":         input_id,
        //"input_revision":   input_revision,
      };

      // Tell the server to run the object
      //this.terminal.open();
      this.terminal.runLink().trigger('click');
    }
    else {
      // Pull the task
      var objectURL = "/task";

      // Hide right panel
      self.runList.showHideList(false);

      // Show viewer card
      var runForm = this.element.querySelector(".run-form");
      runForm.classList.remove('active');

      // We need to clone the viewerPanel
      var newViewer;
      var viewerTemplate = this.element.querySelector("template.viewer");
      if ('content' in viewerTemplate) {
        newViewer = document.importNode(viewerTemplate.content, true);
        newViewer = newViewer.querySelector("div.viewer");
      }
      else {
        newViewer = viewerTemplate.querySelector("div.viewer").cloneNode(true);
      }

      var nextViewerId = self.element.querySelectorAll(".viewer[data-viewer-id]").length;
      newViewer.setAttribute('data-viewer-id', nextViewerId);
      var iframe = newViewer.querySelector('iframe');

      if (this.runButton) {
        const inputId = this.runButton.getAttribute("data-input-id");
        const inputRevision = this.runButton.getAttribute("data-input-revision");
        const inputFile = this.runButton.getAttribute("data-input-file");

        if (inputId && inputRevision) {
          iframe.setAttribute("data-input-id", inputId);
          iframe.setAttribute("data-input-revision", inputRevision);
        }

        if (inputFile) {
          iframe.setAttribute("data-input-file", inputFile);
        }
      }
      iframe.src = "";
      self.bindIFrameEvents(iframe);
      newViewer.classList.add('active');
      self.element.appendChild(newViewer);

      // Load occam general events (mostly for separator)
      Occam.loadAll(newViewer);

      // Bind panel split events
      self.bindSplitEvents(newViewer);

      // Create an entry in the run list
      var entry = this.runList.append({
        status: "viewing",
        action: "viewing",
        type: "generating",
        name: "task"
      });
      entry.setAttribute("data-viewer-id", nextViewerId);
      this.runList.select(entry);

      // TODO: this should be a part of the run-form
      var path = iframe.getAttribute('data-input-file');

      var options = {
        "toEnvironment":  self.targetEnvironment(),
        "toArchitecture": self.targetArchitecture()
      }

      if (iframe.getAttribute("data-input-id")) {
        options["inputs"] = iframe.getAttribute("data-input-id") + "@" + iframe.getAttribute("data-input-revision");
      }

      var currentObject = Occam.object;

      if (self.usingAutoComplete) {
        currentObject = self.usingAutoComplete.object();
      }

      options["fromObject"] = currentObject.id;
      options["fromRevision"] = currentObject.revision;

      if (path) {
        options["path"] = path;
      }

      // Make sure the read token gets passed in
      if (currentObject.token) {
        options["token"] = currentObject.token;
      }

      Occam.Util.get(objectURL, function(data) {
        var running = data.running[data.running.length - 1].process;
        var object  = running[running.length - 1];

        // Add the access token to the input file info
        if (currentObject.token) {
          var subRunning = object.running[object.running.length - 1].process;
          var subObject = subRunning[subRunning.length - 1];
          subObject.token = currentObject.token;

          // TODO: get rid of this when 'input' / 'running' in widget task generation is fixed
          if (subObject.inputs.length > 0) {
            if (subObject.inputs[0]["connections"].length > 0) {
              subObject.inputs[0]["connections"][0].token = currentObject.token;
            }
          }
        }

        self.taskInfo = object;

        // Set iframe object information
        iframe.setAttribute('data-object-id',       object.id);
        iframe.setAttribute('data-object-revision', object.revision);
        iframe.setAttribute('data-object-name',     object.name);
        iframe.setAttribute('data-object-type',     object.type);

        var entryObject = object;
        if (self.usingAutoComplete) {
          entryObject = self.usingAutoComplete.object();
        }
        self.runList.update(entry, {
          name: entryObject.name,
          type: entryObject.type,
          id: entryObject.id,
          revision: entryObject.revision
        });

        // Update iframe url
        var widgetURL = "/" + object.id + "/" + object.revision + "/raw/" + object.file;
        iframe.setAttribute('src', widgetURL);
        iframe.setAttribute('tabindex', '1000');
        iframe.focus();

        if (object.size) {
          self.handleUpdateSize({
            "name": "updateSize",
            "data": object.size
          });
        }
      }, "json", options);
    }
  };

  /*
   * This method returns whether or not there is a server-side option
   * available.
   */
  Runner.prototype.canServerSide = function() {
    var ret = false;
    this.backendSelector.items().forEach(function(item) {
      if (item.getAttribute('data-run-type') === "server") {
        ret = true;
      }
    });

    return ret;
  };

  /*
   * This method returns whether or not there is a client-side option
   * available.
   */
  Runner.prototype.canClientSide = function() {
    var ret = false;
    this.backendSelector.items().forEach(function(item) {
      if (item.getAttribute('data-run-type') === "client") {
        ret = true;
      }
    });

    return ret;
  };

  /*
   * This method returns true when "any" is selected as a backend.
   */
  Runner.prototype.isAny = function() {
    return this.backendSelector.selected().getAttribute('data-icon') === "any";
  };

  /*
   * This method returns true when the given run inputs imply it will run on
   * the server.
   */
  Runner.prototype.isServerSide = function() {
    if (Occam.object.type == "experiment") {
      return true;
    }

    if (this.isAny()) {
      return !this.canClientSide();
    }

    return this.backendSelector.selected().getAttribute('data-run-type') === "server";
  };

  /*
   * This method returns true when the given run inputs imply it will run on
   * the client.
   */
  Runner.prototype.isClientSide = function() {
    if (this.isAny()) {
      return this.canClientSide();
    }

    return this.backendSelector.selected().getAttribute('data-run-type') === "client";
  };

  Runner.prototype.targetEnvironment = function() {
    return this.backendSelector.selected().getAttribute('data-environment');
  };

  Runner.prototype.targetArchitecture = function() {
    return this.backendSelector.selected().getAttribute('data-architecture');
  };

  /*
   * This method returns the environment for the running object.
   */
  Runner.prototype.environment = function() {
    return this.usingAutoComplete.environment();
  };

  /*
   * This method returns the architecture for the running object.
   */
  Runner.prototype.architecture = function() {
    return this.usingAutoComplete.architecture();
  };

  /*
   * This method returns the backend for the running object.
   */
  Runner.prototype.backend = function() {
    return this.usingAutoComplete.backend();
  };

  /*
   * This method returns the list of capabilities required for this task.
   */
  Runner.prototype.capabilities = function() {
    return this.backendSelector.selected().getAttribute('data-capabilities').split(',');
  };

  // Invokes queuing the current configured run.
  Runner.prototype.queue = function() {
  };

  Runner.prototype.bindEvents = function() {
    var self = this;

    if (document.querySelector('.content').classList.contains('minimal')) {
      document.addEventListener('resize', function(event) {
        self.resize(self.height());
      });
    }

    // When a new object is selected, pull out the possible backends
    // and populate the dropdowns for Backend and Dispatch.
    // TODO: Dispatch dropdown
    if (this.usingAutoComplete) {
      this.usingAutoComplete.on('change', function() {
        var selectedObject = this.object();
        var previous = self.backendSelector.selected();
        self.backendSelector.loading();

        selectedObject.objectInfo(function(info) {
          // Is this a javascript widget?
          if (info.environment == "html" && info.architecture == "javascript") {
            // Add the javascript option
            self.backendSelector.dropdown.querySelector('*[data-architecture="javascript"]').removeAttribute('hidden');
          }
          else {
            self.backendSelector.dropdown.querySelector('*[data-architecture="javascript"]').setAttribute('hidden', true);
          }

          // Determine other backends
          selectedObject.backends([["html", "javascript"]], function(backends) {
            self.backendSelector.loading(true);
            /*self.backendSelector.dropdown.querySelector('li').getAttribute('aria-hidden', 'true');
            backends.forEach(function(backend) {
              if ('backend' in backend) {
                self.backendSelector.dropdown.getAttribute('li[data-backend=' + backend.backend + ']').attr('aria-hidden', 'false');
              }
              else {
                self.backendSelector.dropdown.getAttribute('li[data-environment=' + backend.environment + '][data-architecture=' + backend.architecture + ']').attr('aria-hidden', 'false');
              }
            });

            self.backendSelector.dropdown.getAttribute('li[data-icon=any]').attr('aria-hidden', 'false');
            self.backendSelector.loading(true);

            // Keep the last selected backend when it still exists for the
            // selected object. Otherwise, pick the first available option.
            if (previous.getAttribute('aria-hidden') == "false") {
              self.backendSelector.select(previous.parentNode.children.indexOf(previous));
            }
            else {
              var available = self.backendSelector.dropdown.querySelector('li[aria-hidden=false]');
              self.backendSelector.select(available.parentNode.children.indexOf(available));
            }*/
            self.backendSelector.select(1);
          });
        });
      });
    }

    // When the backend is selected, we should update the object selector
    // to filter out only objects that can be used with that backend.
    if (this.backendSelector) {
      this.backendSelector.on('change', function(event) {
      });
    }

    // When the run button is pressed, invoke the run method
    this.runButton.addEventListener('click', function(event) {
      event.stopPropagation();
      event.preventDefault();

      // Pull out the object to run, and the input object if it exists
      var object_id = self

      // Pass those along to trigger a new job
      self.run();
    });

    // Configuration panel close button
    this.configurationPanel.querySelector("button.close").addEventListener("click", function(event) {
      self.configurationPanel.classList.remove("reveal");
    });

    this.runList.on("action.view", function(item) {
      var objectURL = "";
      objectURL += "/" + item.getAttribute("data-object-id");
      objectURL += "/" + item.getAttribute("data-object-revision");
      window.open(objectURL,'_blank');
    });

    // Open the configuration panel
    this.runList.on("action.configure", function(item) {
      self.configurationPanel.classList.add("reveal");
    });

    // Remove the entry and its viewer
    this.runList.on("action.delete", function(item) {
      // Remove the panel
      self.currentPanel().remove();

      // Go to the queue panel
      self.runList.select(0);

      // Remove the entry
      item.remove();
    });

    // Go to fullscreen
    this.runList.on("action.fullscreen", function(item) {
      if (!self.fullScreen) {
        var currentViewer = self.currentPanel();
        if (!currentViewer) {
          return;
        }

        var iframe = currentViewer.querySelector("iframe");

        if (iframe) {
          currentViewer = iframe;
        }

        if (currentViewer.requestFullScreen) {
          currentViewer.requestFullscreen();
        }
        else if (currentViewer.mozRequestFullscreen) {
          currentViewer.mozRequestFullscreen();
        }
        else if (currentViewer.webkitRequestFullscreen) {
          currentViewer.webkitRequestFullscreen();
        }
        else {
          currentViewer.style.position = "fixed";
          currentViewer.style.left = 0;
          currentViewer.style.right = 0;
          currentViewer.style.top = 0;
          currentViewer.style.bottom = 0;
          currentViewer.style.zIndex = 999999;
          currentViewer.style.height = "100%";
          currentViewer.style.width  = "100%";
        }
        currentViewer.focus();
      }
      self.fullScreen = !self.fullScreen;
    });
  };

  /* Establishes events for the split panel.
   */
  Runner.prototype.bindSplitEvents = function(viewer) {
    var self = this;

    var splitSelectorElement = viewer.querySelector("select.selector");
    var splitSelector = Occam.Selector.load(splitSelectorElement);
    splitSelector.clear();
  };

  /* Sets up events for iframe communication.
   */
  Runner.prototype.bindIFrameEvents = function(iframe) {
    var self = this;
    window.addEventListener('message', function(event) {
      var message = event.data;

      // React if the source of the message is the iframe
      if (event.source === iframe.contentWindow) {
        if (message.name === 'updateStatus') {
          self.handleUpdateStatus(iframe, message);
        }
        else if (message.name === 'updateConfiguration') {
          self.handleUpdateConfiguration(iframe, message);
        }
        else if (message.name === 'updateInput') {
          self.handleUpdateInput(iframe, message);
        }
        else if (message.name === 'updateFile') {
          // Stores data to the file (if possible)
          self.handleUpdateFile(iframe, message);
        }
        else if (message.name === 'updateTask') {
          self.handleUpdateTask(iframe, message);
        }
        else if (message.name === 'updateSize') {
          self.handleUpdateSize(iframe, message);
          if (window.parent) {
            window.parent.postMessage(message, '*');
          }
        }
      }
    });

    self.sendReadyStatus(iframe);
  };
}

