/* This file handles pulling objects and tasks from other systems.
 */

var initOccamImporter = function(Occam) {
  var Importer = Occam.Importer = function() {
  };

  Importer.prototype.lookup = function(id_or_url, host) {
    if (id_or_url === undefined) {
      throw("Error: Requires ID or URL as an argument");
    }
    if ((typeof id_or_url) !== "string" && !(id_or_url instanceof String)) {
      throw("Error: ID or URL argument must be a string");
    }

    // Check all known hosts
    if (host === undefined) {
    }
    // Check the given host
    else {
    }
  };
};
