/*
 * This module handles the RunViewer, which is a widget that manages a running
 * task. This displays information like which object is currently running, how
 * long the task is running for, and contains a terminal to see the running
 * process' log.
 */

/*
$(function() {
  var runs = $('ul.runs-list li.run');

  runs.each(function() {
    var run = new Occam.RunViewer($(this));
  });
});
*/

var initOccamRunViewer = function(Occam) {
  'use strict';

  /*
   * This constructor creates an object that represents a Run.
   */
  var RunViewer = Occam.RunViewer = function(element) {
    this.element = element;
    this.events = {};

    // Bind events
    this.bindEvents();
    this.bindWebSocket();
    this.initTerminals();

    // Add to update queue
    this.add();
  };

  /* The number of milliseconds between requests to poll the server.
   */
  RunViewer.UPDATE_INTERVAL_MS = 3000;

  /* The current number of run viewers loaded on the page
   */
  RunViewer.count = 0;

  /* The loaded run widgets on the page
   */
  RunViewer.widgets = [];

  /* Adds a run object to the timer queue. It will initialize the timer for
   * the first run widget it sees
   */
  RunViewer.prototype.add = function() {
    var self = this;

    // Increment number of run viewers
    RunViewer.count++;
    RunViewer.widgets.push(self);

    /* Initialize timer */
    if (RunViewer.count > 0) {
      RunViewer.timerId = setInterval(function() {
        for (var i = 0; i < RunViewer.count; i++) {
          RunViewer.widgets[i].update();
        }
      }, RunViewer.UPDATE_INTERVAL_MS);
    }

    return this;
  }

  /*
   * Initializes the log terminals that are attached to this run widget.
   */
  RunViewer.prototype.initTerminals = function() {
    var self = this;
    var elements = self.element.find('.terminal.log-terminal');

    elements.each(function() {
      var element = $(this);

      var job_id = element.data('job-id');

      // Retrieve the terminal object
      var logTerminal = new window.Occam.Terminal("logs", "tty", {
        "job_id": job_id
      }, element);
    });
  };

  RunViewer.prototype.trigger = function(name) {
    if (this.events[name]) {
      this.events[name].call(this, {});
    };
    return this;
  };

  RunViewer.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  /*
   * This function will bind click events to collapse/expand for the widget.
   */
  RunViewer.prototype.bindEvents = function() {
    this.element.children('span.expand').on('click', function(event) {
      var span = $(this);
      console.log(span);

      span.toggleClass('shown');
      console.log("OK:" + span.attr('class'));
      // Get associated description div
      if (span.hasClass('shown')) {
        span.next().stop().animate({"height": span.data('full-height')});
        span.next().children('.run-header').css({
          "border-bottom": "none",
          "border-bottom-right-radius": "0px",
          "border-bottom-left-radius": "0px"
        });
        span.next().find('.run-action').css({
          "border-bottom": "none",
          "border-bottom-right-radius": "0px",
          "border-bottom-left-radius": "0px",
          "height": "39px"
        });
        span.text("\u25be");
      }
      else {
        span.data('full-height', span.next().height());
        span.next().children('.run-header').css({
          "border-bottom": "none",
          "border-bottom-right-radius": "0px",
          "border-bottom-left-radius": "0px"
        });
        span.next().find('.run-action').css({
          "border-bottom": "none",
          "border-bottom-right-radius": "0px",
          "border-bottom-left-radius": "0px",
          "height": "39px"
        });
        span.next().animate({"height": "40px"}, function() {
          span.next().children('.run-header').css({
            "border-bottom": "",
            "border-bottom-left-radius": "10px"
          });
          span.next().find('.run-action').css({
            "border-bottom": "",
            "border-bottom-right-radius": "10px",
            "height": "40px"
          });
        });
        span.text("\u25b8");
      }
      event.stopPropagation();
      event.preventDefault();
    }).css({
      cursor: 'pointer'
    });
  };

  /*
   * Uses the given run metadata (retrieved from /runs/<id>) and refreshes the
   * content of the run widget to reflect any changes.
   * @param {Object} data The run metadata
   */
  RunViewer.prototype.refresh = function(data) {
    var self = this;
    var runSection = this.element;
    var runId = runSection.data('run-id');
    var workflowSection = runSection.find('.workflow');

    var unfinishedJob = null;
    data["jobs"].forEach(function(job) {
      /* Get the element representing the job on the progress bar */
      var progressElement = runSection.find('.progress-cell[data-job-id=' + job.id + ']');

      /* Get the elements in the workflow visualization for this job */
      var connectionElements = job.connections.map(function(index) {
        return workflowSection.find('.container[data-connection-index=' + index + ']');
      });

      if (job.status != "finished") {
        unfinishedJob = job;
      }

      if (job.status == "finished") {
        progressElement.addClass("complete");
        progressElement.removeClass("in-progress");

        connectionElements.forEach(function(connection) {
          connection.addClass("done");
          connection.removeClass("running");

          connection.find('.spinner').remove();
        });
      }
      else if (job.status == "running") {
        progressElement.removeClass("complete");
        progressElement.addClass("in-progress");

        connectionElements.forEach(function(connection) {
          connection.removeClass("done");
          connection.addClass("running");

          if (connection.children('.container-content').children('.spinner').length == 0) {
            connection.children('.container-content').prepend("<div class='spinner'><div class='top'></div><div class='top-left'></div><div class='top-right'></div><div class='left'></div><div class='right'></div><div class='bottom'></div><div class='bottom-left'></div><div class='bottom-right'></div></div>");
          }
        });
      }
    });

    // If all jobs are done, update the view of the workflow
    if (unfinishedJob == null) {
      var archiveForm = $('<form action="/runs/' + runId + '" method="post"></form>');

      var newArchivedValue = (data.archived == 1 ? 0 : 1);
      var archivedInput = $('<input type="hidden" name="archived" value="' + newArchivedValue + '"></input>');
      archiveForm.append(archivedInput);

      var archiveButton = $('<input type="submit" style="float: right" value="archive"></input>');
      archiveButton.addClass('archive-link');
      archiveButton.addClass('run-action');
      archiveForm.append(archiveButton);

      var cancelLink = runSection.find('.cancel-link').parent();
      cancelLink.replaceWith(archiveForm);
    }
  };

  RunViewer.prototype.update = function() {
    var self = this;
    var runSection = this.element;
    var runId = runSection.data('run-id');

    /* Pull run metadata out of the server */
    $.getJSON('/runs/' + runId, function(data, status, xhr) {
      self.refresh(data);
    });
  };

  /*
   * This function will establish a websocket event for this run section
   * such that when events come in for that run, the widget can reflect them.
   */
  RunViewer.prototype.bindWebSocket = function() {
  };
};
