/* This file handles dropdown widgets and object/person selectors.
 *
 * This will replace any <SELECT> that has a 'selector' class with the dropdown
 * widget. Any <OPTION> with certain attributes will have certain behaviors:
 *
 * <option> attributes:
 * data-icon: the css class to attach with the 'icon' class to add an icon
 */

// Initialize all selectors on the page
window.addEventListener("load", function() {
});

var initOccamSelector = function(Occam) {
  var Selector = Occam.Selector = function(element) {
    if (element === undefined) {
      return;
    }

    if (element.tagName.toUpperCase() == "SELECT") {
      this.element = undefined;
      this.selectElement = element;
    }
    else {
      this.element = element;
      this.selectElement = element.previousElementSibling;
    }

    this.events = {};

    // Bind events
    this.bindEvents();

    Selector.count++;
    this.element.setAttribute('data-loaded-index', 'selector-' + Selector.count);
    this.selectElement.setAttribute('data-loaded-index', this.element.getAttribute('data-loaded-index'));
    Selector._loaded[this.element.getAttribute('data-loaded-index')] = this;
  };

  Selector.count = 0;

  Selector._loaded = {};

  Selector.loadAll = function(element) {
    var selectors = element.querySelectorAll('select.selector');

    selectors.forEach(function(element) {
      Occam.Selector.load(element);
    });
  };

  Selector.load = function(element) {
    if (!element) {
      return;
    }

    var index = element.getAttribute('data-loaded-index');

    if (index) {
      return Selector._loaded[index];
    }

    return new Occam.Selector(element);
  };

  Selector.prototype.trigger = function(name) {
    if (this.events[name]) {
      this.events[name].call(this, {});
    };
    return this;
  };

  Selector.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  /*
   * Clears out the selector and replaces it with a loading spinner.
   */
  Selector.prototype.loading = function(finished) {
    if (finished) {
      this.element.innerHTML = "";
      this.element.style.backgroundImage = "";
      this.element.classList.remove('waiting');
    }
    else {
      this.clear();
      this.element.innerHTML = "";
      this.element.style.backgroundImage = "none";
      this.element.classList.add('waiting');

      var loadingElement = document.createElement("div");
      loadingElement.classList.add("loading");
      this.element.appendChild(loadingElement);
    }
  };

  /*
   * Clears items.
   */
  Selector.prototype.clear = function() {
    this.dropdown.querySelectorAll('button').forEach(function(element) { element.setAttribute('hidden', ''); });
  };

  /*
   * Returns the list of items in the dropdown as an element array.
   */
  Selector.prototype.items = function() {
    return this.dropdown.querySelectorAll('button');
  };

  /*
   * Returns the list item that is currently selected.
   */
  Selector.prototype.selected = function() {
    return this.dropdown.querySelector('button:nth-child('+(parseInt(this.element.getAttribute('data-selected-index'))+1)+')');
  };

  /*
   * This function will select the given list item by its index. Index 0 is the
   * first item in the list.
   */
  Selector.prototype.select = function(index) {
    var last = this.selected();
    if (last) {
      last.removeAttribute("selected");
      last.setAttribute("aria-checked", "false");
    }

    var listItem = this.dropdown.querySelector('button:nth-child(' + (index+1) + ')');
    var selector = this.element;
    selector.textContent = listItem.querySelector('h2').textContent;
    selector.setAttribute('data-original-text', listItem.getAttribute('data-original-text'));
    selector.style.backgroundImage = window.getComputedStyle(listItem.querySelector('h2')).backgroundImage;

    this.selectElement.children[index].setAttribute('selected', true);

    listItem.setAttribute('selected', true);
    listItem.setAttribute('aria-checked', 'true');
    this.element.setAttribute('data-selected-index', index);

    this.trigger('change');
  };

  /* 
   * this function reveals the dropdown list.
   */
  Selector.prototype.open = function() {
    var self = this;

    // Do not load and show the dropdown if the selector is in a loading
    // state.
    if (self.element.classList.contains('waiting')) {
      return;
    }

    self.element.setAttribute("aria-expanded", "true");

    var inputStyle = window.getComputedStyle(self.element);
    var inputWidth  = self.element.offsetWidth - parseInt(inputStyle.borderLeftWidth) - parseInt(inputStyle.borderRightWidth);
    var inputHeight = self.element.offsetHeight;

    var offset = self.element.getBoundingClientRect();

    self.dropdown.style.width = inputWidth + "px";
    self.dropdown.style.height = "";
    self.dropdown.style.display = "block";
    self.dropdown.style.left = offset.left + "px";
    self.dropdown.style.top  = (offset.top + inputHeight) + "px";
    self.dropdown.removeAttribute("hidden");
    if (parseInt(self.dropdown.style.top) + self.dropdown.clientHeight > (window.innerHeight + window.scrollY)) {
      self.dropdown.style.top = (parseInt(self.dropdown.style.top) - self.dropdown.clientHeight - inputHeight) + "px";
    }
    var selectedItem = self.dropdown.querySelector('button[aria-checked="true"]');
    if (selectedItem) {
      selectedItem.setAttribute("tabindex", "0");
      selectedItem.focus();
      selectedItem.setAttribute("tabindex", "-1");
    }
    self.dropdown.removeEventListener('blur', self.blurEvent.bind(self));
    self.dropdown.addEventListener('blur', self.blurEvent.bind(self));
  };

  Selector.prototype.blurEvent = function(event, forceClose) {
    var self = this;

    // Ignore blur event if the target is a button within the dropdown
    if (forceClose || (!event.relatedTarget || event.relatedTarget.parentNode != event.target)) {
      self.dropdown.setAttribute("hidden", true);
      self.element.setAttribute("aria-expanded", "false");
      self.element.focus();
    }
  };

  /*
   * This function initializes the selector and binds interactive events.
   */
  Selector.prototype.bindEvents = function(force) {
    var self = this;

    if (this.element) {
      if (!force && this.element.classList.contains('bound')) {
        return;
      }
    }

    var selectedOption = this.selectElement.querySelector('option[selected]');
    if (!selectedOption) {
      var firstOption = this.selectElement.querySelector('option:first-child');
      if (firstOption) {
        firstOption.setAttribute("selected", true);
      }
    }

    var options = this.selectElement.querySelectorAll('option');

    // Form dropdown section
    var dropdown = this.dropdown = document.createElement("div");
    dropdown.classList.add("dropdown");
    dropdown.setAttribute("tabindex", "0");
    dropdown.setAttribute('hidden', '');
    dropdown.setAttribute('role', 'menu');

    options.forEach(function(option) {
      var item = document.createElement("button");
      var header = document.createElement("h2");
      var attributes = option.attributes;

      item.setAttribute("type", "button");
      item.setAttribute("role", "menuitemradio");
      item.setAttribute("tabindex", "-1");

      for (var i = 0; i < attributes.length; i++) {
        var attr = attributes[i];
        if (attr.name.startsWith("data-")) {
          item.setAttribute(attr.name, attr.value);
        }
      }
      if (option.getAttribute('hidden') !== null) {
        item.setAttribute('hidden', option.getAttribute('hidden'));
      }

      header.setAttribute('data-object-type', option.getAttribute('data-icon') || "object");

      if (option.hasAttribute('class') && option.getAttribute('class').trim() != "") {
        header.classList.add(option.getAttribute('class'));
      }

      if (option.hasAttribute('data-class') && option.getAttribute('data-class').trim() != "") {
        header.classList.add(option.getAttribute('data-class'));
      }

      header.classList.add('icon');
      header.textContent = option.textContent;
      item.setAttribute('data-original-text', option.textContent);

      item.removeAttribute("selected");
      item.setAttribute("aria-checked", "false");

      if (option.hasAttribute("selected")) {
        item.setAttribute("selected", true);
        item.setAttribute("aria-checked", "true");
      }

      if (option.hasAttribute('data-i18n')) {
        header.textContent = option.getAttribute('data-i18n');
      }

      item.appendChild(header);
      dropdown.appendChild(item);
    });

    // Append dropdown to body
    document.querySelector('.content').appendChild(dropdown);

    var selector = this.element;
    var selectedItem = this.selectElement.querySelector('option:checked');
    var selected = 0;

    var current = selectedItem;
    while (current && current.previousElementSibling) {
      selected++;
      current = current.previousElementSibling;
    }

    if (this.element === undefined) {
      selector = document.createElement("button");
      selector.setAttribute('class', this.selectElement.getAttribute('class'));
      selector.setAttribute('aria-haspopup', 'true');
      selector.setAttribute('aria-expanded', 'false');
      selector.setAttribute('type', 'button');
      if (selectedItem) {
        selector.textContent = selectedItem.textContent;
        if (selectedItem.hasAttribute('data-i18n')) {
          selector.textContent = selectedItem.getAttribute('data-i18n');
        }
      }
      this.selectElement.parentNode.insertBefore(selector, this.selectElement.nextElementSibling);
      this.element = selector;
      this.element.setAttribute('data-selected-index', selected);
    }

    if (dropdown.children.length > 0) {
      var originalText = dropdown.children[selected].getAttribute('data-original-text');
      selector.getAttribute('data-original-text', originalText);

      selector.style.backgroundImage = window.getComputedStyle(dropdown.children[selected].querySelector('h2')).backgroundImage;
    }

    this.selectElement.style.display = "none";

    dropdown.querySelectorAll('button').forEach(function(item) {
      item.addEventListener('click', function(event) {
        self.blurEvent(event);

        event.stopPropagation();
        event.preventDefault();

        var index = window.getChildIndex(this);

        self.select(index);
      });

      item.addEventListener('blur', function(event) {
        if (!event.relatedTarget || event.relatedTarget.parentNode != this.parentNode) {
          // Ensure relatedTarget doesn't keep the dropdown open
          self.blurEvent(event, true);
        }
      });

      item.addEventListener('keydown', function(event) {
        var toggle = null;
        if (event.keyCode == 38) { // up arrow
          toggle = item;
          do {
            toggle = toggle.previousElementSibling;
          } while (toggle && toggle.hasAttribute("hidden"));
        }
        else if (event.keyCode == 40) { // down arrow
          toggle = item;
          do {
            toggle = toggle.nextElementSibling;
          } while (toggle && toggle.hasAttribute("hidden"));
        }
        else if (event.keyCode == 27) { // escape
          // Ensure relatedTarget doesn't keep the dropdown open
          self.blurEvent(event, true);
          return;
        }

        if (toggle) {
          toggle.setAttribute("tabindex", "0");
          toggle.focus();
          toggle.setAttribute("tabindex", "-1");
        }
      });
    });

    this.element.addEventListener('keydown', function(event) {
      if (event.keyCode == 40) {
        self.open();
      };
    });

    this.element.addEventListener('click', function(event) {
      event.stopPropagation();
      event.preventDefault();
      console.log(this);

      self.open();
    });

    this.element.classList.add('bound');
  };
};
