/*
 * This module handles the Configuration widget which displays structured forms
 * for viewing and updating configuration options for objects/widgets.
 */

// TODO: Recipes
// TODO: Ajax Submission (Tie in with Object js object)

var initOccamConfiguration = function(Occam) {
  'use strict';

  /*
   * This constructor creates an object that represents an element containing
   * a structured configuration form in OCCAM according to a schema.
   */
  var Configuration = Occam.Configuration = function(element, callback) {
    // Element is the description of the configuration
    //   as defined in views/experiments/_configuration.haml
    //   .tab-panel.configuration
    this.element = element;

    // Give it a sequential id for local(?) use
    this.element.data('configuration-internal-id', Configuration.count);
    Configuration.count++;

    // Store it for fast usage
    Configuration.loaded.push(this);

    //this.element.children('.configuration-search-options').perfectScrollbar();
    //
    this.schema = {};
    // TODO: What is this line doing?
    this.callbacks
    this.events = {};
    this.label = atob(this.element.data('key'));

    // Calling the prototype Configuration.prototype.load(ready)
    this.load(callback);

    return this;
  };

  Configuration.count = 0;
  Configuration.loaded = [];

  Configuration.loadAll = function(element) {
    var configurations = element.querySelectorAll('.content .tab-panel.configuration');

    configurations.forEach(function(element) {
      // Load all configurations
      var configuration = Occam.Configuration.load($(element));
    });
  };

  Configuration.load = function(element, callback) {
    if (element.data('configuration-internal-id') !== undefined) {
      // Already parsed.
      var configuration = Configuration.loaded[element.data('configuration-internal-id')];
      if (callback !== undefined) {
        callback(configuration);
      }
      return configuration;
    }
    else {
      return new Configuration(element, callback);
    }
  };

  /*
   * This helper method pulls out the values representing an array option
   * from the given ul.configuration-group element.
   */
  Configuration.pullArray = function(dest, element) {
    var key = 0;
    element.children('li').each(function() {
      var data = {};

      var group = $(this).children('.configuration-group');
      Configuration.pullData(data, group);

      dest.push(data);
      key += 1;
    });
  };

  /*
   * This helper method pulls out the values representing a group option
   * from the given ul.configuration-group element.
   */
  Configuration.pullData = function(dest, element) {
    var nesting = element.data('nesting');

    element.children('li').each(function() {
      var label = $(this).children('label');
      var base64Key = $(this).data('key');
      var key = atob(base64Key);

      if (label.length > 0) {
        // Simple Data Point

        var value = $(this).children('input').val();

        var type = $(this).children('label').attr('class');
        if (type === "enum") {
          value = $(this).children('.select').find('select option:selected').text();
        }
        else if (type === "boolean") {
          value = $(this).children('input[type=checkbox]').prop('checked');
        }
        else if (type === "float") {
          value = parseFloat(value);
        }
        else if (type === "int") {
          value = parseInt(value);
        }
        else if (type === "datapoints") {
          value = [];

          // Go into datapoint list
          $(this).children('ul').children('li').each(function() {
            var dataPoint = $(this);

            // Form the data point metadata object
            var dataPointMetadata = {};

            // Pull out the object
            var object = dataPoint.children('span.output').first();
            var objectId = object.data('object-id');
            var objectRevision = object.data('object-revision');

            dataPointMetadata.object = {};
            dataPointMetadata.object.id = objectId;
            dataPointMetadata.object.revision = objectRevision;

            dataPointMetadata.nesting = [];

            // Pull out the datapoint nesting
            var keys = dataPoint.children('span:not(.output)');
            keys.each(function() {
              var key = $(this);
              if (key.hasClass('range')) {
                var list = [];

                key.children('span').each(function() {
                  var subKey = $(this);
                  if (subKey.hasClass('range')) {
                    var min = subKey.children('span').first().text();
                    var max = subKey.children('span').slice(1,2).text();
                    list.push([parseInt(min), parseInt(max)]);
                  }
                  else {
                    list.push(parseInt(subKey.text()));
                  }
                });
                dataPointMetadata.nesting.push(list);
              }
              else {
                dataPointMetadata.nesting.push(key.text());
              }
            });

            // Push the data point metadata
            value.push(dataPointMetadata);
          });
        }

        dest[key] = value;
      }
      else {
        // Group / Array
        var group = $(this).children('.configuration-group');

        if ($(this).children('.element').length > 0) {
          dest[key] = [];
          Configuration.pullArray(dest[key], group);
        }
        else {
          dest[key] = {};
          Configuration.pullData(dest[key], group);
        }
      }
    });
  };

  Configuration.prototype.dataPointsData = function() {
    var self = this;

    var dataPointElements = self.dataPoints();

    // For each data point element, retrieve the datapoints listed
    dataPointElements.each(function() {
      var dataPoint = $(this);
    });
  };

  /*
   * This method will pull out the data from the configuration form and return
   * it as an object.
   */
  Configuration.prototype.data = function() {
    var self = this;

    // Parse through the DOM and pull out input values
    if (self._data === undefined) {
      self._data = {};

      var start = this.element.find('.configuration-search-options').children('.configuration-group');
      if (start.length > 0) {
        Configuration.pullData(self._data, start);
      }
      else {
        self._data = undefined;
      }
    }

    return self._data;
  };

  /*
   */
  Configuration.prototype.trigger = function(name, data) {
    if (data === undefined) {
      data = {};
    }

    if (this.events[name]) {
      this.events[name].call(this, data);
    };

    return this;
  };

  /*
   */
  Configuration.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  /*
   * This method returns a collection of all of the datapoint elements.
   */
  Configuration.prototype.dataPoints = function() {
    return this.element.find('label.datapoints');
  };

  Configuration.prototype.dataPointLabelFor = function(datapoint) {
    var thisLabel   = datapoint.text();
    var newElement  = datapoint.parents('div.element');
    var arrayParent = datapoint.parents('li.array').first();

    var name = datapoint.text();
    if (arrayParent.length > 0) {
      // Get the name of the array element (for example, it might be 'Groups')
      name = arrayParent.children('h2').children('.group-label').text();
    }

    if (newElement.length == 0) {
      // Yield the name of the array
      var index = datapoint.parents('li.element').index();
      name = name + "[" + index + "]";
    }

    return [name, thisLabel]
  };

  /*
   * Adds the given data point to the given key.
   */
  Configuration.prototype.addDataPoint = function(objectId, objectRevision, nesting, datapoint, dataViewer) {
    var self = this;

    // Nesting is a list of keys: ['key', 'key', 'key', [0, 1, 2], 'key', ...]
    // The nesting will determine where in the document to find the data points
    //
    // datapoint is the label element of the datapoint to add
    var newElement = datapoint.parents('div.element').first();
    if (newElement.length > 0) {
      // Click on that "add-element" button first
      newElement.first().next().children('input.add-element').trigger('click.append');

      // Then set datapoint to be the new element
      datapoint = newElement.first().next().children('li:last').find('label.datapoints').first();
    }

    // Add datapoint to the given datapoint collection
    var dataPointList = datapoint.parent().children('ul');
    var dataPointItem = $('<li class="datapoint"></li>');
    var objectLink    = $('<span class="output"></span>');
    var objectURL     = "/" + objectId + "/" + objectRevision;
    objectLink.data('object-id', objectId);
    objectLink.data('object-revision', objectRevision);
    objectLink.append('<a href="' + objectURL + '">output</a>');
    dataPointItem.append(objectLink);
    nesting.reverse().forEach(function(key) {
      if (key instanceof Array) {
        var rangeItem = $('<span class="range"></span>');
        rangeItem.append('[');
        key.forEach(function(item) {
          if (item instanceof Array) {
            // Range
            rangeItem.append('<span class="range"><span class="min">' + item[0] + '</span>..<span class="max">' + item[1] + '</span></span>');
          }
          else {
            rangeItem.append('<span class="item">' + item + '</span>');
          }
        });
        rangeItem.append(']');
        dataPointItem.append(rangeItem);
      }
      else if (typeof key == "number") {
        dataPointItem.append('<span class="range">[<span class="item">' + key + '</span>]</span>');
      }
      else {
        dataPointItem.append('<span class="key">' + key + '</span>');
      }
    });
    dataPointList.append(dataPointItem);

    // Update 'data'
    self.updateKey(datapoint);

    //
    if (newElement.length > 0) {
      var labels = self.dataPointLabelFor(datapoint);
      var dataLabel = labels[0];
      var dataKey   = labels[1];

      // Add the datapoint back to the data viewer
      if (dataViewer) {
        dataViewer.addTarget(dataLabel, dataKey, self, datapoint);
      }
    }
  };

  Configuration.prototype.removeDataPoint = function(nesting) {
  };

  Configuration.prototype.bindExpandEvents = function(element) {
    // Description Expanders
    element.find('li:not(.field) > label').on('click', function(e) {
      $(this).parent().children('span.expand').trigger('click');
    }).css({
      cursor: 'pointer'
    });

    element.find('li:not(.field) > span.expand').on('click', function(e) {
      $(this).toggleClass('shown');
      // Get associated description div
      if ($(this).hasClass('shown')) {
        $(this).parent().find('.description').slideDown(150);
        $(this).parent().find('.parameters').slideDown(150);
        $(this).text("[ - ]");
      }
      else {
        $(this).parent().find('.description').slideUp(150);
        $(this).parent().find('.parameters').slideUp(150);
        $(this).text("[+]");
      }
    }).css({
      cursor: 'pointer'
    });

    // Group Expanders
    element.find('h2').on('click', function(e) {
      var span = $(this).children('span.expand');
      span.toggleClass('shown');
      // Get associated description div
      if (span.hasClass('shown')) {
        span.parent().parent().children('ul').slideDown(150);
        span.text("\u25be");
      }
      else {
        span.parent().parent().children('ul').slideUp(150);
        span.text("\u25b8");
      }
    }).css({
      cursor: 'pointer'
    });

    element.find('h2 > span.expand').on('click', function(e) {
    }).css({
      cursor: 'pointer'
    });

    return this;
  };

  /*
   * This method decodes the input name field or nesting fields into an array
   * of keys.
   */
  Configuration.decodeNesting = function(nesting) {
    // Nesting looks like this:
    // data[<connection-index>][<configuration-index>][key1][key2] ...

    // Get the parts of the nesting that matter. That is, remove the
    // "data" and each connection/configuration index. Those indices only
    // matter to the backend when it is determining which configuration
    // form this is.
    var parts = nesting.split('[').slice(3);
    var keys  = parts.map(function(e) { return atob(e.slice(0, e.length-1)); });

    return keys;
  };

  Configuration.prototype.updateKey = function(element) {
    var self = this;

    // Update the data with any changed field
    var dataElement = element.parents("li").first();
    var dataGroup   = element.parents('.configuration-group');
    var base        = element.parents('.configuration-search-options').first();
    var nav         = base.parent().children('ul.configuration-nav');

    // Retrieve keys
    var nesting   = Configuration.decodeNesting(dataGroup.data('nesting'));
    var base64Key = dataElement.data('key');
    var key       = atob(base64Key);

    // Update the data
    var data = self.data();
    nesting.forEach(function(parentKey) {
      if (data instanceof Array) {
        data = data[parseInt(parentKey)];
      }
      else {
        if (data[parentKey] === undefined) {
          data[parentKey] = {};
        }
        data = data[parentKey];
      }
    });
    var value = "";
    var type = element.parent().children('label').attr('class');
    if (type === "enum") {
      value = element.find('select option:selected').text();
    }
    else if (type === "boolean") {
      value = element.prop('checked');
    }
    else if (type === "float") {
      value = element.val();
      value = parseFloat(value);
    }
    else if (type === "int") {
      value = element.val();
      value = parseInt(value);
    }
    else if (type === "string" || type === "color") {
      value = element.val();
    }
    else if (type === "datapoints") {
      value = [];

      // TODO: only parse one datapoint
      var group = {};
      Configuration.pullData(group, dataGroup.first());
      value = group[key];
    }

    if (data !== undefined) {
      data[key] = value;
    }

    var fullKey = btoa(dataGroup.data('nesting') + "[" + base64Key + "]").replace(/=/g, ''); 

    var navName  = nav.find('li[data-ref-key=' + fullKey + '] span.sublabel');
    var navColor = nav.find('li span.color[data-ref-key=' + fullKey + ']');
    if (navName.length > 0) {
      navName.text(value);
    }
    if (navColor.length > 0) {
      navColor.css({'background-color': value});
    }
    var headerName  = base.find('h3.subheader[data-ref-key=' + fullKey + '] span.sublabel');
    var headerColor = base.find('h3.subheader span.color[data-ref-key=' + fullKey + ']');
    if (headerName.length > 0) {
      headerName.text(value);
    }
    if (headerColor.length > 0) {
      headerColor.css({'background-color': value});
    }

    self.trigger('change', self.data());
  };

  /*
   * This method will bind events that are triggered when values change. For
   * instance, when values will disable or change the visibility of other
   * fields.
   */
  Configuration.prototype.bindValueEvents = function(element) {
    var self = this;

    element.find('input:not(.button)').on('change keyup', function(e) {
      self.updateKey($(this));
    });

    // Displays values only when enabled
    element.find('li > .select > select').on('change.show-hide keyup.show-hide', function(e) {
      var enablesCount = parseInt($(this).data("enables-count"));
      var enabled = {};
      for (var i = 0; i < enablesCount; i++) {
        var is = $(this).data("enables-is-" + i);
        var input = $(this).children("option:selected").text();

        var selection = [];
        var keyParts = $(this).data("enables-key-" + i).split(':');
        var key = keyParts[1];
        var group = keyParts[0];
        if (group === "all") {
          var selector = '.configuration-group li[data-key='+key+']';
          selection = self.element.find(selector);
        }
        else if (group === "sibling") {
          var selector = 'li[data-key='+key+']';
          selection = $(this).parent().parent().parent().find(selector);
        }
        if(is === input) {
          enabled[key] = true;
          selection.find('input' ).prop('disabled', false);
          selection.find('select').prop('disabled', false);
        }
        else if (!enabled[key]) {
          selection.find('input' ).prop('disabled', true);
          selection.find('select').prop('disabled', true);
        }
      }
      var disablesCount = parseInt($(this).data("disables-count"));
      var disabled = {};
      for (var i = 0; i < disablesCount; i++) {
        var is = $(this).data("disables-is-" + i);
        var input = $(this).children("option:selected").text();

        var selection = [];
        var keyParts = $(this).data("disables-key-" + i).split(':');
        var key = keyParts[1];
        var group = keyParts[0];
        if (group === "all") {
          var selector = '.configuration-group li[data-key='+key+']';
          selection = self.element.find(selector);
        }
        else if (group === "sibling") {
          var selector = 'li[data-key='+key+']';
          selection = $(this).parent().parent().parent().find(selector);
        }
        if(is === input) {
          disabled[key] = true;
          selection.find('select').prop('disabled', true);
          selection.find('input' ).prop('disabled', true);
        }
        else if (!disabled[key]) {
          selection.find('select').prop('disabled', false);
          selection.find('input' ).prop('disabled', false);
        }
      }
      var showsCount = parseInt($(this).data("shows-count"));
      var shown = {};
      for (var i = 0; i < showsCount; i++) {
        var is = $(this).data("shows-is-" + i);
        var input = $(this).children("option:selected").text();

        var selection = [];
        var keyParts = $(this).data("shows-key-" + i).split(':');
        var key = keyParts[1];
        var group = keyParts[0];
        if (group === "all") {
          var selector = '.configuration-group li[data-key='+key+']';
          selection = self.element.find(selector);
        }
        else if (group === "sibling") {
          var selector = 'li[data-key='+key+']';
          selection = $(this).parent().parent().parent().find(selector);
        }
        if(is === input) {
          shown[key] = true;
          selection.css({
            display:''
          });
        }
        else if (!shown[key]) {
          selection.css({
            display:'none'
          });
        }
      }
      var hidesCount = parseInt($(this).data("hides-count"));
      var hidden = {};
      for (var i = 0; i < hidesCount; i++) {
        var is = $(this).data("hides-is-" + i);
        var input = $(this).children("option:selected").text();

        var selection = [];
        var keyParts = $(this).data("hides-key-" + i).split(':');
        var key = keyParts[1];
        var group = keyParts[0];
        if (group === "all") {
          var selector = '.configuration-group li[data-key='+key+']';
          selection = self.element.find(selector);
        }
        else if (group === "sibling") {
          var selector = 'li[data-key='+key+']';
          selection = $(this).parent().parent().parent().find(selector);
        }
        if(is === input) {
          hidden[key] = true;
          selection.css({
            display:'none'
          });
        }
        else if (!hidden[key]) {
          selection.css({
            display:''
          });
        }
      }
    }).trigger('change.show-hide');

    element.find('li > .select > select').on('change keyup', function(e) {
      // Update data
      self.updateKey($(this).parent());
    });

    return this;
  };

  Configuration.prototype.bindValidation = function(element) {
    var form = element;

    if (form.length != 0) {
      var parsley = form.parsley();
      parsley.destroy();
      parsley = form.parsley({
        errorsContainer: function(field) {
          var elem = document.querySelector('input[data-parsley-id="'+field.__id__+'"]');
          var elemParent = elem.parentNode;
          if (elemParent.classList.contains("tuple")) {
            elemParent = elemParent.parentNode;
          }

          var ret = elemParent.querySelector('.description');
          return $(ret);
        }
      });
    }

    return this;
  };

  Configuration.prototype.bindColorPicker = function(element) {
    // TODO: set the input with any new value from the color picker
    var inputs = element.find('input.color');
    inputs.each(function() {
      var input = $(this);
      input.spectrum({
        showInput: true,                    // Show a textbox
        showAlpha: true,                    // Allow alpha selection
        showPalette: true,
        showSelectionPalette: true,         // Show 'recently-used' palette
        localStorageKey: "spectrum.widget", // Shared palette
        clickoutFiresChange: true,          // Allow blur to set
        replacerClassName: 'color-picker',  // Class name for custom styling
        preferredFormat: "rgb"              // Ensure set value is a hex value
      }).on("dragstop.spectrum", function(event, color) {
        input.val(color.toRgbString());
        input.trigger('change');
      }).on("hide.spectrum", function(event, color) {
        if (color.toRgbString() != input.val()) {
          input.val(color.toRgbString());
          input.trigger('change');
        }
      });
    });

    return this;
  };

  Configuration.prototype.bindArrayEvents = function(element) {
    var self = this;

    element.find('input.delete.circle').on('click.delete-element', function(event) {
      event.preventDefault();
      event.stopPropagation();

      var header = $(this).parent();
      var key    = header.data('ref-key');

      // Find the array element region
      var arrayElement = $(this).parents('li.element').first();
      var arrayGroup   = arrayElement.children('ul');
      var arrayIndex   = arrayElement.index();

      // Find the navigation region
      var navRegion = $(this).parents('.configuration-search-options').first().parent().children('ul.configuration-nav');
      var navHeader = navRegion.find('li[data-ref-key=' + key + ']');

      var baseElement = arrayElement.parent().parent().children('div.element').children('ul');
      var baseNesting = baseElement.data('nesting');

      // Delete from configuration data
      // First, retrieve the base nesting from the template array element:
      var nestingParts = Configuration.decodeNesting(baseNesting);
      var data = self.data();
      // Find the section in the data that is recording this array element
      nestingParts.forEach(function(parentKey) {
        if (data instanceof Array) {
          data = data[parseInt(parentKey)];
        }
        else {
          data = data[parentKey];
        }
      });
      // Remove that data
      data.splice(arrayIndex, 1);

      // Update keys/names to reflect change in index for every following element
      var elements = arrayElement.parent().children('li.element');
      elements.slice(arrayIndex+1).each(function() {
        var nextArrayElement = $(this).children('ul.configuration-group');
        var index = $(this).index() - 1;
        var nextNesting = baseNesting + "[" + (btoa(index).replace(/=/g,'')) + "]";
        nextArrayElement.data('nesting', nextNesting);
        nextArrayElement.find('.configuration-group').each(function() {
          var group = $(this).parents('.configuration-group');
          var groupNesting = group.data('nesting');

          $(this).data('nesting', groupNesting + "[" + $(this).data('key') + "]");
        });

        // Update header and nav bar
        var header = $(this).children('h3.subheader');
        var sublabel = header.children('span.sublabel');
        var color = header.children('span.color');

        if (header.length > 0) {
          var nextNavHeader   = navRegion.find('li[data-ref-key=' + header.attr('data-ref-key') + ']');
          var nextNavSublabel = nextNavHeader.find('span.sublabel');
          var nextNavColor    = nextNavHeader.find('span.color');
          var currentKey      = header.attr('data-ref-key');
          var currentColorKey = nextNavColor.attr('data-ref-key');

          var nextBaseNesting = nextNesting.slice(baseNesting.length);
          var nextKey         = atob(currentKey).slice(baseNesting.length);
          nextKey = nextKey.replace(/\[[^\]]+\]/, '')
          nextKey = baseNesting + nextBaseNesting + nextKey;

          var nextColorKey    = atob(currentColorKey).slice(baseNesting.length);
          nextColorKey = nextColorKey.replace(/\[[^\]]+\]/, '')
          nextColorKey = baseNesting + nextBaseNesting + nextColorKey;

          nextKey = btoa(nextKey).replace(/=/g,'');
          nextColorKey = btoa(nextColorKey).replace(/=/g,'');
          if (color.length > 0) {
            nextNavColor.attr('data-ref-key', nextColorKey);
            color.attr('data-ref-key', nextColorKey);
          }

          nextNavHeader.attr('data-ref-key', nextKey);
          header.attr('data-ref-key', nextKey);
        }
      });

      // Delete array data from the page
      arrayElement.remove();

      // Remove from navigation
      navHeader.remove();

      // Save changes
      self.trigger('change', self.data());
    });

    // Add Button
    element.find('input.add-element').on('click.append', function(event) {
      event.preventDefault();
      event.stopPropagation();

      var baseElement = $(this).parents('li.array').first();
      var newElement = baseElement.find('div.element').children('ul').clone();
      newElement.find('.color-picker').remove();

      // Get the next index
      var index = $(this).parent().children('li').length;

      // Update nesting
      var nesting = newElement.data('nesting');

      // Add to data
      var nestingParts = Configuration.decodeNesting(nesting);

      var data = self.data();
      nestingParts.forEach(function(parentKey) {
        if (data instanceof Array) {
          data = data[parseInt(parentKey)];
        }
        else {
          data = data[parentKey];
        }
      });

      nesting = nesting + "[" + (btoa(index).replace(/=/g, '')) + "]";
      newElement.data('nesting', nesting);

      newElement.find('.configuration-group').each(function() {
        var group = $(this).parents('.configuration-group');
        var baseNesting = group.data('nesting');

        $(this).data('nesting', baseNesting + "[" + $(this).data('key') + "]");
      });

      // Update entries to their default values if they are in lists
      var inputs = newElement.find('input[default_count]');
      inputs.each(function() {
        var input = $(this);
        var defaultCount = parseInt(input.attr('default_count'));
        var defaultIndex = index % defaultCount;
        input.val(atob(input.attr('default_' + defaultIndex)));
      });
      // TODO: do the same for enum values in <select>

      var newItem = $('<li class="element"></li>');
      newItem.append(baseElement.find('div.element').children('h3').clone());
      newItem.append(newElement);

      $(this).before(newItem);

      var elementData = {};

      Configuration.pullData(elementData, newElement);

      data.push(elementData);

      // Add ref keys to headers
      var header = newItem.find('h3.subheader');
      var sublabel = header.find('span.sublabel');
      var color  = header.find('span.color');

      var newKey = btoa(nesting).replace(/=/g, '');
      newItem.attr('id', newKey);

      var newHeaderKey = btoa(nesting + atob(header.attr('data-nesting'))).replace(/=/g, '');
      header.attr('data-ref-key', newHeaderKey);

      if (color.length > 0) {
        var newColorKey = btoa(nesting + atob(color.attr('data-nesting'))).replace(/=/g, '');
        color.attr('data-ref-key', newColorKey);
      }

      // Update the name and color of the element if necessary
      var partialNesting = atob(color.attr('data-nesting'));
      var colorSelector = "li[data-key=" + partialNesting.slice(1,partialNesting.length-1).split('][').join("] li[data-key=") + "]";
      partialNesting = atob(header.attr('data-nesting'));
      var headerSelector = "li[data-key=" + partialNesting.slice(1,partialNesting.length-1).split('][').join("] li[data-key=") + "]";
      // Get the value in the field
      sublabel.text(newElement.find(headerSelector).find('input').val());
      // Get the value in the field
      if (color.length > 0) {
        color.css({'background-color': newElement.find(colorSelector).find('input').val()});
      }

      // Add navigation bar entries if necessary

      // We add them to the nav bar if they are off the top-level group
      if (baseElement.parents('ul.configuration-group').length == 1) {
        // Find the navigation region
        var navRegion = $(this).parents('.configuration-search-options').first().parent().children('ul.configuration-nav');

        // Find the last array element in the navigation bar
        var lastHeader = navRegion.find('li[data-key=' + baseElement.attr('data-key') + ']').last();

        // Add a new navigation entry after this last one
        var newNavHeader = $("<li><a><span class='sublabel'></span></a></li>");
        newNavHeader.attr('data-ref-key', newHeaderKey);
        newNavHeader.find('a').attr('href', '#' + newKey);

        var newSublabel  = newNavHeader.find('span.sublabel');
        newSublabel.text(sublabel.text());

        if (color.length > 0) {
          newSublabel.before($("<span class='color'></span>").attr('data-ref-key', newColorKey).css({'background-color': color.css('background-color')}));
        }
        lastHeader.after(newNavHeader);

        self.bindNavigationEvents(self.element);
      }

      self.bindEvents(newItem);
    });

    element.find('input.add-element').on('click.update', function(event) {
      self.trigger('change', self.data());
    });

    return this;
  };

  Configuration.prototype.bindNavigationEvents = function(element) {
    var navigationBar = element.find('.configuration-nav');

    var navigationLinks = navigationBar.find('li');

    navigationLinks.each(function() {
      var link = $(this);
      if (link.hasClass('bound')) {
        return;
      }

      link.on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();

        var panel = element.find('.configuration-search-options');
        var targetId = $(this).find('a').attr('href');
        var target = element.find(targetId);
        var sectionY = target.position().top + panel.scrollTop();
        panel.stop().animate({'scrollTop': sectionY}, 400);
      });
    });
  };

  Configuration.prototype.bindEvents = function(element) {
    // Expand/Collapse sections and descriptions
    this.bindExpandEvents(element);

    // Disabling/Hide events for value changes
    this.bindValueEvents(element);

    // Validation
    this.bindValidation(element);

    // Color Picker
    this.bindColorPicker(element);

    // Array Add
    this.bindArrayEvents(element);

    // Navigation Bar
    this.bindNavigationEvents(element);

    // Submission??
  };

  /* Returns the id of the object this configuration belongs to.
   */
  Configuration.prototype.objectId = function() {
    return this.element.data('object-id');
  };

  /* Returns the revision of the object this configuration belongs to.
   */
  Configuration.prototype.objectRevision = function() {
    return this.element.data('object-revision');
  };

  /* Returns the connection index within the workflow this configuration is
   * attached to.
   */
  Configuration.prototype.connectionIndex = function() {
    return this.element.data('connection-index');
  };

  /* Returns the configuration index within the object this configuration
   * belongs to.
   */
  Configuration.prototype.inputIndex = function() {
    return this.element.data('input-index');
  };

  Configuration.prototype.load = function(ready) {
    var self = this;

    var object_id            = this.element.data('object-id');
    var object_revision      = this.element.data('object-revision');

    var base_object_id       = this.element.data('base-object-id');
    var base_object_revision = this.element.data('base-object-revision');

    var connection_index     = this.element.data('connection-index');
    var input_index          = this.element.data('input-index');

    // If this is attached to an experiment, then we need to read the
    // experiment's rendering of the configuration form. This rendering will
    // also attach the experiment's current values for the options instead
    // of the defaults.
    var url = "";

    /*if (experiment_id) {
      url = "/worksets/"       + workset_id    + "/" + workset_revision    +
            "/experiments/"    + experiment_id + "/" + experiment_revision +
            "/connections/"    + connection_index +
            "/configurations/" + configuration_index;
    }
    else if (paper_id) {
      url = "/worksets/"       + workset_id    + "/" + workset_revision +
            "/"        + paper_id      + "/" + paper_revision   +
            "/pages/"          + page_index    +
            "/items/"          + item_index    +
            "/configurations/" + configuration_index;
    }
    else {*/
      // Just get the default rendering of the form
    /*}*/

    if (!object_id && base_object_id) {
      url = "/"        + base_object_id + "/" + base_object_revision +
            "/inputs/" + input_index;
    }
    else if (object_id) {
      url = "/"        + object_id + "/" + object_revision +
            "/file";
    }

    if (url === "") {
      return;
    }

    if (Occam.DEBUG) {
      console.debug("Loading Configuration from " + url);
    }

    this.element.addClass("loading");

    // Pull down the HTML
    Occam.Util.get(url, function(data) {
      self.element[0].querySelector(":scope > .configuration").innerHTML = data;

      self.bindEvents(self.element);
      self.element.removeClass('loading');
      self.element.css('height', '');

      Occam.Modal.loadAll(self.element[0]);
      Occam.Tooltip.loadAll(self.element[0]);

      if (ready !== undefined) {
        ready.call(self, self);
      }
    }, "text/html+configuration");

    return this;
  };
};
