/* This file handles the general UI functionality of the website's Card
 * system.
 */

var initOccamCard = function(Occam) {
  var Card = Occam.Card = function(card, rebind) {
    this.card = card;

    if (rebind === undefined) {
      rebind = false;
    }

    if (card.classList.contains("bound") && !rebind) {
      return this;
    }

    if (card.classList.contains("separator")) {
      this.applySeparatorEvents();
    }

    if (card.classList.contains("collapsable")) {
      this.applyCollapsableEvents();
    }

    card.classList.add("bound");
  };

  Card.loadAll = function(element) {
    var cards = element.querySelectorAll('.card');

    cards.forEach(function(element) {
      var card = new Occam.Card(element);
    });
  };

  /* This method applies events and logic to cards which can be collapsed and
   * expanded.
   */
  Card.prototype.applyCollapsableEvents = function() {
    var self = this;

    // Put children into a separate container
    if (self.card.querySelectorAll('.collapsed-card-container').length == 0) {
      var container = document.createElement("div");
      container.classList.add("collapsed-card-container");
      container.setAttribute('aria-hidden', 'false');

      self.card.childNodes.forEach(function(element, i) {
        if (i > 0 && !element.classList.contains("help")) {
          container.appendChild(element);
        }
      });

      // Now we have a container of everything except the header and the
      // help bubble!

      // Append that container to the card
      self.card.appendChild(container);
    }

    // For that first header (h2 or h3), restyle it so it looks and acts
    // interactable. Add interactive events.
    var cardHeader = self.card.querySelector('h2:first-child, h3:first-child');
    cardHeader.style.cursor = "pointer";
    cardHeader.addEventListener("click", function(event) {
      var span = cardHeader.querySelector('span.expand');
      span.classList.toggle('shown');

      // Get associated description div
      var card = span.parentNode.parentNode;

      var container = card.querySelector('.collapsed-card-container');

      if (span.classList.contains('shown')) {
        card.classList.remove('collapsed');
        container.slideDown(250, function() {
          container.attr('aria-hidden', container.attr('aria-hidden') == "true" ? "false" : "true");
        });
        span.textContent = "\u25be";
      }
      else {
        card.classList.add('collapsed');
        container.slideUp(250, function() {
          container.attr('aria-hidden', container.attr('aria-hidden') == "true" ? "false" : "true");
        });
        span.textContent = "\u25b8";
      }

      event.stopPropagation();
      event.preventDefault();
    })

    // Prepend a little arrow indicator next to the header
    if (cardHeader.querySelector('span.expand')) {
      var expandElement = document.createElement("span");
      expandElement.classList.add("expand");
      expandElement.classList.add("shown");
      expandElement.textContent = "\u25be";
      cardHeader.insertBefore(expandElement, cardHeader.firstChild);
    }

    // Expand the first sections
    if (self.card.classList.contains('collapsed') && self.card.querySelector('h2:first-child, h3:first-child') && self.card.querySelector('h2:first-child, h3:first-child').querySelector('.expand.shown')) {
      self.card.children('h2:first-child, h3:first-child').trigger('click');
    }
  };

  /* This method applies events and logic to cards which act as separators
   * between two other cards
   */
  Card.prototype.applySeparatorEvents = function() {
    var self = this;

    var clickY = 0;
    var moving = false;

    var page = self.card.previousElementSibling;
    if (page.getAttribute('aria-hidden') == 'true') {
      page = page.previousElementSibling;
    }
    page.querySelectorAll('.separator-resize').forEach(function(element) {
      var height = page.clientHeight;
      var offset = element.getBoundingClientRect();
      height -= offset.top
      element.style.height = height;
    });

    self.card.addEventListener('mousedown', function(event) {
      event.stopPropagation();
      event.preventDefault();

      if (event.which == 1) {
        clickY = event.pageY;
        moving = true;

        var draggingSurface = document.createElement("div");
        draggingSurface.classList.add("dragging-area");
        draggingSurface.style.width    = "100%";
        draggingSurface.style.height   = "100%";
        draggingSurface.style.zIndex   = 99999;
        draggingSurface.style.position = "fixed";
        draggingSurface.style.left     = 0;
        draggingSurface.style.top      = 0;
        draggingSurface.style.cursor   = "ns-resize";
        document.body.appendChild(draggingSurface);

        var separator = this;
        separator.classList.add('resizing');
        var page = separator.previousElementSibling;
        while (page && (page.hasAttribute('hidden') || page.tagName.toUpperCase() == "TEMPLATE")) {
          page = page.previousElementSibling;
        }
        var elements = page.querySelector('.separator-resize');
        var oldHeight = elements.clientHeight;

        var pageGap = page.clientHeight - elements.clientHeight;

        var dragEvent = function(event) {
          event.stopPropagation();
          event.preventDefault();

          var deltaY = clickY - event.pageY;

          var newCardHeight = oldHeight - deltaY;
          var minHeight = parseInt(window.getComputedStyle(elements).minHeight) || 0;

          if (newCardHeight < minHeight) {
            newCardHeight = minHeight;
          }

          var newPageHeight = newCardHeight + pageGap;

          page.style.height = newPageHeight + "px";
          elements.style.height = newCardHeight + "px";

          page.style.flex = "0 1 auto";
          elements.style.flex = "0 1 auto";
        };

        var dragFinishEvent = function(event) {
          event.stopPropagation();
          event.preventDefault();

          if (event.which == 1) {
            document.body.removeEventListener('mousemove', dragEvent);
            document.body.removeEventListener('mouseup',   dragFinishEvent);

            separator.classList.remove('resizing');

            draggingSurface.remove();
          }
        };

        document.body.addEventListener('mousemove', dragEvent);
        document.body.addEventListener('mouseup',   dragFinishEvent);
      }
    });
  };
};
