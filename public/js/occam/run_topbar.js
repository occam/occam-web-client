/*
 * This module handles the RunTopbar which is a topbar widget that visualizes
 * currently running tasks.
 */

/*$(function() {
  if ($('.topbar #username a').length > 0) {
    // Set the update interval
    var topbar = $('.topbar');

    var widget = new Occam.RunTopbar(topbar);
  }
});*/

var initOccamRunTopbar = function(Occam) {
  'use strict';

  /*
   * This constructor creates an object that represents the top bar run widget.
   */
  var RunTopbar = Occam.RunTopbar = function(element) {
    var self = this;
    this.element = element;

    RunTopbar.timerId = setInterval(function() {
      self.refresh();
    }, RunTopbar.UPDATE_INTERVAL_MS);
  };

  /* The number of milliseconds between requests to poll the server.
   */
  RunTopbar.UPDATE_INTERVAL_MS = 3000;

  RunTopbar.prototype.bindEvents = function() {
  };

  /*
   * This function adds the run task section when it didn't exist before. This
   * happens when a brand new job appears after the page was loaded with an
   * empty queue.
   */

  /*
   * this function removes the task section when the last job is completed.
   */
  RunTopbar.prototype.updateTopbarToNoJob = function() {
    var section = this.element.find('li#job-progress');

    section.children('.task-selector').remove();
    section.children('.current-task').remove();
    section.children('.container').remove();
    section.children('.progress-bar').remove();
    section.children('.job-title').remove();
    var workHistoryURL = this.element.find('#username a').attr('href') + "/runs";
    /* TODO: localization means this will have to be on the page somewhere first: */
    section.append("<p>None</p><a href='" + workHistoryURL + "'>Work History</a>");
  }

  /*
   * This function updates the job progress to replace the information about
   * the current job with new information.
   */
  RunTopbar.prototype.updateTopbarToAddJob = function(runsData) {
    var section = this.element.find('li#job-progress');
    var self = this;

    if (runsData.length == 0) {
      return;
    }

    var data = runsData[0];

    section.children('p').remove();
    section.children('a').remove();

    var taskSelector = $("<div class='task-selector'></div>");
    taskSelector.append("<div class='previous'>&#x25b2;</div>");
    taskSelector.append("<span class='task-number'>1</div>");
    taskSelector.append("/");
    taskSelector.append("<span class='task-count'>" + runsData.length + "</div>");
    taskSelector.append("<div class='next'>&#x25bc;</div>");

    section.append(taskSelector);

    var currentTask = $("<div class='current-task'></div>");
    currentTask.data('run-id', data.id);
    currentTask.append("<div class='label type'></div>");
    currentTask.append("<div class='label name'></div>");

    section.append(currentTask);

    var container = $("<div class='container'></div>");
    var spinner = $("<div class='spinner'></div>");
    spinner.append("<div class='top'></div><div class='top-left'></div>");
    spinner.append("<div class='top-right'></div><div class='left'></div>");
    spinner.append("<div class='right'></div><div class='bottom'></div>");
    spinner.append("<div class='bottom-left'></div><div class='bottom-right'></div>");
    container.append(spinner);

    section.append(container);

    var progressBar = $("<ul class='progress-bar'></ul>");
    section.append(progressBar);

    /* For every job add a progress bar cell */
    var jobs = data.jobs;
    var runningJobData = null;
    jobs.forEach(function(job) {
      var progressCell = $("<li class='progress-cell'></li>");
      progressCell.data('job-id', job.id);
      progressCell.attr('data-job-id', job.id);

      if (job.status == "finished") {
        progressCell.addClass('complete');
      }
      else if (job.status == "running") {
        runningJobData = job;
        progressCell.addClass('in-progress');
      }
      progressBar.append(progressCell);
    });

    var jobTitle = $("<span class='job-title'><a></a></span>");
    section.append(jobTitle);

    if (runningJobData !== null) {
      currentTask.data('job-id', runningJobData.id);
      self.updateTopbarWithNewJob(data, runningJobData);
    }
  };

  RunTopbar.prototype.updateTopbarWithNewJob = function(runData, jobData) {
    var self = this;
    var section = this.element.find('li#job-progress');

    /* Query for job information to populate the current object section */

    /* We need the object for the first connection of our target job. */
    var connectionIndex = jobData.connections[0];
    var experimentURL = "/worksets/" + jobData.workset + "/"
                                     + jobData.worksetRevision + "/experiments/"
                                     + jobData.experiment + "/"
                                     + jobData.experimentRevision;
    var connectionURL = experimentURL + "/connections/" + connectionIndex;

    $.getJSON(connectionURL, function(data, status, xhr) {
      self.element.find('.label.type').text(data["type"]);
      self.element.find('.label.name').text(data["name"]);
    });
  };

  /*
   * This function takes in data and refreshes the topbar widget to reflect any
   * changes.
   */
  RunTopbar.prototype.refresh = function() {
    var self = this;
    var runSection = this.element.find('.current-task');

    if (runSection.length > 0) {
      var runId = runSection.data('run-id');

      /* Pull run metadata out of the server */
      $.getJSON('/runs/' + runId, function(data, status, xhr) {
        self.updateTopbarJob(data);
      });
    }
    else {
      /* Look for any new work to display. */
      var currentPerson = this.element.find('#username a');
      var runsURL = currentPerson.attr('href') + "/runs/running";
      $.getJSON(runsURL, function(data, status, xhr) {
        if (data.length > 0) {
          self.updateTopbarToAddJob(data);
        }
      });
    }
  };

  /*
   * This function, when given run metadata, updates the run section with any
   * changes.
   */
  RunTopbar.prototype.updateTopbarJob = function(data) {
    var self = this;
    var section = this.element.find('li#job-progress');

    /* Get the job id of the current focused job */
    var currentJobId = section.children('.current-task').data('job-id');

    /* Get the elements representing the workflow node */
    var typeElement = section.find('.label.type');
    var nameElement = section.find('.label.name');

    data["jobs"].forEach(function(job) {
      console.log(job);
      /* Get the element representing the job on the progress bar */
      var progressElement = section.find('.progress-cell[data-job-id=' + job.id + ']');

      if (job.status == "finished") {
        progressElement.addClass("complete");
        progressElement.removeClass("in-progress");

        /* If the job currently representing this run has been completed,
         * we must choose another to take its place. */
        if (job.id == currentJobId) {
          /* Go through jobs list and get the next 'running' task */
          var foundJob = false;
          data["jobs"].forEach(function(innerJob) {
            if (innerJob.status == "running") {
              foundJob = true;
              return;
            }
          });
          if (!foundJob) {
            console.log('remove jobs');
            self.updateTopbarToNoJob();
          }
        }
      }
      else if (job.status == "running") {
        progressElement.removeClass("complete");
        progressElement.addClass("in-progress");
      }
    });

    // If all jobs are done, acquire a new run
    // TODO: get the next run task (probably easiest to invoke 'remove task'
    //   followed by 'add new job' since those already exist and work
  };
};
