/* This file handles the image gallery panel.
 *
 * The gallery is a series of images which are represented by a series of thumb
 * images (smaller images). When somebody clicks on the thumb, the larger image
 * is displayed above.
 *
 * By default, a slideshow is started where the images cycle through in the
 * thumbnail order every few seconds (configured using SLIDESHOW_INTERVAL).
 *
 * A particular image may be displayed on page load when targetted in the URL
 * using the hash syntax: "/foo/bar#gallery-image-3" will select the 4th image.
 * This targetting also allows the gallery to be used when javascript is
 * disabled through the use of css ":target" pseudo selector. This is why
 * styling is done on both :target and .active, and why this module has to
 * set the style of the opacity to 0.0 for all elements since the CSS-only
 * way of doing things always reveals the first image, in case no hash is
 * given in the URL.
 *
 * When a particular image is selected in this manner, the slideshow feature is
 * disabled. Also, when somebody clicks on a particular image, it is assumed
 * they want to look at it, so the slideshow is similarly disabled.
 *
 */

var initOccamGallery = function(Occam) {
  var Gallery = Occam.Gallery = function(element) {
    this.element     = element;

    Gallery.count++;
    this.element.setAttribute('data-loaded-index', Gallery.count);

    Gallery._loaded[this.element.getAttribute('data-loaded-index')] = this;

    if (this.element.querySelector("ul.thumbs")) {
      // If there are multiple images, reset the opacity on them
      //
      // This is because there is a failsafe for the CSS-only (js-less) version
      // of the gallery.
      this.element.querySelectorAll("ul.preview li").forEach(function(image) {
        image.style.opacity = 0.0;
        image.classList.remove('active');
      });
    }

    this.bindEvents();
    this.events = {};

    var initialImage = 0;
    this.autoplay = true;

    if (window.location.hash) {
      // See if the hash contains a valid image
      var image = this.element.querySelector("ul.preview li" + window.location.hash);
      if (image) {
        initialImage = parseInt(window.location.hash.split("-")[2]);
      }
      this.autoplay = false;
    }

    this.select(initialImage);
  };

  // Time (in ms) before automatically revealing next image
  Gallery.SLIDESHOW_INTERVAL = 4000;

  // The amount of pixels to ensure are to the left or right of the current
  // thumb.
  Gallery.THUMB_PADDING = 64;

  Gallery.count = 0;

  Gallery._loaded = {};

  Gallery.loadAll = function(element) {
    var galleries = element.querySelectorAll('.gallery');

    galleries.forEach(function(element) {
      Occam.Gallery.load(element);
    });
  }

  Gallery.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-loaded-index');

    if (index) {
      return Occam.Gallery._loaded[index];
    }

    return new Occam.Gallery(element);
  };

  Gallery.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Gallery.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  /*
   * Slideshow timer event which will advance the image.
   */
  Gallery.slideshowTimerEvent = function(self) {
    // Get the current image
    var currentImage = self.element.querySelector("ul.preview .active");
    if (!currentImage) {
      currentImage = self.element.querySelector("ul.preview li:last-child");
    }

    if (!currentImage) {
      return;
    }

    var index = parseInt(currentImage.getAttribute('id').split('-')[2]);
    index++;

    self.select(index);
  };

  /*
   * This function selects and displays the given image by index.
   *
   * If the index is too large, it will use index = 0.
   */
  Gallery.prototype.select = function(index) {
    // Load the appropriate image
    var thumb = this.element.querySelector("ul.thumbs li#gallery-image-thumb-" + index);
    if (!thumb) {
      index = 0;
      thumb = this.element.querySelector("ul.thumbs li#gallery-image-thumb-" + index);
    }
    if (!thumb) {
      return;
    }

    var id = thumb.getAttribute("id").replace('thumb-', '');
    this.element.querySelectorAll("ul.preview .active").forEach(function(image) {
      image.style.opacity = 0.0;
      image.classList.remove('active');
    });
    this.element.querySelectorAll("ul.thumbs .active").forEach(function(activeThumb) {
      activeThumb.classList.remove('active');
    });

    var image = this.element.querySelector("ul.preview #" + id);
    if (image) {
      image.style.opacity = "";
      image.classList.add('active');
    }
    thumb.classList.add('active');

    // Ensure thumb is visible by scrolling thumb area
    var thumbs = this.element.querySelector("ul.thumbs");
    var thumbsW = thumbs.getBoundingClientRect().width;
    var thumbX = thumb.getBoundingClientRect().x - thumbs.getBoundingClientRect().x;
    var thumbW = thumb.getBoundingClientRect().width;

    if (thumbX + thumbW + Gallery.THUMB_PADDING > thumbsW) {
      thumbs.scrollLeft += (thumbX + thumbW + Gallery.THUMB_PADDING) - thumbsW;
    }
    else if (thumbX < Gallery.THUMB_PADDING) {
      thumbs.scrollLeft += (thumbX - Gallery.THUMB_PADDING);
    }

    // Reset the autoplay timer
    if (this.timer) {
      window.clearInterval(this.timer);
    }
    if (this.autoplay) {
      this.timer = window.setInterval(Gallery.slideshowTimerEvent, Gallery.SLIDESHOW_INTERVAL, this);
    }
  };

  /*
   * This function initializes the gallery and binds interactive events.
   */
  Gallery.prototype.bindEvents = function(force) {
    var self = this;

    if (this.element) {
      if (!force && this.element.classList.contains('gallery-loaded')) {
        return;
      }
    }

    // Bind the thumb image events
    self.element.querySelectorAll("ul.thumbs li a").forEach(function(thumb) {
      thumb.addEventListener("click", function(event) {
        // Get the index of this thumb
        var index = parseInt(thumb.parentNode.getAttribute("id").split('-')[3]);

        // Turn off slideshow
        self.autoplay = false;

        // Select this index
        self.select(index);
      });
    });

    self.element.classList.add('gallery-loaded');
  };
};
