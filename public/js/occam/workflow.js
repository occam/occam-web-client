/* This file handles the interactivity of workflows.
 */

// TODO: accept a lack of a 'position' in imported json
// TODO: accept a lack of 'inputs' and 'outputs'

// Initialize any Workflows on the current page
window.addEventListener("load", function(event) {
});

var initOccamWorkflow = function(Occam) {
  var Workflow = Occam.Workflow = function(element, index) {
    var self = this;

    // Initialize events
    self.events = {};

    self.configurationPanel = Occam.ConfigurationPanel.loadAll(element[0])[0];

    var index = Occam.object.index.slice();
    index.push(0);
    self.workflowObject = new Occam.Object(Occam.object.rootId, Occam.object.rootRevision, "workflow", "Main", undefined, index, Occam.object.link);

    self.element = element;
    self.element.css('max-height', 'none');

    // Look for a run id
    self.runID = self.element[0].getAttribute("data-run-id");
    self.lastPoll = false;

    if (self.runID) {
      // This represents a run
      // The workflow should not be editable
      // And we need to get the run information
      self.runObject = new Occam.Object(self.element[0].getAttribute("data-run-object-id"),
                                        self.element[0].getAttribute("data-run-object-revision"));
      self.runData(self.runObject, self.runID, function(data) {
        // Trigger started event only once!
        self.initialData = data;

        // Create a polling timer for updating the run (if it is not finished/failed)
        if (!data.run.failureTime && !data.run.finishTime) {
          self.pollRunTimer = window.setInterval(function() {
            if (self.lastPoll) {
              window.clearInterval(self.pollRunTimer);
            }

            // Poll the run info and pass it to the workflow
            self.pollRun();
          }, Workflow.POLL_TIME);
        }

        self.initializeWidget(data.nodes);
      });
    }
    else {
      self.initializeWidget();
    }
  };

  Workflow.loadAll = function(element) {
    var workflows = element.querySelectorAll('.content occam-workflow');

    workflows.forEach(function(element, index) {
      var workflow = new Occam.Workflow($(element), index);
    });
  };

  /* The time in milliseconds between polling for updates in a run.
   */

  Workflow.POLL_TIME = 2000;

  /* Fires a callback for the given event.
   */
  Workflow.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      var eventInfo = this.events[name];
      eventInfo.callback.call(eventInfo.target, data);
    };
    return this;
  };

  /* Establishes an event callback.
   *
   * Events:
   *
   * change: When the run status for this workflow changes.
   * done:   When the workflow run completes.
   */
  Workflow.prototype.on = function(name, callback, target) {
    if (callback === undefined) {
      return this.events[name];
    }

    if (target === undefined) {
      target = this;
    }

    this.events[name] = {
      "callback": callback,
      "target": target
    };
    return this;
  };

  Workflow.prototype.pollRun = function() {
    var self = this;

    self.runData(self.runObject, self.runID, function(data) {
      // Pass along job details to workflow
      self.workflow.updateJobs(data.nodes);

      // Detect a change in the run status
      if (data.run.finishTime != self.initialData.run.finishTime) {
        self.trigger('change', data);
      }

      if (data.run.finishTime || data.run.failureTime) {
        self.lastPoll = true;
        self.trigger('done', data);
      }

      // Also pass along relevant information to the job list panel
      var jobPanel = self.element[0].querySelector(".jobs.sidebar");
      if (jobPanel) {
        var jobsList = jobPanel.querySelector("ul.jobs");

        var nodeIndex = jobsList.getAttribute("data-node-index");
        if (nodeIndex) {
          if (data.nodes && data.nodes[nodeIndex] && data.nodes[nodeIndex].jobs) {
            var runList = Occam.RunList.load(jobsList);
            data.nodes[nodeIndex].jobs.forEach(function(job, i) {
              // Update job information
              var entry = runList.elementFor(i);
              if (entry) {
                runList.update(entry, job);
              }
            });
          }
        }
      }
    });
  };

  Workflow.prototype.runData = function(object, runID, callback) {
    Occam.Util.get(object.url({"path": "runs/" + runID}), function(data) {
      callback(data);
    }, "json");
  };

  Workflow.prototype.jobList = function(object, runID, nodeIndex, callback) {
    Occam.Util.get(object.url({"path": "runs/" + runID + "/" + nodeIndex}), function(html) {
      callback(html);
    }, "text/html");
  };

  Workflow.prototype.initializeWidget = function(jobs) {
    var self = this;

    var options = {};

    options.buttons = [];

    if (self.runID || self.element[0].classList.contains("run")) {
      options.allowSelections    = false;
      options.allowNodeMovement  = false;
      options.allowWireSelection = false;
      options.buttons.push({
        classes: ["view-jobs-button"]
      });
    }
    else {
      options.buttons.push({
        classes: ["view-button"]
      });
      options.buttons.push({
        classes: ["configure-button"]
      });
    }

    self.workflow = new window.Workflow(self.element[0], options, jobs);
    var sidebar = self.element.find(".sidebar li.connection")[0];
    if (sidebar) {
      var selectionNode = window.Workflow.Node.createFor(sidebar, self.workflow);

      var empty = self.element.find('.input.start').length == 1;

      self.sidebar = self.element.find('.sidebar:not(.right)');
      self.sidebar2 = self.element.find('.sidebar.right');

      self.element.appear();
      self.element.one('appear', function() {
        self.workflow.redraw();
        selectionNode.arrangeOutputWires();
        selectionNode.arrangeInputWires();
        selectionNode.repositionInputs();
      });
    }

    self.workflow.on("button-click", function(event) {
      if (event.element.classList.contains("configure-button")) {
        self.showConfigurations();
        self.configurationPanel.loadConfiguration(event.node.index(), self.workflowObject, {
          id:       event.node.element.getAttribute("data-object-id"),
          revision: event.node.element.getAttribute("data-object-revision")
        })
      }
      else if (event.element.classList.contains("view-button")) {
        var objectURL = "";
        objectURL += "/" + event.node.element.getAttribute("data-object-id");
        objectURL += "/" + event.node.element.getAttribute("data-object-revision");
        window.open(objectURL,'_blank');
      }
      else if (event.element.classList.contains("view-jobs-button")) {
        var jobPanelCollapse = self.element[0].querySelector(".collapse.jobs");
        var jobPanel = self.element[0].querySelector(".jobs.sidebar");

        // Open the jobs list panel
        if (jobPanelCollapse) {
          jobPanelCollapse.classList.remove("reveal");
        }

        if (jobPanel) {
          // Make sure the loading icon is displayed
          var jobsList = jobPanel.querySelector("ul.jobs");
          var loadingDiv = jobPanel.querySelector(".loading");
          var runList = Occam.RunList.load(jobsList);
          runList.clear();

          // Destroy existing terminal
          var terminalElement = self.element[0].querySelector("*:not(template) > .terminal.job-viewer");
          if (terminalElement) {
            terminalElement.remove();
          }

          // Update object node visualization
          var node = jobPanel.querySelector(".connection.dummy");

          // Prevent it from being colored as the default 'help' colors
          node.classList.remove("initial");

          // Update name and icon
          var name = event.node.nameLabel();
          node.querySelector(".name").textContent = name;
          var type = event.node.typeLabel();
          node.querySelector(".type").textContent = type;
          var icon = event.node.element.querySelector("img.icon");
          if (icon) {
            node.querySelector("img.icon").src = icon.src;
          }

          if (!loadingDiv) {
            loadingDiv = document.createElement("div");
            loadingDiv.classList.add("loading");
            jobsList.appendChild(loadingDiv);
          }

          jobsList.setAttribute("data-node-index", event.node.index());

          // Populate the jobs list
          self.jobList(self.runObject, self.runID, event.node.index(), function(html) {
            loadingDiv.remove();

            runList.loadHTML(html);

            runList.on("change", function(item) {
              var jobID = item.getAttribute("data-job-id");

              // Destroy existing terminal
              var terminalElement = self.element[0].querySelector("*:not(template) > .terminal.job-viewer");
              if (terminalElement) {
                terminalElement.remove();
              }

              // Clone template terminal
              var template = self.element[0].querySelector("template.job-terminal");
              var newTerminal = null;
              if ('content' in template) {
                newTerminal = document.importNode(template.content, true);
                newTerminal = newTerminal.querySelector("div");
              }
              else {
                newTerminal = template.querySelector("div").cloneNode(true);
              }

              if (newTerminal) {
                template.parentNode.insertBefore(newTerminal, template.nextElementSibling);

                terminalElement = newTerminal;
              }

              // Update terminal
              if (terminalElement) {
                terminalElement.setAttribute("data-job-id", jobID);
                var terminal = Occam.Terminal.load($(terminalElement));

                runList.on("action.fullscreen", function(item) {
                  // Fullscreen the terminal
                  if (terminalElement.requestFullScreen) {
                    terminalElement.requestFullscreen();
                  }
                  else if (terminalElement.mozRequestFullscreen) {
                    terminalElement.mozRequestFullscreen();
                  }
                  else if (terminalElement.webkitRequestFullscreen) {
                    terminalElement.webkitRequestFullscreen();
                  }
                  else {
                    terminalElement.style.position = "fixed";
                    terminalElement.style.left = 0;
                    terminalElement.style.right = 0;
                    terminalElement.style.top = 0;
                    terminalElement.style.bottom = 0;
                    terminalElement.style.zIndex = 999999;
                    terminalElement.style.height = "100%";
                    terminalElement.style.width  = "100%";
                  }

                  terminalElement.onfullscreenchange =
                  terminalElement.onwebkitfullscreenchange =
                  terminalElement.onmozfullscreenchange = 
                  terminalElement.MSFullscreenChange = function(event) {
                    terminalElement.classList.toggle("fullscreen");
                  };

                  terminalElement.focus();
                });
              }
            });
          });
        }
      }
    });

    self.workflow.on("node-removed", function(event) {
      var node = event.node;
      var element = event.element;

      var index = parseInt(element.getAttribute('data-index'));

      // Remove configuration tab
      //self.configurationTabs.removeTab(index);
    });

    self.connections = self.element.children("ul.connections").first();
    self.draggable = self.connections;

    if (self.element[0].classList.contains("editable")) {
      var blah = self.sidebar[0].querySelector('h2:first-child');
      blah.addEventListener('click', function(event) {
        self.exportJSON();
      });
      self.setupSaveButton();

      self.connectToConfigurationTabs();
      self.applySidebarEvents();
      self.applySidebarNodeEvents(self.sidebar.find("li.connection"));
    }

    if (self.element[0].querySelector(".collapse")) {
      self.initializeSidebar();
    }

    // Load from the experiment
    if (!self.element[0].classList.contains("mock")) {
      Occam.object = Occam.object || new Occam.Object();
      if (Occam.object && Occam.object.type == "experiment") {
        Occam.object.objectInfo(function(info) {
          (info.contains || []).forEach(function(item) {
            if (item.type == "workflow") {
              self.loadFrom(new Occam.Object(item.id, item.revision, item.type));
            }
          });
        });
      }
    }
  };

  Workflow.prototype.setupSaveButton = function() {
    var self = this;
    this.save = this.element.children('.save');
    this.save.on('click', function(event) {
      var experiment_data = self.exportJSON();

      var url = Occam.object.url({path: "/0/files/data.json"});

      // POST the new workflow data
      Occam.Util.post(url, experiment_data, {
        onload: function(metadata) {
          var query = (window.location.href.split("?", 2)[1] || "");
          var newURL = metadata.url.split("?", 2)[0] + "/../../../workflow";
          if (query) {
            newURL += "?" + query;
          }
          window.location.replace(newURL);
        },
        onprogress: function(event) {
          /*if (event.lengthComputable) {
            fileUploadEntry.style.backgroundSize = Math.round((event.loaded / event.total) * 100, 2) + "% 100%";
          }*/
        }
      }, "json");
    });
  };

  /* This function gets references to the tab strip for configurations.
   */
  Workflow.prototype.connectToConfigurationTabs = function() {
    var tabElement = this.element.parent().next().children('.tabs');
    this.configurationTabs = Occam.Tabs.load(tabElement[0]);
  };

  /* This function shows the object selection sidebar, if obscured.
   */
  Workflow.prototype.showObjectSelector = function() {
    if (this.objectSelectorCollapse) {
      this.objectSelectorCollapse.classList.remove("reveal");
    }
  };

  /* This function hides the object selection sidebar, if shown.
   */
  Workflow.prototype.hideObjectSelector = function() {
    if (this.objectSelectorCollapse) {
      this.objectSelectorCollapse.classList.add("reveal");
    }
  };

  /* This function shows the configuration sidebar, if obscured.
   */
  Workflow.prototype.showConfigurations = function() {
    if (this.configurationCollapse) {
      this.configurationCollapse.classList.remove("reveal");
    }
  };

  /* This function hides the configuration sidebar, if shown.
   */
  Workflow.prototype.hideConfigurations = function() {
    if (this.configurationCollapse) {
      this.configurationCollapse.classList.add("reveal");
    }
  };

  /* This function sets up the dynamic interactions with the sidebar.
   */
  Workflow.prototype.initializeSidebar = function() {
    var self = this;
    var collapseBars = this.element.parent().find('.collapse');

    this.objectSelectorCollapse = this.element[0].parentNode.querySelector(".collapse:not(.right)");
    this.configurationCollapse  = this.element[0].parentNode.querySelector(".collapse.right");

    if (this.objectSelectorCollapse) {
      this.objectSelectorCollapse.addEventListener('click', function(event) {
        this.classList.toggle('reveal');
      });
    }

    if (this.configurationCollapse) {
      this.configurationCollapse.addEventListener('click', function(event) {
        this.classList.toggle('reveal');
      });
    }
  };

  Workflow.prototype.loadObject = function(object) {
    var self = this;

    // Remove input/output pins
    var nodes = self.sidebar.find("li.connection");
    nodes[0].classList.remove("initial");

    nodes.children(".name").text(object["name"]);
    nodes.children(".type").text(object["object_type"]);
    nodes.children(".icon")[0].setAttribute('data-object-type', object["object_type"]);
    nodes.children(".icon")[0].src = object["icon"];

    var node = nodes[0];

    // Stop dragging of node
    var objectSelected = self.sidebar.find(".object-selected")[0];
    objectSelected.classList.add("disabled");

    node.setAttribute("data-object-revision", object["revision"]);
    node.setAttribute("data-object-id", object["uid"]);

    var inputs  = node.querySelector("ul.inputs");
    var outputs = node.querySelector("ul.outputs");

    var pins = inputs.querySelectorAll("li.input");
    if (pins) {
      pins.forEach(function(pin) {
        pin.remove();
      });
    }
    pins = outputs.querySelectorAll("li.output");
    if (pins) {
      pins.forEach(function(pin) {
        pin.remove();
      });
    }

    // Ping site to add a recently-used link
    var currentPersonURL = document.querySelector("#username").parentNode.getAttribute("href");
    var recentlyUsedURL = currentPersonURL + "/recentlyUsed";
    Occam.Util.post(recentlyUsedURL, {
      "object_id": object["uid"],
      "object_revision": object["revision"]
    });

    // Load input/output pins
    var realized = new Occam.Object(object["uid"], object["revision"], object["object_type"]);
    self.currentObject = realized;
    if(object["object_type"] === "workflow") {
      realized.objectInfo(function(info) {
        if (info.file === undefined) {return;}
        realized.retrieveJSON(info.file, function(data) {
          let inputCounter = 0;
          let outputCounter = 0;
          let selfOutput={
            "name": "self",
            "type": info.type
          };

          // TODO: Refactor this! A lot of repeated code
          // Add self
          var pin = document.createElement("li");
          pin.classList.add("output");
          pin.setAttribute("data-index", outputCounter);

          var label = document.createElement("div");
          label.classList.add("label");

          var name = document.createElement("span");
          name.classList.add("name");
          name.innerHTML = selfOutput.name || "output";

          var type = document.createElement("span");
          type.classList.add("type");
          type.innerHTML = selfOutput.type || "";

          label.appendChild(type);
          label.appendChild(name);

          pin.appendChild(label);

          outputs.appendChild(pin);
          outputCounter++;

          (data.connections || []).forEach(function (subNode) {
            if (subNode.inputs) {
              subNode.inputs.forEach(function(input) {
                if ((input.connections || []).length !== 0) {
                  return;
                }
                if (input.type === "configuration") {
                  return;
                }
                var pin = document.createElement("li");
                pin.classList.add("input");
                pin.setAttribute("data-index", inputCounter);

                var label = document.createElement("div");
                label.classList.add("label");

                var name = document.createElement("span");
                name.classList.add("name");
                name.innerHTML = input.name || "input";

                var type = document.createElement("span");
                type.classList.add("type");
                type.innerHTML = input.type || "";

                label.appendChild(type);
                label.appendChild(name);

                pin.appendChild(label);

                inputs.appendChild(pin);
                inputCounter++;
              });
            }
            if (subNode.outputs) {
              subNode.outputs.forEach(function(output) {
                if((output.connections||[]).length !== 0) {return;}
                var pin = document.createElement("li");
                pin.classList.add("output");
                pin.setAttribute("data-index", outputCounter);

                var label = document.createElement("div");
                label.classList.add("label");

                var name = document.createElement("span");
                name.classList.add("name");
                name.innerHTML = output.name || "output";

                var type = document.createElement("span");
                type.classList.add("type");
                type.innerHTML = output.type || "";

                label.appendChild(type);
                label.appendChild(name);

                pin.appendChild(label);

                outputs.appendChild(pin);
                outputCounter++;
              });
            }
          });

          // Redraw node
          var workflowNode = window.Workflow.Node.createFor(node, self.workflow);
          workflowNode.redraw();
        });
      });
    }else{
      realized.objectInfo(function(data) {
        if (data.inputs) {
          data.inputs.forEach(function(input, i) {
            var pin = document.createElement("li");
            pin.classList.add("input");
            pin.setAttribute("data-index", i);

            if (input.type == "configuration") {
              pin.classList.add("hidden");
              pin.setAttribute("hidden", true);
            }

            if (input.max) {
              pin.setAttribute("data-max", input.max);
            }

            var label = document.createElement("div");
            label.classList.add("label");

            var name = document.createElement("span");
            name.classList.add("name");
            name.innerHTML = input.name || "input";

            var type = document.createElement("span");
            type.classList.add("type");
            type.innerHTML = input.type || "";

            label.appendChild(type);
            label.appendChild(name);

            pin.appendChild(label);

            inputs.appendChild(pin);
          });
        }
        data.outputs = (data.outputs || []);
        data.outputs.unshift({
          "name": "self",
          "type": data.type
        });
        if (data.outputs) {
          data.outputs.forEach(function(output, i) {
            var pin = document.createElement("li");
            pin.classList.add("output");
            pin.setAttribute("data-index", i);

            var label = document.createElement("div");
            label.classList.add("label");

            if (i == 0 && data.outputs.length > 1) {
              // Hide the 'self' pin
              pin.classList.add("hidden");
              pin.setAttribute("hidden", true);
            }

            var name = document.createElement("span");
            name.classList.add("name");
            name.innerHTML = output.name || "output";

            var type = document.createElement("span");
            type.classList.add("type");
            type.innerHTML = output.type || "";

            label.appendChild(type);
            label.appendChild(name);

            pin.appendChild(label);

            outputs.appendChild(pin);
          });
        }

        // Redraw node
        var workflowNode = window.Workflow.Node.createFor(node, self.workflow);
        workflowNode.redraw();

        // Allow dragging
        objectSelected.classList.remove("disabled");
      });
    }
  };

  /* This function will set up the events to attach the sidebar attach
   * button to the current input box.
   */
  Workflow.prototype.applySidebarEvents = function(newActiveBox) {
    var self = this;
    if (newActiveBox !== undefined) {
      this.sidebar.find('.no-input')[0].setAttribute('hidden', true);
      this.sidebar.find('.selection')[0].removeAttribute('hidden');

      var collapse = this.element.children('.collapse:not(.right)');
      if (collapse.hasClass('reveal')) {
        collapse.trigger('click');
      }

      collapse = this.element.children('.collapse.right');
      if (collapse.hasClass('reveal')) {
        collapse.trigger('click');
      }
    }

    // General object autocomplete
    var autoCompleteType = Occam.AutoComplete.load(self.sidebar.find('.auto-complete[name=type]')[0]);
    var autoCompleteElement = self.sidebar.find('.auto-complete[name=name]');
    var autoComplete = Occam.AutoComplete.load(autoCompleteElement[0]);
    autoCompleteType.on("change", function(event) {
      autoComplete.clear();
    });

    autoComplete.on("change", function(event) {
      var object = {}
      var hidden            = autoCompleteElement.parent().children('input[name=object-id]');
      object["object_type"] = autoCompleteElement[0].getAttribute('data-object-type');
      object["revision"]    = autoCompleteElement[0].getAttribute('data-revision');
      object["icon"]        = autoCompleteElement[0].getAttribute('data-icon');
      object["name"]        = autoCompleteElement.val();
      object["uid"]         = hidden.val();

      self.loadObject(object);
    });

    this.sidebar.find('ul.object-list li').on('click', function(event) {
      var object = {}
      object["object_type"] = this.querySelector('h2 span.type').textContent.trim();
      object["revision"]    = this.getAttribute('data-object-revision');
      object["icon"]        = this.querySelector('img').getAttribute('src');
      object["name"]        = this.querySelector('h2 span.name').textContent.trim();
      object["uid"]         = this.getAttribute('data-object-id');

      self.loadObject(object);
    });

    this.sidebar.find('.button[type=submit]').on('click', function(event) {
      if (newActiveBox) {
        var object = {}
        var autoComplete = self.sidebar.find('.auto-complete[name=name]');
        var hidden       = autoComplete.parent().children('.hidden[name=object-id]');
        object["object_type"] = autoComplete.data('object_type');
        object["revision"]    = autoComplete.data('revision');
        object["name"]        = autoComplete.val();
        object["uid"]         = hidden.val();

        /* Set fields to reflect choice */
        newActiveBox.find('form input.object-type').val(object["object_type"]);
        newActiveBox.find('form input.object-name').val(object["name"]);
        /* Set hidden field to: object['uid'] */
        newActiveBox.find('form input.object-id').val(object["uid"]);
        newActiveBox.find('form input.object-revision').val(object["revision"]);
        newActiveBox.find('form input.button').removeAttr('disabled', '');

        newActiveBox.find('form input.object-type').css({
          "background-image": autoComplete.css('background-image')
        });

      }
      event.stopPropagation();
      event.preventDefault();
    });

    // Ensure workflow widget does not get keypresses when input boxes are focused
    this.sidebar.find('input').on('keypress', function(event) {
      event.stopPropagation();
    });
    this.sidebar.find('input').on('keydown', function(event) {
      event.stopPropagation();
    });
    this.sidebar.find('input').on('keyup', function(event) {
      event.stopPropagation();
    });
  };

  Workflow.prototype.loadFrom = function(obj) {
    var self = this;
    obj.objectInfo(function(info) {
      if (info.file === undefined) {
        return;
      }
      obj.retrieveJSON(info.file, function(data) {
        // Get workflow pan position
        var width  = self.workflow.element.offsetWidth;
        var height = self.workflow.element.offsetHeight;

        data.center = data.center || {"x": 0, "y": 0};

        // Center the workflow diagram upon the stored point
        // (The workflow's viewport position is the top left coordinate)
        var workflowLeft = data.center.x + (width  / 2);
        var workflowTop  = data.center.y + (height / 2);

        // For each node, add that node at its given position
        (data.connections || []).forEach(function(nodeInfo, i) {
          var newNode = document.createElement("li");
          newNode.classList.add("connection");
          nodeInfo.position = nodeInfo.position || {"x": 0, "y": 0};
          nodeInfo.position.x = nodeInfo.position.x || 0;
          nodeInfo.position.y = nodeInfo.position.y || 0;
          newNode.style.left = nodeInfo.position.x + "px";
          newNode.style.top  = nodeInfo.position.y + "px";

          var icon = document.createElement("img");
          icon.classList.add("icon");
          icon.setAttribute("src", "/images/icons/for?object-type=" + nodeInfo.type);
          newNode.appendChild(icon);

          var labelType = document.createElement("span");
          labelType.classList.add("type");
          labelType.innerHTML = nodeInfo.type;
          newNode.appendChild(labelType);

          var labelName = document.createElement("span");
          labelName.classList.add("name");
          labelName.innerHTML = nodeInfo.name;
          newNode.appendChild(labelName);

          newNode.setAttribute("data-index", i);
          newNode.setAttribute("data-object-type",     nodeInfo.type);
          newNode.setAttribute("data-object-id",       nodeInfo.id);
          newNode.setAttribute("data-object-revision", nodeInfo.revision);

          var inputs  = document.createElement("ul");
          inputs.classList.add("inputs");
          var outputs = document.createElement("ul");
          outputs.classList.add("outputs");

          // For each wire, add the corresponding wire
          nodeInfo.inputs.forEach(function(pinInfo, j) {
            if (pinInfo.connections.length == 0) {
              var newWire = document.createElement("li");
              newWire.classList.add("input");
              newWire.classList.add("disconnected");
              newWire.setAttribute("data-index", j);

              var label = document.createElement("div");
              label.classList.add("label");

              var wireLabelType = document.createElement("span");
              wireLabelType.classList.add("type");
              wireLabelType.innerHTML = pinInfo.type;
              label.appendChild(wireLabelType);

              if (pinInfo.type == "configuration" && (pinInfo.visibility == "hidden" || (pinInfo.connections || []).length == 0)) {
                // Hide the configuration, only if it is connected to a
                // default configuration object! (or nothing)
                newWire.classList.add("hidden");
                newWire.setAttribute("hidden", true);
              }

              var wireLabelName = document.createElement("span");
              wireLabelName.classList.add("name");
              wireLabelName.innerHTML = pinInfo.name;
              label.appendChild(wireLabelName);

              newWire.appendChild(label);
              inputs.appendChild(newWire);
            }
            else {
              pinInfo.connections.forEach(function(wireInfo, k) {
                var newWire = document.createElement("li");
                newWire.classList.add("input");
                newWire.setAttribute("data-index", j);
                newWire.setAttribute("data-item-index", k);

                // Increment all output wire indexes
                wireInfo.to[1] = wireInfo.to[1] + 1;

                newWire.setAttribute("data-connected-to", wireInfo.to[0] + "-" + wireInfo.to[1] + "-" + (wireInfo.to[2] || 0));

                if (pinInfo.type == "configuration" && (pinInfo.visibility == "hidden" || (pinInfo.connections || []).length == 0)) {
                  newWire.classList.add("hidden");
                  newWire.setAttribute("hidden", true);
                }

                if (k == 0) {
                  var label = document.createElement("div");
                  label.classList.add("label");

                  var wireLabelType = document.createElement("span");
                  wireLabelType.classList.add("type");
                  wireLabelType.innerHTML = pinInfo.type;
                  label.appendChild(wireLabelType);

                  var wireLabelName = document.createElement("span");
                  wireLabelName.classList.add("name");
                  wireLabelName.innerHTML = pinInfo.name;
                  label.appendChild(wireLabelName);

                  newWire.appendChild(label);
                }

                inputs.appendChild(newWire);
              });
            }
          });

          // For each wire, add the corresponding wire
          // First, add the 'self' wire
          nodeInfo["self"] = nodeInfo["self"] || {"connections": []};
          if (nodeInfo["self"].connections.length == 0) {
            var selfWire = document.createElement("li");
            selfWire.classList.add("output");
            selfWire.classList.add("disconnected");

            var label = document.createElement("div");
            label.classList.add("label");

            var wireLabelType = document.createElement("span");
            wireLabelType.classList.add("type");
            wireLabelType.innerHTML = nodeInfo.type;
            label.appendChild(wireLabelType);

            var wireLabelName = document.createElement("span");
            wireLabelName.classList.add("name");
            wireLabelName.innerHTML = "self";
            label.appendChild(wireLabelName);

            selfWire.setAttribute("data-index", "0");

            selfWire.appendChild(label);
            outputs.appendChild(selfWire);

            // If there are other outputs, this wire is hidden by default
            if (nodeInfo.outputs.length > 0) {
              selfWire.setAttribute("hidden", true);
              selfWire.classList.add("hidden");
            }
          }
          else {
            nodeInfo["self"].connections.forEach(function(wireInfo, k) {
              var selfWire = document.createElement("li");
              selfWire.classList.add("output");
              selfWire.classList.add("disconnected");
              selfWire.setAttribute("data-index", "0");
              selfWire.setAttribute("data-item-index", k);

              var label = document.createElement("div");
              label.classList.add("label");

              var wireLabelType = document.createElement("span");
              wireLabelType.classList.add("type");
              wireLabelType.innerHTML = nodeInfo.type;
              label.appendChild(wireLabelType);

              var wireLabelName = document.createElement("span");
              wireLabelName.classList.add("name");
              wireLabelName.innerHTML = "self";
              label.appendChild(wireLabelName);

              selfWire.setAttribute("data-connected-to", wireInfo.to[0] + "-" + wireInfo.to[1] + "-" + (wireInfo.to[2] || 0));

              selfWire.appendChild(label);
              outputs.appendChild(selfWire);
            });
          }

          nodeInfo.outputs.forEach(function(pinInfo, j) {
            if (pinInfo.connections.length == 0) {
              var newWire = document.createElement("li");
              newWire.classList.add("output");
              newWire.classList.add("disconnected");
              newWire.setAttribute("data-index", j+1);

              var label = document.createElement("div");
              label.classList.add("label");

              var wireLabelType = document.createElement("span");
              wireLabelType.classList.add("type");
              wireLabelType.innerHTML = pinInfo.type;
              label.appendChild(wireLabelType);

              var wireLabelName = document.createElement("span");
              wireLabelName.classList.add("name");
              wireLabelName.innerHTML = pinInfo.name;
              label.appendChild(wireLabelName);

              newWire.appendChild(label);
              outputs.appendChild(newWire);
            }
            else {
              pinInfo.connections.forEach(function(wireInfo, k) {
                var newWire = document.createElement("li");
                newWire.classList.add("output");
                newWire.setAttribute("data-index", j+1);
                newWire.setAttribute("data-item-index", k);
                newWire.setAttribute("data-connected-to", wireInfo.to[0] + "-" + wireInfo.to[1] + "-" + (wireInfo.to[2] || 0));

                if (k == 0) {
                  var label = document.createElement("div");
                  label.classList.add("label");

                  var wireLabelType = document.createElement("span");
                  wireLabelType.classList.add("type");
                  wireLabelType.innerHTML = pinInfo.type;
                  label.appendChild(wireLabelType);

                  var wireLabelName = document.createElement("span");
                  wireLabelName.classList.add("name");
                  wireLabelName.innerHTML = pinInfo.name;
                  label.appendChild(wireLabelName);

                  newWire.appendChild(label);
                }

                outputs.appendChild(newWire);
              });
            }
          });

          newNode.appendChild(inputs);
          newNode.appendChild(outputs);
          self.connections[0].appendChild(newNode);
        });

        self.workflow.reinitialize();
      });
    });
  };

  Workflow.prototype.applySidebarNodeEvents = function(nodes) {
    var self = this;

    nodes.on('mousedown.occam-node-move', function(event) {
      // Adds the node

      var node = $(this);
      var sidebarConnections = node.parents("ul.connections").first();
      var clonedNode = node.clone();

      var startX = 0;
      var startY = 0;
      node.css({
        left: node.offset().left - self.draggable.offset().left,
        top:  node.offset().top  - self.draggable.offset().top,
        "z-index": 99999
      });

      node[0].classList.remove("dummy");
      node[0].setAttribute("data-index", self.workflow.nodes().length);

      self.connections.append(node);
      var workflowNode = window.Workflow.Node.createFor(node[0], self.workflow);
      workflowNode.initialize();
      self.workflow.initializeNode(workflowNode);
      window.Workflow.nodeClickEvent.call(this, event);
      workflowNode.element.style.zIndex = 99999;

      self.element.on('mousemove.occam-node-move', function(event) {
      }).one('mouseup.occam-node.move', function(event) {
        $(this).off('mousemove.occam-node-move');
        node.off('mousedown.occam-node-move');
        node.css({
          "z-index": ""
        });
      });

      sidebarConnections.append(clonedNode);
      self.applySidebarNodeEvents(clonedNode);

      var collapse = self.element.parent().find('.collapse:not(.right)');
      collapse.trigger('click');

      self.exportJSON();
    });
  };

  Workflow.prototype.exportJSON = function() {
    var self = this;

    // Attach this object to the workflow
    var workflowData = self.workflow.exportJSON();

    // Mutate this into the appropriate structure
    var canonicalData = {};

    canonicalData.connections = [];
    workflowData.connections.forEach(function(nodeData) {
      var canonicalNode = {};

      canonicalNode.inputs = [];
      canonicalNode.position = {
        "x": nodeData.left,
        "y": nodeData.top
      };
      canonicalNode.name     = nodeData.name;
      canonicalNode.type     = nodeData.type;
      canonicalNode.id       = nodeData.data["object-id"]
      canonicalNode.revision = nodeData.data["object-revision"]

      nodeData.inputs.forEach(function(pinData, pinIndex) {
        var canonicalPin = {};
        canonicalPin.connections = [];
        canonicalPin.name = pinData.name;
        canonicalPin.type = pinData.type;
        canonicalPin.visibility = pinData.visibility;
        pinData.wires.forEach(function(wireData) {
          var canonicalWire = {};
          canonicalWire.to = [wireData.toNode, wireData.toWire-1, wireData.toIndex]
          if (canonicalWire.to[0] !== undefined) {
            canonicalPin.connections.push(canonicalWire);
          }
        });
        canonicalNode.inputs.push(canonicalPin);
      });

      canonicalNode.outputs = [];
      nodeData.outputs.forEach(function(pinData, pinIndex) {
        // self pin
        var canonicalPin = {};
        canonicalPin.connections = [];
        canonicalPin.name = pinData.name;
        canonicalPin.type = pinData.type;
        canonicalPin.visibility = pinData.visibility;
        pinData.wires.forEach(function(wireData) {
          var canonicalWire = {};
          canonicalWire.to = [wireData.toNode, wireData.toWire, wireData.toIndex]
          if (canonicalWire.to[0] !== undefined) {
            canonicalPin.connections.push(canonicalWire);
          }
        });
        if(pinIndex == 0){
          canonicalNode.self = canonicalPin;
        }
        else{
          canonicalNode.outputs.push(canonicalPin);
        }
      });

      canonicalData.connections.push(canonicalNode);
    });

    var width  = self.workflow.element.clientWidth;
    var height = self.workflow.element.clientHeight;

    canonicalData.center = workflowData.center;

    return JSON.stringify(canonicalData);
  };

  /* This function is the event handler for when the "Attach" button is
   * clicked. It should add an "attach" event to the queue.
   */
  Workflow.prototype.submitAttach = function(newActiveBox, container, workflow) {
    // Obviously, name and type might be wrong when somebody types something
    // weird in after selecting an object from the dropdown.

    var form = newActiveBox.find('form');

    // Get the hidden field with the object id requested to add
    var objectId = form.find(".object-id").val().trim();
    var objectRevision = form.find(".object-revision").val().trim();
    var connectionIndex = form.find("input[name=connection_index]").val().trim();
    var objectType = form.find("input[name=object_type]").val().trim();
    var objectName = form.find("input[name=object_name]").val().trim();

    Occam.object.queuePushAttach(connectionIndex, objectId, objectRevision, function(success) {
      console.log("Attach finished: " + success);
    });
  };
};
