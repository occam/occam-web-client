/*
 * This module handles Terminal views. These are the nice TTY and logging
 * windows that exist throughout the site. You can create a Terminal by
 * passing along the element that will serve as the terminal or log container
 * along with some text for the button/link to initiate it.
 */

var initOccamTerminal = function(Occam) {
  'use strict';

  /*
   * This constructor creates an object that represents an active Terminal
   * session. This can be a "tty" which emulates a real terminal session via
   * emulation. Or it can be a "log" which parses Occam log information and
   * presents it styled.
   */
  var Terminal = Occam.Terminal = function(name, type, data, element, link) {
    var self = this;

    // Determine a unique id
    this.terminalId = name + Math.floor(Math.random() * (1000 + 1));

    // Remember this terminal
    Occam.Terminal._terminals[this.terminalId] = this;

    // The terminal data to send upon open
    this.data = data || {};

    // Initialize the vt100 context
    this.context = Occam.VT100.initializeContext();

    // Pull out link text from element
    if (element.data('link-text') != undefined) {
      link = element.data('link-text');
    }

    // Initialize events
    this.events = {};

    this.name    = name;
    this.link    = link || "Run Terminal";
    this.type    = type;
    this.element = element;
    this.element.data('terminal-id', this.terminalId);

    this.jobID = this.element[0].getAttribute("data-job-id");

    this.pendingLogItem = "";

    // Open websocket route.
    this.ws = Occam.WebSocket.route(this.terminalId, Terminal.prototype.write, this);

    // Append the run link to the tty terminal
    if (type == "tty") {
      var runLink = this.runLink();
      element.prepend(runLink);

      element[0].setAttribute("tabindex", "0");

      // Map key events

      element.on('keydown', function(event) {
        // Handle special cases
        var key = "";
        if (event.which == 8) {
          key = "\b";
        }
        else if (event.which == 0x1b) {
          key = "\x1b";
        }
        else if (event.which == 37) { // Left arrow
          key = "\x1b[D";
        }
        else if (event.which == 38) { // Up arrow
          key = "\x1b[A";
        }
        else if (event.which == 39) { // Right arrow
          key = "\x1b[C";
        }
        else if (event.which == 40) { // Down arrow
          key = "\x1b[B";
        }

        if (key != "") {
          self.sendKey(key);
          event.stopPropagation();
          event.preventDefault();
        }
      });

      element.on('keyup', function(event) {
        // Handle special cases
        if (event.which == 8) {
          event.stopPropagation();
          event.preventDefault();
        }
      });

      element.on('keypress', function(event) {
        event.stopPropagation();
        event.preventDefault();

        var key = event.key;
        if (key == undefined) {
          key = String.fromCharCode(event.which || event.charCode);
        }

        if (event.which == 0x1e) {
          return;
        }
        else if (event.which == 13) {
          key = "\n";
        }
        else if (event.which == 9 || event.charCode == 9 || event.key == "Tab") {
          key = "\t";
        }
        else if (event.which == 8) {
          return;
        }
        else if (key.length > 1) {
          return;
        }
        else if (event.ctrlKey) {
          key = String.fromCharCode(key.charCodeAt(0) - "a".charCodeAt(0) + 1);
        }
        else if (event.altKey) {
          key = "\e" + key;
        }

        self.sendKey(key);
      });
    }
    else {
      var terminal = this;
      terminal.logTabPanels = element.parent().parent();
      terminal.logTabs      = terminal.logTabPanels.prev();

      terminal.logRootElement = terminal.logTabPanels.children('.tab-panel.run-log').find('.log');

      var list = $('<ul></ul>');
      terminal.logRootElement.append(list);
      terminal.logListElement = list;

      var logElement = $('<li></li>');
      logElement.addClass('debug');
      logElement.text('Waiting to run.');
      terminal.logListElement.append(logElement);

      terminal.lastLogElement = logElement;

      terminal.logRootElement.parent().parent().find('input.start-run').on('click', function(event) {
        /*var tab_panel = terminal.logRootElement.parent();
        var tab_index = tab_panel.index();
        tab_panel.parent().prev().children('.tab').slice(tab_index, tab_index+1).trigger('click');

        event.stopPropagation();
        event.preventDefault();

        $(this).remove();

        terminal.open();*/
      });
    }

    return this;
  };

  /* Contains a list of all active terminals
   */
  Terminal._terminals = {};

  /* Fires a callback for the given event.
   */
  Terminal.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      var eventInfo = this.events[name];
      eventInfo.callback.call(eventInfo.target, data);
    };
    return this;
  };

  /* Establishes an event callback.
   */
  Terminal.prototype.on = function(name, callback, target) {
    if (callback === undefined) {
      return this.events[name];
    }

    if (target === undefined) {
      target = this;
    }

    this.events[name] = {
      "callback": callback,
      "target": target
    };
    return this;
  };

  Terminal.load = function(element) {
    var id = element.data('terminal-id');

    if (id === undefined) {
      var object_id        = element.data('object-id');
      var object_revision  = element.data('object-revision');
      var workset_id       = element.data('workset-id');
      var workset_revision = element.data('workset-revision');
      var input_id         = element.data('input-id');
      var input_revision   = element.data('input-revision');
      var job_id           = element.data('job-id');
      var build_id         = element.data('build-id');
      var build_revision   = element.data('build-revision');
      var token            = element.data('account-token');
      var type             = element.data('terminal-type');
      var logType          = "tty";
      if (type === "run") {
        //logType = "log";
      }

      new Occam.Terminal(type, logType, {
        "object_id":        object_id,
        "object_revision":  object_revision,
        "workset_id":       workset_id,
        "workset_revision": workset_revision,
        "input_id":         input_id,
        "input_revision":   input_revision,
        "build_id":         build_id,
        "build_revision":   build_revision,
        "job_id":           job_id,
        "token":            token,
      }, element);
    }

    return Terminal.retrieve(element);
  };

  Terminal.retrieve = function(element_or_terminalId) {
    var id = element_or_terminalId;
    if (id.data !== undefined) {
      id = id.data('terminal-id');
    }
    return Occam.Terminal._terminals[id];
  };

  Terminal.prototype.reset = function() {
    var tmp = $('<span></span>');
    tmp.css({
      "font-family": this.element.css('font-family'),
      "font-size":   this.element.css('font-size'),
      "font-weight": this.element.css('font-weight'),
      "position": "absolute"
    });
    $('body').append(tmp);
    tmp.text('X');
    this.context.charHeight = tmp.height();
    this.context.charWidth  = tmp.width();
    this.context.height = 25;

    this.context.height = Math.round(this.context.height - 0.5);
    Occam.VT100.addLines(this.context.height, this.element, this.context);

    this.context.width  = this.element[0].querySelector('.line').clientWidth / this.context.charWidth;
    this.context.width  = Math.round(this.context.width  - 0.5);

    this.context.scroll.start = 0;
    this.context.scroll.end   = this.context.height - 1;
    // Recalcuate the average cell width because some browsers
    // are trying to be too clever with monospace fonts.
    tmp.text(new Array(this.context.width + 1).join("\u00a0"));
    this.context.charWidth = tmp.width() / this.context.width;
    tmp.remove();

    //terminal.element.innerHeight(terminal.context.height * terminal.context.charHeight + 30);

    this.context.caret.css({
      "height": this.context.charHeight,
      "width":  this.context.charWidth,
      "left":   this.element[0].querySelector('.line').offsetLeft + "px",
      "top":    "0px"
    });

    this.element.append(this.context.caret);

    this._runLink.remove();
    Occam.VT100.move(0,0,this.element,this.context);
  }

  Terminal.prototype.write = function(data) {
    var terminal = this;

    if (data.base64 || data.base64 == "") {
      data.output = atob(data.base64) || "";
    }

    if (terminal.type == "tty") {
      var maxScroll = terminal.element[0].scrollHeight - terminal.element[0].clientHeight;
      var autoScroll = (terminal.element[0].scrollTop >= maxScroll - 0.5);
      var response = Occam.VT100.parse(data.output, terminal.element, terminal.context);

      if (response.length > 0) {
        var message = {
          "request": "write",
          "terminal": terminal.terminalId,
          "input":    response,
          "data": {"job_id": terminal.jobID}
        };
        terminal.ws.send(message);
      }
      if (autoScroll) {
        terminal.element[0].scrollTop = terminal.element[0].scrollHeight - terminal.element[0].clientHeight;
      }
    }
    else {
      var inputs = data.output.split('\n');

      var maxScroll = terminal.logRootElement[0].scrollHeight - terminal.logRootElement[0].clientHeight;
      var autoScroll = (terminal.logRootElement[0].scrollTop >= maxScroll - 0.5);

      if (inputs.length > 0) {
        inputs[0] = terminal.pendingLogItem + inputs[0];
        terminal.pendingLogItem = "";
      }

      for (var i = 0; i < inputs.length; i++) {
        var logItem = {};

        inputs[i] = inputs[i].trim();
        if (inputs[i].length == 0) {
          continue;
        }

        try {
          logItem = JSON.parse(inputs[i]);
        }
        catch(e) {
          if (i == inputs.length-1) {
            terminal.pendingLogItem = inputs[i];
          }
          continue;
        }

        if (logItem.params === undefined) {
          logItem.params = {};
        }
        logItem.params = logItem.params || {};

        if (logItem.type == "event") {
          // Handle the event
          if (logItem.message == "video") {
            // Switch tabs and start the VNC client
            var videoTab = $(terminal.logTabs.children('.tab').get(3));
            videoTab.attr('aria-hidden', 'false').trigger('click');
            var button = $(terminal.logTabPanels.children('.tab-panel').get(3)).find('#stream_connect_button');
            button.data('port', logItem.params.port);
            button.trigger('click');
          }
          continue;
        }

        var logElement = $('<li></li>');
        logElement.addClass(logItem.type);
        logElement.text(logItem.message);
        logElement.css({"opacity": 0});
        logItem.params = logItem.params || {};
        if (logItem.params['source']) {
          logElement.data('source', logItem.params['source']);
        }

        if (logItem.params['source'] && (terminal.lastLogElement.data('source') == logElement.data('source'))) {
          // Combine elements with the same source
          terminal.lastLogElement.append('<br>');
          var section = $('<span></span>');
          section.text(logItem.message);
          terminal.lastLogElement.append(section);
        }
        else {
          terminal.logListElement.append(logElement);
          terminal.lastLogElement = logElement;

          logElement.animate({"opacity": 1.0});
        }

        if (autoScroll) {
          terminal.logRootElement[0].scrollTop = terminal.logRootElement[0].scrollHeight - terminal.logRootElement[0].clientHeight;
        }
      }
    }
  };

  Terminal.prototype.runLink = function() {
    var terminal = this;

    if (this._runLink === undefined) {
      this._runLink = $('<a>' + terminal.link + '</a>')
        .attr('href', '#')
        .on('click', function(event) {
          event.stopPropagation();
          event.preventDefault();

          terminal.reset();

          terminal.open();
      });
    }

    return this._runLink;
  };

  /*
   * Sends the key to the server.
   */
  Terminal.prototype.sendKey = function(key) {
    this.trigger("write", key);
    /*
    var message = {
      "request": "write",
      "terminal": this.terminalId,
      "input": key,
      "data": {"job_id": this.jobID}
    };
    this.ws.send(message);
    */
  };

  /*
   * Runs the terminal.
   */
  Terminal.prototype.open = function() {
    var terminal = this;

    var message = {
      "request":  "open",
      "spawning": terminal.name,
      "data": terminal.data,
      "rows": terminal.context.height,
      "cols": terminal.context.width,
      "terminal": terminal.terminalId
    };

    this.ws.send(message);
  };
};
