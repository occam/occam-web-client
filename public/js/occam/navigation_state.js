/*
 * This module handles page navigation and back/forward button semantics.
 */

window.addEventListener("load", function(e) {
  // Add the callback for when the browser's back/forward button is pressed.
  window.onpopstate = Occam.NavigationState.popState;
});

var initOccamNavigationState = function(Occam) {
  'use strict';

  // Module definition
  var NavigationState = Occam.NavigationState = {};

  /*
   * This method is used to create a new entry in the history. "module" is the
   * string of the Occam javascript module. "index" is used to call that
   * module's load() method to get the instance of that page item. That item
   * will have its updateState method called with the given 'oldData' or
   * 'newData' fields when the back/forward button is pushed. It is
   * responsible for reverting/updating its page state respectively.
   */
  NavigationState.pushState = function(module, index, oldData, newData, newURL, newTitle) {
    if (!NavigationState.initial) {
      // Store the initial page information
      NavigationState.initial = {
        'title':  window.document.title, // The old page title
        'href':   window.location.href, // The page url
        'state':  oldData,
        'module': module,
        'index':  index
      }
    }

    // Store the state of this new page
    var state = {
      'title':  newTitle, // The current page title
      'href':   newURL,   // The current URL
      'state':  newData,  // The current state to pass into the module (tab button index)
      'module': module,   // The module: For instance "Tabs"
      'index':  index,    // For instance: The tab strip index (data-tabs-index)
    };

    window.history.pushState(state, "", newURL);
  };

  /*
   * Performs the given state changes to the current page.
   * Generally called internally. Not meant to be called directly.
   */
  NavigationState.perform = function(state) {
    const item = Occam[state.module].load(state.index);
    if (item) {
      item.updateState(state.state);
    }
  };

  /*
   * This method returns the browser's current url. It is window.location.pathname;
   */
  NavigationState.currentLocation = function() {
    return window.location.pathname;
  };

  /*
   * This method returns the path from the current object.
   */
  NavigationState.currentPath = function() {
    var currentURL = NavigationState.currentLocation();

    var paths = currentURL.slice(1).split("/");
    var SEPARATORS = ["objects", "groups", "experiments",
                      "metadata", "files", "tree", "output",
                      "run", "view"]

    if (paths[0] == "worksets") {
      paths.shift(); // worksets
      paths.shift(); // id

      // Check for revision
      if (SEPARATORS.indexOf(paths[0]) == -1 && paths[0].match(/^[a-z0-9]+$/)) {
        paths.shift();
      }
    }

    if (paths[0] == "objects") {
      paths.shift(); // objects
      paths.shift(); // id

      if (SEPARATORS.indexOf(paths[0]) == -1 && paths[0].match(/^[a-z0-9]+$/)) {
        paths.shift();
      }
    }

    return "/" + paths.join("/");
  };

  NavigationState.updateLocation = function(newURL) {
    window.history.replaceState(window.history.state, "", newURL);
  };

  /*
   * This is the callback that is fired when a person presses the back or
   * forward buttons in their browser. The given event will have the index of
   * the "state" in our history. This function will iterate through the history
   * to recreate the page state.
   */
  NavigationState.popState = function(event) {
    if (event === null || event.state === null) {
      // Original page
      NavigationState.perform(NavigationState.initial);
    }
    else {
      NavigationState.perform(event.state);
    }
  };
};
