/* This file handles the javascript for the configuration sidebar.
 *
 * This sidebar is for the Workflow widget and handles the loading
 * of configurations per object on the workflow.
 */

var initOccamConfigurationPanel = function(Occam) {
  var ConfigurationPanel = Occam.ConfigurationPanel = function(element) {
    this.element = element;

    this.bindEvents();
  };

  ConfigurationPanel.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-configuration-panel-index');

    if (index) {
      return Occam.ConfigurationPanel._loaded[index];
    }

    return new Occam.ConfigurationPanel(element);
  };

  ConfigurationPanel.loadAll = function(element) {
    var panels = element.querySelectorAll('.configuration-panel');

    var ret = [];
    panels.forEach(function(subElement) {
      var panel = Occam.ConfigurationPanel.load(subElement);
      ret.push(panel);
    });

    return ret;
  };

  ConfigurationPanel.prototype.bindEvents = function() {
  };

  /* Creates the panel for the given object.
   */
  ConfigurationPanel.prototype.loadConfiguration = function(index, workflowObject, objectInfo) {
    // Update headers for the given type/name

    // Create a pane for the configuration.
    //   (Other objects will preserve their contents via the node index)
    var pane = this.element.querySelector('li.pane[data-node-index="' + index + '"]');
    if (!pane) {
      pane = document.createElement("li");
      pane.classList.add("pane");
      pane.classList.add("loading");
      pane.setAttribute("data-node-index", index);
      this.element.querySelector("ul.panes").appendChild(pane);

      // Dynamically load the configuration tabs
      var url = workflowObject.url({ path: "/configure/" + index });
      Occam.Util.get(url,
        function(html) {
          // Update the pane to show this configuration
          pane.classList.remove("loading");
          pane.innerHTML = html;

          Occam.loadAll(pane);
        }
      );
    }
    var active = this.element.querySelector('li.pane.active');
    if (active && active != pane) {
      active.classList.remove("active");
    }
    pane.classList.add("active");
  };
};
