/*
 * This module handles the object tree widget which lets you select objects within
 * objects or browse a Person's worksets, etc.
 * (See: views/objects/_object_tree.haml)
 */

var initOccamObjectTree = function(Occam) {
  'use strict';

  var ObjectTree = Occam.ObjectTree = function(element, root) {
    var self = this;

    // Initialize events
    self.events = {};

    if (root === undefined) {
      root = element;
      while(root.parentNode &&
            root.parentNode.parentNode &&
            root.parentNode.parentNode.classList.contains("object-tree")) {
        root = root.parentNode.parentNode;
      }
    }

    // Keep track of element and tree type
    self.element = element;
    self.type    = element.getAttribute("data-tree-type") || "selection";
    self.root    = root;

    // Initialize all event functions
    self.bindEvents();
  };

  /* Loads all object trees within the given element.
   */
  ObjectTree.loadAll = function(element) {
    element.querySelectorAll("ol.object-tree").forEach(function(tree) {
      new Occam.ObjectTree(tree);
    });
  };

  /* Fires a callback for the given event.
   */
  ObjectTree.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      var eventInfo = this.events[name];
      eventInfo.callback.call(eventInfo.target, data);
    };
    return this;
  };

  /* Establishes an event callback.
   */
  ObjectTree.prototype.on = function(name, callback, target) {
    if (callback === undefined) {
      return this.events[name];
    }

    if (target === undefined) {
      target = this;
    }

    this.events[name] = {
      "callback": callback,
      "target": target
    };
    return this;
  };

  /* Expands the given row.
   */
  ObjectTree.prototype.expand = function(row) {
    var self = this;

    var childList = row.querySelector(":scope > ol.object-tree");
    var expander = row.querySelector(":scope > span.expand");

    // If there isn't a list loaded, load that list
    if (!childList) {
      childList = document.createElement("ol");
      childList.classList.add("object-tree");
      childList.classList.add("loading");

      row.appendChild(childList);

      var url = expander.getAttribute("data-url");
      console.log("gathering " + url);
      Occam.Util.get(url, function(html) {
        var dummyNode = document.createElement("div");
        dummyNode.innerHTML = html;
        childList.innerHTML = dummyNode.querySelector("ol").innerHTML;
        childList.classList.remove("loading");
        new Occam.ObjectTree(childList, self.root);
      });
    }

    // Create the row by polling the server
    expander.classList.toggle("collapse");

    childList.setAttribute("aria-hidden", !expander.classList.contains("collapse"));
  };

  /* Selects the row corresponding to the given element within the row.
   */
  ObjectTree.prototype.select = function(element) {
    var row = element;

    while (row && row.tagName.toUpperCase() != "LI") {
      row = element.parentNode;
    
    }

    if (!row) {
      return;
    }

    var nameElement = row.querySelector(":scope > span.name");

    // Unselect the last item from this tree
    this.root.querySelectorAll("span.selected").forEach(function(lastSelected) {
      lastSelected.classList.remove("selected");
    });

    // Select this item
    nameElement.classList.add("selected");

    // Update form items
    this.root.querySelector('input[name="to[id]"]').setAttribute("value",       row.querySelector("span.id").textContent.trim());
    this.root.querySelector('input[name="to[revision]"]').setAttribute("value", row.querySelector("span.revision").textContent.trim());
    this.root.querySelector('input[name="to[index]"]').setAttribute("value",    row.querySelector("span.index").textContent.trim());
    this.root.querySelector('input[name="to[link]"]').setAttribute("value",     row.querySelector("span.link").textContent.trim());
  };

  /* Attaches events to the given item element.
   */
  ObjectTree.prototype.bindEvents = function() {
    var self = this;

    self.element.querySelectorAll(":scope > li > span").forEach(function(span) {
      span.addEventListener("click", function(event) {
        if (self.type == "selection" && this.classList.contains("name")) {
          self.select(this);
        }
        else {
          self.expand(this.parentNode);
        }
      });
    });
  };
}
