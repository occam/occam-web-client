/* This file handles the social component and comment streams.
 */

var initOccamSocial = function(Occam) {
  var Social = Occam.Social = function(element) {
    // element is the .social.sidebar on the details page
    var self = this;

    if (element === undefined) {
      return;
    }

    this.element = element;
    this.collapse = element.parentNode.querySelector(".social.sidebar ~ .collapse");

    Social.count++;
    this.element.setAttribute('data-social-index', Social.count);

    Social._loaded[this.element.getAttribute('data-social-index')] = this;

    this.bindEvents();
    this.events = {};
  };

  Social.count = 0;
  Social._loaded = {};

  Social.loadAll = function(element) {
    var revealLink = element.querySelector('*[data-sidebar="social"]');
    if (revealLink) {
      revealLink.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        if (Social.count > 0) {
          Social._loaded["1"].show();
        }
      });
    }

    var elements = element.querySelectorAll('.social.sidebar');

    elements.forEach(function(element) {
      Social.load(element);
    });
  };

  Social.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-social-index');

    if (index) {
      return Occam.Social._loaded[index];
    }

    return new Occam.Social(element);
  };

  Social.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Social.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  Social.prototype.loadPanel = function() {
    var self = this;

    // Download the social view
    if (!this.element.classList.contains("pjax-loaded")) {
      self.element.classList.add("loading");
      Occam.Util.get(Occam.object.url({path: "/social"}), function(html) {
        self.element.innerHTML = html;
        self.element.classList.add("pjax-loaded");
        self.element.classList.remove("loading");

        // Get the edit form and reply form
        self.editForm  = self.element.querySelector("form.edit-field");
        self.replyForm = self.element.querySelector("form.comment-field");
        self.mainList  = self.element.querySelector(".card > ul.comments");

        // Bind panel events
        self.bindPanelEvents();
      });
    }
  };

  Social.prototype.show = function() {
    this.element.classList.add("reveal");
    this.loadPanel();
  };

  Social.prototype.hide = function() {
    this.element.classList.remove("reveal");
  };

  Social.prototype.bindEvents = function() {
    var self = this;

    // Allow for the social sidebar collapse/expand
    self.collapse.addEventListener("click", function(event) {
      event.preventDefault();
      event.stopPropagation();

      if (self.element.classList.contains("reveal")) {
        self.hide();
      }
      else {
        self.show();
      }
    });
  };

  Social.prototype.bindPanelEvents = function() {
    var self = this;

    // Bind events for every comment
    self.element.querySelectorAll('li.comment').forEach(function(comment) {
      self.bindComment(comment);
    });

    self.replyForm.addEventListener("submit", function(event) {
      event.stopPropagation();
      event.preventDefault();

      // Submit the form
      Occam.Util.submitForm(self.replyForm, function(html) {
        // Add the new comment to the list
        var node = document.createElement("div");
        node.innerHTML = html;
        var newElement = node.querySelector("li.comment");
        self.mainList.appendChild(newElement);
        self.bindComment(newElement);
      });
    });
  };

  Social.prototype.showReplyBox = function(commentID, element) {
    var self = this;
    if (!element.querySelector(':scope > .comment-body + form')) {
      var newForm = this.replyForm.cloneNode(true);
      newForm.removeAttribute("id");
      newForm.removeAttribute("hidden");
      newForm.classList.add("reply-box");
      newForm.querySelector('input[name="inReplyTo"]').setAttribute("value", commentID);
      newForm.querySelector('label').setAttribute("for", commentID + "-post-anonymous");
      newForm.querySelector('input[type="checkbox"]').setAttribute("id", commentID + "-post-anonymous");
      element.insertBefore(newForm, element.querySelector('.comment-body + .replies.comments'));
      newForm.classList.add("revealed");

      newForm.addEventListener("submit", function(event) {
        event.stopPropagation();
        event.preventDefault();

        // Submit the form
        Occam.Util.submitForm(newForm, function(html) {
          newForm.remove();
          // Add the new comment to the list
          var node = document.createElement("div");
          node.innerHTML = html;
          var newElement = node.querySelector("li.comment");
          var replies = element.querySelector(':scope > .comment-body ~ .replies.comments');
          replies.appendChild(newElement);
          self.bindComment(newElement);
        });
      });
    }
  };

  Social.prototype.showEditBox = function(commentID, element) {
    var self = this;
    if (!element.querySelector(':scope > .comment-body + form')) {
      var newForm = this.editForm.cloneNode(true);
      newForm.removeAttribute("id");
      newForm.removeAttribute("hidden");
      newForm.classList.add("edit-box");
      newForm.setAttribute("action", newForm.getAttribute("action") + commentID);
      element.insertBefore(newForm, element.querySelector('.comment-body + .replies.comments'));
      newForm.classList.add("revealed");
      newForm.querySelector(':scope > textarea').value = element.querySelector('.comment-body > .content').innerText;

      newForm.addEventListener("submit", function(event) {
        event.stopPropagation();
        event.preventDefault();

        // Submit the form
        Occam.Util.submitForm(newForm, function(html) {
          newForm.remove();

          // Update with the new comment
          var node = document.createElement("div");
          node.innerHTML = html;
          var newElement = node.querySelector("li.comment");
          element.parentNode.replaceChild(newElement, element);
          self.bindComment(newElement);
        });
      });
    }
  };

  Social.prototype.showMoreReplies = function(commentID, element) {
    var self = this;
    var replies = element.querySelector('.comment-body ~ .replies.comments');
    var link = element.querySelector('.comment-body ~ .replies.comments + span > .show-replies');
    var url = link.getAttribute("href");
    replies.classList.add("loading");
    link.parentNode.remove();
    Occam.Util.get(url, function(html) {
      replies.classList.remove("loading");
      replies.innerHTML = html;
      element.appendChild(replies.firstChild);
      replies.remove();
      element.querySelectorAll('li.comment').forEach(function(comment) {
        self.bindComment(comment);
      });
    });
  };

  Social.prototype.bindComment = function(element) {
    var self = this;

    var commentID = element.getAttribute('id').substring(8);

    // Bind the show more comments link.
    var showMoreLink = element.parentNode.querySelector(".comment#comment-" + commentID + " > .comment-body ~ .replies.comments + span > .show-replies");
    if (showMoreLink) {
      showMoreLink.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        self.showMoreReplies(commentID, element);
      });
    }

    // Bind the 'reply' link
    var replyLink = element.parentNode.querySelector(".comment#comment-" + commentID + " > .comment-body > span.actions > .reply-link");
    if (replyLink) {
      replyLink.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        self.showReplyBox(commentID, element);
      });
    }

    // Bind the 'edit' link
    var editLink = element.parentNode.querySelector(".comment#comment-" + commentID + " > .comment-body > span.actions > .edit-link");
    if (editLink) {
      editLink.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        self.showEditBox(commentID, element);
      });
    }

    // Bind the 'delete' action
    var deleteForm = element.querySelector("form.delete-form");
    if (deleteForm) {
      deleteForm.addEventListener("submit", function(event) {
        event.stopPropagation();
        event.preventDefault();

        // Submit the form
        Occam.Util.submitForm(deleteForm, function(html) {
          // Add the new comment to the list
          var node = document.createElement("div");
          node.innerHTML = html;
          var newElement = node.querySelector("li.comment");
          element.parentNode.replaceChild(newElement, element);
          self.bindComment(newElement);
        });
      });
    }
  };
};
