/* This file implements any dynamic auto-complete feature across the site.
 * The autocomplete dropdown shares the dropdown styling of Occam.Selector.
 * Generally, autocomplete is used for object lookup and account/person lookup.
 *
 * To mark an input as an object autocomplete:
 * - Apply class 'auto-complete' to the input.
 * - Using 'object-type' class will query for object types.
 * - Use the following attributes to build queries:
 *    data-object-type: a string for the object type to filter
 * - You can have a object-type filter input by placing an input.auto-complete.object-type as a sibling
 */

var initOccamAutoComplete = function(Occam) {
  var AutoComplete = Occam.AutoComplete = function(element) {
    this.element = element;
    this.objectTypeSelector = null;
    this.search = null;

    var typeSelector = this.element.parentNode.querySelector('.auto-complete.object-type');

    if (typeSelector) {
      this.objectTypeSelector = typeSelector;
    }

    var viewsTypeSelector = this.element.parentNode.querySelector('.auto-complete.views-type');

    if (viewsTypeSelector) {
      this.objectViewsTypeSelector = viewsTypeSelector;
    }

    var providesEnvironmentSelector = this.element.parentNode.querySelector('.auto-complete.provides-environment');

    if (providesEnvironmentSelector) {
      this.objectProvidesEnvironmentSelector = providesEnvironmentSelector;
    }

    var providesArchitectureSelector = this.element.parentNode.querySelector('.auto-complete.provides-architecture');

    if (providesArchitectureSelector) {
      this.objectProvidesArchitectureSelector = providesArchitectureSelector;
    }

    this.initialize();
    this.bindEvents();

    AutoComplete.count++;

    this.element.setAttribute('data-loaded-index', 'auto-complete-' + AutoComplete.count);
    AutoComplete._loaded[this.element.getAttribute('data-loaded-index')] = this;
  };

  AutoComplete.count = 0;

  AutoComplete.loadAll = function(element) {
    var autocompletes = element.querySelectorAll('.auto-complete');

    autocompletes.forEach(function(element) {
      var autocomplete = Occam.AutoComplete.load(element);
    });
  };

  /* This function will create the dropdown and attach it to the body.
   */
  AutoComplete.prototype.initialize = function() {
    // Form dropdown section
    this.dropdown = document.createElement("ul");
    this.dropdown.classList.add("dropdown");
    this.dropdown.setAttribute("tabindex", "1");
    this.dropdown.setAttribute("hidden", true);

    this.events = {};

    // Append dropdown to body
    document.querySelector('.content').appendChild(this.dropdown);

    // Form hidden form component for the object id
    var existing = this.element.parentNode.querySelector(":scope > input[type=hidden][name=object-id]");
    if (existing) {
      this.hidden = existing;
    }
    else {
      this.hidden = document.createElement("input");
      this.hidden.setAttribute("hidden", "");
      this.hidden.setAttribute("name", "object-id");
      this.hidden.setAttribute("type", "hidden");

      this.element.parentNode.appendChild(this.hidden);
    }

    // Form hidden form component for the object revision
    existing = this.element.parentNode.querySelector(":scope > input[type=hidden][name=object-revision]");
    if (existing) {
      this.hidden_revision = existing;
    }
    else {
      this.hidden_revision = document.createElement("input");
      this.hidden_revision.setAttribute("hidden", "");
      this.hidden_revision.setAttribute("name", "object-revision");
      this.hidden_revision.setAttribute("type", "hidden");

      this.element.parentNode.appendChild(this.hidden_revision);
    }
  };

  AutoComplete._loaded = {};

  AutoComplete.load = function(element) {
    if (!element) {
      return null;
    }

    var index = element.getAttribute('data-loaded-index');

    if (index) {
      return AutoComplete._loaded[index];
    }

    return new Occam.AutoComplete(element);
  };

  AutoComplete.prototype.trigger = function(name) {
    if (this.events[name]) {
      this.events[name].call(this, {});
    };
    return this;
  };

  AutoComplete.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  /* This function returns the Object currently displayed in the field.
   */
  AutoComplete.prototype.object = function() {
    return new Occam.Object(this.id(), this.revision(), this.type(), this.name());
  };

  /* This function returns the id of the object currently selected.
   */
  AutoComplete.prototype.id = function() {
    return this.hidden.value;
  };

  /* This function returns the revision of the object currently selected.
   */
  AutoComplete.prototype.revision = function() {
    return this.hidden_revision.value;
  };

  /* This function returns the name of the object currently selected.
   */
  AutoComplete.prototype.name = function() {
    return this.element.getAttribute("data-object-name");
  };

  /* This function returns the type of the object currently selected.
   */
  AutoComplete.prototype.type = function() {
    return this.element.getAttribute("data-object-type");
  };

  /* This function returns the environment of the object currently selected.
   */
  AutoComplete.prototype.environment = function() {
    return this.element.getAttribute('data-environment');
  };

  /* This function returns the architecture of the object currently selected.
   */
  AutoComplete.prototype.architecture = function() {
    return this.element.getAttribute('data-architecture');
  };

  /* This function clears the field and voids the dropdown.
   */
  AutoComplete.prototype.clear = function() {
    var self = this;

    this.element.value = "";
    self.dropdown.childNodes.forEach(function(node) { node.remove() });
  };

  /* This function issues a search to fill the dropdown with possible objects.
   */
  AutoComplete.prototype.fillDropdown = function() {
    var self = this;

    var dropdownItem = document.createElement("li");
    dropdownItem.classList.add("object");

    var url = '/search';
    var inputElement = this.element;
    var input_type = inputElement.value;

    var types_toggle   = "off";
    var objects_toggle = "on";

    if (this.element.classList.contains('object-type')) {
      objects_toggle = "off";
      types_toggle   = "on";
    }

    var obj_type      = null;
    var views_type    = this.element.getAttribute('data-views-type');
    var views_subtype = this.element.getAttribute('data-views-subtype');
    var environment   = this.element.getAttribute('data-environment');
    var architecture  = this.element.getAttribute('data-architecture');

    // Allow filtering of object types using a sibling input object-type field
    if (self.objectTypeSelector) {
      obj_type = self.objectTypeSelector.value;
    }

    if (self.objectViewsTypeSelector) {
      views_type = self.objectViewsTypeSelector.value;
    }

    if (self.objectProvidesEnvironmentSelector) {
      environment = self.objectProvidesEnvironmentSelector.value;
    }

    if (self.objectProvidesArchitectureSelector) {
      architecture = self.objectProvidesArchitectureSelector.value;
    }

    if (self.search) {
      // Abort any ongoing search
      self.search.abort();
    }

    var searchOptions = {
      "search":  input_type,
      "objects": objects_toggle,
      "types":   types_toggle,
      "environment": environment,
      "architecture": architecture
    };

    if (obj_type) {
      searchOptions["type"] = obj_type;
    }

    if (views_type) {
      searchOptions["views-type"] = views_type;
      if (views_subtype) {
        searchOptions["views-subtype"] = views_subtype;
      }
    }

    self.search = $.get(url, searchOptions, function(data) {
      self.dropdown.innerHTML = "";

      if (types_toggle == "on") {
        data["types"].forEach(function(object) {
          var item = dropdownItem.cloneNode(true);

          var header = obj_type;
          header = object["type"];
          var h2 = document.createElement("h2");
          h2.classList.add("type");
          item.appendChild(h2);
          h2.textContent = header;
          h2.classList.add('icon');
          h2.setAttribute('data-object-type', object["type"]);
          h2.setAttribute('data-icon', object["icon"]);
          h2.setAttribute('data-small-icon', object["smallIcon"]);
          h2.style.backgroundImage = "url('" + object["smallIcon"] + "')";

          item.addEventListener('mousedown', function(event) {
            /* Set fields to reflect choice */
            self.element.value = object["type"];
            self.element.setAttribute("data-object-type", object["type"]);
            self.trigger('change');
          });

          self.dropdown.appendChild(item);

          // Ensure the dropdown is at least a certain size
          var itemStyle = window.getComputedStyle(item);
          var itemHeight = item.offsetHeight + parseInt(itemStyle.marginTop) + parseInt(itemStyle.marginBottom);
          var dropdownHeight = itemHeight * self.dropdown.querySelectorAll('li').length;
          self.dropdown.style.height = dropdownHeight + "px";
        });
      }
      else {
        data["objects"].slice(0,25).forEach(function(object) {
          var item = dropdownItem.cloneNode(true);

          var header = obj_type;
          header = object["type"];
          if (obj_type === "person") {
            if ("organization" in object) {
              header = object["organization"];
            }
            else {
              header = null;
            }
          }

          if (header !== null) {
            var h2 = document.createElement("h2");
            h2.classList.add("type");
            h2.textContent = header;
            item.appendChild(h2);

            var p = document.createElement("p");
            p.textContent = object["name"];
            item.appendChild(p);
          }
          else {
            var h2 = document.createElement("h2");
            h2.classList.add("type");
            h2.textContent = object["name"];
            item.appendChild(h2);
          }

          var icon = null;
          if (obj_type === "person") {
            icon = "/people/" + object["id"] + "/avatar?size=20";
          }
          if (icon !== null) {
            item.querySelector('h2').style.backgroundImage = "url('" + icon + "')";
          }
          else {
            item.querySelector('h2').classList.add('icon');
            item.querySelector('h2').setAttribute('data-object-type', object["type"]);
            item.querySelector('h2').setAttribute('data-icon', object["icon"]);
            item.querySelector('h2').setAttribute('data-small-icon', object["smallIcon"]);
            item.querySelector('h2').style.backgroundImage = "url('" + object["smallIcon"] + "')";
          }

          item.addEventListener('mousedown', function(event) {
            /* Set fields to reflect choice */
            self.element.value = object["name"];
            self.hidden.value  = object["id"];
            self.hidden_revision.value = object["revision"];
            self.element.setAttribute('data-revision', object["revision"]);
            self.element.setAttribute('data-object-type', object["type"]);
            self.element.setAttribute('data-object-name', object["name"]);
            self.element.setAttribute('data-icon', object["icon"]);
            self.element.setAttribute('data-small-icon', object["smallIcon"]);
            self.element.style.backgroundImage = window.getComputedStyle(this.querySelector('h2')).backgroundImage;
            self.showDropdown = false;
            self.element.focus();
            self.showDropdown = true;

            // Hide dropdown
            self.close();

            event.stopPropagation();
            event.preventDefault();

            self.trigger('change');
          });

          self.dropdown.appendChild(item);

          // Ensure the dropdown is at least a certain size
          var itemStyle = window.getComputedStyle(item);
          var itemHeight = item.offsetHeight + parseInt(itemStyle.marginTop) + parseInt(itemStyle.marginBottom);
          var dropdownHeight = itemHeight * self.dropdown.querySelectorAll('li').length;
          self.dropdown.style.height = dropdownHeight + "px";
        });
      }
    }, 'json');
  };

  /* This function opens the dropdown.
   */
  AutoComplete.prototype.open = function() {
    if (this.dropdown.hasAttribute("hidden")) {
      var inputStyle = window.getComputedStyle(this.element);
      var inputWidth  = this.element.offsetWidth  + parseInt(inputStyle.marginLeft) + parseInt(inputStyle.marginRight);
      var inputHeight = this.element.offsetHeight;
      this.dropdown.style.width = inputWidth + "px";
      var offset = this.element.getBoundingClientRect();

      this.dropdown.removeAttribute("hidden");
      this.dropdown.style.left = offset.left + "px";
      this.dropdown.style.top  = (offset.top + inputHeight) + "px";
      this.dropdown.style.display = "block";
      this.dropdown.style.height = "0px";

      var item = this.dropdown.querySelector("li");
      var dropdownHeight = 0;
      if (item) {
        var itemStyle = window.getComputedStyle(item);
        var itemHeight = item.offsetHeight + parseInt(itemStyle.marginTop) + parseInt(itemStyle.marginBottom);
        dropdownHeight = itemHeight * this.dropdown.querySelectorAll('li').length;
      }

      this.dropdown.style.height = dropdownHeight + "px";
    }

    this.fillDropdown();
  };

  /* This function closes the dropdown.
   */
  AutoComplete.prototype.close = function() {
    this.dropdown.setAttribute("hidden", true);
  };

  /* This function will attack the change events that will perform the
   * queries and update/show the dropdown.
   */
  AutoComplete.prototype.bindEvents = function() {
    var self = this;

    this.element.addEventListener('focus', function() {
      if (self.showDropdown == false) {
        self.showDropdown = true;
        return;
      }

      self.open();
    });

    this.element.addEventListener('keyup', function(event) {
      self.open();
    });
    
    this.element.addEventListener('blur', function(event) {
      event.stopPropagation();
      event.preventDefault();

      self.close();
    });
  };
};
