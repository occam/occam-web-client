/* This file handles markdown editing.
 */

var initOccamMarkdown = function(Occam) {
  var Markdown = Occam.Markdown = function(element) {
    var self = this;

    if (element === undefined) {
      return;
    }

    if (Markdown.renderer == undefined) {
      Markdown.renderer = new showdown.Converter();
    }

    this.element = element;

    this.input = element.querySelector("textarea.markdown");
    this.preview = element.querySelector(".markdown-preview");
    this.tabs = Occam.Tabs.load(element.querySelector("ul.tabs"));

    Markdown.count++;
    this.element.setAttribute('data-markdown-index', Markdown.count);

    Markdown._loaded[this.element.getAttribute('data-markdown-index')] = this;

    this.bindEvents();
    this.events = {};
  };

  Markdown.count = 0;
  Markdown._loaded = {};

  Markdown.loadAll = function(element) {
    var elements = element.querySelectorAll('.markdown-editor');

    elements.forEach(function(element) {
      Markdown.load(element);
    });
  };

  Markdown.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-markdown-index');

    if (index) {
      return Occam.Markdown._loaded[index];
    }

    return new Occam.Markdown(element);
  };

  Markdown.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Markdown.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  Markdown.prototype.updatePreview = function() {
  };

  Markdown.prototype.showPreview = function() {
  };

  Markdown.prototype.bindEvents = function() {
    var self = this;

    this.tabs.on('change', function(index) {
      if (index == 1) {
        // Update preview
        var html = Markdown.renderer.makeHtml(self.input.value);
        self.preview.innerHTML = html;
      }
    });
  };
};
