/* This file handles the javascript for the storage and publish pages.
 */

var initOccamStorage = function(Occam) {
  var Storage = Occam.Storage = function(element) {
    this.element = element;

    this.bindEvents();
  };

  Storage.loadAll = function(element) {
    var panels = element.querySelectorAll('.card.storage');

    panels.forEach(function(subElement) {
      var panel = new Occam.Storage(subElement);
    });

    panels = element.querySelectorAll('.card.publish');

    panels.forEach(function(subElement) {
      var panel = new Occam.Storage(subElement);
    });
  };

  Storage.prototype.bindEvents = function() {
    if (this.element.classList.contains("publish")) {
      // Capture the 'publish' button and issue the form request and gather
      // the result.
    }
    else if (this.element.classList.contains("storage")) {
      // Capture the add and update buttons and capture the result/errors.
    }
  };
};
