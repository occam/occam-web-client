/* This file handles drop down widgets on the page. Namely the top bar
 * dropdowns for the Person.
 */

var initOccamDropdown = function(Occam) {
  var Dropdown = Occam.Dropdown = function(element) {
    if (element === undefined || element.length <= 0) {
      return;
    }

    this.element     = element;
    this.linkElement = this.element.querySelector("a, input, button");
    this.menuElement = this.element.querySelector("ul.dropdown-menu-options");

    Dropdown.count++;
    this.element.setAttribute('data-loaded-index', Dropdown.count);

    Dropdown._loaded[this.element.getAttribute('data-loaded-index')] = this;

    this.bindEvents();
    this.events = {};
  };

  Dropdown.count = 0;

  Dropdown._loaded = {};

  Dropdown.loadAll = function(element) {
    var dropdowns = element.querySelectorAll('.dropdown-menu');

    dropdowns.forEach(function(element) {
      Occam.Dropdown.load(element);
    });
  }

  Dropdown.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-loaded-index');

    if (index) {
      return Occam.Dropdown._loaded[index];
    }

    return new Occam.Dropdown(element);
  };

  Dropdown.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Dropdown.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  Dropdown.blurEvent = function(event) {
    var self = Dropdown.load(this.parentNode);

    // Detect if focus is transferred to something within ourselves
    if (event.relatedTarget && event.relatedTarget.parentNode &&
                               event.relatedTarget.parentNode.parentNode &&
                               event.relatedTarget.parentNode.parentNode.parentNode) {
      if (event.relatedTarget.parentNode === event.target ||
        event.relatedTarget.parentNode.parentNode === event.target ||
        event.relatedTarget.parentNode.parentNode.parentNode === event.target) {
        return;
      }
    }

    self.linkElement.classList.remove("disable-tooltip");

    self.menuElement.style.display = "";
  };

  /*
   * This function initializes the dropdown and binds interactive events.
   */
  Dropdown.prototype.bindEvents = function(force) {
    var self = this;

    if (this.element) {
      if (!force && this.element.classList.contains('bound')) {
        return;
      }
    }

    self.menuElement.setAttribute("tabindex", "1");

    self.linkElement.addEventListener('click', function(event) {
      self.linkElement.classList.add("disable-tooltip");

      self.menuElement.style.display = "block";
      self.menuElement.removeEventListener("blur", Dropdown.blurEvent);
      self.menuElement.addEventListener("blur", Dropdown.blurEvent);

      var items = self.menuElement.querySelectorAll("li");
      items.forEach(function(itemElement, i) {
        if (!itemElement.classList.contains("bound")) {
          itemElement.addEventListener("click", function(event) {
            if (self.events["selected"]) {
              event.preventDefault();

              self.trigger("selected", {"element": itemElement,
                                        "index":   i});
            }
            self.menuElement.style.display = "none";
          });
          itemElement.classList.add("bound");
        }
      });

      // Focus on the element to detect when focus is removed
      self.menuElement.focus();

      // Do not go to the main link (for non-js uses)
      event.stopPropagation();
      event.preventDefault();
    });

    self.element.classList.add('bound');
  };
};
