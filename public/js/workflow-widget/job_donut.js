"use strict";

var initWorkflowWidgetJobDonut = function(window) {
  var Workflow = window.Workflow;

  window.JobDonut = function(element, options) {
    var parts = options.count;

    var radius = options.radius || 30;
    var thickness = options.thickness || 15;
    var padding = options.padding || 36;

    var svgns = "http://www.w3.org/2000/svg";

    if (parts > 8) {
      padding *= 2;
    }

    if (parts > 25) {
      padding *= 2;
    }

    var jobs = options.jobs;

    var svg = document.createElementNS(svgns, "svg");
    svg.setAttribute("width", radius*2);
    svg.setAttribute("height", radius*2);

    if (parts > 50) {
      padding = padding / 4;
    }

    padding = 2*Math.PI / padding;

    if (parts == 1) {
      padding = 0.0001;
    }

    if (parts > 50) {
      // Just show each job type in the circular graph
      
      // Count the number of running/finished/queued/failed
      var total = jobs.length;
      var arcs = {};
      var types = [];
      
      // Have at least the first job type as "started"
      types.push("started");
      arcs["started"] = 0;
      
      jobs.forEach(function(job) {
        if (arcs[job.status] === undefined) {
          arcs[job.status] = 0;
          types.push(job.status);
        }
        arcs[job.status] ++;
      });
      
      // Get the first arc length
      var theta = (2*Math.PI) * (arcs["started"] / total) - padding;

      // Start from the middle of the top
      var position = -Math.PI/2 - theta/2 - padding;

      var x = radius + radius*Math.cos(position);
      var y = radius + radius*Math.sin(position);

      var path = "";
      types.forEach(function(jobStatus) {
        position += padding;

        x = radius + radius*Math.cos(position);
        y = radius + radius*Math.sin(position);

        var startX = x;
        var startY = y;

        var vx = (x - radius);
        var vy = (y - radius);
        var innerX = radius + ((radius-thickness)/radius) * vx;
        var innerY = radius + ((radius-thickness)/radius) * vy;

        position += (2 * Math.PI) * (arcs[jobStatus] / total) - padding;

        x = radius + radius*Math.cos(position);
        y = radius + radius*Math.sin(position);

        vx = (x - radius);
        vy = (y - radius);
        var innerNextX = radius + ((radius-thickness)/radius) * vx;
        var innerNextY = radius + ((radius-thickness)/radius) * vy;
        
        var sweep = 0;
        
        if ((arcs[jobStatus] / total) >= 0.5) {
          sweep = 1;
        }

        var arc = document.createElementNS(svgns, "path");
        arc.setAttribute("d", " M " + startX + " " + startY +
                              " A " + radius + " " + radius + ", 0, " + sweep + ", 1, " + x + " " + y +
                              " L " + innerNextX + " " + innerNextY +
                              " A " + (radius-thickness) + " " + (radius-thickness) + ", 0, " + sweep + ", 0, " + innerX + " " + innerY +
                              " L " + startX + " " + startY);

        arc.setAttribute("data-job-status", jobStatus);

        svg.appendChild(arc);
      });
    }
    else {
      var theta = (2*Math.PI) / parts - padding;

      // Start from the middle of the top
      var position = -Math.PI/2 - theta/2 - padding;

      var x = radius + radius*Math.cos(position);
      var y = radius + radius*Math.sin(position);

      var path = "";

      jobs.forEach(function(job) {
        position += padding;

        x = radius + radius*Math.cos(position);
        y = radius + radius*Math.sin(position);

        var startX = x;
        var startY = y;

        var vx = (x - radius);
        var vy = (y - radius);
        var innerX = radius + ((radius-thickness)/radius) * vx;
        var innerY = radius + ((radius-thickness)/radius) * vy;

        position += theta;

        x = radius + radius*Math.cos(position);
        y = radius + radius*Math.sin(position);

        vx = (x - radius);
        vy = (y - radius);
        var innerNextX = radius + ((radius-thickness)/radius) * vx;
        var innerNextY = radius + ((radius-thickness)/radius) * vy;
        
        var sweep = 0;
        
        if (theta >= Math.PI) {
          sweep = 1;
        }

        var arc = document.createElementNS(svgns, "path");
        arc.setAttribute("d", " M " + startX + " " + startY +
                              " A " + radius + " " + radius + ", 0, " + sweep + ", 1, " + x + " " + y +
                              " L " + innerNextX + " " + innerNextY +
                              " A " + (radius-thickness) + " " + (radius-thickness) + ", 0, " + sweep + ", 0, " + innerX + " " + innerY +
                              " L " + startX + " " + startY);

        arc.setAttribute("data-job-status", job.status);
        arc.setAttribute("data-job-id", job.id);

        svg.appendChild(arc);
      });
    }

    element.appendChild(svg);
    this.element = element;
    this.svg = svg;
  }

  JobDonut.prototype.updateJobs = function(jobs) {
    var self = this;

    jobs.forEach(function(job) {
      var arc = self.svg.querySelector('path[data-job-id="' + job.id + '"]');
      if (arc) {
        arc.setAttribute("data-job-status", job.status);
      }
    });
  };
};
