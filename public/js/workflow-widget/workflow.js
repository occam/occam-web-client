"use strict";

// Default Options

var defaultOptions = {
  horizontalSnapTolerance: 10,
  verticalSnapTolerance:   20,
  nodeOverlapTolerance:    20,
  padding:                 30,
  allowSelections:         true,
  allowNodeMovement:       true,
  allowWireSelection:      true,
  wire: {
    horizontalThickness:   1,
    verticalThickness:     3,
    arcRadius:             20,
  },
  debug: {
    showCollisionBoxes:      false,
    showLabelCollisionBoxes: false,
    showQuadtreeBoxes:       false,
  },
  buttons: []
}

// Geometry

var rectIntersects = function(r1, r2) {
  return !(r2.left   > r1.right  ||
           r2.right  < r1.left   ||
           r2.top    > r1.bottom ||
           r2.bottom < r1.top);
}

var Workflow = function(element, options, jobs) {
  var self = this;
  self.options = Object.assign({}, defaultOptions, options || {});
  this.element = element;
  this.element.workflowObject = this;

  // Keep track of those touch screen fingers
  this.ongoingTouches = [];

  this.zoom = 1.0;

  this.events = {};

  this._worldX = 0;
  this._worldY = 0;

  this.clearCollisionChecks();

  this.draggable = element.querySelector(":scope > ul.connections");
  this.draggable.classList.add("draggable");

  this.svgPlane = document.createElementNS(Workflow.SVGNS, "svg");
  this.svgPlane.workflowObject = this;
  this.svgPlane.style.position = "absolute";
  this.svgPlane.style.left = 0;
  this.svgPlane.style.top  = 0;
  this.moveTo(0, 0);
  this.element.insertBefore(this.svgPlane, this.draggable);

  this.selection = [];

  this.selectionBox = document.createElement("div");
  this.selectionBox.classList.add("selection");
  this.selectionBox.style.display = "none";
  this.element.appendChild(this.selectionBox);

  this.dropdown = document.createElement("ul");
  this.dropdown.classList.add("dropdown");
  this.dropdown.setAttribute("hidden", true);
  this.dropdown.setAttribute("tabindex", "0");
  this.element.appendChild(this.dropdown);

  this.dropdown.addEventListener("blur", function(event) {
    self.hideDropdown();
  });

  // Create quadtree
  var bounds = self.bounds();
  self.quadtree = new Quadtree({
    x: bounds.left,
    y: bounds.top,
    width: bounds.right - bounds.left,
    height: bounds.bottom - bounds.top
  });
  self.createQuadtree();

  // Initialize
  this.nodeIndexMax = 0;
  self.nodes().forEach(function(node) {
    node.arrangeOutputWires();
    node.arrangeInputWires();
    node.repositionInputs();

    if (node.index() > self.nodeIndexMax) {
      self.nodeIndexMax = node.index();
    }
  });

  self.selectedWire = null;
  self.makingWire = false;

  if (self.options.allowWireSelection) {
    // Dragging Wires
    self.element.addEventListener('mousedown', Workflow.wireDragEvent);

    // Determine when the mouse is over a wire
    self.draggable.addEventListener('mousemove', Workflow.wireSelectionEvent);
    self.svgPlane.addEventListener('mousemove',  Workflow.wireSelectionEvent);
  }

  // Disable the right-click menu
  self.svgPlane.addEventListener('contextmenu', function(event) {
    var elementFromPoint = document.elementFromPoint(event.pageX, event.pageY);
    if (elementFromPoint === self.svgPlane || window.getParents(elementFromPoint, "ul.connections")[0].parentNode === self.element) {
      event.preventDefault();
      event.stopPropagation();
    }
  });

  // Mouse Down Event
  self.svgPlane.addEventListener('mousedown', Workflow.workflowClickEvent);

  // Touch Device Events
  self.svgPlane.addEventListener('touchstart',  Workflow.workflowTouchStartEvent);
  self.svgPlane.addEventListener('touchmove',   Workflow.workflowTouchMoveEvent);
  self.svgPlane.addEventListener('touchend',    Workflow.workflowTouchEndEvent);
  self.svgPlane.addEventListener('touchcancel', Workflow.workflowTouchEndEvent);

  self.element.addEventListener('mousewheel', Workflow.workflowWheelEvent);

  self.element.addEventListener('keypress', Workflow.workflowKeyEvent);

  self.jobData = jobs;

  // Initialize each node that already exists in the workflow view
  this.nodes().forEach(function(node) {
    self.initializeNode(node);
  });
};

// SVG Namespace
Workflow.SVGNS = "http://www.w3.org/2000/svg";

Workflow.prototype.reinitialize = function() {
  var self = this;
  this._nodes = undefined;
  this.nodes().forEach(function(node) {
    self.initializeNode(node);
  });

  this.redraw();
};

Workflow.prototype.localCoordinates = function(pageX, pageY) {
  return [pageX, pageY];
  return [pageX / this.zoom, pageY / this.zoom];
};

Workflow.prototype.updateJobs = function (jobs) {
  this.jobData = jobs;

  this.nodes().forEach(function(node) {
    if (jobs[node.index()]) {
      node.updateJobs(jobs[node.index()].jobs);
    }
  });
};

Workflow.prototype.on = function(event, callback) {
  this.events[event] = callback;

  return this;
};

Workflow.prototype.off = function(event) {
  if (this.events[event]) {
    delete this.events[event];
  }

  return this;
}

Workflow.prototype.trigger = function(event, data) {
  if (this.events[event]) {
    this.events[event].call(this, data)
  }

  return this;
}

Workflow.createFor = function(element) {
  if (!element.workflowObject) {
    element.workflowObject = new Workflow(element);
  }

  return element.workflowObject;
};

Workflow.wireSelectionEvent = function(event) {
  var self = this.parentNode.workflowObject;

  var eventPoint = self.localCoordinates(event.pageX, event.pageY);
  var eventX = eventPoint[0];
  var eventY = eventPoint[1];

  // Deselect current wire maybe
  if (self.selectedWire) {
    var wire = self.selectedWire;
    self.selectedWire = null;

    var wireOffset = wire.element.getBoundingClientRect();

    var left = wireOffset.left;
    var top  = wireOffset.top + wire.element.clientHeight;
    if (wire.isOutput()) {
      left += wire.element.clientWidth;
      left += 10;
    }
    else {
      left -= 10;
    }
    if (eventX > left - 10 &&
        eventX < left + 10 &&
        eventY > top  - 10 &&
        eventY < top  + 10) {
      wire.element.classList.add("selected");
      self.selectedWire = wire;
    }

    if (!self.selectedWire) {
      wire.element.classList.remove("selected");
    }
  }

  // Select a new wire
  if (!self.selectedWire) {
    self.wires().forEach(function(wire) {
      if (!wire.isConnected()) {
        var wireOffset = wire.element.getBoundingClientRect();

        var left = wireOffset.left;
        var top  = wireOffset.top + wire.element.clientHeight;
        if (wire.isOutput()) {
          left += wire.element.offsetWidth;
          left += 10;
        }
        else {
          left -= 10;
        }

        if (eventX > left - 10 &&
            eventX < left + 10 &&
            eventY > top  - 10 &&
            eventY < top  + 10) {
          wire.element.classList.add("selected");
          self.selectedWire = wire;
        }
      }
    });
  }
};

Workflow.wireClickEvent = function(event) {
  var wire = this.workflowWireObject;
  var self = wire.workflow;

  // Undo selections
  self.clearSelection();

  if (wire.isConnected()) {
    var o_wire = wire.connectedWire();
    if (!wire.isOutput()) {
      var temp = o_wire;
      o_wire = wire;
      wire = temp;
    }

    var node = wire.node();
    var o_node = wire.connectedNode();
    [wire, o_wire].forEach(function(subWire) {
      if (!(subWire.deleteButton)) {
        subWire.deleteButton = document.createElement("div");
        subWire.deleteButton.classList.add('delete-button');
        subWire.element.appendChild(subWire.deleteButton);
        subWire.workflow.highlightedWire = subWire;
        subWire.positionDeleteButton();

        if (subWire.subPath) {
          subWire.subPath.classList.add("selected");
        }

        subWire.element.classList.add("selected");

        subWire.deleteButton.addEventListener("click", function(event) {
          event.preventDefault();
          event.stopPropagation();

          var subWire = this.parentElement.workflowWireObject;
          subWire.disconnect();
        });
      }
      else {
        subWire.element.classList.remove("selected");
        subWire.workflow.highlightedWire = false;

        if (subWire.subPath) {
          subWire.subPath.classList.remove("selected");
        }

        subWire.deleteButton.parentNode.removeChild(subWire.deleteButton);
        subWire.deleteButton = undefined;
      }
    });
  }
}

Workflow.wireDragEvent = function(event) {
  var self = this.workflowObject;

  var draggableOffset = self.draggable.getBoundingClientRect();

  var eventPoint = self.localCoordinates(event.pageX, event.pageY);
  var eventX = eventPoint[0];
  var eventY = eventPoint[1];

  var pointX = eventX - draggableOffset.left;
  var pointY = eventY - draggableOffset.top;

  if (self.selectedWire) {
    event.stopPropagation();
    event.preventDefault();

    self.makingWire = true;

    // start the wire drag
    // attach the wire to a dummy node that we will move around
    var wire = self.selectedWire;
    var dummyPlane = self.element.querySelector(":scope > ul.connections");
    var anchorNode = wire.node().element;
    var dummyNode  = dummyPlane.querySelector("li.connection").cloneNode(false);
    dummyNode.innerHTML = "";
    dummyNode.classList.add("dummy");
    dummyNode.removeAttribute("hidden");

    // Ensure node list is cached
    self.nodes();

    var dummyNodeIndex = self.nodes().length;

    var nodeIndex = wire.node().index();
    var wireIndex = wire.index();
    var itemIndex = wire.nextItemIndex();

    var offsetX = 0;
    if (!wire.isOutput()) {
      offsetX = wire.node().element.clientWidth;
    }

    offsetX += 5;

    dummyNode.style.left = (pointX - offsetX) + "px";
    dummyNode.style.top  = (pointY - 50) + "px";
    dummyNode.style.borderColor = "transparent";
    dummyNode.style.background = "transparent";

    var dummyWireList = document.createElement("ul");
    var dummyWire = document.createElement("li");
    dummyWireList.appendChild(dummyWire);
    dummyWire.setAttribute('data-connected-to', nodeIndex + '-' + wireIndex + '-' + itemIndex);
    dummyWire.setAttribute('data-index', '0');
    dummyWire.setAttribute('data-item-index', '0');

    if (!wire.isOutput()) {
      dummyWireList.classList.add("outputs");
      dummyWire.classList.add("output");
    }
    else {
      dummyWireList.classList.add("inputs");
      dummyWire.classList.add("input");
    }
    dummyNode.appendChild(dummyWireList);
    // Insert the dummy node as the first node to ensure mouse events
    // can get to other nodes.
    dummyPlane.insertBefore(dummyNode, dummyPlane.children[0]);

    dummyNode.setAttribute('data-index', dummyNodeIndex);

    var dummyNodeObject = Workflow.Node.createFor(dummyNode, self);

    wire._connectedNode = undefined;
    wire._connectedWire = undefined;
    wire.element.setAttribute('data-connected-to', dummyNodeIndex + '-0-0');
    wire.element.setAttribute('data-item-index', itemIndex);
    wire.element.classList.remove('disconnected');

    dummyNodeObject._left = undefined;
    dummyNodeObject._top = undefined;
    dummyNodeObject._bounds = undefined;

    dummyNodeObject.arrangeInputWires();
    dummyNodeObject.arrangeOutputWires();
    dummyNodeObject.repositionInputs();
    dummyNodeObject.repositionOutputs();

    var wireDragMoveEvent = function(event) {
      var draggableOffset = self.draggable.getBoundingClientRect();

      var eventPoint = self.localCoordinates(event.pageX, event.pageY);
      var eventX = eventPoint[0];
      var eventY = eventPoint[1];

      var pointX = eventX - draggableOffset.left;
      var pointY = eventY - draggableOffset.top;

      dummyNode.style.left = (pointX - offsetX) + "px";
      dummyNode.style.top  = (pointY - 50) + "px";

      dummyNodeObject._left = undefined;
      dummyNodeObject._top = undefined;
      dummyNodeObject._bounds = undefined;

      dummyNodeObject.repositionInputs();
      dummyNodeObject.repositionOutputs();
    };
    this.addEventListener('mousemove', wireDragMoveEvent);

    var wireDragEnd = function(event) {
      self.makingWire = false;

      this.removeEventListener('mousemove',  wireDragMoveEvent);
      this.removeEventListener('mouseleave', wireDragEnd);
      this.removeEventListener('mouseup',    wireDragEnd);

      var dummyWireObject = Workflow.Wire.createFor([dummyWire], self)[0];

      if (dummyWireObject.subPath) {
        dummyWireObject.subPath.remove();
        dummyWireObject.subPath = null;
      }

      if (wire.subPath) {
        wire.subPath.remove();
        wire.subPath = null;
      }

      // The wires can be connected only if one is input and the other is output
      //    Therefore they must be of different types
      if (self.selectedWire && self.selectedWire.isConnected() && self.selectedWire != wire) {
        // Create a dummy disconnected wire here
        // And then set the selected wire to that dummy wire
        var newWire = self.selectedWire.element.cloneNode(false);
        newWire.removeAttribute("data-connected-to");
        newWire.removeAttribute("data-item-index");
        newWire.setAttribute("data-item-index", self.selectedWire.nextItemIndex());
        self.selectedWire.element.parentNode.insertBefore(newWire, self.selectedWire.element);
        self.selectedWire = Workflow.Wire.createFor([newWire], self)[0];
      }

      if (self.selectedWire && (self.selectedWire.isOutput() !== wire.isOutput() ) ) {
        wire.element.setAttribute('data-connected-to', self.selectedWire.node().index() + '-' + self.selectedWire.index() + '-' + self.selectedWire.itemIndex());

        self.selectedWire.element.setAttribute('data-connected-to', wire.node().index() + '-' + wire.index() + '-' + wire.itemIndex());
        self.selectedWire.element.classList.remove('disconnected');
        self.selectedWire.element.setAttribute('data-item-index', self.selectedWire.itemIndex());
        wire.element.classList.remove('disconnected');

        wire.connectedNode()._wires = undefined;
        wire.connectedNode().inputWires = undefined;
        wire.connectedNode().outputWires = undefined;

        self.selectedWire.connectedNode()._wires = undefined;
        self.selectedWire.connectedNode().inputWires = undefined;
        self.selectedWire.connectedNode().outputWires = undefined;

        wire.connectedNode().repositionOutputs();
        wire.connectedNode().repositionInputs();
      }
      else {
        wire.element.removeAttribute('data-connected-to');
        // If there is another wire connected at this index, destroy the wire
        var wires = wire.element.parentNode.querySelectorAll('li[data-index="' + wire.element.getAttribute("data-index") + '"]');

        if (wires.length > 1) {
          wire.element.remove();
          wire = null;
        }
        else {
          wire.collapse();
        }
      }

      if (self.selectedWire) {
        self.selectedWire.element.classList.remove("selected");
        self.selectedWire._connectedNode = undefined;
        self.selectedWire._connectedWire = undefined;
      }

      if (wire) {
        wire._connectedNode = undefined;
        wire._connectedWire = undefined;
        wire._isDown = undefined;
        wire.reposition();
      }

      self.selectedWire = null;

      dummyNode.remove();
    };
    
    this.addEventListener('mouseup', wireDragEnd);
    this.addEventListener('mouseleave', wireDragEnd);
  }
};

Workflow.nodeClickEvent = function(event) {
  if (event && event.target && (event.target.classList.contains("delete-button") || event.target.classList.contains("attach-button"))) {
    return;
  }

  var node = this.workflowNodeObject;
  var self = node.workflow;

  if (event.button == 2) {
    Workflow.workflowClickEvent.call(self.element, event);
    return;
  }

  // Hide dropdown if someone clicked on a node
  self.hideDropdown();

  // Unselect wires
  self.deselectWires();

  if (self.selectedWire) {
    return;
  }

  event.stopPropagation();
  event.preventDefault();

  if (node.element.classList.contains("frozen")) {
    return;
  }

  var eventPoint = self.localCoordinates(event.pageX, event.pageY);
  var eventX = eventPoint[0];
  var eventY = eventPoint[1];

  var nodesToMove = [node];
  if (self.selection.length > 0) {
    if (!node.element.classList.contains("selected")) {
      self.clearSelection();
    }
    else {
      nodesToMove = self.selection;
    }
  }

  var nodeOffset = node.element.getBoundingClientRect();

  if (!(eventX > nodeOffset.left &&
        eventX < nodeOffset.left + node.width() * self.zoom &&
        eventY > nodeOffset.top &&
        eventY < nodeOffset.top + node.height() * self.zoom)) {
    return;
  }

  var draggableOffset = self.draggable.getBoundingClientRect();

  var nodeX = node.left() + draggableOffset.left;
  var nodeY = node.top()  + draggableOffset.top;
  var startX = eventX;
  var startY = eventY;

  // Where we can safely place the node that it doesn't overlap
  var safeX = nodeX - draggableOffset.left;
  var safeY = nodeY - draggableOffset.top;
  var isSafe = true;

  var offsets = [];
  nodesToMove.forEach(function(nodeToMove) {
    offsets.push({
      x: nodeToMove.left() - node.left(),
      y: nodeToMove.top()  - node.top()
    });
  });

  node.element.classList.add("pre-selected");

  node.element.querySelectorAll(".label").forEach(function(element) {
    element.classList.remove("revealed");
  });

  node.element.style.zIndex = 9999;

  var moved = false;

  var moveNodeEvent = function(event) {
    var draggableOffset = self.draggable.getBoundingClientRect();

    var eventPoint = self.localCoordinates(event.pageX, event.pageY);
    var eventX = eventPoint[0];
    var eventY = eventPoint[1];

    var pointX = eventX - draggableOffset.left;
    var pointY = eventY - draggableOffset.top;

    node.workflow.clearCollisionChecks();

    event.stopPropagation();
    event.preventDefault();

    var deltaX = (startX - eventX) / self.zoom;
    var deltaY = (startY - eventY) / self.zoom;

    if (deltaX != 0) {
      moved = true;
    }

    var currentX = nodeX - deltaX - draggableOffset.left;
    var currentY = nodeY - deltaY - draggableOffset.top;

    // Move all selected nodes
    nodesToMove.forEach(function(nodeToMove, i) {
      nodeToMove._left = undefined;
      nodeToMove._top  = undefined;
      nodeToMove._bounds = undefined;

      nodeToMove.element.style.left = (currentX + offsets[i].x) + "px";
      nodeToMove.element.style.top  = (currentY + offsets[i].y) + "px";
    });

    // Snap to align with existing nodes
    var snapX = 20;
    node.workflow.nodes().forEach(function(subNode) {
      if (subNode.is(node)) {
        return;
      }

      if (nodesToMove.indexOf(subNode) >= 0) {
        return;
      }

      // TODO: Quadtree can select nodes for the vertical region
      if (subNode.left() - self.options.verticalSnapTolerance < node.left() && subNode.left() + self.options.verticalSnapTolerance > node.left()) {
        var newSnapX = node.left() - subNode.left();
        if (Math.abs(newSnapX) < Math.abs(snapX)) {
          snapX = newSnapX;
        }
      };
    });

    // Snap to straight connections
    var snapY = 0;
    nodesToMove.forEach(function(nodeToMove) {
      nodeToMove.wires().forEach(function(wire) {
        var subWire = wire.connectedWire();
        if (subWire) {
          var thisTop      = wire.top();
          var connectedTop = subWire.top();
          if ((thisTop - self.options.horizontalSnapTolerance) < connectedTop && (thisTop + self.options.horizontalSnapTolerance) > connectedTop) {
            // Align with their top
            snapY = thisTop - connectedTop;
          }
        }
      });
    });

    if (snapY != 0) {
      currentY -= snapY;
    }

    if (snapX != 20) {
      currentX -= snapX;
    }

    // Move all selected nodes to final position
    nodesToMove.forEach(function(nodeToMove, i) {
      nodeToMove._left = undefined;
      nodeToMove._top  = undefined;
      nodeToMove._bounds = undefined;

      nodeToMove.element.style.left = currentX + offsets[i].x + "px";
      nodeToMove.element.style.top  = currentY + offsets[i].y + "px";
    });

    // Check for overlap (for all moving nodes)
    var safe = true;
    nodesToMove.forEach(function(nodeToMove) {
      var nodeBound = nodeToMove.bounds();
      node.workflow.nodes().forEach(function(subNode) {
        if (subNode.is(nodeToMove)) {
          return;
        }

        var subNodeBound = {
          left:   subNode.left() - self.options.nodeOverlapTolerance,
          right:  subNode.left() + subNode.height() + self.options.nodeOverlapTolerance,
          top:    subNode.top()  - self.options.nodeOverlapTolerance,
          bottom: subNode.top()  + subNode.height() + self.options.nodeOverlapTolerance
        };

        if (rectIntersects(nodeBound, subNodeBound)) {
          safe = false;
        }
      });
    });

    if (safe) {
      safeX = currentX;
      safeY = currentY;
      isSafe = true;
    }
    else {
      isSafe = false;
    }

    nodesToMove.forEach(function(nodeToMove) {
      nodeToMove.repositionInputs();
      nodeToMove.repositionOutputs();
      node.workflow.addCollisionCheck(nodeToMove, nodeToMove.bounds(), []);
    });

    // Collision Check
    node.workflow.triggerCollisions();
  };

  if (self.options.allowNodeMovement) {
    document.addEventListener('mousemove', moveNodeEvent);
  }
  
  var removeSelectionEvent = function(event) {
    document.removeEventListener('mouseup',    removeSelectionEvent);
    document.removeEventListener('mouseleave', removeSelectionEvent);

    if (self.options.allowNodeMovement) {
      document.removeEventListener('mousemove',  moveNodeEvent);
    }

    node.element.classList.remove("pre-selected");

    if (!moved && event.type == "mouseup") {
      node.workflow.clearSelection();
      node.workflow.selection = [node];
      node.element.classList.add("selected");
      node.element.classList.add("viewing");
    }

    // Prevent overlap
    if (!isSafe) {
      node.element.classList.add("frozen");

      var start = null;
      var startX = node.left();
      var startY = node.top();

      var moveTowardSafe = function(timestamp) {
        if (!start) {
          start = timestamp;
        }
        var progress = Math.min((timestamp - start) / 500, 1.0);
        var newX = startX + (safeX - startX) * progress;
        var newY = startY + (safeY - startY) * progress;

        node._left   = undefined;
        node._top    = undefined;
        node._bounds = undefined;

        node.element.style.left = newX + "px";
        node.element.style.top  = newY + "px";

        node.repositionInputs();
        node.repositionOutputs();

        nodesToMove.forEach(function(nodeToMove, i) {
          if (nodeToMove.element == node.element) {
            return;
          }

          nodeToMove._left = undefined;
          nodeToMove._top  = undefined;
          nodeToMove._bounds = undefined;

          nodeToMove.element.style.left = (newX + offsets[i].x) + "px";
          nodeToMove.element.style.top  = (newY + offsets[i].y) + "px";

          nodeToMove.repositionInputs();
          nodeToMove.repositionOutputs();

          node.workflow.addCollisionCheck(nodeToMove, nodeToMove.bounds(), []);
        });

        // Collision Check
        node.workflow.addCollisionCheck(node, node.bounds(), []);
        node.workflow.triggerCollisions();
        node.workflow.clearCollisionChecks();

        if (progress < 1.0) {
          window.requestAnimationFrame(moveTowardSafe);
        }
        else {
          node.element.classList.remove("frozen");
        }
      };

      window.requestAnimationFrame(moveTowardSafe);
    }
    else {
      node.element.style.zIndex = 999;
    }
  };

  document.addEventListener('mouseup', removeSelectionEvent);
  document.addEventListener('mouseleave', removeSelectionEvent);
};

Workflow.prototype.moveTo = function(x, y) {
  var self = this;

  self._worldX = x;
  self._worldY = y;

  var plane = self.element.querySelector(":scope > ul.connections");
  plane.style.left = (x * self.zoom) + "px";
  plane.style.top  = (y * self.zoom) + "px";

  plane.style.transform = "scale(" + self.zoom + ")";
  plane.style["transform-origin"] = "top left";

  self.svgPlane.setAttribute("width",  (self.element.clientWidth  / self.zoom) + "px");
  self.svgPlane.setAttribute("height", (self.element.clientHeight / self.zoom) + "px");

  self.svgPlane.style.transform = "scale(" + self.zoom + ")";
  self.svgPlane.style["transform-origin"] = "top left";

  self.svgPlane.setAttribute("viewBox", -x+ " " + -y + " " + (self.element.clientWidth / self.zoom) + " " + (self.element.clientHeight / self.zoom));
};

Workflow.workflowKeyEvent = function(event) {
  var self = this.workflowObject;

  var key = event.char || event.key;
  if (key == "=" || key == "-") {
    if (key == "=") {
      self.zoom += 0.25;
    }
    else {
      self.zoom -= 0.25;
    }
    if (self.zoom < 0.25) {
      self.zoom = 0.25;
    }
    self.moveTo(self._worldX, self._worldY);

    event.stopPropagation();
    event.preventDefault();
  }
};

Workflow.workflowWheelEvent = function(event) {
  var element = this;
  var self = element.workflowObject;

  if (event.ctrlKey) {
    self.zoom += (event.wheelDeltaY / 120) * 0.25;
    if (self.zoom < 0.25) {
      self.zoom = 0.25;
    }
    self.moveTo(self._worldX, self._worldY);

    event.stopPropagation();
    event.preventDefault();
  }
};

Workflow.prototype.deselectWires = function() {
  var self = this;

  if (self.highlightedWire) {
    var o_wire = self.highlightedWire.connectedWire();
    [self.highlightedWire, o_wire].forEach(function(subWire) {
      if (subWire && subWire.subPath) {
        subWire.subPath.classList.remove("selected");
      }
      if (subWire) {
        subWire.element.classList.remove("selected");
      }
      if (subWire && subWire.deleteButton) {
        subWire.deleteButton.remove();
        subWire.deleteButton = false;
      }
    });
    self.highlightedWire = false;
  }
}

/* Copies a touch event
 */
function copyTouch(touch) {
  return { identifier: touch.identifier, pageX: touch.pageX, pageY: touch.pageY };
}

function ongoingTouchIndexById(ongoingTouches, idToFind) {
  for (var i = 0; i < ongoingTouches.length; i++) {
    var id = ongoingTouches[i].identifier;
    
    if (id == idToFind) {
      return i;
    }
  }
  return -1;    // not found
}

/* Handle the 'touchstart' event for touch screen devices.
 */
Workflow.workflowTouchStartEvent = function(event) {
  var element = this;
  var self = element.workflowObject;

  // Suppress the mouse click
  event.preventDefault();

  var touches = event.changedTouches;
  for (var i = 0; i < touches.length; i++) {
    var touch = touches[i];

    self.ongoingTouches.push(copyTouch(touch));
  };

  // Always clear selections for touch devices
  self.clearSelection();

  if (touches.length == 1) {
    var eventPoint = self.localCoordinates(touches[0].pageX, touches[0].pageY);
    var eventX = eventPoint[0];
    var eventY = eventPoint[1];

    self.tap(eventX, eventY, "move");
  }
  if (touches.length == 2) {
    var eventPoint = self.localCoordinates(touches[0].pageX, touches[0].pageY);
    var eventX = eventPoint[0];
    var eventY = eventPoint[1];

    self.tap(eventX, eventY, "move");
  }
};

/* Handle the 'touchmove' event for touch screen devices.
 */
Workflow.workflowTouchMoveEvent = function(event) {
  var element = this;
  var self = element.workflowObject;

  // Suppress the mouse move
  event.preventDefault();

  var touches = event.changedTouches;
  if (touches.length == 1) {
    // Two fingers (Move workflow)
    var eventPoint = self.localCoordinates(touches[0].pageX, touches[0].pageY);
    var eventX = eventPoint[0];
    var eventY = eventPoint[1];

    self.drag(eventX, eventY);
  }
  else if (touches.length == 1) {
    // One finger (Selection)
  }
};

/* Handle the 'touchend' event for touch screen devices.
 */
Workflow.workflowTouchEndEvent = function(event) {
  var element = this;
  var self = element.workflowObject;

  // Suppress the mouse click
  event.preventDefault();

  var touches = event.changedTouches;
  for (var i = 0; i < touches.length; i++) {
    var touch = touches[i];

    var idx = ongoingTouchIndexById(self.ongoingTouches, touch.identifier);
    if (idx >= 0) {
      self.ongoingTouches.splice(idx, 1);
    }
  };

  self.element.classList.remove("moving");
};

Workflow.prototype.drag = function(x, y) {
  var self = this;

  if (self.element.classList.contains("moving")) {
    var draggableOffset = self.draggable.getBoundingClientRect();

    var pointX = x - draggableOffset.left;
    var pointY = y - draggableOffset.top;

    var deltaX = self.moveStartX - x;
    var deltaY = self.moveStartY - y;

    var currentX = self.moveWorldX - deltaX;
    var currentY = self.moveWorldY - deltaY;

    self.moveTo(currentX, currentY);
  }
};

Workflow.prototype.tap = function(x, y, action) {
  var self = this;

  self.element.setAttribute("tabindex", "0");
  self.element.focus();
  self.element.setAttribute("tabindex", "-1");

  // Hide dropdown if someone clicked on the surface.
  self.hideDropdown();

  // Unselect wires
  self.deselectWires();

  // Determine what is under the tap
  var elementFromPoint = document.elementFromPoint(x, y);
  /*if (elementFromPoint !== element || window.getParents(elementFromPoint, "ul.connections", true).length > 0) {
    return;
  }*/

  // Don't do anything if we are playing with wires
  if (self.selectedWire || self.makingWire) {
    return;
  }

  if (action == "move") {
    // Move everything around
    self.moveStartX = x;
    self.moveStartY = y;
    self.moveWorldX = self._worldX;
    self.moveWorldY = self._worldY;

    var moved = false;

    self.element.classList.add("moving");

    var worldDragEvent = function(event) {
      var eventPoint = self.localCoordinates(event.pageX, event.pageY);
      var eventX = eventPoint[0];
      var eventY = eventPoint[1];

      event.stopPropagation();
      event.preventDefault();

      self.drag(eventX, eventY);
    };

    document.addEventListener('mousemove', worldDragEvent);

    var stopMovingEvent = function(event) {
      this.removeEventListener('mouseup', stopMovingEvent);
      this.removeEventListener('mousemove', worldDragEvent);

      self.element.classList.remove("moving");
    };

    document.addEventListener('mouseup', stopMovingEvent);
  }
  else if (event.button == 0) {
    // Make selection

    // Selection
    event.stopPropagation();
    event.preventDefault();

    // Undo selections
    self.clearSelection();

    var elementOffset = self.element.getBoundingClientRect();

    var pointX = event.pageX - elementOffset.left;
    var pointY = event.pageY - elementOffset.top;

    if (self.options.allowSelections) {
      var selectionX = pointX;
      var selectionY = pointY;

      self.selectionBox.style.left    = selectionX + "px";
      self.selectionBox.style.top     = selectionY + "px";
      self.selectionBox.style.width   = 0 + "px";
      self.selectionBox.style.height  = 0 + "px";
      self.selectionBox.style.display = "block";

      self.element.classList.add("selecting");
      self.element.style.cursor = "crosshair";

      var selectionDragEvent = function(event) {
        var elementOffset = self.element.getBoundingClientRect();

        var pointX = event.pageX - elementOffset.left;
        var pointY = event.pageY - elementOffset.top;

        var selectionWidth  = pointX - selectionX;
        var selectionHeight = pointY - selectionY;

        var selectionLeft = selectionX;
        if (selectionWidth < 0) {
          selectionLeft = pointX;
          selectionWidth = -selectionWidth;
        }

        var selectionTop = selectionY;
        if (selectionHeight < 0) {
          selectionTop = pointY;
          selectionHeight = -selectionHeight;
        }

        self.selectionBox.style.left   = selectionLeft   + "px";
        self.selectionBox.style.top    = selectionTop    + "px";
        self.selectionBox.style.width  = selectionWidth  + "px";
        self.selectionBox.style.height = selectionHeight + "px";
      };

      document.addEventListener('mousemove', selectionDragEvent);

      var selectionEndEvent = function(event) {
        document.removeEventListener('mousemove', selectionDragEvent);
        document.removeEventListener('mouseup',   selectionEndEvent);

        var eventPoint = self.localCoordinates(event.pageX, event.pageY);
        var eventX = eventPoint[0];
        var eventY = eventPoint[1];

        var draggableOffset = self.draggable.getBoundingClientRect();
        var selectionOffset = self.selectionBox.getBoundingClientRect();

        var selectionLeft   = selectionOffset.left - draggableOffset.left;
        var selectionTop    = selectionOffset.top  - draggableOffset.top;
        var selectionWidth  = self.selectionBox.clientWidth;
        var selectionHeight = self.selectionBox.clientHeight;

        self.element.classList.remove("selecting");
        self.element.style.cursor = "";

        self.selectionBox.style.display = "none";

        // Select boxes that are contained within the selection
        var selectionBounds = {
          left:   selectionLeft / self.zoom,
          right:  selectionLeft / self.zoom + selectionWidth / self.zoom,
          top:    selectionTop  / self.zoom,
          bottom: selectionTop  / self.zoom + selectionHeight / self.zoom
        };

        self.selection = [];
        self.nodes().forEach(function(node) {
          if (rectIntersects(selectionBounds, node.bounds())) {
            self.selection.push(node);
            node.element.classList.add("selected");
          }
        });

        if (self.selection.length == 1) {
          self.selection[0].element.classList.add("viewing");
        }
      };

      document.addEventListener('mouseup', selectionEndEvent);
    }
  }
};

Workflow.workflowClickEvent = function(event) {
  var element = this;
  var self = element.workflowObject;

  var eventPoint = self.localCoordinates(event.pageX, event.pageY);
  var eventX = eventPoint[0];
  var eventY = eventPoint[1];

  var action = ((event.button == 2) ? "move" : "select");

  event.stopPropagation();
  event.preventDefault();

  self.tap(eventX, eventY, action);
};

/**
 * Removes all nodes and wires from the workflow.
 */
Workflow.prototype.clear = function() {
  this.nodes().forEach(function(node) {
    node.destroy();
  });
};

Workflow.prototype.fillDropdown = function(wires) {
  var self = this;
  self.dropdown.innerHTML = "";

  var wireCount = 0;
  var last = -1;

  wires.forEach(function(wire) {
    var index = parseInt(wire.element.getAttribute("data-index"));
    if (index > last) {
      if (wire.element.classList.contains("hidden")) {
        last = index;
        var item = document.createElement("li");
        var type = document.createElement("span");
        type.classList.add("type");
        type.textContent = wire.typeLabel();
        item.appendChild(type);
        var name = document.createElement("span");
        name.classList.add("name");
        name.textContent = wire.nameLabel();
        item.appendChild(name);

        item.addEventListener("click", function(event) {
          wire.show();
          self.hideDropdown();

          if (wire.isOutput() && wire.node().hiddenOutputCount() == 0) {
            wire.node().outputAddButton.setAttribute("hidden", true);
          }
          if (!wire.isOutput() && wire.node().hiddenInputCount() == 0) {
            wire.node().inputAddButton.setAttribute("hidden", true);
          }
        });
        self.dropdown.appendChild(item);
      }
    }
  });

  if (last == -1) {
    // No items
    // TODO: i18n
    self.dropdown.innerHTML = "<p>No other wires</p>";
  }
};

Workflow.prototype.hideDropdown = function() {
  this.dropdown.setAttribute("hidden", true);
};

Workflow.prototype.showDropdown = function(x, y) {
  this.dropdown.style.opacity = 0.0;
  this.dropdown.removeAttribute("hidden");
  this.dropdown.style.left = (x - this.dropdown.clientWidth / 2) + "px";
  this.dropdown.style.top  = (y) + "px";
  this.dropdown.style.opacity = 1.0;
  this.dropdown.focus();
};

Workflow.prototype.showInputDropdown = function(node, x, y) {
  this.fillDropdown(node.inputs());
  this.showDropdown(x, y);
};

Workflow.prototype.showOutputDropdown = function(node, x, y) {
  this.fillDropdown(node.outputs());
  this.showDropdown(x, y);
};

Workflow.prototype.initializeNode = function(node) {
  var self = this;

  self._nodes = undefined;
  self._wires = undefined;

  if (self.element.classList.contains("editable")) {
    // Add input add
    node.inputAddButton = document.createElement("div");
    node.inputAddButton.classList.add("input-add-button");
    node.element.appendChild(node.inputAddButton);
    node.inputAddButton.addEventListener("click", function(event) {
      // Display input dropdown
      var elementOffset = self.element.getBoundingClientRect();

      var pointX = event.pageX - elementOffset.left;
      var pointY = event.pageY - elementOffset.top;
      self.showInputDropdown(node, pointX, pointY);
    });
    if (node.hiddenInputCount() == 0) {
      node.inputAddButton.setAttribute("hidden", true);
    }

    node.outputAddButton = document.createElement("div");
    node.outputAddButton.classList.add("output-add-button");
    node.element.appendChild(node.outputAddButton);
    node.outputAddButton.addEventListener("click", function(event) {
      // Display output dropdown
      var elementOffset = self.element.getBoundingClientRect();

      var pointX = event.pageX - elementOffset.left;
      var pointY = event.pageY - elementOffset.top;
      self.showOutputDropdown(node, pointX, pointY);
    });
    if (node.hiddenOutputCount() == 0) {
      node.outputAddButton.setAttribute("hidden", true);
    }

    node.deleteButton = document.createElement("div");
    node.deleteButton.classList.add("delete-button");
    node.element.appendChild(node.deleteButton);
    node.deleteButton.addEventListener("click", function(event) {
      event.preventDefault();
      event.stopPropagation();

      self.trigger("node-removed", {
        "element": node.element,
        "node": node
      });
      node.destroy();
    });
  }

  (self.options.buttons || []).forEach(function(buttonInfo, i) {
    var button = document.createElement("div");
    (buttonInfo.classes || []).forEach(function(buttonClass) {
      button.classList.add(buttonClass);
    });

    if (self.options.buttons.length == 1) {
      button.classList.add("of-one");
    }

    if (self.options.buttons.length == 2) {
      button.classList.add("of-two");
    }

    if (self.options.buttons.length == 3) {
      button.classList.add("of-three");
    }

    if (self.options.buttons.length == 4) {
      button.classList.add("of-four");
    }

    button.classList.add("bottom-button");

    button.classList.add(["one", "two", "three", "four"][i]);

    node.element.appendChild(button);

    button.addEventListener("click", function(event) {
      self.trigger("button-click", {
        "element": button,
        "node": node
      });
    });
  });

  node.element.addEventListener('mousedown', Workflow.nodeClickEvent);

  node.element.addEventListener('contextmenu', function(event) {
    var elementFromPoint = document.elementFromPoint(event.pageX, event.pageY);
    if (elementFromPoint === self.svgPlane || window.getParents(elementFromPoint, "ul.connections")[0].parentNode === self.element) {
      event.preventDefault();
      event.stopPropagation();
    }
  });

  node.element.addEventListener('mouseenter', function(event) {
    var node = this;
    node.querySelectorAll("li .label").forEach(function(element) {
      element.classList.add("revealed");
    });
  });
  
  node.element.addEventListener('mouseleave', function(event) {
    var node = this;
    node.querySelectorAll("li .label").forEach(function(element) {
      element.classList.remove("revealed");
    });
  });

  node.arrangeInputWires();
  node.arrangeOutputWires();
  node.repositionInputs();
  node.repositionOutputs();

  // TODO: figure out a new index
  node.element.setAttribute("data-index", getChildIndex(node.element));

  if (self.jobData) {
    var jobData = [];
    if (self.jobData[node.index()]) {
      jobData = self.jobData[node.index()].jobs;
    }

    if (jobData.length > 0) {
      node.element.classList.add("tracking-jobs");
      node.jobDonut = new window.JobDonut(node.element, {"count": jobData.length, "jobs": jobData});
    }
  }
};

Workflow.prototype.addNode = function(data) {
  var self = this;

  var nodeElement = document.createElement("li");
  nodeElement.classList.add('connection');

  nodeElement.setAttribute('data-index', data.data.index);

  var nameLabel = document.createElement("span");
  nameLabel.classList.add("name");
  nameLabel.textContent = data.name;

  var typeLabel = document.createElement("span");
  typeLabel.classList.add("type");
  typeLabel.textContent = data.type;

  var icon = document.createElement("div");
  icon.classList.add("icon");

  for (var key in (data.image || {})) {
    icon.setAttribute('data-' + key, data.image[key]);
  }

  nodeElement.appendChild(icon);
  nodeElement.appendChild(typeLabel);
  nodeElement.appendChild(nameLabel);

  var inputs  = document.createElement("ul");
  inputs.classList.add("inputs");
  var outputs = document.createElement("ul");
  outputs.classList.add("outputs");

  // Add wires
  nodeElement.appendChild(inputs);
  nodeElement.appendChild(outputs);

  // Position Node
  nodeElement.style.top  = data.top + "px";
  nodeElement.style.left = data.left + "px";

  self.element.querySelector(":scope > ul.connections").appendChild(nodeElement);

  var node = Node.createFor(nodeElement, self);
  self.initializeNode(node);

  (data.outputs || []).forEach(function(wires, wireIndex) {
    wires.forEach(function(wireData) {
      node.addWire(wireData, wireIndex, false);
    });
  });

  (data.inputs  || []).forEach(function(wires, wireIndex) {
    wires.forEach(function(wireData) {
      node.addWire(wireData, wireIndex, true);
    });
  });

  return node;
};

Workflow.prototype.redraw = function() {
  this.moveTo(this._worldX, this._worldY)
  this.nodes().forEach(function(node) {
    node.redraw();
  });
};

Workflow.prototype.clearSelection = function() {
  this.element.querySelectorAll("ul.connections li.connection.selected:not(.dummy)").forEach(function(nodeElement) {
    nodeElement.classList.remove("selected");
    nodeElement.classList.remove("viewing");
  });
  this.selection = [];
};

Workflow.prototype.createQuadtree = function() {
  var self = this;

  var bounds = self.bounds();

  self.quadtree.clear();

  if (self.options.debug.showQuadtreeBoxes) {
    self.element.querySelectorAll(".debug.quadtree").forEach(function(element) {
      element.remove();
    });
  }

  // Add all nodes
  self.nodes().forEach(function(node) {
    var bounds = node.bounds();
    var quadtreeObject = {
      x: bounds.left,
      y: bounds.top,
      width: bounds.right - bounds.left,
      height: bounds.bottom - bounds.top,
      object: node,
      type: "node"
    };

    if (self.options.debug.showQuadtreeBoxes) {
      var collisionBox = document.createElement("div");
      collisionBox.classList.add("debug");
      collisionBox.classList.add("quadtree");
      collisionBox.style.display = "block";
      collisionBox.style.left    = quadtreeObject.x + self.draggable.offsetLeft;
      collisionBox.style.top     = quadtreeObject.y + self.draggable.offsetTop;
      collisionBox.style.width   = quadtreeObject.width;
      collisionBox.style.height  = quadtreeObject.height;

      self.element.appendChild(collisionBox);
    }

    node.quadtreeObject = quadtreeObject;

    self.quadtree.insert(quadtreeObject);
  });

  // Add all wires
  self.wires().forEach(function(wire) {
    // Add each section of the wire
    var bounds = {};
    if (wire.isConnected()) {
      bounds = wire.verticalBounds();
      var quadtreeObject = {
        x: bounds.left,
        y: bounds.top,
        width: bounds.right - bounds.left,
        height: bounds.bottom - bounds.top,
        object: wire,
        type: "wire"
      };
      if (self.options.debug.showQuadtreeBoxes) {
        var collisionBox = document.createElement("div");
        collisionBox.classList.add("debug");
        collisionBox.classList.add("quadtree");
        collisionBox.style.display = "block";
        collisionBox.style.left    = quadtreeObject.x + self.draggable.offsetLeft;
        collisionBox.style.top     = quadtreeObject.y + self.draggable.offsetTop;
        collisionBox.style.width   = quadtreeObject.width;
        collisionBox.style.height  = quadtreeObject.height;
        self.element.appendChild(collisionBox);
      }

      wire.quadtreeObjectB = quadtreeObject;

      self.quadtree.insert(quadtreeObject);

      bounds = wire.horizontalBounds();
    }
    else {
      bounds = wire.disconnectedBounds();
    }

    var quadtreeObject = {
      x: bounds.left,
      y: bounds.top,
      width: bounds.right - bounds.left,
      height: bounds.bottom - bounds.top,
      object: wire,
      type: "wire"
    };

    if (self.options.debug.showQuadtreeBoxes) {
      var collisionBox = document.createElement("div");
      collisionBox.classList.add("debug");
      collisionBox.classList.add("quadtree");
      collisionBox.style.display = "block";
      collisionBox.style.left    = quadtreeObject.x + self.draggable.offsetLeft;
      collisionBox.style.top     = quadtreeObject.y + self.draggable.offsetTop;
      collisionBox.style.width   = quadtreeObject.width;
      collisionBox.style.height  = quadtreeObject.height;
      self.element.appendChild(collisionBox);
    }

    wire.quadtreeObjectA = quadtreeObject;

    self.quadtree.insert(quadtreeObject);
  });
};

Workflow.prototype.updateQuadtree = function(object) {
  var self = this;

  if (object.quadTreeObject) {
    // node
    var node = object;
    var bounds = node.bounds();
    var quadtreeObject = {
      x: bounds.left,
      y: bounds.top,
      width: bounds.right - bounds.left,
      height: bounds.bottom - bounds.top,
      object: node,
      type: "node"
    };

    this.quadtree.removeObject(node.quadtreeObject);
    node.quadtreeObject = quadtreeObject;
    this.quadtree.insert(node.quadtreeObject);
  }
  if (object.quadTreeObjectA) {
    // wire
    var wire = object;
    var bounds = wire.verticalBounds();
    var quadtreeObject = {
      x: bounds.left,
      y: bounds.top,
      width: bounds.right - bounds.left,
      height: bounds.bottom - bounds.top,
      object: wire,
      type: "wire"
    };

    this.quadtree.removeObject(wire.quadtreeObjectA);
    wire.quadtreeObjectA = quadtreeObject;
    this.quadtree.insert(wire.quadtreeObjectA);
  }
  if (object.quadTreeObjectB) {
    // wire
    var wire = object;
    var bounds = wire.horizontalBounds();
    var quadtreeObject = {
      x: bounds.left,
      y: bounds.top,
      width: bounds.right - bounds.left,
      height: bounds.bottom - bounds.top,
      object: wire,
      type: "wire"
    };

    this.quadtree.removeObject(wire.quadtreeObjectB);
    wire.quadtreeObjectB = quadtreeObject;
    this.quadtree.insert(wire.quadtreeObjectB);
  }
};

Workflow.prototype.cleanUpQuadtree = function() {
  this.quadtree.cleanUp();
};

Workflow.prototype.queryQuadtree = function(region) {
  return this.quadtree.retrieve({
    x: region.left,
    y: region.top,
    height: region.bottom - region.top,
    width: region.right - region.left
  });
};

Workflow.prototype.clearCollisionChecks = function() {
  this.collisionRegions = [];
  this.collisionExceptions = new Set([]);
};

/* This function marks a region of the widget as having moved. Each moved
 * region is checked against collisions when the widget has finished updating.
 */
Workflow.prototype.addCollisionCheck = function(object, region, exceptions) {
  // Push collision region
  this.collisionRegions.push({
    "object": object,
    "region": region
  });

  // Append exceptions
  for (var elem of exceptions) {
    this.collisionExceptions.add(elem);
  }

  // Add the object that requested to the exceptions list as well
  this.collisionExceptions.add(object);
};

Workflow.prototype.positionLabels = function() {
  var self = this;

  self.wires().forEach(function(wire) {
    wire.positionLabel();
    wire.positionDeleteButton();
  });
};

/* This function will accept a region that has been updated (for instance,
 * this will fire when a wire is moved to indicate it is now affecting the
 * given area.) This function will trigger collision events for things within
 * the given region such that they might move or disappear, etc.
 */
Workflow.prototype.triggerCollisions = function() {
  var self = this;

  self.element.querySelectorAll(".debug.collision").forEach(function(element) {
    element.remove();
  });

  for (var i = 0; i < this.collisionRegions.length; i++) {
    var collision = this.collisionRegions[i];
    self.triggerCollision(collision.object, collision.region);
  }

  self.clearCollisionChecks();

  // Position Wire Labels (After collisions have resolved and wires have moved)
  self.positionLabels();
};

/**
 * Checks for any collisions with the given object inside the given region.
 * @returns 
 */
Workflow.prototype.collisionCheck = function(object, region) {
  var self = this;

  // Find collisions
  var collisions = self.queryQuadtree(region);

  if (self.options.debug.showCollisionBoxes) {
    var subWireBound = region;

    var collisionBox = document.createElement("div");
    collisionBox.classList.add("debug");
    collisionBox.classList.add("collision");

    collisionBox.style.display = "block";
    collisionBox.style.left    = subWireBound.left + self.draggable.offsetLeft;
    collisionBox.style.top     = subWireBound.top  + self.draggable.offsetTop;
    collisionBox.style.width   = subWireBound.right - subWireBound.left;
    collisionBox.style.height  = subWireBound.bottom - subWireBound.top;
    collisionBox.style.backgroundColor = "yellow";
    self.element.appendChild(collisionBox);
  }

  var collisionObjects = new Set([]);

  collisions.forEach(function(possibleCollisionObject) {
    if (possibleCollisionObject.type == "node") {
      var subNode = possibleCollisionObject.object;

      if (object === null || !object.is(subNode)) {
        var subNodeBound = subNode.bounds();

        if (rectIntersects(subNodeBound, region)) {
          if (!self.collisionExceptions.has(subNode)) {
            collisionObjects.add(subNode);
            self.collisionExceptions.add(subNode);
          }
        }
      }
    }
    else if (possibleCollisionObject.type == "wire") {
      var subWireBound = {
        left:   possibleCollisionObject.x,
        right:  possibleCollisionObject.x + possibleCollisionObject.width,
        top:    possibleCollisionObject.y,
        bottom: possibleCollisionObject.y + possibleCollisionObject.height,
      };

      if (self.options.debug.showCollisionBoxes) {
        var collisionBox = document.createElement("div");
        collisionBox.classList.add("debug");
        collisionBox.classList.add("collision");

        collisionBox.style.display = "block";
        collisionBox.style.left    = subWireBound.left + self.draggable.offsetLeft;
        collisionBox.style.top     = subWireBound.top  + self.draggable.offsetTop;
        collisionBox.style.width   = subWireBound.right - subWireBound.left;
        collisionBox.style.height  = subWireBound.bottom - subWireBound.top;
        collisionBox.style.backgroundColor = "rgba(0, 0, 256, 0.333)";
        self.element.appendChild(collisionBox);
      }

      // Tell the node to update.
      var subWire = possibleCollisionObject.object;
      var subNode = subWire.node();
      if (object === null || !object.is(subWire)) {
        if (rectIntersects(subWireBound, region)) {
          if (self.options.debug.showCollisionBoxes) {
            var collisionBox = document.createElement("div");
            collisionBox.classList.add("debug");
            collisionBox.classList.add("collision");

            collisionBox.style.display = "block";
            collisionBox.style.left    = subWireBound.left + self.draggable.offsetLeft;
            collisionBox.style.top     = subWireBound.top  + self.draggable.offsetTop;
            collisionBox.style.width   = subWireBound.right - subWireBound.left;
            collisionBox.style.height  = subWireBound.bottom - subWireBound.top;
            self.element.appendChild(collisionBox);
          }

          if (!self.collisionExceptions.has(subWire)) {
            collisionObjects.add(subWire);
            self.collisionExceptions.add(subWire);
          }
        }
      }
    }
  });

  return collisionObjects;
};

/**
 * Yields a Object form of the the workflow which can then be later imported.
 * @returns {Object}
 */
Workflow.prototype.exportJSON = function() {
  var ret = {};

  // TODO: add the center point

  ret.connections = [];

  this.nodes().forEach(function(node) {
    var nodeData = {};
    nodeData.left = node.left();
    nodeData.top  = node.top();

    nodeData.name = node.nameLabel();
    nodeData.type = node.typeLabel();

    nodeData.inputs  = [];
    nodeData.outputs = [];

    nodeData.data = {};
    for (var i = 0; i < node.element.attributes.length; i++) {
      var attr = node.element.attributes[i];
      if (attr.name.startsWith("data-")) {
        nodeData.data[attr.name.substring(5)] = attr.value;
      }
    }

    nodeData.image = {};
    var imageElement = node.element.querySelector(".icon");
    for (var i = 0; i < imageElement.attributes.length; i++) {
      var attr = imageElement.attributes[i];
      if (attr.name.startsWith("data-")) {
        nodeData.image[attr.name.substring(5)] = attr.value;
      }
    }

    var currentWireList = nodeData.inputs;

    var iterateWire = function(wire) {
      var wireData = {};

      var wireIndex = wire.index();
      var wirePosition = wire.itemIndex();

      wireData.position = wire.position();
      wireData.left     = wire.element.offsetLeft;
      wireData.top      = wire.element.offsetTop;
      wireData.width    = wire.element.offsetWidth;
      wireData.height   = wire.element.offsetHeight;

      if (wire.isConnected()) {
        wireData.toNode  = wire.connectedNode().index();
        wireData.toWire  = wire.connectedWire().index();
        wireData.toIndex = wire.connectedWire().itemIndex();
      }

      wireData.isDown  = wire.isDown();
      wireData.isLeft  = wire.isLeft();
      wireData.isRight = wire.isRight();

      wireData.name    = wire.nameLabel();
      wireData.type    = wire.typeLabel();

      wireData.visibility = (wire.element.hasAttribute("hidden") ? "hidden" : "visible");

      while (currentWireList.length <= wireIndex) {
        currentWireList.push({
          "wires": [],
          "name":  wireData.name,
          "type":  wireData.type,
          "visibility": wireData.visibility,
        });
      }

      while (currentWireList[wireIndex].wires.length <= wirePosition) {
        currentWireList[wireIndex].wires.push({});
      }

      currentWireList[wireIndex].wires[wirePosition] = wireData;
    };

    node.inputs().forEach(iterateWire);

    currentWireList = nodeData.outputs;
    node.outputs().forEach(iterateWire);

    ret.connections.push(nodeData);
  });

  var offsetX = this.element.clientWidth  / 2;
  var offsetY = this.element.clientHeight / 2;

  ret.center   = {};
  ret.center.x = (parseInt(this.draggable.style.left) || 0) + offsetX;
  ret.center.y = (parseInt(this.draggable.style.top)  || 0) + offsetY;

  return ret;
};

/**
 * Adds the workflow given by 'data' to the existing workflow. Each new
 * node will be added to the end of the node list such that the indexes
 * within the imported data will all be incremented by the number of current
 * nodes.
 *
 * If the workflow is clear, the workflow is simply loaded.
 */
Workflow.prototype.importJSON = function(data) {
  var self = this;
  var startIndex = self.nodes().length;

  var offsetX = self.element.clientWidth  / 2;
  var offsetY = self.element.clientHeight / 2;

  this.draggable.style.left = (((data.center || {}).x || 0) - offsetX) + "px";
  this.draggable.style.top  = (((data.center || {}).y || 0) - offsetY) + "px";

  // Go through the nodes and add them to the workflow
  data.connections.forEach(function(nodeData) {
    var node = self.addNode(nodeData);
  });

  self.nodes().forEach(function(node) {
    node.repositionInputs();
    node.repositionOutputs();
  });
};

Workflow.prototype.triggerCollision = function(object, region) {
  var self = this;

  // Find collision objects
  var collisionObjects = self.collisionCheck(object, region);

  for (var obj of object.collisionChildren) {
    collisionObjects.add(obj);
  }

  object.collisionChildren = [];

  for (var obj of collisionObjects) {
    if (obj.type == "wire") {
      var subWire = obj;

      if (!subWire.isConnected()) {
        subWire.collapse();
      }
      else if (!subWire.isOutput()) {
        subWire.recalculatePosition();
      }
      else {
        subWire.connectedWire().recalculatePosition();
      }
    }
  }
};

Workflow.prototype.nodes = function() {
  if (!this._nodes) {
    var self = this;
    self._nodes = [];
    this.element.querySelectorAll(":scope > ul.connections li.connection").forEach(function(node) {
      self._nodes.push(Workflow.Node.createFor(node, self));
    });
  }

  return this._nodes;
};

Workflow.prototype.wires = function() {
  if (!this._wires) {
    var self = this;
    self._wires = Workflow.Wire.createFor(this.element.querySelectorAll("li.input, li.output"), self);
  }
  return this._wires;
};

Workflow.prototype.bounds = function() {
  if (!this._bounds) {
    var self = this;
    var bounds = null;
    this.nodes().forEach(function(node) {
      var subBounds = {
        left:   node.element.offsetLeft - self.options.padding,
        right:  node.element.offsetLeft + node.element.offsetWidth + self.options.padding,
        top:    node.element.offsetTop - self.options.padding,
        bottom: node.element.offsetTop + node.element.offsetHeight + self.options.padding
      };
      if (!bounds) {
        bounds = subBounds;
      }
      else {
        if (bounds.left > subBounds.left) {
          bounds.left = subBounds.left;
        }
        if (bounds.top > subBounds.top) {
          bounds.top = subBounds.top;
        }
        if (bounds.right < subBounds.right) {
          bounds.right = subBounds.right;
        }
        if (bounds.bottom < subBounds.bottom) {
          bounds.bottom = subBounds.bottom;
        }
      }
    });
    if (bounds === null) {
      bounds = {left: 0, right: 0, top: 0, bottom: 0};
    }
    this._bounds = bounds;
  }

  return this._bounds;
};

window.Workflow = Workflow;

initWorkflowWidgetNode(window)
initWorkflowWidgetWire(window)
initWorkflowWidgetJobDonut(window)

