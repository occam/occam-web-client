"use strict";

var initWorkflowWidgetWire = function(window) {
  var Workflow = window.Workflow;
  var Wire = Workflow.Wire = function(element, workflow) {
    var self = this;

    // Capture element
    this.element  = element;

    // Self-refer
    this.element.workflowWireObject = this;

    // Capture workflow reference
    this.workflow = workflow;

    // Set type
    this.type     = "wire";

    this.collisionChildren = [];

    if (this.isConnected()) {
      this.element.classList.remove("disconnected");

      this.element.addEventListener('click', Workflow.wireClickEvent);
    }
    else {
      this.element.classList.add("disconnected");
    }

    if (this.workflow.options.allowWireSelection) {
      this.attachButton = document.createElement("div");
      this.attachButton.classList.add('attach-button');
      this.element.appendChild(this.attachButton);
      this.positionAttachButton();

      this.attachButton.addEventListener("mousemove", function(event) {
        self.workflow.selectedWire = self;

        event.stopPropagation();
        event.preventDefault();
      });

      this.attachButton.addEventListener("mousedown", function(event) {
        event.stopPropagation();
        event.preventDefault();

        // Create a dummy disconnected wire here
        // And then set the selected wire to that dummy wire
        var dummyWire = self.element.cloneNode(false);
        dummyWire.removeAttribute("data-connected-to");
        self.element.parentNode.insertBefore(dummyWire, self.element);

        self.workflow.selectedWire = Wire.createFor([dummyWire], self.workflow)[0];
        self.workflow.selectedWire.disconnect();

        Workflow.wireDragEvent.call(self.workflow.element, event);
      });
    }

    if (this.element.classList.contains("hidden")) {
      this.hide();
    }
  };

  Wire.createFor = function(elements, workflow) {
    var ret = [];

    elements.forEach(function(element) {
      if (!element.workflowWireObject) {
        element.workflowWireObject = new Wire(element, workflow);
      }
      ret.push(element.workflowWireObject);
    });

    return ret;
  };

  /**
   * Moves the label to a position that does not collide with other
   * elements.
   *
   * There are three positions a label wants to be. The first, and most desired,
   * position is directly above the wire next to the Node. The second and third
   * are on either side of the midpoint of the wire.
   *
   * Wires have two labels. One describing the context of its output and one
   * describing the context of its input. We must also make sure that these
   * labels do not overlap in a poor way.
   */
  Wire.prototype.positionLabel = function() {
    var bounds = {};
    var self = this;
    var options = self.workflow.options;

    var tolerance = 5;

    var region = {};

    // First position: next to the node, above the wire.
    if (self.isOutput()) {
      region.left  = self.element.offsetLeft;

      bounds.left  = 0;
      bounds.right = "initial";
    }
    else {
      region.left  = self.element.offsetLeft + self.element.offsetWidth - self.label().offsetWidth;

      bounds.right = 0;
      bounds.left  = "initial";
    }

    if (self.isDown()) {
      bounds.top  = -self.label().offsetHeight - 5;
      bounds.bottom = "initial";

      region.top    = self.element.offsetTop + bounds.top - 1;
    }
    else {
      bounds.top = "initial";
      bounds.bottom = 4;

      region.top    = self.element.offsetTop + self.element.offsetHeight - bounds.bottom - self.label().offsetHeight - 1;
    }

    region.left += self.node().left();
    region.top  += self.node().top();

    region.right  = region.left + self.label().offsetWidth;
    region.bottom = region.top  + self.label().offsetHeight;

    region.left   += tolerance;
    region.top    += tolerance;
    region.right  -= tolerance;
    region.bottom -= tolerance;

    if (options.debug.showLabelCollisionBoxes) {
      var collisionBox = document.createElement("div");
      collisionBox.classList.add("debug");
      collisionBox.classList.add("quadtree");
      collisionBox.style.display = "block";
      collisionBox.style.left    = region.left + self.draggable.offsetLeft;
      collisionBox.style.top     = region.top +  self.draggable.offsetTop;
      collisionBox.style.width   = region.right  - region.left;
      collisionBox.style.height  = region.bottom - region.top;

      self.element.appendChild(collisionBox);
    }

    // Collision Check
    self.workflow.clearCollisionChecks();

    var ignore = null;
    if (!self.isConnected()) {
      ignore = self;
    }

    var objects = self.workflow.collisionCheck(ignore, region);

    if (objects.size == 0) {
      return;
    }
  };

  Wire.prototype.positionDeleteButton = function() {
    if (this.deleteButton) {
      var delta_y = 0;
      var delta_x = 0;

      if (this.isDown()) {
        delta_y = this.measuredHeight();
      }

      this.deleteButton.style.top  = (delta_y - 10) + "px";

      if (this.isOutput()) {
        this.deleteButton.style.left = -10 + "px";
      }
      else {
        this.deleteButton.style.left = "auto";
        this.deleteButton.style.right = -10 + "px";
      }
    }
  }

  Wire.prototype.positionAttachButton = function() {
    if (this.attachButton) {
      var delta_y = 0;
      var delta_x = 0;

      if (this.isDown()) {
        delta_y = this.measuredHeight();
      }

      this.attachButton.style.top  = (delta_y - 10) + "px";

      if (this.isOutput()) {
        this.attachButton.style.left = -10 + "px";
      }
      else {
        this.attachButton.style.left = "auto";
        this.attachButton.style.right = -10 + "px";
      }
    }
  }

  Wire.prototype.isDown = function() {
    if (!this._isDown) {
      this._isDown = this.element.classList.contains("down");
    }

    return this._isDown;
  };

  Wire.prototype.isLeft = function() {
    if (!this._isLeft) {
      this._isLeft = this.element.classList.contains("left");
    }

    return this._isLeft;
  };

  Wire.prototype.isBackward = function() {
    if (!this._isBackward) {
      this._isBackward = this.element.classList.contains("backward");
    }

    return this._isBackward;
  };

  Wire.prototype.isRight = function() {
    if (!this._isRight) {
      this._isRight = this.element.classList.contains("right");
    }

    return this._isRight;
  };

  Wire.prototype.isOutput = function() {
    if (!this._isOutput) {
      this._isOutput = this.element.classList.contains("output");
    }

    return this._isOutput;
  };

  Wire.prototype.recalculatePosition = function() {
    if (this.isHidden()) {
      return;
    }

    if (!this.isConnected()) {
      return;
    }

    if (this.isOutput()) {
      return;
    }

    var self = this;
    var subWire = this.connectedWire();

    var currentX = this.node().left();
    var currentY = this.top();

    var currentX2 = subWire.node().left() + subWire.node().width();
    var currentY2 = subWire.top();

    var midY = this.node().top() + this.node().height() + (subWire.node().top() - (this.node().top() + this.node().height())) / 2;

    var wireLabel = subWire.label();
    var wireWidth = 50;
    if (wireLabel) {
      wireWidth = wireLabel.offsetWidth + 15;
    }

    var options = self.workflow.options;
    var subPath = createWire(currentX2, currentY2, currentX, currentY, wireWidth, midY, options);
    var selected = false;
    if (self.subPath) {
      selected = self.subPath.classList.contains("selected");
      self.subPath.remove();
    }
    if (selected) {
      subPath.classList.add("selected");
    }
    self.subPath = subPath;
    subPath.workflowWireObject = self;
    subPath.addEventListener('click', Workflow.wireClickEvent);
    if (self.element.classList.contains("hidden")) {
      subPath.setAttribute("hidden", true);
    }
    self.workflow.svgPlane.appendChild(subPath);

    subWire._verticalBounds   = undefined;
    subWire._horizontalBounds = undefined;
    this._verticalBounds   = undefined;
    this._horizontalBounds = undefined;

    //this.workflow.addCollisionCheck(subWire, subWire.verticalBounds(), [subWire, this, this.node()]);
    //this.workflow.addCollisionCheck(subWire, subWire.horizontalBounds(), [subWire, this, this.node()]);
    //this.workflow.addCollisionCheck(this, this.verticalBounds(), [subWire, this, this.node()]);
    //this.workflow.addCollisionCheck(this, this.horizontalBounds(), [subWire, this, this.node()]);
  };

  Wire.prototype.reposition = function(noRecurse) {
    this._logicalTop = undefined;
    this._top        = undefined;
    this._position   = undefined;

    this.element.style.top = this.logicalTop() + "px";

    if (!noRecurse && this.connectedWire()) {
      this.connectedWire().reposition(true);
      if (!this.isOutput()) {
        this.recalculatePosition();
      }
    }
  };

  Wire.prototype.is = function(wire) {
    return this.element === wire.element;
  };

  Wire.prototype.label = function() {
    if (!this._label) {
      var index = this.index();
      var query = 'li[data-index="' + index + '"] > .label';
      this._label = this.element.parentNode.querySelector(query);
    }

    return this._label;
  };

  Wire.prototype.nameLabel = function() {
    return this.label().querySelector('.name').textContent;
  };

  Wire.prototype.typeLabel = function() {
    return this.label().querySelector('.type').textContent;
  };

  Wire.prototype.node = function() {
    if (!this._node) {
      this._node = Workflow.Node.createFor(this.element.parentNode.parentNode, this.workflow);
    }

    return this._node;
  };

  Wire.prototype.disconnect = function() {
    if (!this.isConnected()) {
      return;
    }

    // There seems to be a problem with uninitialized connected wire
    this.connectedWire().connectedWire();
    var connected = this.connectedWire();

    // We have to update our data-item-index, and update all connected wires
    var currentItemIndex = this.itemIndex();

    // Go through each of our siblings and update their indices
    this.shared().forEach(function(wire) {
      if (wire.itemIndex() > currentItemIndex) {
        var o_wire = wire.connectedWire();

        wire.element.setAttribute('data-item-index', wire.itemIndex() - 1);
        wire._itemIndex = undefined;

        // Update the connection at the other end of the wire
        if (o_wire) {
          var connectedTo = o_wire.element.getAttribute('data-connected-to');
          if (connectedTo) {
            var nodeIndex = parseInt(connectedTo.split('-')[0]);
            var wireIndex = parseInt(connectedTo.split('-')[1]);
            var itemIndex = parseInt(connectedTo.split('-')[2]);

            o_wire.element.setAttribute('data-connected-to', nodeIndex + '-' + wireIndex + '-' + wire.itemIndex());
          }
        }
      }
    });

    this.element.removeAttribute('data-connected-to');
    this.element.removeAttribute('data-item-index');
    this.element.classList.remove("selected");
    this.element.removeEventListener("click", Workflow.wireClickEvent)

    if (this.subPath) {
      this.subPath.remove();
      this.subPath = null;
    }

    if (this.deleteButton) {
      this.deleteButton.remove();
      this.deleteButton=undefined
    }

    connected.disconnect();

    this._connectedNode = undefined;
    this._connectedWire = undefined;
    this._isDown = undefined;

    // If this wire is the only connection at this pin,
    // we collapse the pin and retain the "wire" as a
    // disconnected wire.
    if (this.shared().length == 1) {
      this.reposition();
      this.collapse();
    }
    else {
      // Otherwise we will remove the wire completely
      // and let the existing wires take over.

      // Ensure the label stays on one of the wires.
      var label = this.element.querySelector(".label");
      if (label) {
        var newLeader = this.element.parentNode.querySelector('li[data-index="' + this.index() + '"][data-item-index="0"]');
        newLeader.appendChild(label);
      }

      // Invalidate the node
      this.node()._wires = undefined;
      this.node().outputWires = undefined;
      this.node().inputWires = undefined;
      this.workflow._wires = undefined;

      // Destroy the wire
      this.element.remove();
    }
  };

  Wire.prototype.isConnected = function() {
    return this.element.getAttribute('data-connected-to') !== null;
  };

  /* This function returns a list of Wire objects that belong to the same logical
   * wire. That is, a single wire point can have multiple inputs/outputs that lead
   * into it. This will give all Wire objects share this Wire's data index. Will
   * include ourselves.
   */
  Wire.prototype.shared = function() {
    var self = this;

    var siblings = this.element.parentNode.querySelectorAll('li[data-index="' + this.index() + '"]');

    return Wire.createFor(siblings, this.workflow);
  };

  Wire.prototype.siblings = function() {
    if (!this._siblings) {
      this._siblings = Wire.createFor(this.element.parentNode.querySelectorAll("li"), this.workflow);
    }

    return this._siblings;
  };

  Wire.prototype.logicalIndex = function() {
    return window.getChildIndex(this.element);
  };

  Wire.prototype.itemIndex = function() {
    if (!this._itemIndex) {
      this._itemIndex = parseInt(this.element.getAttribute("data-item-index") || "0");
    }

    return this._itemIndex;
  };

  Wire.prototype.nextItemIndex = function() {
    if (!this.isConnected()) {
      return 0;
    }

    return this.shared().length;
  };

  Wire.prototype.index = function() {
    if (!this._index) {
      this._index = parseInt(this.element.getAttribute("data-index"));
    }

    return this._index;
  };

  Wire.prototype.shownIndex = function() {
    var self = this;

    var wireCount = 0;
    var last = -1;

    // Determine how many visible wires exist along this node
    this.element.parentNode.querySelectorAll("li").forEach(function(wire) {
      var index = parseInt(wire.getAttribute("data-index"));
      if (wire.workflowWireObject == self) {
        return wireCount;
      }
      if (index > last) {
        if (!wire.classList.contains("hidden")) {
          last = index;
          wireCount++;
        }
      }
    });
  };

  Wire.prototype.position = function() {
    var self = this;

    if (!this._position) {
      var node = this.node();
      var nodeHeight = node.height();

      var wireCount = 0;
      var last = -1;

      var shownIndex = -1;

      // Determine how many visible wires exist along this node
      this.element.parentNode.querySelectorAll("li").forEach(function(wire) {
        var index = parseInt(wire.getAttribute("data-index"));
        if (index > last) {
          if (index == self.index()) {
            shownIndex = wireCount;
          }
          if (!wire.classList.contains("hidden")) {
            last = index;
            wireCount++;
          }
        }
      });
      var gapHeight = Math.ceil(nodeHeight / (wireCount+1));

      var wireY = gapHeight * (shownIndex + 1);

      // Special cases for smaller numbers
      if (wireCount == 2) {
        wireY = 30;
        if (shownIndex == 1) {
          wireY = 70;
        }
      }
      else if (wireCount == 3) {
        wireY = 15 + shownIndex * 35;
      }

      this._position = wireY;
    }

    return this._position;
  };

  Wire.prototype.measuredHeight = function() {
    if (!this._measuredHeight) {
      this._measuredHeight = this.element.offsetHeight;
    }

    return this._measuredHeight;
  };

  Wire.prototype.measuredWidth = function() {
    if (!this._measuredWidth) {
      this._measuredWidth = this.element.offsetWidth;
    }

    return this._measuredWidth;
  };

  /* Determines the Y coordinate relative to the canvas for the
   * given wire.
   */
  Wire.prototype.top = function(wire) {
    return this.position() + this.node().top();
  };

  /* Determines the Y coordinate for the given wire
   */
  Wire.prototype.logicalTop = function() {
    if (!this._logicalTop) {
      var wireY = this.position();
      this._logicalTop = wireY;
    }

    return this._logicalTop;
  };

  Wire.prototype.disconnectedBounds = function() {
    if (!this._bounds || !this._boundsIndex || this.node().boundsIndex != this._boundsIndex) {
      var wireBound = {};

      var node = this.node();
      var wireLabel = this.label();

      var wireWidth = wireLabel.offsetWidth + 15;

      if (this.isOutput()) {
        wireBound = {
          left:   this.element.offsetLeft + this.node().left(),
          right:  this.element.offsetLeft + this.node().left() + wireWidth + 30,
          top:    this.element.offsetTop  + this.node().top() + this.element.offsetHeight - 10,
          bottom: this.element.offsetTop  + this.node().top() + this.element.offsetHeight + 10
        };
      }
      else {
        wireBound = {
          left:   this.element.offsetLeft + this.node().left() + this.element.offsetWidth - wireWidth - 30,
          right:  this.element.offsetLeft + this.node().left() + this.element.offsetWidth,
          top:    this.element.offsetTop  + this.node().top()  + this.element.offsetHeight - 10,
          bottom: this.element.offsetTop  + this.node().top()  + this.element.offsetHeight + 10
        };
      }

      this._bounds = wireBound;
      this._boundsIndex = this.node().boundsIndex;
    }

    return this._bounds;
  };

  Wire.prototype.horizontalBounds = function() {
    if (!this._horizontalBounds) {
      this._horizontalBounds = {};
      if (this.element.classList.contains("up")) {
        this._horizontalBounds.left   = this.element.offsetLeft + this.node().left();
        this._horizontalBounds.top    = this.element.offsetTop  + this.node().top() + this.element.offsetHeight - 10;
        this._horizontalBounds.right  = this._horizontalBounds.left + this.element.offsetWidth;
        this._horizontalBounds.bottom = this._horizontalBounds.top + 20;
      }
      else if (this.element.classList.contains("down")) {
        this._horizontalBounds.left   = this.element.offsetLeft + this.node().left();
        this._horizontalBounds.top    = this.element.offsetTop  + this.node().top() - 10;
        this._horizontalBounds.right  = this._horizontalBounds.left + this.element.offsetWidth;
        this._horizontalBounds.bottom = this._horizontalBounds.top + 20;
      }
      else {
        this._horizontalBounds = this.verticalBounds();
      }
    }

    return this._horizontalBounds;
  };

  Wire.prototype.verticalBounds = function() {
    if (!this._verticalBounds) {
      this._verticalBounds = {};
      if (this.element.classList.contains("output")) {
        this._verticalBounds.left   = this.element.offsetLeft + this.node().left() + this.element.offsetWidth - 10;
        this._verticalBounds.top    = this.element.offsetTop + this.node().top() - 10;
        this._verticalBounds.right  = this._verticalBounds.left + 20;
        this._verticalBounds.bottom = this._verticalBounds.top + this.element.offsetHeight + 20;
      }
      else if (this.element.classList.contains("input")) {
        this._verticalBounds.left   = this.element.offsetLeft + this.node().left() - 10;
        this._verticalBounds.top    = this.element.offsetTop + this.node().top() - 10;
        this._verticalBounds.right  = this._verticalBounds.left + 20;
        this._verticalBounds.bottom = this._verticalBounds.top + this.element.offsetHeight + 20;
      }
    }

    return this._verticalBounds;
  };

  Wire.prototype.collapse = function() {
    var self = this;
    var options = self.workflow.options;
    this.element.classList.add("disconnected");
    this.element.classList.remove("down");
    this.element.classList.remove("up");
    this.element.classList.remove("left");
    this.element.classList.remove("right");
    this._isDown  = false;
    this._isLeft  = false;
    this._isRight = false;

    this.element.style.height = 0;

    var node      = self.node();
    var wireLabel = self.label();

    var wireWidth = wireLabel.offsetWidth + 15;

    // TODO: make all disconnected inputs the same width when possible
    // taking into account labels that don't fit.

    // Get the natural wire bounds (if the wire were as long as it wanted)
    var wireBound = self.disconnectedBounds();

    // Find collisions
    var collisions = self.workflow.queryQuadtree(wireBound);

    if (options.debug.showCollisionBoxes) {
      //$(".debug.collision").remove();
    }

    var collided = null;

    collisions.forEach(function(possibleCollisionObject) {
      if (possibleCollisionObject.type == "node") {
        var subNode = possibleCollisionObject.object;
        if (!subNode.is(node)) {
          var spacing = wireWidth;
          var subNodeBound = subNode.bounds();

          if (rectIntersects(subNodeBound, wireBound)) {
            if (self.isOutput()) {
              spacing = subNode.element.offsetLeft - (node.element.offsetLeft + node.width()) - 30;
            }
            else {
              spacing = node.element.offsetLeft - (subNode.element.offsetLeft + subNode.width()) - 30;
            }

            if (spacing < wireWidth) {
              wireWidth = spacing;
              collided = subNode;
            }
          }
        }
      }
      else if (possibleCollisionObject.type == "wire") {
        var subWire = possibleCollisionObject.object;
        if (!subWire.is(self)) {
          // Check for collisions
          var subWireBound = subWire.verticalBounds();
          var spacing = wireWidth;

          if (rectIntersects(subWireBound, wireBound)) {
            if (options.debug.showCollisionBoxes) {
              var collisionBox = $("<div class='debug collision'></div>");
              collisionBox.css({
                display: "block",
                left: subWireBound.left + node.workflow.draggable.offsetLeft,
                top:  subWireBound.top  + node.workflow.draggable.offsetTop,
                width: subWireBound.right - subWireBound.left,
                height: subWireBound.bottom - subWireBound.top
              });
              //node.workflow.element.append(collisionBox);
            }
            var spacing = node.element.offsetLeft - subWire.element.offsetLeft - 30;
            if (subWire.isOutput()) {
              // TODO: this is wrong, isn't it? future me will know.
              spacing = node.element.offsetLeft - (subWire.element.offsetLeft + subWire.element.offsetWidth) - 30;
            }
            if (spacing < wireWidth) {
              wireWidth = spacing;
              collided = subWire;
            }
          }

          subWireBound = subWire.horizontalBounds();
          if (rectIntersects(subWireBound, wireBound)) {
            if (subWire.element.classList.contains("output")) {
              spacing = node.element.offsetLeft - (subWire.element.offsetLeft + subWire.element.offsetWidth) - 30;
            }
            else if (subWire.element.classList.contains("input")) {
              spacing = node.element.offsetLeft - (subWire.element.offsetLeft) - 30;
            }

            if (options.debug.showCollisionBoxes) {
              var collisionBox = document.createElement("div");
              collisionBox.classList.add("debug");
              collisionBox.classList.add("quadtree");
              collisionBox.style.display = "block";
              collisionBox.style.left    = subWireBound.left + node.workflow.draggable.offsetLeft;
              collisionBox.style.top     = subWireBound.top + node.workflow.draggable.offsetTop;
              collisionBox.style.width   = subWireBound.right - subWireBound.left;
              collisionBox.style.height  = subWireBound.bottom - subWireBound.top;

              //node.workflow.element.appendChild(collisionBox);
            }

            if (spacing < wireWidth) {
              wireWidth = spacing;
              collided = subWire;
            }
          }
        }
      }
    });

    // Check if line labels should be visible
    if (self.label().offsetWidth > wireWidth) {
      self.element.classList.remove("shown");
    }
    else {
      self.element.classList.add("shown");
    }

    self._measuredWidth    = undefined;
    self._horizontalBounds = undefined;
    self._verticalBounds   = undefined;

    if (collided) {
      collided.collisionChildren.push(self);
    }

    self.element.style.width = wireWidth + "px";
  };

  Wire.prototype.show = function(showNode) {
    this.element.removeAttribute("hidden");
    if (this.subPath) {
      this.subPath.removeAttribute("hidden");
    }
    this.element.classList.remove("hidden");

    if (this.connectedWire() && this.connectedWire().isHidden()) {
      this.connectedWire().show(true);
    }

    if (!showNode && this.connectedNode()) {
      this.connectedNode().show();
    }

    var wires = this.node().inputs();

    if (this.isOutput()) {
      wires = this.node().outputs();
    }

    wires.forEach(function(wire) {
      wire.reposition();
    });

    // Reconsider the node's input button
    if (this.isOutput() && this.node().hiddenOutputCount() == 0) {
      this.node().outputAddButton.setAttribute("hidden", true);
    }
    if (!this.isOutput() && this.node().hiddenInputCount() == 0) {
      this.node().inputAddButton.setAttribute("hidden", true);
    }
  };

  Wire.prototype.isHidden = function() {
    return this.element.classList.contains("hidden");
  };

  Wire.prototype.hide = function(showNode) {
    this.element.setAttribute("hidden", true);
    if (this.subPath) {
      this.subPath.setAttribute("hidden", true);
    }
    this.element.classList.add("hidden");

    if (this.connectedWire() && !this.connectedWire().isHidden()) {
      this.connectedWire().hide(true);
    }

    if (!showNode && this.connectedNode()) {
      this.connectedNode().hide();
    }
  };

  Wire.prototype.connectedNode = function() {
    if (!this._connectedNode) {
      var connectedTo = this.element.getAttribute('data-connected-to');
      if (connectedTo) {
        var nodeIndex = parseInt(connectedTo.split('-')[0]);
        var wireIndex = parseInt(connectedTo.split('-')[1]);
        var itemIndex = parseInt(connectedTo.split('-')[2]);

        var nodeElement = this.workflow.element.querySelector(':scope > ul.connections li.connection[data-index="' + nodeIndex + '"]');
        var wireElement = null;
        if (this.element.classList.contains("input")) {
          wireElement = nodeElement.querySelector('li.output[data-index="' + wireIndex + '"][data-connected-to="' + this.node().index() + '-' + this.index() + '-' + this.itemIndex() + '"][data-item-index="' + itemIndex + '"]');
        }
        else {
          wireElement = nodeElement.querySelector('li.input[data-index="' + wireIndex + '"][data-connected-to="' + this.node().index() + '-' + this.index() + '-' + this.itemIndex() + '"][data-item-index="' + itemIndex + '"]');
        }

        if (wireElement === null) {
          this._connectedNode = null;
          this._connectedWire = null;
          return this._connectedNode;
        }

        this._connectedNode = Workflow.Node.createFor(nodeElement, this.workflow);
        this._connectedWire = Workflow.Wire.createFor([wireElement], this.workflow)[0];
      }
      else {
        this._connectedNode = null;
        this._connectedWire = null;
      }
    }

    return this._connectedNode;
  };

  Wire.prototype.connectedWire = function() {
    if (!this._connectedWire) {
      // This will get the wire as well
      this.connectedNode()
    }

    return this._connectedWire;
  };

  var createBentWire = function(x1, y1, x2, y2, width, midY, options) {
    var arcRadius = options.wire.arcRadius;

    var path = document.createElementNS(Workflow.SVGNS, "path");

    var horizontalThickness = options.wire.horizontalThickness;
    var verticalThickness = options.wire.verticalThickness;

    var direction = 1;
    var sweep = 0;
    if (y1 < y2) {
      direction = -1;
      sweep = 1;
    }

    var reverseSweep = Math.abs(sweep-1);

    // Start at (x1, y1)
    var command = "M " + x1 + " " + y1;

    // Go along "width" for the main part of the wire
    command += " L " + (x1 + width) + " " + y1 + " ";

    var midArcRadius = Math.min(Math.abs((x1+width+arcRadius) - (x2 - arcRadius)) / 2, options.wire.arcRadius);

    // Arc toward midY
    command += " A " + arcRadius + " " + arcRadius + " 0 0 " + sweep + " " + (x1 + width + arcRadius) + " " + (y1 - (arcRadius * direction));
    command += " L " + (x1 + width + arcRadius) + " " + (midY + (midArcRadius * direction));
    command += " A " + midArcRadius + " " + midArcRadius + " 0 0 " + sweep + " " + (x1 + width + arcRadius - midArcRadius) + " " + midY;

    // Go to (x2, midY)
    if (midArcRadius == arcRadius) {
      command += " L " + x2 + " " + midY;
    }

    // Arc into y2
    command += " A " + midArcRadius + " " + midArcRadius + " 0 0 " + reverseSweep + " " + (x2 - arcRadius) + " " + (midY - (midArcRadius * direction));
    command += " L " + (x2 - arcRadius) + " " + (y2 + (arcRadius * direction));
    command += " A " + arcRadius + " " + arcRadius + " 0 0 " + reverseSweep + " " + x2 + " " + y2;

    // Going back
    command += " L " + x2 + " " + (y2 + (horizontalThickness * direction));

    // Arc into midY
    command += " A " + arcRadius + " " + arcRadius + " 0 0 " + sweep + " " + (x2 - arcRadius + verticalThickness) + " " + (y2 + (arcRadius + horizontalThickness) * direction);
    command += " L " + (x2 - arcRadius + verticalThickness) + " " + (midY - (midArcRadius - horizontalThickness) * direction);
    command += " A " + midArcRadius + " " + midArcRadius + " 0 0 " + sweep + " " + (x2 - arcRadius + midArcRadius + verticalThickness) + " " + (midY - (horizontalThickness * direction));

    // Go back along midY

    if (midArcRadius == arcRadius) {
      command += " L " + (x1 + width + verticalThickness) + " " + (midY - (horizontalThickness * direction));
    }

    // Arc to y1 from midY
    command += " A " + midArcRadius + " " + midArcRadius + " 0 0 " + reverseSweep + " " + (x1 + width + arcRadius + verticalThickness) + " " + (midY + (midArcRadius + horizontalThickness) * direction);
    command += " L " + (x1 + width + arcRadius + verticalThickness) + " " + (y1 - (arcRadius + horizontalThickness) * direction);
    command += " A " + arcRadius + " " + arcRadius + " 0 0 " + reverseSweep + " " + (x1 + width + verticalThickness) + " " + (y1 + (horizontalThickness * direction));

    // Line to (x1,y1)
    command += " L " + (x1) + " " + (y1 + (horizontalThickness * direction));

    //*/

    path.setAttribute("d", command);

    return path;
  };

  var createNaturalWire = function(x1, y1, x2, y2, width, options) {
    var arcRadius = Math.min(Math.abs(y1 - y2) / 2, options.wire.arcRadius);

    var horizontalThickness = options.wire.horizontalThickness;
    var verticalThickness = options.wire.verticalThickness;

    var path = document.createElementNS(Workflow.SVGNS, "path");

    var direction = 1;
    var sweep = 0;
    if (y1 < y2) {
      direction = -1;
      sweep = 1;
    }

    var reverseSweep = Math.abs(sweep-1);

    // Start at (x1, y1)
    var command = "M " + x1 + " " + y1;

    var diffRadius = options.wire.arcRadius - arcRadius;

    // Go along "width" for the main part of the wire
    command += " L " + (x1 + width + diffRadius) + " " + y1 + " ";

    // Maybe arc to go from y1 to y2
    if (y1 != y2) {
      command += " A " + arcRadius + " " + arcRadius + " 0 0 " + sweep + " " + (x1 + width + options.wire.arcRadius) + " " + (y1 - arcRadius * direction);
      command += " L " + (x1 + width + options.wire.arcRadius) + " " + (y2 + arcRadius * direction);
      command += " A " + arcRadius + " " + arcRadius + " 0 0 " + reverseSweep + " " + (x1 + width + options.wire.arcRadius + arcRadius) + " " + y2;
    }

    // Go to (x2, y2)
    command += " L " + x2 + " " + y2;

    // Coming back... right wire edge
    command += " L " + x2 + " " + (y2+horizontalThickness*direction);

    // Maybe arc to go from y2 to y1
    if (y1 != y2) {
      // Go back to midpoint
      command += " L " + (x1 + width + options.wire.arcRadius + arcRadius + verticalThickness) + " " + (y2+horizontalThickness*direction);

      command += " A " + arcRadius + " " + arcRadius + " 0 0 " + sweep + " " + (x1 + width + options.wire.arcRadius + verticalThickness) + " " + (y2 + (arcRadius - horizontalThickness) * direction);
      command += " L " + (x1 + width + options.wire.arcRadius + verticalThickness) + " " + (y1 - arcRadius * direction);
      command += " A " + arcRadius + " " + arcRadius + " 0 0 " + reverseSweep + " " + (x1 + width + diffRadius + verticalThickness) + " " + (y1 + horizontalThickness*direction);
    }

    // Left edge
    command += " L " + (x1) + " " + (y1 + horizontalThickness*direction);
    path.setAttribute("d", command);

    return path;
  };

  var createWire = function(x1, y1, x2, y2, width, midY, options) {
    var path = null;
    if (x2 - width - options.wire.arcRadius*2 < x1) {
      path = createBentWire(x1, y1, x2, y2, width, midY, options);
    }
    else {
      path = createNaturalWire(x1, y1, x2, y2, width, options);
    }

    return path;
  };
};
