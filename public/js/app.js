/*
 * This is the main module for OCCAM's front end infrastructure. This file will
 * initialize the different modules and initiate some interactive elements on
 * the page.
 */

// Sprockets Metadata
//= require ./workflow-widget/job_donut.js
//= require ./workflow-widget/quadtree.js
//= require ./workflow-widget/node.js
//= require ./workflow-widget/wire.js
//= require ./workflow-widget/workflow.js
//= require_tree ./occam

// The main OCCAM container
var Occam = window.Occam = {};

// Flags
window.Occam.DEBUG = true;

window.Occam.Util = {};

function getParents(el, parentSelector /* optional */, filter /* optional */) {
  // If no parentSelector defined will bubble up all the way to *document*
  if (parentSelector === undefined) {
    parentSelector = document;
  }

  if (filter === undefined) {
    filter = false;
  }

  var parents = [];
  var p = el.parentNode;

  while (p && p.matches && !(p.matches(parentSelector))) {
    var o = p;
    if (!filter) {
      parents.unshift(o);
    }
    p = o.parentNode;
  }

  if (p.matches) {
    parents.unshift(p);
  }

  return parents;
}

function getChildIndex(el, filter) {
  if (filter) {
    return (Array.prototype.indexOf.call(el.parentNode.querySelectorAll(":scope > " + filter), el));
  }

  return (Array.prototype.indexOf.call(el.parentNode.children, el));
}

function getLastChild(el) {
  return (el.children[el.children.length - 1]);
}

function submitForm(form, data_or_callback, callback) {
  var data = data_or_callback;
  if (callback === undefined) {
    callback = data_or_callback;
    data = undefined;
  }

  var formdata = new FormData(form);

  if (data) {
    data.forEach(function(tuple) {
      formdata.set(tuple[0], tuple[1]);
    });
  }

  var method = form.getAttribute("method") || "GET";
  var url    = form.getAttribute("action");

  if (method.toUpperCase() == "GET") {
    let params = [...formdata.entries()].map(e => encodeURIComponent(e[0]) + "=" + encodeURIComponent(e[1]));
    url = url + "?" + params.join("&");
  }

  return window.Occam.Util.ajax(method, url, formdata, callback);
}

window.Occam.Util.submitForm = submitForm;

window.Occam.Util.ajax = function(method, url, data, callback, responseType) {
  var oReq = new XMLHttpRequest();

  // This ensures that the browser doesn't cache a partial ajax request as the
  // actual page. If you don't do this, the browser may respond to a back button
  // or page reload with the response of the ajax request if the URL is the
  // same. Kinda baffling it doesn't cache XML Request stuff separate really!!
  if (method == "GET") {
    if (url.includes("?")) {
      url = url + "&_ajax";
    }
    else {
      url = url + "?_ajax";
    }
  }

  if (callback) {
    if (callback.onprogress) {
      oReq.addEventListener("progress", function(event) {
        callback.onprogress(event);
      });
    }
    oReq.addEventListener("load", function(event) {
      if (oReq.readyState == 4 && oReq.status == 200) {
        var response = oReq.responseText;

        if (responseType == "application/json" || responseType == "json") {
          // TODO: handle errors
          response = JSON.parse(response);
        }

        if (callback.onload) {
          callback.onload(response);
        }
        else {
          callback(response);
        }
      }
    });
  }

  oReq.open(method, url);
  oReq.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

  if (responseType == "json" || responseType == "application/json") {
    oReq.setRequestHeader('Accept', 'application/json');
  }
  else if (responseType) {
    oReq.setRequestHeader('Accept', responseType);
  }

  if (data) {
    if ((typeof data) == "string") {
      oReq.setRequestHeader('Content-Type', 'application/octet-stream');
    }
    oReq.send(data);
  }
  else {
    oReq.send();
  }

  return oReq;
};

window.Occam.Util.post = function(url, data, callback, responseType) {
  var postData = undefined;

  if (data) {
    if ((typeof data) == "string") {
      postData = data;
    }
    else {
      postData = new FormData();
      Object.keys(data).forEach(function(key) {
        postData.set(key, data[key]);
      });
    }
  }

  return window.Occam.Util.ajax("POST", url, postData, callback, responseType);
}

window.Occam.Util.get = function(url, callback, responseType, options) {
  if (options) {
    let params = Object.entries(options).map(e => encodeURIComponent(e[0]) + "=" + encodeURIComponent(e[1]));
    url = url + "?" + params.join("&");
  }

  return window.Occam.Util.ajax("GET", url, null, callback, responseType);
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
  results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function copyToClipboard(text) {
  if (window.clipboardData && window.clipboardData.setData) {
    // IE specific code path to prevent textarea being shown while dialog is visible.
    return clipboardData.setData("Text", text); 

  } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
    var textarea = document.createElement("textarea");
    textarea.textContent = text;
    textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
    document.body.appendChild(textarea);
    textarea.select();
    try {
      return document.execCommand("copy");  // Security exception may be thrown by some browsers.
    } catch (ex) {
      console.warn("Copy to clipboard failed.", ex);
      return false;
    } finally {
      document.body.removeChild(textarea);
    }
  }
}

var copyActions = document.querySelectorAll("a.copy");

copyActions.forEach(function(element) {
  element.removeAttribute("hidden");

  // When we click on this, add the contents of the "data-value" to the clipboard
  element.addEventListener("click", function(event) {
    event.preventDefault();
    event.stopPropagation();
    copyToClipboard(window.location.protocol + "//" + window.location.host + element.getAttribute("data-url"));
  });
});

/* Permissions Pane */

/* Object Header */
var header = document.querySelector(".content > h1");

// Bookmarks
if (header) {
  var bookmarkToggle = header.querySelector("li.bookmark");
  if (bookmarkToggle) {
    console.log("bookmark found");
    var bookmarkDelete = header.querySelector("li.bookmark_delete");

    bookmarkToggle.querySelector("input[type=\"submit\"]").addEventListener("click", function(event) {
      event.preventDefault();

      var form = this.parentNode;

      Occam.Util.submitForm(form);

      bookmarkToggle.setAttribute("hidden", "");
      bookmarkDelete.removeAttribute("hidden");
    });

    bookmarkDelete.querySelector("input[type=\"submit\"]").addEventListener("click", function(event) {
      event.preventDefault();

      var form = this.parentNode;

      Occam.Util.submitForm(form);

      bookmarkDelete.setAttribute("hidden", "");
      bookmarkToggle.removeAttribute("hidden");
    });
  }
}

// Autocomplete for experiment tags
$(function(){
  var tag_cache = {};
  $('input.tagged').tagit({
    allowSpaces: true,
    singleField: true,
    singleFieldDelimiter: ';'
  });
  $('input.tagged.autocomplete').tagit({
    singleField: true,
    singleFieldDelimiter: ';',
    autocomplete: {
      delay: 0,
      minLength: 2,
      source: function(request, response) {
        var term = request.term;
        if (term in tag_cache) {
          response(tag_cache[term]);
          return;
        }

        $.getJSON($('input.tagged.autocomplete').data("source"), {term: term}, function(data, status, xhr) {
          tag_cache[term] = data;
          response(data);
        });
      },
    }
  });
  $('#search').searchlight('/search', {
    showIcons: false,
    align: 'left'
  });

  $('.configuration-group > li > .recipe > select').on('change', function(e) {
    var selected = $(this).children(':selected');
    var ul_group = $(this).parent().parent().children('ul.configuration-group');
    var expand = $(this).parent().parent().children('h2').children('span.expand');
    var nesting = ul_group.data('nesting');

    // Expand group
    if (!expand.hasClass('shown')) {
      expand.trigger('click');
    }

    // Ask for the config options
    jQuery.getJSON(selected.data('template'), function(data, status, xhr) {
      // Set all input fields
      $.each(data, function(key, value) {
        code = $.base64.encode(key);
        // Replace + / with - _
        code = code.replace('+', '-').replace('/', '_');
        // Remove padding
        while (code.charAt(code.length-1) == '=') {
          code = code.slice(0, code.length-1);
        }
        code = nesting + '[' + code + ']';

        $('*[name="'+code+'"]').each(function(e) {
          $(this).val(value);
        });
      });
    });
  });
});

// From https://stackoverflow.com/questions/36721830/convert-hsl-to-rgb-and-hex

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   {number}  h       The hue
 * @param   {number}  s       The saturation
 * @param   {number}  l       The lightness
 * @return  {Array}           The RGB representation
 */
function hslToRgb(h, s, l){
    var r, g, b;

    if(s == 0){
        r = g = b = l; // achromatic
    }else{
        var hue2rgb = function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

initOccamNavigationState(Occam);
initOccamCard(Occam);
initOccamSearch(Occam);
initOccamThemeManager(Occam);
initOccamStorage(Occam);
initOccamObject(Occam);
initOccamSocial(Occam);
initOccamHelp(Occam);
initOccamPermissions(Occam);
initOccamModal(Occam);
initOccamTooltip(Occam);
initOccamMarkdown(Occam);
initOccamObjectTree(Occam);
initOccamDropdown(Occam);
initOccamSelector(Occam);
initOccamDirectory(Occam);
initOccamFileList(Occam);
initOccamAutoComplete(Occam);
initOccamValidator(Occam);
initOccamTabs(Occam);
initOccamConfiguration(Occam);
initOccamConfigurationPanel(Occam);
initOccamConfigurator(Occam);
initOccamDataViewer(Occam);
initOccamJob(Occam);
initOccamTask(Occam);
initOccamRunList(Occam);
initOccamRunTopbar(Occam);
initOccamRunner(Occam);
initOccamVT100(Occam);
initOccamWebSocket(Occam);
initOccamTerminal(Occam);
initOccamImporter(Occam);
initOccamPaper(Occam);
initOccamWorkflow(Occam);
initOccamGallery(Occam);

Occam.object = new Occam.Object();

Occam.loadAll = function(element) {
  // Apply scrollbars
  var containers = element.querySelectorAll(".perfect-scrollbar");
  containers.forEach(function(container) {
    new PerfectScrollbar(container);
  });

  Occam.Card.loadAll(element);
  Occam.Search.loadAll(element);
  Occam.FileList.loadAll(element);
  Occam.AutoComplete.loadAll(element);
  Occam.Selector.loadAll(element);
  Occam.Configuration.loadAll(element);
  Occam.ConfigurationPanel.loadAll(element);
  Occam.Configurator.loadAll(element);
  Occam.Directory.loadAll(element);
  Occam.Dropdown.loadAll(element);
  Occam.Help.loadAll(element);
  Occam.Modal.loadAll(element);
  Occam.ObjectTree.loadAll(element);
  Occam.Permissions.loadAll(element);
  Occam.Runner.loadAll(element);
  Occam.Social.loadAll(element);
  Occam.Storage.loadAll(element);
  Occam.Tabs.loadAll(element);
  Occam.ThemeManager.loadAll(element);
  Occam.Tooltip.loadAll(element);
  Occam.Markdown.loadAll(element);
  Occam.Workflow.loadAll(element);
  Occam.Gallery.loadAll(element);
};

Occam.loadAll(window.document.body);
