require 'puma'

require_relative "../lib/config"
require_relative "../lib/https"

puts "Running server..."

config = Occam::Config.configuration

host = config["host"] || "0.0.0.0"
port = config["port"] || 9292

Occam::HTTPS.ensureKey

pwd = Dir.pwd
bind "tcp://#{host}:#{port}?key=#{pwd}/key/server.key&cert=#{pwd}/key/server.crt"
