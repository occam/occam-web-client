#!/usr/bin/env rake

if ARGV[0] && ARGV[0].start_with?('test')
  ENV['RACK_ENV'] = "test"
end

require 'rake/testtask'
require_relative './config/environment'

Rake::TestTask.new do |t|
  t.pattern = "spec/**/*_spec.rb"
end

namespace :test do
  desc "Run all tests (rake test will do this be default)"
  task :all do
    Rake::TestTask.new("all") do |t|
      t.pattern = "spec/**/*_spec.rb"
    end
    task("all").execute
  end

  desc "Run all unit tests"
  task :units do
    test_task = Rake::TestTask.new("unittests") do |t|
      t.pattern = "spec/[!a]*/**/*_spec.rb"
    end
    task("unittests").execute
  end

  desc "Run single helper"
  task :helper, :file, :filter do |task, args|
    test_task = Rake::TestTask.new("unittests") do |t|
      if args.file
        file = "spec/helpers/#{args.file}_spec.rb"
        t.pattern = file
        puts "Testing #{file}"
        if args.filter
          puts "Filters tests matching '#{args.filter}'"
          filter = args.filter
          if not filter.start_with? "/"
            filter = "/::#{filter}/"
          end
          t.options = "--name=#{filter}"
        end
      else
        t.pattern = "spec/helpers/*_spec.rb"
      end
    end
    task("unittests").execute
  end

  desc "Run single model"
  task :model, :file, :filter do |task, args|
    test_task = Rake::TestTask.new("unittests") do |t|
      if args.file
        file = "spec/models/#{args.file}_spec.rb"
        t.pattern = file
        puts "Testing #{file}"
        if args.filter
          puts "Filters tests matching '#{args.filter}'"
          filter = args.filter
          if not filter.start_with? "/"
            filter = "/::#{filter}/"
          end
          t.options = "--name=#{filter}"
        end
      else
        t.pattern = "spec/models/*_spec.rb"
      end
    end
    task("unittests").execute
  end

  desc "Run single controller"
  task :controller, :file do |task, args|
    test_task = Rake::TestTask.new("unittests") do |t|
      if args.file
        file = "spec/controllers/#{args.file}_spec.rb"
        t.pattern = file
        puts "Testing #{file}"
      else
        t.pattern = "spec/controllers/*_spec.rb"
      end
    end
    task("unittests").execute
  end

  desc "Run acceptance tests"
  task :acceptance, :file, :filter do |task, args|
    test_task = Rake::TestTask.new("acceptance_tests") do |t|
      if args.file
        file = "spec/acceptance/#{args.file}_spec.rb"
        t.pattern = file
        puts "Testing #{file}"
        if args.filter
          puts "Filters tests matching '#{args.filter}'"
          filter = args.filter
          if not filter.start_with? "/"
            filter = "/::#{filter}/"
          end
          t.options = "--name=#{filter}"
        end
      else
        t.pattern = "spec/acceptance/*_spec.rb"
      end
    end
    task("acceptance_tests").execute
  end

  desc "Run single file"
  task :file, :file do |task, args|
    test_task = Rake::TestTask.new("unittests") do |t|
      if args.file
        file = args.file
        unless file.start_with? "spec/"
          file = "spec/#{args.file}"
        end
        t.pattern = file
        puts "Testing #{file}"
      else
        t.pattern = "spec/**/*_spec.rb"
      end
    end
    task("unittests").execute
  end
end

require 'jasmine'
load 'jasmine/tasks/jasmine.rake'
