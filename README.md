# Occam Web

Occam is a digital preservation tools that focuses on interactive, computation artifacts.
This supplies a web-server and direct support for running HTML and javascript interactive content.
For the code that serves as the daemon and object store, see [occam core](https://gitlab.com/occam-archive/occam).

## Installing

Start an Occam daemon. (See the instructions at the [occam daemon](https://gitlab.com/occam-archive/occam).)

Install ruby 2.4.1 through your system package manager or through a tool such as `rvm`.
Install bundler with `gem install bundler` if you don't already have it.
Install git.

Clone this repository:

```
git clone https://gitlab.com/occam-archive/occam-web-client
```

Change into the created directory:

```
cd occam-web-client
```

Install libraries:

```
bundle install
```

Run the server:

```
puma
```

Go to [http://localhost:9292](https://localhost:9292)

## Testing

If you are working with the code directly, you may want to periodically run the test suite.
To run the tests, you need to ensure the testing libraries are installed:

```
bundle install --with test
```

And then you invoke the test suite by running:

```
rake test
```

For more information, see [the spec directory](spec/README.md).

## Project Layout

```
Gemfile         - List of package dependencies
Gemfile.lock    - The frozen version information for those dependencies
LICENSE.txt     - Copyright information (AGPL 3.0)
Rakefile        - Scripts that manage certain aspects of the site; runs tests
CODE_OF_CONDUCT - Code of conduct for community interaction.
/lib            - Contains the main application and support code.
/helpers        - General code that is used throughout the site
/spec           - Testing code
/models         - Classes that wrap data structures used in Occam.
  | account     - Describes an authorized person (one who can log in)
  | authorship  - Describes an attached person for an object
  | job         - Describes a particular deployed task
  | layout      - Describes the parameters of the rendered page
  | link        - Describes a relationship between two objects
  | object      - Describes an object!
  | person      - Describes an actor in the system
  | review_c... - Describes a review token to allow anonymous viewing
  | system      - Describes the system at large
  | task        - Describes a task that runs or views an object
  | workflow    - Describes a set of interconnected objects
/controllers    - Web routes
  | accounts    - Routes related to authorizable people
  | objects     - Routes related to object retrieval
  | people      - Routes related to all people
  | search      - Routes related to general search
  | static      - Routes related to static pages (404, 500, etc)
  | system      - Routes related to system stats and health
  | task        - Routes related to task generation
/views          - HTML templates for rendering the content
  | stylesheets - Sass files for the css stylesheets
/config         - Library configuration and locales
  | locales     - Internationalization
/public         - Static content, such as javascript and images, served directly
  | js          - Front-end Scripts
  | images      - Static images
  | css         - Static css
  | fonts       - Downloaded fonts
/key            - [Generated] contains the self-signed keys
```

## Contributing

The following are accepted and cherished forms of contribution:

* Filing a bug issue. (We like to see feature requests and just normal bugs)
* Fixing a bug. (It's obviously helpful!)
* Adding documentation. (Help us with our docs or send us a link to your blog post!)
* Adding features.
* Adding artwork. (Art is the true visual form of professionalism)

The following are a bit harder to really accept, in spite of the obvious effort that may go into them, so please avoid this:

* Changing all of the javascript to coffeescript because it is "better"
* Rewriting all of the sass to whatever is newer. (It's happened to me before)
* Porting everything to rails.
* Creating a pull request with a "better" software license.

In general, contributions are easily provided by doing one of the following:

* Fork and clone the project.
* Update the code on your end however you see fit.
* Push that code to a public server.
* Create a pull request from your copy to ours.

The above is the most convenient process. You may create an issue with a link to a repository or tar/zip containing your code or patches as well, if git is not your thing.

## Acknowledgements

All attribution and crediting for contributors is located within [this file](views/static/credits.md). Or by visiting `/acknowledgements` in any Occam website.

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

## License

Occam is licensed under the AGPL 3.0. Refer to the [LICENSE.txt](LICENSE.txt) file in the root of the repository for specific details.
